//
//  ProfileChangePINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/28/20.
//

import UIKit
import SwiftyJSON
import app

class ProfileChangePINVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()

    @IBOutlet weak var tfCurrentPIN: DesignableTextField!
    @IBOutlet weak var btnShowHideCurrent: UIButton!
    @IBOutlet weak var tfNewPIN: DesignableTextField!
    @IBOutlet weak var btnShowHideNew: UIButton!
    @IBOutlet weak var tfRetypeNewPIN: DesignableTextField!
    @IBOutlet weak var btnShowHideReType: UIButton!
    @IBOutlet weak var lblForgotPIN: UILabel!
    @IBOutlet weak var btnSave: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.keyboardCheck()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.keyboardCheck()
        //self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func didTapButtonShowHideCurrent(_ sender: UIButton) {
        if sender.isSelected {
            tfCurrentPIN.isSecureTextEntry = false
        } else {
            tfCurrentPIN.isSecureTextEntry = true
        }
        sender.isSelected = !sender.isSelected
        tfCurrentPIN.isSecureTextEntry = !tfCurrentPIN.isSecureTextEntry
    }
    
    @IBAction func didTapButtonShowHideNew(_ sender: UIButton) {
        if sender.isSelected {
            tfNewPIN.isSecureTextEntry = false
        } else {
            tfNewPIN.isSecureTextEntry = true
        }
        sender.isSelected = !sender.isSelected
        tfNewPIN.isSecureTextEntry = !tfNewPIN.isSecureTextEntry
    }
    
    @IBAction func didTapButtonShowHideRetype(_ sender: UIButton) {
        if sender.isSelected {
            tfRetypeNewPIN.isSecureTextEntry = false
        } else {
            tfRetypeNewPIN.isSecureTextEntry = true
        }
        sender.isSelected = !sender.isSelected
        tfRetypeNewPIN.isSecureTextEntry = !tfRetypeNewPIN.isSecureTextEntry
        
    }
    
    @IBAction func didTapButtonSave(_ sender: Any) {
        let current_pin = tfCurrentPIN.text
        let new_pin = tfNewPIN.text
        let confirm_pin = tfRetypeNewPIN.text

        if(current_pin!.count < 6) {
            //change_pin_edittext_current_pin.error = resources.getString(R.string.error_invalid_pin)
            self.handleError(displaytitle: "Invalid Current PIN", "Please input a valid PIN (must be 6 digits).")
            return
        }

        if(new_pin!.count < 6) {
//            showDialog_main("", "", true, "Invalid New PIN", "Please input a valid PIN (must be 6 digits).", "", "OK")
            self.handleError(displaytitle: "Invalid Current PIN", "Please input a valid PIN (must be 6 digits).")
            return
        }

        if(confirm_pin!.count < 6) {
//            showDialog_main("", "", true, "Invalid Re-Type New PIN", "Please input a valid PIN (must be 6 digits).", "", "OK")
            self.handleError(displaytitle: "Invalid Re-Type New PIN", "Please input a valid PIN (must be 6 digits).")
            return
        }

        if (current_pin == new_pin)
        {
//            showDialog_main("", "", true, "Current PIN and New PIN must not match", "Please re-type your new PIN correctly.", "", "OK")
            self.handleError(displaytitle: "Current PIN and New PIN must not match", "Please re-type your new PIN correctly.")
            return
        }

        if (new_pin != confirm_pin)
        {
            self.handleError("ED100")
            return
        }

        initPINUpdate(
            customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
            old_pin: current_pin!,
            new_pin: new_pin!)
    }
        
    private func initPINUpdate(
        customer_id: String,
        old_pin: String,
        new_pin: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlPINUpdate,
            params: "customer_id:\(customer_id)||old_pin:\(old_pin)||new_pin:\(new_pin)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    self.hideLoading()
                    let alert = UIAlertController(title: "dialog_reset_mpin_success_title".localized(), message: "dialog_reset_mpin_success_message".localized(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                        self.dismiss(animated: true)
                    }))
                    self.present(alert, animated: true)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                      
                }

                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
        
    }
}
