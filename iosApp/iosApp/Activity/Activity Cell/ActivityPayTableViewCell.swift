//
//  ActivityPayTableViewCell.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/25/20.
//

import UIKit

class ActivityPayTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTransactionNumber: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var btnView: DesignableButton!
    
    var buttonCallback : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
     self.btnView.addTarget(self, action: #selector(didTapButtonView(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didTapButtonView(_ sender: UIButton) {
        buttonCallback?()
    }

}
