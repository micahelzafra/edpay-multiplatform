package com.ed2e.ed2eapp.view.activity.otp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginActivity;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.PinCreateAcivity;
import com.ed2e.ed2eapp.view.SplashActivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.login.LoginMPinActivity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegisterCheckOTP;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegisterReviseSendOTP;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegisterSendOTP;

public class OTPActivity extends BaseActivity {

    public static final String TAG = OTPActivity.class.getSimpleName();

    @BindView(R.id.otp_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.otp_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.otp_layout_submit_otp)
    LinearLayout layout_submit_otp;
    @BindView(R.id.otp_layout_resend_otp)
    LinearLayout layout_resend_otp;

    @BindView(R.id.otp_pinview)
    PinView pinview;
    @BindView(R.id.otp_textView_timer)
    TextView textView_timer;
    @BindView(R.id.otp_button_submit)
    Button button_submit;
    @BindView(R.id.otp_button_request_call)
    Button button_request_call;
    @BindView(R.id.otp_button_resend_sms)
    Button button_resend_sms;

    private String mobileNumber = "";
    private String customerID = "";
    private String prevPage = "";
    public static final int OTP_TIME = 60 * 1000;
    public static long timerCountSMS = 60;
    public static long timerCountCall = 60;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mobileNumber = "";
            customerID = "";
            prevPage = "";
        } else {
            mobileNumber = extras.getString(key_mobile_number);
            customerID = extras.getString(key_cust_id);
            prevPage = extras.getString(key_prev_page);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        //updateButton(false);

        //mobileNumber = "";

        layout_submit_otp.setVisibility(View.GONE);
        layout_resend_otp.setVisibility(View.VISIBLE);
        button_request_call.setEnabled(true);
        button_resend_sms.setEnabled(false);

        textView_timer.setOnClickListener(v -> {
//            if(timerCountSMS <= 0) {
//                timerCountSMS = 60;
//                updateButtonSMS(false);
//            } else {
//                return;
//            }
        });


        button_request_call.setOnClickListener(v -> {
            //initAPISendOTP(mobileNumber, "1");
            timerCountCall = 60;
            updateButtonCall(false);
        });

        button_resend_sms.setOnClickListener(v -> {
            //initAPISendOTP(mobileNumber, "0");
            timerCountSMS = 60;
            updateButtonSMS(false);
        });


//        showDialog_main("", "", true, getResources().getString(R.string.dialog_invalid_amount_title), getResources().getString(R.string.dialog_invalid_amount_message), "", "OK");

        button_submit.setOnClickListener(v -> {
            if(pinview.getText().toString().trim().length() < 6) {
                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
            } else {
                initAPICheckOTP(mobileNumber, pinview.getText().toString().trim());
            }

            //showDialog_main("", "button_ok", true,"Registration Successful", "You may use your registered mobile number to login and enjoy EDPAY App features.", "", "OK");
        });

        if(mobileNumber.equalsIgnoreCase("") || mobileNumber == null) {
            showDialog_APIError("", "tag_button_ok", true, "ED72", "", "OK");
        } else {
            initAPISendOTP(mobileNumber, "0");
            //startTimer();
        }

        pinview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinview.getText().toString().trim().length() >= 6) {
                    if(pinview.getText().toString().trim().length() < 6) {
                        showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
                    } else {
                        initAPICheckOTP(mobileNumber, pinview.getText().toString().trim());
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initAPICheckOTP(String mobileNumber, String otpCode) {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlRegisterCheckOTP,
                "mobile_number:"+ mobileNumber+ "||" +
                        "otp_code:" + otpCode,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + mobileNumber + " | " + otpCode + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        showDialog_main("", "tag_register_success_button_ok", false, getResources().getString(R.string.dialog_registration_success_title), getResources().getString(R.string.dialog_registration_success_message), "", "OK");
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();

                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "  " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });


    }

    private void initAPISendOTP(String mobileNumber, String otpType)
    {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlRegisterReviseSendOTP,
                "mobile_number:" + mobileNumber + "||" +
                        "otp_type:" + otpType,
                response -> {
                    hideProgress();
                    Log.d(TAG, "URL: " + urlRegisterSendOTP );
                    Log.d(TAG, "DATA: | " + mobileNumber + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        //String data = jsonObj.get(key_data).getAsString();
                        if(otpType.equalsIgnoreCase("0")) {
                            startTimerSMS();
                        } else {
                            startTimerCall();
                        }
                    }
                    else
                    {
                        if(otpType.equalsIgnoreCase("0")) {
                            timerCountSMS = 0;
                            updateButtonSMS(true);
                        } else {
                            timerCountCall = 0;
                            updateButtonCall(true);
                        }
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");


//                        showDialog_APIError("", "tag_edpay_shake_reward_error", false, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    if(otpType.equalsIgnoreCase("0")) {
                        timerCountSMS = 0;
                        updateButtonSMS(true);
                    } else {
                        timerCountCall = 0;
                        updateButtonCall(true);
                    }
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }


//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }




    private void updateButtonSMS(boolean isShowResend) {
        runOnUiThread(() -> {
            if (isShowResend) {
                //textView_timer.setText(getResources().getString(R.string.button_resend));
                button_resend_sms.setText(getResources().getString(R.string.button_resend_via_sms));
                button_resend_sms.setEnabled(true);
            } else {
                //textView_timer.setText(getResources().getString(R.string.label_resend_in_seconds, String.valueOf(timerCountSMS) ));
                button_resend_sms.setEnabled(false);
                initAPISendOTP(mobileNumber, "0");
                //startTimer();
            }
        });
    }

    private void startTimerSMS() {
        runOnUiThread(() ->
        {
            new CountDownTimer(OTP_TIME, 1000) {

                public void onTick(long millisUntilFinished) {
                    timerCountSMS = millisUntilFinished / 1000;
                    //Log.d("TIMER", "Countdown in: " + millisUntilFinished / 1000);
                    //textView_timer.setText(getResources().getString(R.string.label_resend_in_seconds, "" + timerCountSMS));
                    String final_timerCountSMS = "";
                    if(String.valueOf(timerCountSMS).length() == 1) {
                        final_timerCountSMS = "0" + timerCountSMS;
                    } else {
                        final_timerCountSMS = "" + timerCountSMS;
                    }
                    button_resend_sms.setText(getResources().getString(R.string.button_resend_via_sms) + " (00:" + final_timerCountSMS + ")");
                }

                public void onFinish() {
                    updateButtonSMS(true);
                }

            }.start();
        });
    }

    private void updateButtonCall(boolean isShowResend) {
        runOnUiThread(() -> {
            if (isShowResend) {
                //textView_timer.setText(getResources().getString(R.string.button_resend));
                button_request_call.setText(getResources().getString(R.string.button_request_via_call));
                button_request_call.setEnabled(true);
            } else {
                //textView_timer.setText(getResources().getString(R.string.label_resend_in_seconds, String.valueOf(timerCountSMS) ));
                button_request_call.setEnabled(false);
                initAPISendOTP(mobileNumber, "1");
                //startTimer();
            }
        });
    }

    private void startTimerCall() {
        runOnUiThread(() ->
        {
            new CountDownTimer(OTP_TIME, 1000) {

                public void onTick(long millisUntilFinished) {
                    timerCountCall = millisUntilFinished / 1000;
                    //Log.d("TIMER", "Countdown in: " + millisUntilFinished / 1000);
                    //textView_timer.setText(getResources().getString(R.string.label_resend_in_seconds, "" + timerCountCall));
                    String final_timerCountCall = "";
                    if(String.valueOf(timerCountCall).length() == 1) {
                        final_timerCountCall = "0" + timerCountCall;
                    } else {
                        final_timerCountCall = "" + timerCountCall;
                    }
                    button_request_call.setText(getResources().getString(R.string.button_request_via_call) + " (00:" + final_timerCountCall + ")");
                }

                public void onFinish() {
                    updateButtonCall(true);
                }

            }.start();
        });
    }


    private void initPreviousActivity() {
        showDialog_main("", "tag_otp_incomplete_button_ok", true,getResources().getString(R.string.dialog_register_incomplete_title), getResources().getString(R.string.dialog_register_incomplete_message), "CANCEL", "OK");
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "button_ok":
//                showToast_short("To Homescreen");
                intent = new Intent(getContext(), CreateMPinActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case "tag_register_success_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(OTPActivity.this, CreateMPinActivity.class);
                    intent.putExtra(key_cust_id, customerID);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(OTPActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    intent = new Intent(OTPActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }

                break;
            case "tag_otp_incomplete_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(getContext(), LoginOrRegAcivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }

                break;


            default:
                break;
        }
    }


    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
