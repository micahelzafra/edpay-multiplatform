package com.ed2e.ed2eapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.*;

public class PinCreateAcivity extends BaseActivity
{
    @BindView(R.id.pin_create_textview_title)
    TextView textView_title;
    @BindView(R.id.pin_create_textview_subtitle)
    TextView textView_subtitle;
    @BindView(R.id.pin_create_pinview)
    PinView pinView;
    @BindView(R.id.pin_create_button_submit)
    Button button;

    ApiInterface apiInterface;
    AppPreference preference;

    Boolean isInitial = true;
    String pin_value ="";
    String pin_value_confirm ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_create);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);
        initGUI();
    }

    private void initGUI() {
        pinView.setHideLineWhenFilled(false);
        labels();
    }

    @OnClick(R.id.pin_create_button_submit) void submit() {
        if (isInitial)
        {
            isInitial = false;
            labels();
            pin_value = Objects.requireNonNull(pinView.getText()).toString();
            pinView.setText("");
        }
        else
        {
            pin_value_confirm = Objects.requireNonNull(pinView.getText()).toString();
            checkPIN();
        }
    }

    private void labels()
    {
        if (isInitial)
        {
            textView_title.setText(label_pin_create_page_1);
            textView_subtitle.setText(label_pin_create_page_1_subtitle);
            button.setText(button_next);
        }
        else
        {
            textView_title.setText(label_pin_create_page_2);
            textView_subtitle.setText("");
            button.setText(button_submit);
        }
    }

    private void checkPIN()
    {
        if (pin_value.equals("") || pin_value_confirm.equals(""))
        {
//            this.errorMsg('ED94')
            showDialogError(request_error_dialog,"ED94");
            return;
        }

        if (pin_value.length() <= 5 || pin_value_confirm.length() <= 5 )
        {
//            this.errorMsg('ED94')
            showDialogError(request_error_dialog,"ED94");
            return;
        }

        if(!pin_value.equals(pin_value_confirm)){
//            this.errorMsg('ED100')
            showDialogError(request_error_dialog,"ED100");
            return;
        }
        initCreatePin(preference.getString(key_user_id),pin_value_confirm);
    }

    private void initCreatePin(String customer_id, String pin)
    {
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlPINCreate,
                "customer_id:"+customer_id+"||" +
                        "pin:"+pin,
                response -> {
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        Intent intent=new Intent(PinCreateAcivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialogError(request_error_dialog,error_code);
                    }

                    return null;
                }
                , failure -> {
                    System.out.println("Throwable:"+ failure);
                    showDialogError(request_error_dialog,failure.getLocalizedMessage());
                    return null;
                });
    }

    @Override
    public void onBackPressed(){

        if (isInitial)
        {
            finish();
        }
        else
        {
            pinView.setText(pin_value);
            isInitial = true;
        }
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {

    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }
}
