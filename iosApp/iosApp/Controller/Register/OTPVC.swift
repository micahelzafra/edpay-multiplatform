//
//  OTPVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/15/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app

class OTPVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnResendSMS: DesignableButton!
    @IBOutlet weak var btnResendCall: DesignableButton!
    @IBOutlet weak var verifyPinView: CBPinEntryView! {
    didSet {
                verifyPinView.delegate = self as CBPinEntryViewDelegate
            }
    }
    
    var timer_count_sms = 60
    var timerSMS = Timer()
    var timer_count_call = 60
    var timerCall = Timer()
    
    private var mobileNumber = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.log("passed data: \(segueData)")
        
        if let mobileNumber = segueData[self.constant.key_mobile_number] as? String
        {
            self.mobileNumber = mobileNumber
            self.initAPISendOTP(mobileNumber: self.mobileNumber, otpType: "0")
        }
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
        self.initAPISendOTP(mobileNumber: self.mobileNumber, otpType: "0")
    }
    
    @IBAction func didTapResendOTPCall(_ sender: Any) {
        self.initAPISendOTP(mobileNumber: self.mobileNumber, otpType: "1")
    }
    
    func runTimer(type:String) {
        if type == "0"
        {
            timerSMS = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimerSMS)), userInfo: nil, repeats: true)
            btnResendSMS.isEnabled = false
            btnResendSMS.setTitle("Resend OTP via SMS", for: .normal)
        }
        else
        {
            timerCall = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimerCall)), userInfo: nil, repeats: true)
            btnResendCall.isEnabled = false
            btnResendCall.setTitle("Resend OTP via Call", for: .normal)
        }
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
    }
    
    @objc func updateTimerSMS() {
        timer_count_sms -= 1     //This will decrement(count down)the seconds.
        if timer_count_sms == 0
        {
            timerSMS.invalidate()
            timer_count_sms = 60
            btnResendSMS.isEnabled = true
            btnResendSMS.titleLabel?.text = "Resend OTP via SMS"
            btnResendSMS.setTitle("Resend OTP via SMS", for: .normal)
        }
        else
        {
            btnResendSMS.setTitle("Resend OTP via SMS (00:\(String(format: "%02d", timer_count_sms)))", for: .disabled)
            btnResendSMS.layoutIfNeeded()
        }
    }
    
    @objc func updateTimerCall() {
        timer_count_call -= 1     //This will decrement(count down)the seconds.
        if timer_count_call == 0
        {
            timerCall.invalidate()
            timer_count_call = 60
            btnResendCall.isEnabled = true
            btnResendCall.titleLabel?.text = "Resend OTP via Call"
            btnResendCall.setTitle("Resend OTP via Call", for: .normal)
        }
        else
        {
            btnResendCall.setTitle("Resend OTP via Call (00:\(String(format: "%02d", timer_count_call)))", for: .disabled)
            btnResendCall.layoutIfNeeded()
        }
    }
                
    private func initAPISendOTP(
        mobileNumber: String,
        otpType:String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlRegisterReviseSendOTP,
            params: "mobile_number:\(mobileNumber)||otp_type:\(otpType)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    self.runTimer(type: otpType)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.hideLoading()
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
                
    private func initAPICheckOTP(
        mobileNumber: String,
        otpCode:String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlRegisterCheckOTP,
        params: "mobile_number:\(mobileNumber)||otp_code:\(otpCode)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    let okAction = UIAlertAction(title: self.constant.button_okay, style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let prev_page = self.segueData[self.constant.key_prev_page] as? String
                        if prev_page == "registration"
                        {
                            let newStoryBoard : UIStoryboard = UIStoryboard(name: "Registration", bundle:nil)
                            let VC = newStoryBoard.instantiateViewController(withIdentifier: "CreatePINVC") as! CreateMobilePIN
                            var segueData = Dictionary<String, AnyObject>()
                            segueData[self.constant.key_cust_id] = (self.segueData[self.constant.key_cust_id] as! Int) as AnyObject
                            VC.segueData = segueData
                            VC.modalPresentationStyle = .fullScreen
                            self.present(VC, animated: true, completion: nil)
                        }
                        else if prev_page == "login"
                        {
                            self.dismiss(animated: false)
                        }
                        else
                        {
                            self.dismiss(animated: false)
                        }
                    }
                    let actions = [okAction]
                    self.showDialog(title: "dialog_registration_success_title".localized(), message: "dialog_registration_success_message".localized(), actions: actions)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.hideLoading()
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}

extension OTPVC: CBPinEntryViewDelegate {
    func entryCompleted(with entry: String?) {
        self.log("pinView \(verifyPinView.getPinAsString())")
        self.initAPICheckOTP(mobileNumber: self.mobileNumber, otpCode: verifyPinView.getPinAsString())
    }

    func entryChanged(_ completed: Bool) {
        if completed {
        }
    }
}
