package com.ed2e.ed2eapp.view.activity.help;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_device_token;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;

public class HelpActivity extends BaseActivity {

    public static final String TAG = HelpActivity.class.getSimpleName();

    @BindView(R.id.help_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.help_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.help_LinearLayout_contact_num_container)
    LinearLayout LinearLayout_contact_num_container;
    @BindView(R.id.help_LinearLayout_email_container)
    LinearLayout LinearLayout_email_container;
    @BindView(R.id.help_LinearLayout_website_container)
    LinearLayout LinearLayout_website_container;
    @BindView(R.id.help_LinearLayout_location_container)
    LinearLayout LinearLayout_location_container;

    @BindView(R.id.help_textView_contact_num)
    TextView textView_contact_num;
    @BindView(R.id.help_textView_email_address)
    TextView textView_email_address;
    @BindView(R.id.help_textView_website)
    TextView textView_website;
    @BindView(R.id.help_textView_location)
    TextView textView_location;

    public String[] help_label_values;
    String prevPage = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initCurrentLocation();
        initToolBar();
        //initData();
        //initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            prevPage = "";
        } else {
            prevPage = extras.getString(key_prev_page);
        }

        switch(getCurrentLocationData(tag_current_location_country_code, HelpActivity.this)){
            case "PH":
                help_label_values = getResources().getStringArray(R.array.help_items_ph);
                break;
            case "NG":
                help_label_values = getResources().getStringArray(R.array.help_items_ng);
                break;
            case "MY":
                help_label_values = getResources().getStringArray(R.array.help_items_my);
                break;
            default:
                help_label_values = getResources().getStringArray(R.array.help_items_ng);
                break;
        }

        initGUI();
    }


//    private void getCountryCurrency() {
//
//        String currencyCode = "";
//
//        switch(getCurrentLocationData(tag_current_location_country_code)){
//            case "PH":
//                currencyCode = "PHP";
//                break;
//            case "NG":
//                currencyCode = "NGN";
//                break;
//            case "MY":
//                currencyCode = "RM";
//                break;
//            default:
//                currencyCode = "";
//                break;
//        }
//
//        edpay_input_amount_textview_currency.setText(currencyCode);
//    }

    Intent intent;

    private void initGUI() {

        //showToast_short(getCurrentLocationData(tag_current_location_country_code));

        textView_contact_num.setText(help_label_values[0]);
        textView_email_address.setText(help_label_values[1]);
        textView_website.setText(help_label_values[2]);
        textView_location.setText(help_label_values[3]);

        LinearLayout_contact_num_container.setOnClickListener(v -> {
            intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + textView_contact_num.getText().toString().trim()));
            startActivity(intent);
        });

        LinearLayout_email_container.setOnClickListener(v -> {
//            Intent emailSelectorIntent = new Intent(Intent.ACTION_SENDTO);
//            emailSelectorIntent.setData(Uri.parse("mailto:"));

            intent = new Intent(Intent.ACTION_SEND);
            intent.setData(Uri.parse("email"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{textView_email_address.getText().toString().trim()});
//                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
//                    intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");
            intent.setType("message/rfc822");
//            intent.setSelector( emailSelectorIntent );
            startActivity(Intent.createChooser(intent, "Send Email"));
        });


        LinearLayout_website_container.setOnClickListener(v -> {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + textView_website.getText().toString().trim()));
            startActivity(intent);
        });

        LinearLayout_location_container.setOnClickListener(v -> {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + textView_location.getText().toString().trim()));
            startActivity(intent);
        });


//        button_log_in.setOnClickListener(v -> {
//
//            if(pinview.getText().toString().trim().length() < 6) {
//                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
//            } else {
//                //showToast_short(pinview.getText().toString().trim());
//                initAPILogin(mobileNumber, pinview.getText().toString().trim(), preference.getString(key_device_token), "nigeria");
//                //showDialog_main("", "button_ok", true,"Incorrect Mobile PIN", "Please make sure to enter the correct PIN.", "", "OK");
//            }
//
//        });
    }

    private void initAPILogin(String mobileNumber, String mobilePIN, String device_token, String country)
    {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlLogin2,
                "mobile_number:"+ mobileNumber + "||" +
                        "pin:" + mobilePIN + "||" +
                        "device_id:" + device_token + "||" +
                        "country_name:" + country,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + mobileNumber + " | " + mobilePIN + " | " + device_token + " | " + country + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        JsonObject data = jsonObj.get(key_data).getAsJsonObject();
                        String user_id = data.get(key_id).getAsString();
                        String mobile_number = data.get(key_mobile_number).getAsString();
                        String pin = data.get(key_pin).getAsString();
                        String otp_verified = data.get(key_otp_verified).getAsString();
                        preference.putString(key_user_id, user_id);

                        if (otp_verified.equals("0"))
                        {
                            Intent intent=new Intent(HelpActivity.this, OTPActivity.class);
                            intent.putExtra(key_mobile_number, mobile_number);
                            intent.putExtra(key_cust_id, user_id);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        if (pin.equals("0"))
                        {
                            Intent intent=new Intent(HelpActivity.this, CreateMPinActivity.class);
                            intent.putExtra(key_cust_id, user_id);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        Intent intent=new Intent(HelpActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();

                        if(error_code.equalsIgnoreCase("ED73")) {
                            showDialog_APIError("", "button_register", true, error_code, "CANCEL", "Register");
                        } else {
                            showDialog_APIError("", "", true, error_code, "", "OK");
                        }
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }



//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }




    private void initCurrentLocation() {
        getCurrentLocation(getContext());
        //Log.d("LOCATION", "Response Location: " + preference.getString(key_current_location, ""));
        initData();
    }

    private void initPreviousActivity() {

        switch (prevPage) {
            case "login":
                Intent intent = new Intent(getContext(), Login1Activity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;

                default:
                    finish();
                    break;
        }

    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "button_register":
                Intent intent = new Intent(HelpActivity.this, RegistrationAcivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
