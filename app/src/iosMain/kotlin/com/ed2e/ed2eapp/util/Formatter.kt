package com.ed2e.ed2eapp.util

import platform.UIKit.UIDevice
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSDate
import platform.Foundation.NSNumberFormatter
import platform.Foundation.NSNumber
import platform.Foundation.NSLocale
import platform.Foundation.NSString
import platform.Foundation.NSNumberFormatterDecimalStyle
import platform.Foundation.NSTimeZone
import platform.Foundation.NSTimeZoneMeta
import platform.Foundation.defaultTimeZone
import platform.Foundation.timeIntervalSince1970
import platform.Foundation.timeZoneForSecondsFromGMT
import platform.Foundation.timeZoneWithName
import platform.Foundation.NSCalendar

actual class KMPDate actual constructor(formatString: String) { // 1

    private val dateFormatter = NSDateFormatter().apply { // 2
        this.dateFormat = formatString
    }

    actual fun asString(): String {
//        return formatter.stringFromDate(NSDate()) // 3
        return ""
    }
}

actual class commaSeparated actual constructor(value: String) {
    private val numberFormat = value
    private val formatter = NSNumberFormatter()
    actual fun asString(): String {
//        formatter.locale = Locale(identifier: "fr_FR") 
        formatter.numberStyle = NSNumberFormatterDecimalStyle
        formatter.setMinimumFractionDigits(2)
        formatter.setMaximumFractionDigits(2)
        formatter.usesGroupingSeparator = true
        val format = formatter.stringFromNumber(NSNumber(numberFormat.toDouble()))
        if (format!=null)
        {
            return format
        }
        return ""
    }
}

actual class paymentType actual constructor(value: String) {
    private val paymentType = value
    actual fun asString(): String {
        var type = "1"
        if (paymentType.equals(key_payment_type_edpay))
        {
            type = "0"
        }
        return type
    }
}

actual class dateFormat actual constructor(
    date: String,
    from: String,
    to: String
) {
    private val stringDate = date
    private val fromformat = from
    private val toformat = to
    private val formatter = NSDateFormatter()
    actual fun asString(): String {
        formatter.dateFormat = fromformat
        val nsdate = formatter.dateFromString(stringDate)
        if (nsdate!=null)
        {
            formatter.dateFormat = toformat
            val format = formatter.stringFromDate(nsdate)
            return format
        }
        else
        {
            return "nsdate"
        }
    }
}

actual class get30DaysBeforeDate actual constructor() {
    actual fun asString(): String {
        return ""
    }
}

actual class getLocalTimeStamp actual constructor(date: String) {
    private val stringDate = date
    private val formatter = NSDateFormatter()
    actual fun asString(): String {
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = NSTimeZone.timeZoneWithName("UTC")!!
        val nsdate = formatter.dateFromString(stringDate)
        if (nsdate!=null)
        {
            formatter.dateFormat = "MMM dd, yyyy hh:mm a"
            formatter.timeZone = NSTimeZone.defaultTimeZone
            val format = formatter.stringFromDate(nsdate)
            return format
        }
        else
        {
            return "invalid date"
        }
    }
}

actual class getCurrentDay actual constructor() {

    private val dateFormatter = NSDateFormatter().apply { // 2
        this.dateFormat = "EEEE"
    }

    actual fun asString(): String {
        return dateFormatter.stringFromDate(NSDate())
    }
}

actual class getOrderStatus actual constructor(status: String) {
    private val orderStatus = status
    actual fun asString(): String {
        var displayStatus = ""
        if (orderStatus == "6")
        {
            displayStatus = "Delivered"
        }
        else if (orderStatus == "10")
        {
            displayStatus = "Cancelled"
        }
        else
        {
            displayStatus = "Pending"
        }
        return displayStatus
    }
}
