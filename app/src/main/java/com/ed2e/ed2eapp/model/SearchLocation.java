package com.ed2e.ed2eapp.model;

public class SearchLocation {

    private String result;

    public SearchLocation(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
