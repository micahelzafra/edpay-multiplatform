package com.ed2e.ed2eapp.view.activity.main.home.edfood

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edfood_cart.*
import kotlinx.android.synthetic.main.activity_edfood_merchants.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import kotlin.coroutines.CoroutineContext


class EDFoodCartActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference
    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    private var arraylist = ArrayList<Any>()

    private var cart = JsonObject()

    private var bookedId = ""
    private var countryName :String? = ""
    private var bookedLocation :String? = ""
    private var restaurantId = ""
    private var isVoucherCodeValid = true

    private var mDiscount = "0"
    private var mDiscountName = ""
    private var mPromoId = ""

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_cart)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
        initListener()
    }

    @SuppressLint("SetTextI18n")
    private fun initGUI() {
        setHideKeyboardOnTouch(this,activity_food_cart_main_layout)
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        bookedLocation = bundle.getString(key_booked_location)
        val parser = JsonParser()
//        val jsonObj: JsonObject = parser.parse(data).asJsonObject
//        Timber.d("jsonObj: %s", jsonObj)
        Timber.d("booked_location: %s", bookedLocation)
        countryName = bundle.getString(key_country) // 1
        bundle.getString(key_booked_id)?.let {
            bookedId = it
        }

        activity_food_cart_textview_delivery_address.text = bookedLocation

        adapter = object : BaseRecyclerViewAdapter<Any>(arraylist, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {
            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
//                return when(obj){
//                    is String->R.layout.child_edfood_menu_category
//                    else->R.layout.child_edfood_menu_item
//                }
                return R.layout.child_edfood_cart_item
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }
        activity_food_cart_recyclerview_orders.layoutManager= LinearLayoutManager(context)
        activity_food_cart_recyclerview_orders.setHasFixedSize(true)
        activity_food_cart_recyclerview_orders.adapter=adapter

        activity_food_cart_button_place_order.setOnClickListener {

            if(!activity_food_cart_edittext_voucher.text.toString().equals("") && !isVoucherCodeValid) {
                showDialog_main(
                    "",
                    "",
                    true,
                    resources.getString(R.string.dialog_invalid_voucher_title),
                    resources.getString(R.string.dialog_invalid_voucher_message),
                    "",
                    "OK"
                )
            } else {

                if (!cart.toString().equals("{}")) {
                    val jsonObj = cart
                    val subTotal = jsonObj[key_sub_total].asString
//                    val containerFee = jsonObj[key_container_fee].asString
//                    val deliveryFee = jsonObj[key_delivery_fee].asString
                    val containerFee = actvity_edfood_cart_container_fee.text.toString().trim().replace( ",", "" )
                    val deliveryFee = actvity_edfood_cart_delivery_fee.text.toString().trim().replace( ",", "" )

                    val serviceFeePercentage = jsonObj[key_service_fee_percentage].asString
                    val serviceFee = jsonObj[key_service_fee].asString
                    val vat = jsonObj[key_vat].asString
                    val vatAmount = jsonObj[key_vat_amount].asString
                    val restoInfo = jsonObj[key_resto_info].asJsonObject
                    val restaurantAddress = restoInfo[key_restaurant_address].asString
                    val restaurantName = restoInfo[key_restaurant_name].asString
//                    val total = jsonObj[key_total].asString
                    val total = activity_food_cart_textview_total_amount.text.toString().trim().replace( ",", "" )


//                    val discount = jsonObj[key_discount].asString
//                    var discountName = ""
//                    if (!jsonObj[key_discount_name].toString().equals("null")) {
//                        discountName = jsonObj[key_discount_name].asString
//                    }
//                    var promoID = promoId

                    val discount = mDiscount
                    var discountName = mDiscountName
                    var promoID = mPromoId


                    val original_sub_total = jsonObj[key_original_sub_total].asString
                    val country_id = jsonObj[key_country_id].asString
                    initCheckOut(
                        preference.getString(key_user_id),
                        bookedId,
                        vat,
                        vatAmount,
                        discount,
                        discountName,
                        subTotal,
                        original_sub_total,
                        total,
                        country_id,
                        serviceFee,
                        serviceFeePercentage,
                        deliveryFee,
                        containerFee,
                        activity_food_cart_textview_total_amount.text.toString(),
                        promoID,
                        edfood_item_details_edittext_special_notes.text.toString()
                    )
                }
            }
        }

        activity_food_cart_edittext_voucher.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isVoucherCodeValid = false
            }
        })

        child_edfood_menu_item_button_apply.setOnClickListener {
            val jsonObj = cart
            val resto_info = jsonObj[key_resto_info].asJsonObject
            val restaurant_id = resto_info[key_restaurant_id].asString
            val total = jsonObj[key_total].asString
            val subTotal = jsonObj[key_sub_total].asString
            initValidateVoucher(
                preference.getString(key_user_id),
                restaurant_id,
                activity_food_cart_edittext_voucher.text.toString(),
                subTotal.replace(",", "")
                )
        }

        if(bookedId != "")
        {
            countryName?.let {
                initGetCartOrderList(preference.getString(key_user_id), bookedId, it)
            }
        }
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        val parser = JsonParser()
        when (tag) {
            tag_load_quantity_increase -> {
                val jsonObj = parser.parse(value.toString()).asJsonObject
                val order_id = jsonObj[key_order_id].asString
                val food_quantity = jsonObj[key_food_quantity].asInt
                val quantity = food_quantity + 1
                val food_price = jsonObj[key_food_price].asString
                val array = jsonObj[key_food_addons].asJsonArray
                updateCart(order_id, order_id , quantity.toString(), food_price, array.toString().replace("price","addon_price"))
            }
            tag_load_quantity_decrease -> {
                val jsonObj = parser.parse(value.toString()).asJsonObject
                val order_id = jsonObj[key_order_id].asString
                val food_quantity = jsonObj[key_food_quantity].asInt
                val quantity = food_quantity - 1
                val food_price = jsonObj[key_food_price].asString
                val array = jsonObj[key_food_addons].asJsonArray
                updateCart(order_id, order_id , quantity.toString(), food_price, array.toString().replace("price","addon_price"))
            }
            tag_load_order_remove -> {
                val jsonObj = parser.parse(value.toString()).asJsonObject
                val order_id = jsonObj[key_order_id].asString
                initDeleteItem(order_id, bookedId)
            }
            tag_load_order_edit -> {
                initGetMerchantMenu(restaurantId)
            }
            tag_intent_edfood_close_prev_act -> {
                finish()
            }
        }
    }

    //get the data to display
    @SuppressLint("SetTextI18n")
    private fun initGetCartOrderList(
        customer_id: String,
        booked_id: String,
        country_name: String
    ) {
        val params =
            "customer_id:" + customer_id + "||" +
                    "country_name:" + country_name + "||" +
                    "distance_km:1" + "||" +
                    "booked_id:" + booked_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartOrderList,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject

                var status = ""
                if(jsonObj[key_status] == null ) {
                    status = ""
                } else {
                    status = jsonObj[key_status].asString
                }

                if (status == status_fail) {

                    val code = jsonObj["code"].asString

                    if (code == "ED12") {
                        finish()
                    } else {
                        showDialogError(request_error_dialog, code)
                    }

                } else {
                    launch(Dispatchers.Main) {
                        cart = jsonObj
                        val sub_total = jsonObj[key_sub_total].asString
                        val container_fee = jsonObj[key_container_fee].asString
                        val delivery_fee = jsonObj[key_delivery_fee].asString
                        val service_fee_percentage = jsonObj[key_service_fee_percentage].asString
                        val service_fee = jsonObj[key_service_fee].asString
                        val vat = jsonObj[key_vat].asString
                        val vat_amount = jsonObj[key_vat_amount].asString
                        val resto_info = jsonObj[key_resto_info].asJsonObject
                        val restaurant_address = resto_info[key_restaurant_address].asString
                        val restaurant_name = resto_info[key_restaurant_name].asString
                        val total = jsonObj[key_total].asString


                        actvity_edfood_cart_subtotal.text = commaSeparated(sub_total.replace(",","")).asString()
                        actvity_edfood_cart_container_fee.text = commaSeparated(container_fee.replace(",","")).asString()
                        actvity_edfood_cart_delivery_fee.text = commaSeparated(delivery_fee.replace(",","")).asString()
                        actvity_edfood_cart_service_fee_percentage.text = getString(R.string.label_service_fee,service_fee_percentage) + "%"
                        actvity_edfood_cart_service_fee.text = commaSeparated(service_fee).asString()
                        actvity_edfood_cart_vat_percentage.text = getString(R.string.label_vat,vat)
                        actvity_edfood_cart_vat.text = commaSeparated(vat_amount.replace(",","")).asString()
                        activity_food_cart_textview_merchant_address.text = restaurant_name
                        activity_food_cart_textview_total_amount.text = total

                        arraylist.clear()
                        val jsonArray = jsonObj[key_order].asJsonArray
                        val addOnsArray = jsonObj[key_food_addons].asJsonArray
                        var quantities = 0
                        for (json in jsonArray)
                        {
                            var jsonObject: JsonObject
                            val jsonItem = json.asJsonObject
                            jsonObject = jsonItem
                            val order_id = jsonItem[key_order_id].asInt

                            val jsonArr = JsonArray()
                            for (data in addOnsArray)
                            {
                                val addonlist = data.asJsonArray
                                for (addon in addonlist)
                                {
                                    try {
                                        val jsonAddonItem = addon.asJsonObject
                                        val add_id = jsonAddonItem[key_order_list_id].asInt
                                        Timber.d("orderID $order_id add_id $add_id")
                                        if (order_id == add_id)
                                        {
                                            jsonArr.add(addon)
                                        }
                                    }
                                    catch (j:Exception)
                                    {
                                        Timber.d("orderID Exception $j")
                                    }
                                }
                            }
                            jsonObject.add("food_addons", jsonArr)
                            val foodQuantity = jsonItem[key_food_quantity].asInt
                            quantities += foodQuantity
                            arraylist.add(jsonObject)
                        }
                        //                    edfood_merchant_item_textview_cart_items.text = quantities.toString()
                        updateList()
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    //For place order
    private fun initCheckOut(
        customer_id: String,
        booked_id: String,
        vat: String,
        vat_amount: String,
        discount: String,
        discount_name: String,
        food_total_with_discount: String,
        sub_total: String,
        total: String,
        country_id: String,
        service_fee: String,
        service_fee_percentage: String,
        delivery_fee: String,
        container_fee: String,
        food_total: String,
        promo_id: String,
        special_notes: String
    ) {
        val params =
            "customer_id:" + customer_id + "||" +
                    "booked_id:" + booked_id + "||" +
                    "vat:" + vat + "||" +
                    "vat_amount:" + vat_amount + "||" +
                    "discount:" + discount + "||" +
                    "discount_name:" + discount_name + "||" +
                    "food_total_with_discount:" + food_total_with_discount + "||" +
                    "sub_total:" + sub_total + "||" +
                    "total:" + total + "||" +
                    "country_id:" + country_id + "||" +
                    "service_fee:" + service_fee + "||" +
                    "service_fee_percentage:" + service_fee_percentage + "||" +
                    "delivery_fee:" + delivery_fee + "||" +
                    "container_fee:" + container_fee + "||" +
                    "food_total:" + food_total + "||" +
                    "promo_id:" + promo_id + "||" +
                    "special_notes:" + special_notes
        Timber.d("params: %s", params)
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartCheckout,
            params,
            { response: String? ->

                Log.d("Logs", "Response: " + response)

                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                launch(Dispatchers.Main) {
                    val intent : Intent
                    intent = Intent(context, EDFoodPaymentMethodActivity::class.java)
                    intent.putExtra(key_country, countryName)
                    intent.putExtra(key_booked_id, booked_id)
                    intent.putExtra(key_total, total)
                    intent.putExtra(key_special_notes, edfood_item_details_edittext_special_notes.text.toString())
                    startActivity(intent)
                    hideProgress()
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            hideProgress()
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    //For editing the orders
    private fun initGetMerchantMenu(
        restaurant_id: String
    ) {
        val params =
            "restaurant_id:" + restaurant_id
        Timber.d("params: %s", params)
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlRestaurantMenu,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                launch(Dispatchers.Main) {

//                    val intent : Intent
//                    intent = Intent(context, EDFoodItemDetailsActivity::class.java)
//                    intent.putExtra(key_booked_location, booked_location)
//                    intent.putExtra(key_restaurant_id, restaurant_id)
//                    intent.putExtra(key_data, value.toString())
//                    intent.putExtra(key_booked_id, bookedId)
//                    startActivity(intent)

                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    //For checking the voucher code
    private fun initValidateVoucher(
        customer_id: String,
        restaurant_id: String,
        promo_code: String,
        total_purchased: String
    ) {

        showProgress()
        val params =
            "customer_id:" + customer_id + "||" +
            "restaurant_id:" + restaurant_id + "||" +
            "promo_code:" + promo_code + "||" +
            "total_purchased:" + total_purchased
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlValidateVoucherCode,
            params,
            { response: String? ->
                hideProgress()
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject

                Log.d("Logs", "Response: " + response)

                launch(Dispatchers.Main) {
                    activity_food_cart_textview_voucher_status.visibility = VISIBLE
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        isVoucherCodeValid = true
                        val jsonObject = jsonObj[key_data].asJsonObject
                        val promoID = jsonObject[key_promo_id].asString
                        val promoName = jsonObject[key_promo_name].asString
                        val discountValue = jsonObject[key_discount_value].asString

                        try {
                            activity_food_cart_textview_voucher_status.setTextColor(getResources().getColor(R.color.greenColor))
                            //activity_food_cart_textview_voucher_status.setTextColor(getColor(R.color.greenColor))
                        } catch(e: Exception) {
                            //activity_food_cart_textview_voucher_status.setTextColor(getResources().getColor(R.color.greenColor))
                        }
                        activity_food_cart_textview_voucher_status.text = "($promoName) less discount - ${commaSeparated(discountValue).asString()}"

                        val freeContainer = jsonObject[key_free_container].asInt
                        val freeDelivery = jsonObject[key_free_delivery].asInt
                        var totalVoucher = jsonObject[key_total_purchased_with_discount].asDouble
                        if (freeContainer == 1)
                        {
                            actvity_edfood_cart_container_fee.text = "0.00"
                        }
                        if (freeDelivery == 1)
                        {
                            actvity_edfood_cart_delivery_fee.text = "0.00"
                        }

                        val additionalFee = actvity_edfood_cart_container_fee.text.toString().replace(",","").toDouble() + actvity_edfood_cart_delivery_fee.text.toString().replace(",","").toDouble() + actvity_edfood_cart_vat.text.toString().replace(",","").toDouble() + actvity_edfood_cart_service_fee.text.toString().replace(",","").toDouble()
                        totalVoucher += additionalFee
                        activity_food_cart_textview_total_amount.text = commaSeparated(totalVoucher.toString()).asString()

                        mDiscount = discountValue
                        mDiscountName = promoName
                        mPromoId = promoID

                    } else {
                        isVoucherCodeValid = false

                        mDiscount = "0"
                        mDiscountName = ""
                        mPromoId = ""

                        val title = jsonObj[key_title].asString
                        try {
                            activity_food_cart_textview_voucher_status.setTextColor(getResources().getColor(R.color.ed2e_red))
                            //activity_food_cart_textview_voucher_status.setTextColor(getColor(R.color.ed2e_red))
                        } catch(e: Exception) {
                            //activity_food_cart_textview_voucher_status.setTextColor(getResources().getColor(R.color.ed2e_red))
                        }
                        activity_food_cart_textview_voucher_status.text = title
                    }
                }
            }
        ) { failure: Throwable? ->
            hideProgress()
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    //Delete item
    private fun initDeleteItem(
        order_id: String,
        booked_id: String
    ) {
        val params =
            "order_id:" + order_id + "||" +
            "booked_id:" + booked_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartDelete,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                launch(Dispatchers.Main) {
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        if(bookedId != "")
                        {
                            countryName?.let {
                                initGetCartOrderList(preference.getString(key_user_id), bookedId, it)
                            }
                        }
                    } else {
                        val errorCode = jsonObj[key_code].asString
                        showDialogError(request_error_dialog, errorCode)
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    //For checking the voucher code
    private fun updateCart(
        order_id: String,
        food_size: String,
        food_quantity: String,
        food_price: String,
        addons: String
    ) {
        val params =
            "order_id:" + order_id + "||" +
                    "food_size:" + food_size + "||" +
                    "food_quantity:" + food_quantity + "||" +
                    "food_price:" + food_price + "||" +
                    "addons:" + addons
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartUpdate,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject

                launch(Dispatchers.Main) {

                    Log.d("LOG", "Data Update: " + response)
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        if(bookedId != "")
                        {
                            countryName?.let {
                                initGetCartOrderList(preference.getString(key_user_id), bookedId, it)
                            }
                        }
                    } else {
                        val errorCode = jsonObj[key_code].asString
                        showDialogError(request_error_dialog, errorCode)
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun updateList()
    {
        this@EDFoodCartActivity.runOnUiThread {
            //            val parser = JsonParser()
//            sortedArraylist.sortWith(Comparator { jinks, jinx ->
//                parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
//            })
            Timber.d("updateList %s", arraylist)
            adapter.notifyDataSetChanged()
        }
    }



    private fun initToolBar() {
        edfood_mycart_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edfood_mycart_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edfood_mycart_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}