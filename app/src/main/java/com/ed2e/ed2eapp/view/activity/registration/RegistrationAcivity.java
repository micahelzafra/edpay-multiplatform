package com.ed2e.ed2eapp.view.activity.registration;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginActivity;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.PinCreateAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.login.LoginMPinActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.defaultPassword;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_current_location;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_fail;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegister;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegister2;

public class RegistrationAcivity extends BaseActivity {

    public static final String TAG = RegistrationAcivity.class.getSimpleName();

    @BindView(R.id.registration_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.registration_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.registration_editText_first_name)
    EditText editText_first_name;
    @BindView(R.id.registration_editText_last_name)
    EditText editText_last_name;
    @BindView(R.id.registration_editText_mobile_number_prefix_plus)
    EditText editText_mobile_number_prefix_plus;
    @BindView(R.id.registration_editText_mobile_number_prefix)
    EditText editText_mobile_number_prefix;
    @BindView(R.id.registration_editText_mobile_number)
    EditText editText_mobile_number;
    @BindView(R.id.registration_editText_email_address)
    EditText editText_email_address;
    @BindView(R.id.registration_editText_referral_code)
    EditText editText_referral_code;
    @BindView(R.id.registration_button_register)
    Button button_register;

    private String mobileNumber = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initCurrentLocation();
        initToolBar();
        initGUI();
    }

    private void initGUI() {

        editText_mobile_number_prefix_plus.setVisibility(View.VISIBLE);
        editText_mobile_number_prefix.setText("234");

        editText_mobile_number_prefix.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editText_mobile_number_prefix.getText().toString().trim().equalsIgnoreCase("")){
                    editText_mobile_number_prefix_plus.setVisibility(View.GONE);
                } else {
                    editText_mobile_number_prefix_plus.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




        button_register.setOnClickListener(v -> {


            if(editText_first_name.getText().toString().trim().equalsIgnoreCase("")){
                editText_first_name.setError(getResources().getString(R.string.error_no_first_name));
                editText_first_name.requestFocus();
                return;
            }

            if(!isNameValid(editText_first_name.getText().toString().trim().replaceAll(" ", ""))){
                editText_first_name.setError(getResources().getString(R.string.error_invalid_name));
                editText_first_name.requestFocus();
                return;
            }

            if(editText_last_name.getText().toString().trim().equalsIgnoreCase("")){
                editText_last_name.setError(getResources().getString(R.string.error_no_last_name));
                editText_last_name.requestFocus();
                return;
            }

            if(!isNameValid(editText_last_name.getText().toString().trim().replaceAll(" ", ""))){
                editText_last_name.setError(getResources().getString(R.string.error_invalid_name));
                editText_last_name.requestFocus();
                return;
            }

            if(editText_mobile_number_prefix.getText().toString().trim().equalsIgnoreCase("")){
                editText_mobile_number.setError(getResources().getString(R.string.error_no_mobile_num_prefix));
                editText_mobile_number_prefix.requestFocus();
                return;
            }

            if(editText_mobile_number.getText().toString().trim().equalsIgnoreCase("")){
                editText_mobile_number.setError(getResources().getString(R.string.error_no_mobile_num));
                editText_mobile_number.requestFocus();
                return;
            }

            if(editText_email_address.getText().toString().trim().equalsIgnoreCase("")){
                editText_email_address.setError(getResources().getString(R.string.error_no_email_address));
                editText_email_address.requestFocus();
                return;
            }

            if(!isEmailValid(editText_email_address.getText().toString().trim())){
                editText_email_address.setError(getResources().getString(R.string.error_invalid_email_address));
                editText_email_address.requestFocus();
                return;
            }

            String mobileNum = "+" + editText_mobile_number_prefix.getText().toString().trim().replaceAll("\\+", "") + editText_mobile_number.getText().toString().trim();

            initAPIRegister(
                    editText_email_address.getText().toString().trim(),
                    editText_last_name.getText().toString().trim(),
                    editText_first_name.getText().toString().trim(),
                    mobileNum,
                    getCurrentLocationData(tag_current_location_country_code, RegistrationAcivity.this),
                    editText_referral_code.getText().toString().trim()
            );
            //showDialog_main("", "button_ok", true,"Registration Successful", "You may now use your registered mobile number to login and enjoy EDPAY App features.", "", "OK");
        });
    }

    private void initAPIRegister(String email, String lastName, String firstName, String mobileNumber, String countryCode, String referralCode)
    {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlRegister2,
                "email:"+ email + "||" +
                        "last_name:" + lastName + "||" +
                        "first_name:" + firstName + "||" +
                        "mobile_number:" + mobileNumber + "||" +
                        "country_code:" + countryCode + "||" +
                        "referral_code:" + referralCode,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + email + " | " + lastName + " | " + firstName + " | " + mobileNumber + " | " + countryCode  + " | " + referralCode + " |");
                    Log.d(TAG, "RESPONSE: " + response);

                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    if (status.equals(status_success))
                    {
                        String custID = jsonObj.get("customer_id").getAsString();
                        Intent intent=new Intent(RegistrationAcivity.this, OTPActivity.class);
                        intent.putExtra(key_mobile_number, mobileNumber);
                        intent.putExtra(key_cust_id, custID);
                        intent.putExtra(key_prev_page, "registration");
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                    else if (status.equals(status_fail))
                    {
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    } else {
                        showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), "", "","OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }


//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }




    private void initCurrentLocation() {
        getCurrentLocation(getContext());
        //Log.d("LOCATION", "Response Location: " + preference.getString(key_current_location, ""));
    }

    private void initPreviousActivity() {
        Intent intent = new Intent(getContext(), LoginOrRegAcivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "button_ok":
                Intent intent=new Intent(RegistrationAcivity.this, OTPActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
