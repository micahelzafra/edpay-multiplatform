//
//  EDFoodCartVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-Zafyrajinx on 2/4/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class EDFoodCartVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()

    let downloader = ImageDownloader()
    
    var arraylist = [JSON]()
    var cart = JSON()

    private var bookedId = ""
    private var countryName :String? = ""
    private var bookedLocation :String? = ""
    private var restaurantId = ""
    private var promoId = ""
    private var promoName = ""
    private var promoAmount = ""
    private var isVoucherCodeValid = true

    var containerFee = ""
    var deliveryFee = ""
    
    //Order Detail
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblContainerFee: UILabel!
    @IBOutlet weak var lblDeliveryFee: UILabel!
    @IBOutlet weak var lblServiceFee: UILabel!
    @IBOutlet weak var lblServiceFeeAmount: UILabel!
    @IBOutlet weak var lblVAT: UILabel!
    @IBOutlet weak var lblVATAmount: UILabel!
    
    //Voucher
    @IBOutlet weak var tfVoucher: UITextField!
    @IBOutlet weak var btnVoucherValidate: DesignableButton!
    @IBOutlet weak var lblVoucherResult: UILabel!
    
    //Address
    @IBOutlet weak var lblOrderFrom: UILabel!
    @IBOutlet weak var lblDeliveryAdd: UILabel!
    
    @IBOutlet weak var tfSpecialNotes: UITextField!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfVoucher.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        var frame = tableView.frame
        frame.size.height = tableView.contentSize.height
        tableView.frame = frame
        
         hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.bookedLocation = (self.segueData[self.constant.key_booked_location] as! String)
        self.countryName = (self.segueData[self.constant.key_country] as! String)
        self.bookedId = (self.segueData[self.constant.key_booked_id] as! String)
        
        
        self.lblDeliveryAdd.text = bookedLocation
        
        if bookedId != ""
        {
            self.initGetCartOrderList(
                customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
                booked_id: bookedId,
                country_name: countryName!
            )
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arraylist.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return arraylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EDFoodCartTableCell", for: indexPath)
                        as! EDFoodCartTableCell

        let data = arraylist[indexPath.row]
        if let imageurl = data[self.constant.key_food_photo].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    cell.imageViewFood.image = image
                }
            }
        }
        cell.lblFoodName.text = data[self.constant.key_food_name].string
        cell.lblPrice.text = data[self.constant.key_food_price].string
        cell.lblQuantity.text = "\(data[self.constant.key_food_quantity].int ?? 0)"
        
        cell.layoutAddon.removeAllArrangedSubviews()
        for addon in data[self.constant.key_food_addons].array!
        {
            let addonObject = addon.self
            let food_addon = addonObject[self.constant.key_food_addon].string
            let display = "w/ \(food_addon ?? "")"
            //For side by side text dynamic
//            let stackView = UIStackView()
//            stackView.axis = .horizontal
//            stackView.alignment = .fill // .Leading .FirstBaseline .Center .Trailing .LastBaseline
//            stackView.distribution = .fill // .FillEqually .FillProportionally .EqualSpacing .EqualCentering
                        
//            let label = UILabel()
//            label.text = "Label"
//            label.font = label.font.withSize(11)
            
//            stackView.addArrangedSubview(lblAddonName)
//            stackView.addArrangedSubview(label)
//            cell.layoutAddon.addArrangedSubview(stackView)
            
            let lblAddonName = UILabel()
            lblAddonName.text = display
            lblAddonName.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            lblAddonName.translatesAutoresizingMaskIntoConstraints = false
            lblAddonName.font = lblAddonName.font.withSize(10)
            
            cell.layoutAddon.addArrangedSubview(lblAddonName)
        }
        
        let tag = (indexPath.section*100)+indexPath.row

        let tapMinus = UITapGestureRecognizer(target: self, action: #selector(tapMinusQuantity(sender:)))
        cell.imageViewLessQuantity.tag = tag
        cell.imageViewLessQuantity.isUserInteractionEnabled = true
        cell.imageViewLessQuantity.addGestureRecognizer(tapMinus)
        
        let tapAdd = UITapGestureRecognizer(target: self, action: #selector(tapAddQuantity(sender:)))
        cell.imageViewAddQuantity.tag = tag
        cell.imageViewAddQuantity.isUserInteractionEnabled = true
        cell.imageViewAddQuantity.addGestureRecognizer(tapAdd)
        
        cell.buttonRemove.tag = tag
        cell.buttonRemove.addTarget(self, action: #selector(removeAction(_ :)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var index = 125
        let data = arraylist[indexPath.row]
        let array = data[self.constant.key_food_addons].array!
        index = index + (array.count * 11)
        return CGFloat(index)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "seguePaymentMethod") {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? EDFoodPaymentMethod {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }
    
    @IBAction func btnVoucherValidate(_ sender: DesignableButton) {
        let jsonObj = self.cart
        let resto_info = jsonObj[self.constant.key_resto_info].self
        let restaurant_id = resto_info[self.constant.key_restaurant_id].int
//        let total = jsonObj[self.constant.key_total].string
        let subTotal = jsonObj[self.constant.key_sub_total].string
        initValidateVoucher(
            customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
            restaurant_id: "\(restaurant_id ?? -1)",
            promo_code: self.tfVoucher.text!,
            total_purchased: (subTotal?.replacingOccurrences(of: ",", with: ""))!)
    }
    
    @IBAction func btnPlaceOrder(_ sender: DesignableButton) {
        if tfVoucher.text != "" && !isVoucherCodeValid
        {
            self.handleError(displaytitle: "dialog_invalid_voucher_title".localized(), "dialog_invalid_voucher_message".localized())
        }
        else
        {
            if "\(self.cart)" != "{}"
            {
                let jsonObj = self.cart
                let subTotal = jsonObj[self.constant.key_sub_total].string
                containerFee = jsonObj[self.constant.key_container_fee].string!
                if type(of: jsonObj[self.constant.key_delivery_fee].object) == String.self
                {
                    deliveryFee = jsonObj[self.constant.key_delivery_fee].string!
                }
                else
                {
                    deliveryFee = "\(jsonObj[self.constant.key_delivery_fee].int ?? 0)"
                }
                let serviceFeePercentage = jsonObj[self.constant.key_service_fee_percentage].string
                var serviceFee = ""
                if type(of: jsonObj[self.constant.key_service_fee].object) == String.self
                {
                    serviceFee = jsonObj[self.constant.key_service_fee].string!
                }
                else
                {
                    serviceFee = "\(jsonObj[self.constant.key_service_fee].int ?? 0)"
                }
                let vat = jsonObj[self.constant.key_vat].string
                var vat_amount = ""
                if type(of: jsonObj[self.constant.key_vat_amount].object) == String.self
                {
                    vat_amount = jsonObj[self.constant.key_vat_amount].string!
                }
                else
                {
                    vat_amount = "\(jsonObj[self.constant.key_vat_amount].int ?? 0)"
                }
//                let restoInfo = jsonObj[self.constant.key_resto_info].self
//                let restaurantAddress = restoInfo[self.constant.key_restaurant_address].string
//                let restaurantName = restoInfo[self.constant.key_restaurant_name].string
                let total = self.lblTotal.text?.replacingOccurrences(of: ",", with: "")
//                var discount = jsonObj[self.constant.key_discount].string
//                var discount = ""
//                if type(of: jsonObj[self.constant.key_discount].object) == String.self
//                {
//                    discount = jsonObj[self.constant.key_discount].string!
//                }
//                else
//                {
//                    discount = "\(jsonObj[self.constant.key_discount].int ?? 0)"
//                }
//                var discountName = ""
//                if let discountname = jsonObj[self.constant.key_discount_name].string {
//                    discountName = discountname
//                }
                let original_sub_total = jsonObj[self.constant.key_original_sub_total].string
                let country_id = jsonObj[self.constant.key_country_id].int
                
                self.initCheckOut(
                    customer_id:  self.defaults.string(forKey: self.constant.key_user_id)!,
                    booked_id: bookedId,
                    vat: vat!,
                    vat_amount: vat_amount,
                    discount: promoAmount,
                    discount_name: promoName,
                    food_total_with_discount: subTotal!,
                    sub_total: original_sub_total!,
                    total: total!,
                    country_id: "\(country_id ?? -1)",
                    service_fee: serviceFee,
                    service_fee_percentage: serviceFeePercentage!,
                    delivery_fee: (self.lblDeliveryFee.text?.replacingOccurrences(of: ",", with: ""))!,
                    container_fee: (self.lblContainerFee.text?.replacingOccurrences(of: ",", with: ""))!,
                    food_total: (self.lblTotal.text?.replacingOccurrences(of: ",", with: ""))!,
                    promo_id: promoId,
                    special_notes: self.tfSpecialNotes.text!)
                
            }
        }
    }
    
    @objc func tapAddQuantity(sender: UITapGestureRecognizer) {
        let section = sender.view!.tag / 100
        let row = sender.view!.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)
        let data = arraylist[indexPath.row]
        
        let order_id = data[self.constant.key_order_id].int!
        let food_price = data[self.constant.key_food_price].string!
        let array = data[self.constant.key_food_addons].array!
        var food_quantity = data[self.constant.key_food_quantity].int!
        food_quantity = food_quantity + 1
        updateCart(order_id: "\(order_id)", food_size: "\(order_id)", food_quantity: "\(food_quantity)", food_price: food_price, addons: "\(array)")
    }
    
    @objc func tapMinusQuantity(sender: UITapGestureRecognizer) {
        let section = sender.view!.tag / 100
        let row = sender.view!.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)
        let data = arraylist[indexPath.row]
        var food_quantity = data[self.constant.key_food_quantity].int!
        let order_id = data[self.constant.key_order_id].int!
        let food_price = data[self.constant.key_food_price].string!
        let array = data[self.constant.key_food_addons].array!
        self.log("tapMinusQuantity \(food_quantity)")
        if food_quantity >= 2
        {
            food_quantity = food_quantity - 1
            updateCart(order_id: "\(order_id)", food_size: "\(order_id)", food_quantity: "\(food_quantity)", food_price: food_price, addons: "\(array)")
        }
    }
    
    @objc func removeAction(_ sender: UIButton) {
        let section = sender.tag / 100
        let row = sender.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)
        let data = arraylist[indexPath.row]
        let order_id = data[self.constant.key_order_id].int!
        initDeleteItem(order_id: "\(order_id)", booked_id: self.bookedId)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.log("textFieldDidChange ")
        isVoucherCodeValid = false
    }

    //get the data to display
    private func initGetCartOrderList(
        customer_id: String,
        booked_id: String,
        country_name: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartOrderList,
            params: "customer_id:\(customer_id)||booked_id:\(booked_id)||distance_km:1||country_name:\(country_name)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                var msg = ""
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    msg = status
                }

                if msg != self.constant.status_fail
                {
                    self.cart = json
                    let sub_total = json[self.constant.key_sub_total].string?.replacingOccurrences(of: ",", with: "")
                    let container_fee = json[self.constant.key_container_fee].string?.replacingOccurrences(of: ",", with: "")
                    let delivery_fee = json[self.constant.key_delivery_fee].string?.replacingOccurrences(of: ",", with: "")
                    let service_fee_percentage = json[self.constant.key_service_fee_percentage].string
                    var service_fee = ""
                    if type(of: json[self.constant.key_service_fee].object) == String.self
                    {
                        service_fee = json[self.constant.key_service_fee].string!
                    }
                    else
                    {
                        service_fee = "\(json[self.constant.key_service_fee].int ?? 0)"
                    }
                    let vat = json[self.constant.key_vat].string
                    var vat_amount = ""
                    if type(of: json[self.constant.key_vat_amount].object) == String.self
                    {
                        vat_amount = json[self.constant.key_vat_amount].string!
                    }
                    else
                    {
                        vat_amount = "\(json[self.constant.key_vat_amount].int ?? 0)"
                    }
                    let resto_info = json[self.constant.key_resto_info].self
//                    let restaurant_address = resto_info[self.constant.key_restaurant_address].string
                    let restaurant_name = resto_info[self.constant.key_restaurant_name].string
                    let total = json[self.constant.key_total].string?.replacingOccurrences(of: ",", with: "")
                    
                    self.lblSubTotal.text = commaSeparated(value: sub_total!).asString()
                    self.lblContainerFee.text = commaSeparated(value: container_fee!).asString()
                    self.lblDeliveryFee.text = commaSeparated(value: delivery_fee!).asString()
                    self.lblServiceFee.text = "Service Fee \(service_fee_percentage ?? "0.00")%"
                    self.lblServiceFeeAmount.text = commaSeparated(value: service_fee).asString()
                    self.lblVAT.text = "VAT \(vat ?? "0.00")"
                    self.lblVATAmount.text = commaSeparated(value: vat_amount).asString()
                    
                    self.lblOrderFrom.text = restaurant_name
                    self.lblTotal.text = commaSeparated(value: total!).asString()
                    
//                    var original_sub_total = ""
//                    if (json[self.constant.key_original_sub_total].string == nil) {
//                        original_sub_total = ""
//                    } else {
//                        original_sub_total = json[self.constant.key_original_sub_total].string!
//                    }
//                    self.lblCartTotalAmount.text = commaSeparated(value: original_sub_total).asString()
//
                    self.arraylist.removeAll()
                    let jsonArray = json[self.constant.key_order].array
                    let addOnsArray = json[self.constant.key_food_addons].array
                    var quantities = 0
                    for json in jsonArray!
                    {
                        let jsonItem = json.self
//                        var jsonObject: JSON = jsonItem
                        let order_id = jsonItem[self.constant.key_order_id].int
                        var jsonArr = [JSON]()
                        for data in addOnsArray!
                        {
                            let addonlist = data.array
                            for addon in addonlist!
                            {
                                let jsonAddonItem = addon.self
                                let add_id = jsonAddonItem[self.constant.key_order_list_id].int
                                if (order_id == add_id)
                                {
                                    jsonArr.append(addon)
                                }
                            }
                        }
//                        jsonObject.append("food_addons", jsonArr)
                        let foodQuantity = jsonItem[self.constant.key_food_quantity].int
                        quantities = quantities + foodQuantity!

                        let jsonObject: JSON = [
                            "food_addons": jsonArr
                        ]

                        let updated = try! jsonObject.merged(with: jsonItem)
                        self.arraylist.append(updated)
                    }
                    self.tableView.reloadData()

                    self.tableView.layoutIfNeeded()
                    self.tableView.heightAnchor.constraint(equalToConstant: self.tableView.contentSize.height).isActive = true
//                    self.tableView.layoutIfNeeded()
//                    let height = CGFloat(125 * self.arraylist.count)
//                    self.log("height \(height)")
//                    self.tableViewHeight.constant = height
//                    self.tableView.reloadData()
//                    
//                    self.tableViewHeight.constant = self.tableView.contentSize.height
//                    self.tableView.isScrollEnabled = false
//                    self.view.layoutIfNeeded()
                }
                else
                {
                    let code = json["code"].string
                    if code == "ED12"
                    {
                        self.dismiss(animated: false)
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                    }
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
    
    //For place order
    private func initCheckOut(
        customer_id: String,
        booked_id: String,
        vat: String,
        vat_amount: String,
        discount: String,
        discount_name: String,
        food_total_with_discount: String,
        sub_total: String,
        total: String,
        country_id: String,
        service_fee: String,
        service_fee_percentage: String,
        delivery_fee: String,
        container_fee: String,
        food_total: String,
        promo_id: String,
        special_notes: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartCheckout,
            params: "customer_id:\(customer_id)||booked_id:\(booked_id)||vat:\(vat)||vat_amount:\(vat_amount)||discount:\(discount)||discount_name:\(discount_name)||food_total_with_discount:\(food_total_with_discount)||sub_total:\(sub_total)||total:\(total)||country_id:\(country_id)||service_fee:\(service_fee)||service_fee_percentage:\(service_fee_percentage)||delivery_fee:\(delivery_fee)||container_fee:\(container_fee)||food_total:\(food_total)||promo_id:\(promo_id)||special_notes:\(special_notes)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    var segueData = Dictionary<String, AnyObject>()
                    segueData[self.constant.key_country] = self.countryName! as AnyObject
                    segueData[self.constant.key_booked_id] = self.bookedId as AnyObject
                    segueData[self.constant.key_total] = total as AnyObject
                    segueData[self.constant.key_special_notes] = self.tfSpecialNotes.text as AnyObject
                    self.performSegue(withIdentifier: "seguePaymentMethod", sender: segueData)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
    
    //For editing the orders
    private func initGetMerchantMenu(
        restaurant_id: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartOrderList,
            params: "restaurant_id:\(restaurant_id)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
    
    //For checking the voucher code
    private func initValidateVoucher(
        customer_id: String,
        restaurant_id: String,
        promo_code: String,
        total_purchased: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlValidateVoucherCode,
            params: "customer_id:\(customer_id)||restaurant_id:\(restaurant_id)||promo_code:\(promo_code)||total_purchased:\(total_purchased)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)


                self.lblVoucherResult.alpha = 1.0
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    if status == self.constant.status_success {
                        self.isVoucherCodeValid = true
                        let jsonObject = json[self.constant.key_data].self
                        let promoName = jsonObject[self.constant.key_promo_name].string
                        let promoID = jsonObject["promo_id"].int
                        let discountValue = jsonObject[self.constant.key_discount_value].string
                        self.promoName = promoName!
                        self.promoAmount = discountValue!
                        self.promoId = "\(promoID ?? -1)"
                        self.lblVoucherResult.text = "\(promoName ?? "") less discount - \(commaSeparated(value: discountValue!).asString())"
                        self.lblVoucherResult.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
    //                    activity_food_cart_textview_voucher_status.setTextColor(getColor(R.color.greenColor))
    //                    activity_food_cart_textview_voucher_status.text = "$promoName less discount - ${commaSeparated(discountValue).asString()}"
    //
    //
                        let freeContainer = jsonObject[self.constant.key_free_container].int
                        let freeDelivery = jsonObject[self.constant.key_free_delivery].int
                        var totalVoucher = Double(jsonObject[self.constant.key_total_purchased_with_discount].string!)!
                        if (freeContainer == 1)
                        {
    //                        actvity_edfood_cart_container_fee.text = "0.00"
                            self.lblContainerFee.text = "0.00"
                            self.containerFee = "0.00"
                        }
                        if (freeDelivery == 1)
                        {
    //                        actvity_edfood_cart_delivery_fee.text = "0.00"
                            self.lblDeliveryFee.text = "0.00"
                            self.deliveryFee = "0.00"
                        }
                        let containerfee = self.lblContainerFee.text
                        let deliveryfee = self.lblDeliveryFee.text
                        let vatamount = self.lblVATAmount.text
                        let servicefee = self.lblServiceFeeAmount.text
                        let additionalFee = Double(containerfee!.replacingOccurrences(of: ",", with: ""))! + Double(deliveryfee!.replacingOccurrences(of: ",", with: ""))! + Double(vatamount!.replacingOccurrences(of: ",", with: ""))! + Double(servicefee!.replacingOccurrences(of: ",", with: ""))!
                        totalVoucher = totalVoucher + additionalFee
                        self.lblTotal.text = commaSeparated(value: "\(totalVoucher)").asString()
                    }
                    else
                    {
                        self.isVoucherCodeValid = false
                        let title = json[self.constant.key_title].string
                        self.lblVoucherResult.text = "\(title ?? "")"
                        self.lblVoucherResult.textColor = #colorLiteral(red: 0.9621763825, green: 0.01609125175, blue: 0, alpha: 1)
                    }
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
    
    //Delete item
    private func initDeleteItem(
        order_id: String,
        booked_id: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartDelete,
            params: "order_id:\(order_id)||booked_id:\(booked_id)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    self.initGetCartOrderList(customer_id: self.defaults.string(forKey: self.constant.key_user_id)!, booked_id: self.bookedId, country_name: self.countryName!)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
    
    //For checking the voucher code
    private func updateCart(
       order_id: String,
       food_size: String,
       food_quantity: String,
       food_price: String,
       addons: String
    ) {
        showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartUpdate,
            params: "order_id:\(order_id)||food_size:\(food_size)||food_quantity:\(food_quantity)||food_price:\(food_price)||addons:\(addons)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    self.initGetCartOrderList(customer_id: self.defaults.string(forKey: self.constant.key_user_id)!, booked_id: self.bookedId, country_name: self.countryName!)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
}
