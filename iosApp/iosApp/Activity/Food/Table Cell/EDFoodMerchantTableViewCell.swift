//
//  EDFoodMerchantTableViewCell.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/1/20.
//

import UIKit

class EDFoodMerchantTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewFood: UIImageView!
    
    @IBOutlet weak var lblFoodRange: UILabel!
    @IBOutlet weak var lblFoodLocation: UILabel!
    @IBOutlet weak var lblFoodStatus: UILabel!
    @IBOutlet weak var lblFoodName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
