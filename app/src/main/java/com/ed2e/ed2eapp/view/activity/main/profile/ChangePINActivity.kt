package com.ed2e.ed2eapp.view.activity.main.profile



import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.otp.ResetPINOTPActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activtity_change_pin.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class ChangePINActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activtity_change_pin)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
    }

    private fun initGUI() {
        change_pin_textview_forgot_pin.setOnClickListener {

            val data = preference.getString(key_user_data)
            //showToast_short("DATA: $data")
            val parser = JsonParser()
            val jsonObj: JsonObject = parser.parse(data).asJsonObject
            Timber.d("jsonObj: %s", jsonObj)

            val mobileNumber = jsonObj["mobile_number"].asString
            val customerID = jsonObj["id"].asString

            val intent = Intent(
                context,
                ResetPINOTPActivity::class.java
            )
            intent.putExtra(key_mobile_number, mobileNumber)
            intent.putExtra(key_cust_id, customerID)
            intent.putExtra(key_prev_page, "change_pin")
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        change_pin_button_save.setOnClickListener {
            val current_pin = change_pin_edittext_current_pin.text
            val new_pin = change_pin_edittext_new_pin.text
            val confirm_pin = change_pin_edittext_retype_new_pin.text

            if(current_pin.isNullOrEmpty() || "$current_pin".length < 6) {
                //change_pin_edittext_current_pin.error = resources.getString(R.string.error_invalid_pin)
                change_pin_edittext_current_pin.requestFocus()
                showDialog_main("", "", true, "Invalid Current PIN", "Please input a valid PIN (must be 6 digits).", "", "OK")
                //showDialog_APIError("", "", true, "ED94", "", "OK")
                return@setOnClickListener
            }

            if(new_pin.isNullOrEmpty() || "$new_pin".length < 6) {
                //change_pin_edittext_new_pin.error = resources.getString(R.string.error_invalid_pin)
                change_pin_edittext_new_pin.requestFocus()
                showDialog_main("", "", true, "Invalid New PIN", "Please input a valid PIN (must be 6 digits).", "", "OK")
                //showDialog_APIError("", "", true, "ED94", "", "OK")
                return@setOnClickListener
            }

            if(confirm_pin.isNullOrEmpty() || "$confirm_pin".length < 6) {
                //change_pin_edittext_retype_new_pin.error = resources.getString(R.string.error_invalid_pin)
                change_pin_edittext_retype_new_pin.requestFocus()
                showDialog_main("", "", true, "Invalid Re-Type New PIN", "Please input a valid PIN (must be 6 digits).", "", "OK")
                //showDialog_APIError("", "", true, "ED94", "", "OK")
                return@setOnClickListener
            }

            if ("$current_pin" == "$new_pin")
            {
                showDialog_main("", "", true, "Invalid New PIN", "Please choose a new PIN that you are not currently using.", "", "OK")
                return@setOnClickListener
            }

            if (!("$new_pin".equals("$confirm_pin")))
            {
                showDialog_APIError("", "", true, "ED100", "", "OK")
                return@setOnClickListener
            }

            initPINUpdate(preference.getString(key_user_id), "$current_pin","$new_pin")
        }


    }

    private fun initPINUpdate(
        customer_id: String,
        old_pin: String,
        new_pin: String
    ) {

        showProgress()
        val params = "customer_id:" + customer_id + "||" +
                "old_pin:" + old_pin + "||" +
                "new_pin:" + new_pin
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlPINUpdate,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    hideProgress()
//                    val data = jsonObj[key_data].asJsonObject
//                    Timber.d("data: %s", data)


                    runOnUiThread {

                        showDialog_main(
                            "",
                            "tag_mpin_success_button_ok",
                            false,
                            resources.getString(R.string.dialog_reset_mpin_success_title),
                            resources.getString(R.string.dialog_reset_mpin_success_message),
                            "",
                            "OK"
                        )


//                        AlertDialog.Builder(this)
//                            .setTitle(label_dialog_title_pin_change_success)
//                            .setMessage(label_dialog_msg_pin_change_success)
//                            .setPositiveButton(
//                                button_okay
//                            ) { dialogInterface: DialogInterface, i: Int ->
//                                dialogInterface.dismiss()
//                                finish()
//                                //TODO intent to my account page
//                            }
//                            .setCancelable(false)
//                            .show()
                    }


                } else {
                    hideProgress()
                    val error_code = jsonObj[key_code].asString
                    //showDialogError(request_error_dialog, error_code)
                    showDialog_APIError("", "", true, error_code, "", "OK")
                }
            }
        ) { failure: Throwable? ->
            hideProgress()
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }



    private fun initToolBar() {
        change_pin_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    change_pin_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    change_pin_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        when (requestTag) {
            "tag_mpin_success_button_ok" -> {
                finish()
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            }
            else -> {
            }
        }
    }
    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
