package com.ed2e.ed2eapp.view.activity.main.home.edfood

import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edfood_item_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.json.JSONObject
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import kotlin.coroutines.CoroutineContext


class EDFoodItemDetailsActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference
    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    private var addons = ArrayList<Any>()
    private var selectedAddons = ArrayList<Any>()
    private var sortedArraylist = ArrayList<Any>()

    private var bookedId = ""
    private var country_name :String? = ""
    private var quantity = 1
    private var addon_amount = 0.00
    private var booked_location :String? = ""
    private var restaurant_id :String? = ""
    private var jsonObj = JsonObject()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_item_details)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
        initListener()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data)
        booked_location = bundle.getString(key_booked_location)
        restaurant_id = bundle.getString(key_restaurant_id)
        val parser = JsonParser()
        val parseString = parser.parse(data).asString
        jsonObj = parser.parse(parseString).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)
        country_name = bundle.getString(key_country)
        bundle.getString(key_country)?.let {
            bookedId = it
        }
        Timber.d("bookedId: %s", bookedId)
        val restaurantPhoto = jsonObj[key_food_photo].asString

        Glide.with(this@EDFoodItemDetailsActivity).load(restaurantPhoto).apply(
            RequestOptions().override(
                256,
                256
            ).centerCrop().placeholder(R.drawable.ic_default_food)
        ).into(edfood_item_details_imageview_display)

        adapter = object : BaseRecyclerViewAdapter<Any>(addons, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {
            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
//                return when(obj){
//                    is String->R.layout.child_edfood_menu_category
//                    else->R.layout.child_edfood_menu_item
//                }
                return R.layout.child_edfood_addon_category
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }
        edfood_item_details_recyclerview_addons.layoutManager= LinearLayoutManager(context)
        edfood_item_details_recyclerview_addons.setHasFixedSize(true)
        edfood_item_details_recyclerview_addons.adapter=adapter

        val foodName = jsonObj[key_food_name].asString
        val foodDescription = jsonObj[key_food_description].asString
        val foodPrice = jsonObj[key_food_price].asString
        edfood_item_details_textview_name.text = foodName
        edfood_item_details_textview_desc.text = foodDescription
        edfood_item_details_textview_amount.text = foodPrice

        val add_ons_category = jsonObj[key_add_ons_category].asJsonArray
        if (add_ons_category.size() == 0)
        {
            edfood_item_details_view_addons_divider.visibility = GONE
        }
        else
        {
            edfood_item_details_view_addons_divider.visibility = VISIBLE
            for (addons in add_ons_category)
            {
                this.addons.add(addons)
            }
        }
        Timber.d("add_ons_category: %s", addons)

        computeTotal()

        edfood_item_details_button_add.setOnClickListener {
            quantity++
            computeTotal()
        }

        edfood_item_details_button_minus.setOnClickListener {
            if (quantity >= 2)
            {
                quantity--
            }
            computeTotal()
        }

        edfood_item_details_button_cart.setOnClickListener {

            Timber.d("addons main %s",addons)
            Timber.d("addons %s",selectedAddons)
            var min_addon = 0
            var max_addon = 0
            var max_addon_optional = 0
            var addon_count = 0
            if (addons.size != 0)
            {
                for (addonCat in addons)
                {
                    val jsonObj = parser.parse(addonCat.toString()).asJsonObject
                    val mandatory = jsonObj["mandatory"].asString
                    val addons = jsonObj["addons"].asJsonArray
                    val min = jsonObj[key_min].asInt
                    val max = jsonObj[key_max].asInt
                    if (mandatory.equals("1"))
                    {
                        min_addon += min
                        max_addon += max
                        for (addon in addons)
                        {
                            for (selected in selectedAddons)
                            {
                                Timber.d("addons $addon selected $selected")
                                if(selected.equals(addon))
                                {
                                    addon_count += 1
                                }
                            }
                        }
                    }
                    else
                    {
                        max_addon_optional += max
                    }
                }


                if(addon_count < min_addon)
                {
                    AlertDialog.Builder(this)
                        .setTitle(label_dialog_title_addon_required)
                        .setMessage(label_dialog_msg_addon_required)
                        .setPositiveButton(button_okay
                        ) { dialogInterface: DialogInterface, i: Int ->
                            dialogInterface.dismiss()
                        }
                        .setCancelable(false)
                        .show()
                    return@setOnClickListener
                }

                if(max_addon != 0)
                {
                    if(addon_count > max_addon)
                    {
                        AlertDialog.Builder(this)
                            .setTitle(label_dialog_title_addon_required)
                            .setMessage(label_dialog_msg_addon_exceed)
                            .setPositiveButton(button_okay
                            ) { dialogInterface: DialogInterface, i: Int ->
                                dialogInterface.dismiss()
                            }
                            .setCancelable(false)
                            .show()
                        return@setOnClickListener
                    }
                }

                if(max_addon_optional != 0)
                {
                    if(addon_count > max_addon_optional)
                    {
                        AlertDialog.Builder(this)
                            .setTitle(label_dialog_title_addon_required)
                            .setMessage(label_dialog_msg_addon_exceed)
                            .setPositiveButton(button_okay
                            ) { dialogInterface: DialogInterface, i: Int ->
                                dialogInterface.dismiss()
                            }
                            .setCancelable(false)
                            .show()
                        return@setOnClickListener
                    }
                }
            }

            val arralist = ArrayList<String>()
            for (selected in selectedAddons)
            {
                val jsonObj = parser.parse(selected.toString()).asJsonObject
                val addon_name = jsonObj["addon_name"].asString
                val addon_price = jsonObj["addon_price"].asString
                val id = jsonObj["id"].asString
                val jsonObject = JSONObject()
                jsonObject.put("food_addon",addon_name)
                jsonObject.put("addon_price",addon_price)
                jsonObject.put("addon_type",this.jsonObj[key_category].asString)
                jsonObject.put("food_id",this.jsonObj[key_food_id].asString)
                jsonObject.put("id",id)
                arralist.add(jsonObject.toString())
            }

            initCreateItemCart(
                booked_location.toString(),
                preference.getString(key_user_id),
                restaurant_id.toString(),
                jsonObj[key_food_id].asString,
                edfood_item_details_textview_quantity.text.toString(),
                jsonObj[key_food_price].asString,
                bookedId,
                jsonObj[key_category].asString,
                jsonObj[key_container_price].asString,
                edfood_item_details_edittext_special_notes.text.toString(),
                arralist.toString()
            )
        }

        initToolBar()
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        val parser = JsonParser()
        val jsonObj = parser.parse(value.toString()).asJsonObject
        val amount = jsonObj[key_addon_price].asDouble
        when (tag) {
            tag_load_addon -> {
                Timber.d("add on $value")
                if (value != null) {
                    selectedAddons.add(value)
                    addon_amount += amount
                    computeTotal()
                }
            }
            tag_load_addon_remove -> {
                Timber.d("remove add on $value")
                if (value != null) {
                    selectedAddons.remove(value)
                    addon_amount -= amount
                    computeTotal()
                }
            }
        }
    }

    private fun computeTotal()
    {
        edfood_item_details_textview_quantity.text = quantity.toString()
        edfood_item_details_textview_total_amount.text = commaSeparated((quantity * (edfood_item_details_textview_amount.text.toString().replace(",","").toDouble() + addon_amount)).toString()).asString()
    }

    private fun initCreateItemCart(
        booked_location: String,
        customer_id:String,
        restaurant_id:String,
        food_id:String,
        food_quantity:String,
        food_price:String,
        booked_id:String,
        category_name:String,
        container_price:String,
        special_notes:String,
        addons:String
    ) {
        val params =
            "booked_location:$booked_location"+"||" +
            "customer_id:$customer_id"+"||" +
            "restaurant_id:$restaurant_id"+"||" +
            "food_id:$food_id"+"||" +
            "food_quantity:$food_quantity"+"||" +
            "food_price:$food_price"+"||" +
            "booked_id:$booked_id"+"||" +
            "category_name:$category_name"+"||" +
            "container_price:$container_price"+"||" +
            "special_notes:$special_notes"+"||" +
            "addons:$addons"
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartCreateItem,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    finish()
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                } else {
                    val errorCode = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, errorCode)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initCartDelete(
        customer_id: String,
        restaurant_id: String
    ) {
        val params =
            "customer_id:" + customer_id + "||" +
                    "restaurant_id:" + restaurant_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartDelete,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    val data = jsonObj[key_data].asJsonObject
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initGetCartOrderList(
        customer_id: String,
        booked_id: String,
        country_name: String
    ) {
        val params =
            "customer_id:" + customer_id + "||" +
                    "country_name:" + country_name + "||" +
                    "distance_km:1" + "||" +
                    "booked_id:" + booked_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartOrderList,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    //TODO
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun updateList()
    {
        this@EDFoodItemDetailsActivity.runOnUiThread {
            //            val parser = JsonParser()
//            sortedArraylist.sortWith(Comparator { jinks, jinx ->
//                parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
//            })
            adapter.notifyDataSetChanged()
        }
    }


    private fun initToolBar() {
        edfood_item_details_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edfood_item_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edfood_item_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
