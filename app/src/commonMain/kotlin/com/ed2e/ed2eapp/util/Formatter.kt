package com.ed2e.ed2eapp.util


//abstract class Formatter
//{
//    fun String.Companion.format(
//        format: String,
//        vararg args: Any?
//    ): String {
//        return format
//    }

//    fun commaSeparated(value:Int) :String
//    {
////        val formattedNumber: String = String.format("%,d", value)
////        println(formattedNumber)
//        return java.lang.String.format("%,.2f", value)
////        return String.format("%,.2f", value)
////        return formattedNumber
//    }
//}

expect class KMPDate(formatString: String) {
    fun asString(): String
}

expect class dateFormat(date: String, from: String , to:String) {
    fun asString(): String
}

expect class commaSeparated(value: String) {
    fun asString(): String
}

expect class paymentType(value: String) {
    fun asString(): String
}

expect class get30DaysBeforeDate() {
    fun asString(): String
}

expect class getLocalTimeStamp(date: String) {
    fun asString(): String
}

expect class getCurrentDay()
{
    fun asString():String
}

expect class getOrderStatus(status: String)
{
    fun asString():String
}