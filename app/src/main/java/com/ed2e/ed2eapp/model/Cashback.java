package com.ed2e.ed2eapp.model;

public class Cashback {

    private String transaction_id;
    private String transaction_no;
    private String payment_type;
    private String created_date;
    private String cashback;

    public Cashback(String transaction_id, String transaction_no, String payment_type, String created_date, String cashback) {
        this.transaction_id = transaction_id;
        this.transaction_no = transaction_no;
        this.payment_type = payment_type;
        this.created_date = created_date;
        this.cashback = cashback;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_no() {
        return transaction_no;
    }

    public void setTransaction_no(String transaction_no) {
        this.transaction_no = transaction_no;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }
}
