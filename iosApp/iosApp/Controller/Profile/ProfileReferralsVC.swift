//
//  ProfileReferralsVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/28/20.
//

import UIKit
import SwiftyJSON
import app

class ProfileReferralsVC: BaseViewController {

    @IBOutlet weak var vPromoCode: DesignableCardView!
    @IBOutlet weak var lblPromoCode: TapAndCopyLabel!
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initGUI()
    }
    
    func initGUI()
    {
        let userData = self.defaults.string(forKey: self.constant.key_user_data)
        let jsonData = userData!.data(using: String.Encoding.utf8)!
        let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
        let json: JSON = JSON(jsonDic)
        self.log(userData)
        lblPromoCode.text = json[self.constant.key_referral_code].string
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
  

}
