//
//  ActivityPayDetailsVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS dev-zafyrajinx on 2/11/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app
import AlamofireImage
import CoreMotion

class ActivityPayDetailsVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    var merchantData = JSON()
    
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblMerchantAmount: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblTransactionNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageViewMerchant: RoundableImageView!
    let downloader = ImageDownloader()
        
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.initGUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    func initGUI()
    {        
        merchantData = segueData[self.constant.key_data] as! JSON
        let jsonObj = merchantData
        let offlineMerchant = jsonObj[self.constant.key_offline_merchant].self
        let merchantName = offlineMerchant[self.constant.key_trade_name].string
        let amount = jsonObj[self.constant.key_paid_amount].string
        let country = jsonObj[self.constant.key_country].self
        let currency = country[self.constant.key_currency_code].string
        let displayAmount = "\(currency ?? "") \(commaSeparated(value: amount!.replacingOccurrences(of: ",", with: "")).asString())}"
        let transactionNo = jsonObj[self.constant.key_transaction_no].string
        let date = jsonObj[self.constant.key_created_date].string
        
        if let paymentMethod = country[self.constant.key_payment_method].string
        {
            lblPaymentMethod.text = paymentMethod
        }
        else
        {
            lblPaymentMethod.text = "EDCredits"
        }
        
        lblMerchantName.text = merchantName
        lblMerchantAmount.text = displayAmount
        lblTransactionNo.text = transactionNo
        lblDate.text = getLocalTimeStamp(date: date!).asString()
        
        self.log("merchantData \(merchantData)")
                
        if let imageurl = merchantData[self.constant.key_shop_facade].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewMerchant.image = image
                }
            }
        }
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
}
