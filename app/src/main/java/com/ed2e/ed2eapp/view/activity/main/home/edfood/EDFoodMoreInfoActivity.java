package com.ed2e.ed2eapp.view.activity.main.home.edfood;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter;
import com.ed2e.ed2eapp.adapter.ReviewsAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.Reviews;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;
import static com.ed2e.ed2eapp.util.ConstantKt.urlReviewListing;

public class EDFoodMoreInfoActivity extends BaseActivity {

    public static final String TAG = EDFoodMoreInfoActivity.class.getSimpleName();

//    @BindView(R.id.announcement_list_home_recyclerview)
//    RecyclerView recyclerview;
//    @BindView(R.id.announcement_list_home_textview_message)
//    TextView textview_message;

    @BindView(R.id.edfood_merchant_more_info_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edfood_merchant_more_info_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.edfood_merchant_more_info_layout_tab_about)
    LinearLayout layout_tab_about;
    @BindView(R.id.edfood_merchant_more_info_layout_tab_reviews)
    LinearLayout layout_tab_reviews;

    @BindView(R.id.edfood_merchant_more_info_layout_about)
    LinearLayout layout_about;
    @BindView(R.id.edfood_merchant_more_info_layout_reviews)
    LinearLayout layout_reviews;
    @BindView(R.id.edfood_merchant_more_info_layout_no_reviews)
    LinearLayout layout_no_reviews;
    @BindView(R.id.edfood_merchant_more_info_about_layout_operating_hours)
    LinearLayout layout_operating_hours;

    @BindView(R.id.edfood_merchant_more_info_imageview_display)
    ImageView imageView_display;
    @BindView(R.id.edfood_merchant_more_info_textview_name)
    TextView textView_name;
    @BindView(R.id.edfood_merchant_more_info_textview_distance)
    TextView textView_distance;
    @BindView(R.id.edfood_merchant_more_info_textview_short_desc)
    TextView textView_short_desc;
    @BindView(R.id.edfood_merchant_more_info_textview_rating_average)
    TextView textView_rating_average;
    @BindView(R.id.edfood_merchant_more_info_textview_review_count)
    TextView textView_review_count;

    @BindView(R.id.edfood_merchant_more_info_about_textview_name)
    TextView about_textView_name;
    @BindView(R.id.edfood_merchant_more_info_about_textview_long_desc)
    TextView about_textView_long_desc;
    @BindView(R.id.edfood_merchant_more_info_about_textview_address)
    TextView about_textView_address;

    @BindView(R.id.edfood_merchant_more_info_ratingbar)
    RatingBar ratingbar;
    @BindView(R.id.edfood_merchant_more_info_reviews_textview_rating_average)
    TextView reviews_textView_rating_average;
    @BindView(R.id.edfood_merchant_more_info_reviews_recyclerview_rating)
    RecyclerView recyclerview;


//    @BindView(R.id.otp_pinview)
//    PinView pinview;
//    @BindView(R.id.otp_textView_timer)
//    TextView textView_timer;
//    @BindView(R.id.otp_button_submit)
//    Button button_submit;

    private String restaurant_data = "";
    private String rest_id = "";
    private Boolean hasReviews = false;

    private List<Reviews> modelList;
    static RecyclerView recyclerView;
    ReviewsAdapter recyclerAdapter;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edfood_merchant_more_info);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            restaurant_data = "";
        } else {
            restaurant_data = extras.getString(key_data);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.edfood_merchant_more_info_reviews_recyclerview_rating);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new ReviewsAdapter(EDFoodMoreInfoActivity.this, modelList);
        recyclerView.setAdapter(recyclerAdapter);

        layout_about.setVisibility(View.VISIBLE);
        layout_no_reviews.setVisibility(View.GONE);
        layout_reviews.setVisibility(View.GONE);
        setTabSelected(0);


        String id = "";
        String name = "";
        String distance = "";
        String short_desc = "";
        String rating_average = "";
        String review_count = "";
        String long_desc = "";
        String address = "";
        String restaurant_photo = "";

        try {
            JSONObject restaurant_obj = new JSONObject(restaurant_data);

            try {
                id = "" + restaurant_obj.get("restaurant_id");
                rest_id = id;
            } catch (Exception e) {
                id = "" + restaurant_obj.getDouble("restaurant_id");
                rest_id = id;
            }

            name = (String) restaurant_obj.get("restaurant_name");
            try {
                distance = "" + restaurant_obj.get("distance_from_customer");
            } catch (Exception e) {
                distance = "" + restaurant_obj.getDouble("distance_from_customer");
            }
            short_desc = (String) restaurant_obj.get("short_description");
            long_desc = (String) restaurant_obj.get("about");
            address = (String) restaurant_obj.get("address");
            restaurant_photo = (String) restaurant_obj.get("restaurant_photo");

            JSONArray operating_hours_list = restaurant_obj.getJSONArray("operating_info");

            for(int i = 0; i < operating_hours_list.length(); i++)
            {

                JSONObject object = operating_hours_list.getJSONObject(i);

                String day = (String) object.get("day");
                String opening = (String) object.get("opening");
                String closing = (String) object.get("closing");

                addLayout(day, opening, closing);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Glide.with(EDFoodMoreInfoActivity.this)
                .load(restaurant_photo)
                .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_ed2e_logo))
                .into(imageView_display);

        textView_name.setText(name);
        about_textView_name.setText(name);

        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00");

        textView_distance.setText(formatter.format(Double.parseDouble(distance)) + " mi");
        textView_short_desc.setText(short_desc);
        about_textView_long_desc.setText(long_desc);
        about_textView_address.setText(address);




        initAPIGetReviewsList(id, false);

        //updateButton(false);

        //mobileNumber = "";

//        textView_timer.setOnClickListener(v -> {
//            if(timerCount <= 0) {
//                timerCount = 60;
//                updateButton(false);
//            } else {
//                return;
//            }
//        });
//
//        button_submit.setOnClickListener(v -> {
//            if(pinview.getText().toString().trim().length() < 6) {
//                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
//            } else {
//                initAPICheckOTP(mobileNumber, pinview.getText().toString().trim());
//            }
//
//            //showDialog_main("", "button_ok", true,"Registration Successful", "You may use your registered mobile number to login and enjoy EDPAY App features.", "", "OK");
//        });
//
//        if(mobileNumber.equalsIgnoreCase("") || mobileNumber == null) {
//            showDialog_APIError("", "tag_button_ok", true, "ED72", "", "OK");
//        } else {
//            initAPISendOTP(mobileNumber);
//            //startTimer();
//        }

        layout_tab_about.setOnClickListener(v -> {
            layout_about.setVisibility(View.VISIBLE);
            layout_reviews.setVisibility(View.GONE);
            layout_no_reviews.setVisibility(View.GONE);
            setTabSelected(0);
        });

        layout_tab_reviews.setOnClickListener(v -> {
            layout_about.setVisibility(View.GONE);
            setTabSelected(1);
            initAPIGetReviewsList(rest_id, true);
        });
    }

    private void setTabSelected(int tabPosition){
        switch(tabPosition) {
            case 0:
                layout_tab_about.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_tab_selected_background_red));
                layout_tab_reviews.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_tab_deselected));
                break;
            case 1:
                layout_tab_about.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_tab_deselected));
                layout_tab_reviews.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_tab_selected_background_red));
                break;
            default:
                layout_tab_about.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_tab_selected_background_red));
                layout_tab_reviews.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_tab_deselected));
                break;
        }

    }

    private void addLayout(String mDay, String mOpening, String mClosing) {
        View layout_operating_info = LayoutInflater.from(this).inflate(R.layout.child_edfood_merchant_more_info_operating_hours, layout_operating_hours, false);

        TextView textView_weekday = (TextView) layout_operating_info.findViewById(R.id.child_operting_hours_textview_weekday);
        TextView textView_time = (TextView) layout_operating_info.findViewById(R.id.child_operting_hours_textview_time);

        // Get date from string
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        Date dateOpening = null;
        Date dateClosing = null;

        String finalOpening = "";
        String finalClosing = "";

        try {
            dateOpening = dateFormatter.parse(mOpening);
            dateClosing = dateFormatter.parse(mClosing);

            // Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
            finalOpening = timeFormatter.format(dateOpening).toUpperCase();
            finalClosing = timeFormatter.format(dateClosing).toUpperCase();;
        } catch (ParseException e) {
            e.printStackTrace();
        }



        textView_weekday.setText(mDay);
        textView_time.setText(finalOpening + " - " + finalClosing);

        layout_operating_hours.addView(layout_operating_info);
    }

    private void initAPIGetReviewsList(String id, Boolean showReviews) {
        //showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlReviewListing,
                "review_type:"+ "RESTAURANT" + "||" +
                        "reviewee_id:" + id,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | ");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    modelList = new ArrayList<>();
                    modelList.clear();

                    if (status.equals(status_success))
                    {


                        //showToast_short("SUCCESS");

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONObject reviews_data = obj.getJSONObject("data");

                            JSONArray reviews_list = reviews_data.getJSONArray("reviews");

                            String average_rating = "";
                            String review_count = "";


                            try {
                                average_rating = "" + reviews_data.get("average_rating");
                            } catch (Exception e) {
                                average_rating = "" + reviews_data.getDouble("average_rating");
                            }

                            try {
                                review_count = "" + reviews_data.get("review_count");
                            } catch (Exception e) {
                                review_count = "" + reviews_data.getDouble("review_count");
                            }

                            String finalAverage_rating = average_rating;
                            String finalReview_count = review_count;
                            runOnUiThread(() -> {
                                textView_rating_average.setText(finalAverage_rating + " rating");
                                textView_review_count.setText(finalReview_count + " review(s)");
                                reviews_textView_rating_average.setText(finalAverage_rating + " Rating");
                                ratingbar.setRating(Float.parseFloat(finalAverage_rating));

                                //showToast_short("" + Float.parseFloat(finalAverage_rating));

                                if(layout_tab_about.getVisibility() == View.VISIBLE && showReviews == false) {
                                    layout_about.setVisibility(View.VISIBLE);
                                    layout_reviews.setVisibility(View.GONE);
                                    layout_no_reviews.setVisibility(View.GONE);
                                    setTabSelected(0);
                                } else {
                                    setTabSelected(1);
                                    if(Double.parseDouble(finalReview_count) == 0 ){
                                        layout_about.setVisibility(View.GONE);
                                        layout_reviews.setVisibility(View.GONE);
                                        layout_no_reviews.setVisibility(View.VISIBLE);
                                    } else {
                                        layout_about.setVisibility(View.GONE);
                                        layout_reviews.setVisibility(View.VISIBLE);
                                        layout_no_reviews.setVisibility(View.GONE);
                                    }
                                }


                            });

                            //showToast_short("SUCCESS pO: " + reviews_list.length());

//                            runOnUiThread(() -> {
//                                if (announcement_data.length() == 0) {
//                                    textview_message.setVisibility(View.VISIBLE);
//                                } else {
//                                    textview_message.setVisibility(View.GONE);
//                                }
//                            });

                            for(int i = 0; i < reviews_list.length(); i++)
                            {

                                JSONObject object = reviews_list.getJSONObject(i);

                                String customer = (String) object.get("customer");
                                String rating;
                                try {
                                    rating = "" + (String) object.get("rating");
                                } catch (Exception e) {
                                    rating = "" + object.getDouble("rating");
                                }
                                String review_date = (String) object.get("review_date");

                                String review = "";
                                if(object.get("review").toString() == null || object.get("review").toString().equalsIgnoreCase("null") ||  object.get("review").toString().equalsIgnoreCase("")) { } else {
                                    review = (String) object.get("review");
                                }




                                Reviews list = new Reviews(
                                        customer,
                                        rating,
                                        review_date,
                                        review
                                );
                                modelList.add(list);
                            }

                            runOnUiThread(() -> {
                                recyclerAdapter.setModelList(modelList);
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



//                        JsonArray announcement_data = parser.parse(String.valueOf(jsonObj.get("announcement_records"))).getAsJsonArray();
//                        showToast_short("SUCCESS: " + announcement_data.size());



                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }


    private void initPreviousActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "button_ok":
////                showToast_short("To Homescreen");
//                intent = new Intent(getContext(), CreateMPinActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            default:
                break;
        }
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
