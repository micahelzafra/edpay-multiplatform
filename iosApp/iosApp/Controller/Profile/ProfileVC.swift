//
//  ProfileVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 12/27/19.
//

import Foundation
import UIKit
import SwiftyJSON
import app

class ProfileVC: BaseViewController {

    @IBOutlet weak var btnMyAccount: UIButton!
    @IBOutlet weak var btnReferrals: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var imageViewBanner: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initGUI()
    }
    
    func initGUI()
    {
        let userData = self.defaults.string(forKey: self.constant.key_user_data)
        let jsonData = userData!.data(using: String.Encoding.utf8)!
        let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
        let json: JSON = JSON(jsonDic)
        self.log(userData)
        let name = "\(json[self.constant.key_first_name]) \(json[self.constant.key_last_name])"
        lblAccountName.text = name
    }

    @IBAction func didTapButtonMyAccount(_ sender: Any) {
        self.performSegue(withIdentifier: "segueMyAccount", sender: nil)
    }
    
    @IBAction func didTapReferrals(_ sender: Any) {
        self.performSegue(withIdentifier: "segueReferrals", sender: nil)
    }
    
    @IBAction func didTapHelp(_ sender: Any) {
        self.performSegue(withIdentifier: "segueHelp", sender: nil)
    }
    
    @IBAction func didTapLogout(_ sender: Any) {

        let alert = UIAlertController(title: "Log Out", message: self.constant.label_dialog_title_logout, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Log Out", style: .default) { (action:UIAlertAction) in
            print("You've pressed default");
            
            let newStoryBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
                  let VC = newStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                  VC.modalPresentationStyle = .fullScreen
                  self.present(VC, animated: true, completion: {
                    self.defaults.removeObject(forKey: self.constant.key_user_id)
                    self.defaults.removeObject(forKey: self.constant.key_user_data)     
                  })
        }

        let dismissaction = UIAlertAction(title:"Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alert.addAction(dismissaction)
        alert.addAction(action)
        
        self.present(alert, animated: true)
        

    }
}
