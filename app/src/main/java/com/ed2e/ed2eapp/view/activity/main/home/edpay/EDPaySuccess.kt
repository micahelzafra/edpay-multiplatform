package com.ed2e.ed2eapp.view.activity.main.home.edpay


import android.content.Context
import android.content.Intent
import android.hardware.SensorManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.MainActivity
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.goodcodeforfun.seismik.ShakeDetector
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edpay_success.*
import kotlinx.android.synthetic.main.layout_shake_popup.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class EDPaySuccess : BaseActivity(), CoroutineScope, ShakeDetector.Listener {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled: Boolean = false
    private lateinit var shakeDetector: ShakeDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_success)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initGUI()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).getAsJsonObject()
        Timber.d("jsonObj: %s", jsonObj)
        val trade_name = jsonObj[key_trade_name].asString
        val currency_code = jsonObj[key_currency_code].asString
        //val paid_amount = jsonObj[key_paid_amount].asString
        val paid_amount = jsonObj["sub_total_with_discount"].asString
        val payment_method = jsonObj[key_payment_method].asString
        val transaction_no = jsonObj[key_transaction_no].asString
        val transaction_date = jsonObj[key_transaction_date].asString
        val currency_amount = currency_code + " " + commaSeparated(paid_amount).asString()

        var shop_facade = ""
        if(jsonObj[key_shop_facade] == null || jsonObj[key_shop_facade].toString() == "" || jsonObj[key_shop_facade].toString() == "{}" || jsonObj[key_shop_facade].toString() == "null") {
            Glide.with(this).load(R.drawable.ic_default_merchant).into(edpay_success_imageview_merchant_logo)
        } else {
            shop_facade = jsonObj[key_shop_facade].asString
            Glide.with(this).load(shop_facade).into(edpay_success_imageview_merchant_logo)
        }

        edpay_success_textview_merchant_name.text = trade_name
        edpay_success_textview_amount.text = currency_amount
        edpay_success_textview_payment_method.text = payment_method
        edpay_success_textview_transaction_number.text = transaction_no
        edpay_success_textview_date.text = transaction_date
//        Glide.with(this).load(shop_facade).into(edpay_success_imageview_merchant_logo)

        Glide.with(this)
            .load(R.drawable.ic_gif_1)
            .into(edpay_pin_imageview_merchant_logo)


        edpay_success_button_done.setOnClickListener {
            edpay_success_layout_shake_popup.visibility = VISIBLE
            val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
            shakeDetector.start(sensorManager)
        }

        edpay_success_button_shake_later.setOnClickListener {
//            val intent = Intent(
//                this@EDPaySuccess,
//                MainActivity::class.java
//            )
//            startActivity(intent)
            finish()
        }

        shakeDetector = ShakeDetector(this)
        shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_LIGHT)
//        findViewById<RadioGroup>(R.id.radioGroupSensitivity).setOnCheckedChangeListener({ _, i ->
//            when (i) {
//                R.id.radioButtonLight -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_LIGHT)
//                R.id.radioButtonMedium -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_MEDIUM)
//                R.id.radioButtonHard -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_HARD)
//                R.id.radioButtonHarder -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_HARDER)
//                else -> throw IllegalArgumentException()
//            }
//        })
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hearShake() {
        Timber.d("hearShake")

        shakeDetector.stop()

        val intent = Intent(
            this@EDPaySuccess,
            EDPayShakeReward::class.java
        )
        intent.putExtra(key_amount, edpay_success_textview_transaction_number.text)
        intent.putExtra(key_payment_method, edpay_success_textview_payment_method.text)
        startActivity(intent)
        finish()
    }

    override fun onStop() {
        shakeDetector.stop()
        super.onStop()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {

                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
