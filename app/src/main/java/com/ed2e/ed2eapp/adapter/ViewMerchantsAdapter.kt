package com.ed2e.ed2eapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.model.Announcement
import com.ed2e.ed2eapp.view.EDPayActivity


class ViewMerchantsAdapter(supportFragmentManager: FragmentManager) : FragmentStatePagerAdapter(supportFragmentManager) {


    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

//    private val tabTitles = arrayOf("Tab1", "Tab2")
//    private val imageResId = intArrayOf(R.drawable.bg_button_blue, R.drawable.bg_button_red_solid)


    override fun getItem(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

//    fun getTabView(position: Int): View? {
//        val v: View = LayoutInflater.from(activity).inflate(R.layout.bg_tab_selected, null)
//        val tv: TextView = v.findViewById(R.id.layout_custom_tab_textView_title) as TextView
//        tv.setText(tabTitles.get(position))
////        val img: ImageView = v.findViewById(R.id.layout_custom_tab_textView_title) as ImageView
////        img.setImageResource(imageResId.get(position))
//        return v
//    }
}