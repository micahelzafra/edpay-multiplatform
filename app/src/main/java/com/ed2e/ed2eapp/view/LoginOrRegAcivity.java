package com.ed2e.ed2eapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayShakeReward;
import com.ed2e.ed2eapp.view.activity.onboarding.OnboardingActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.KEY_FIRST_TIME_LAUNCH;
import static com.ed2e.ed2eapp.util.ConstantKt.key_current_location;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_name;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_latitude;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_longitude;

public class LoginOrRegAcivity extends BaseActivity {

    @BindView(R.id.login_or_reg_button_login)
    Button button_login;
    @BindView(R.id.login_or_reg_button_reg)
    Button button_reg;

    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_reg);
        ButterKnife.bind(this);

        preference = getInstance(this);

        initCurrentLocation();
//        initGUI();
    }

    private void initGUI() {

        button_login.setOnClickListener(v -> {
            Intent intent=new Intent(LoginOrRegAcivity.this, Login1Activity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
        });

        button_reg.setOnClickListener(v -> {
            Intent intent=new Intent(LoginOrRegAcivity.this, RegistrationAcivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
        });
    }


//    @OnClick(R.id.login_or_reg_button_login) void login() {
//
//        Intent intent=new Intent(LoginOrRegAcivity.this, EDPayShakeReward.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }
//
//    @OnClick(R.id.login_or_reg_button_reg) void register() {
//
//        Intent intent=new Intent(LoginOrRegAcivity.this, RegistrationAcivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }


//
//    @OnClick(R.id.login_or_reg_button_reg) void submit() {
//
//        Intent intent=new Intent(LoginOrRegAcivity.this, RegistrationAcivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }

    private void initCurrentLocation() {
        getCurrentLocation(LoginOrRegAcivity.this);
        //Log.d("LOCATION", "Response Location: " + preference.getString(key_current_location, ""));

//        if(getCurrentLocationData(tag_current_location_country_code, LoginOrRegAcivity.this) == null || getCurrentLocationData(tag_current_location_country_code, LoginOrRegAcivity.this).equalsIgnoreCase("")) {
//            showDialog_main("", "tag_request_button_ok", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
//        } else {
//
//            if (getCurrentLocationData(tag_current_location_country_code, LoginOrRegAcivity.this).equalsIgnoreCase("NG")) {
//                initGUI();
//            } else {
//                showDialog_main("", "tag_request_button_ok", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
//            }
//
//        }

        //showToast_short("Location: " + preference.getString(key_current_location, ""));

        String mCountryCode = "";


        if(preference.getString(key_current_location, "") == null || preference.getString(key_current_location, "").equalsIgnoreCase("")){
            showDialog_main("", "tag_request_button_ok", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
        } else {

            Log.d("getCurrentLocationData", "Current Location Data: (Pref) " + preference.getString(key_current_location, ""));

            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(preference.getString(key_current_location, "")).getAsJsonObject();
            mCountryCode = jsonObj.get("country_code").getAsString();

            if (mCountryCode.equalsIgnoreCase("NG")) {
                initGUI();
            } else {
                showDialog_main("", "tag_request_button_ok", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
            }
        }
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "tag_button_exit_app":
                finishAffinity();
                break;
            case "tag_request_button_ok":
                finishAffinity();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                showDialog_main("", "tag_button_exit_app", true, "",  getResources().getString(R.string.dialog_exit_app_message), "No", "Yes");
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
