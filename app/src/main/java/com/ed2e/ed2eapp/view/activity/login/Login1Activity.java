package com.ed2e.ed2eapp.view.activity.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginActivity;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.PinCreateAcivity;
import com.ed2e.ed2eapp.view.activity.help.HelpActivity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_last_logged_in_mobile_num;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.request_error_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlCheckMobileNum;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;

public class Login1Activity extends BaseActivity {

    public static final String TAG = Login1Activity.class.getSimpleName();

    @BindView(R.id.login_1_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.login_1_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.login_1_editText_mobile_number_plus)
    EditText editText_mobile_number_plus;
    @BindView(R.id.login_1_editText_mobile_number_prefix)
    EditText editText_mobile_number_prefix;
    @BindView(R.id.login_1_editText_mobile_number)
    EditText editText_mobile_number;
    @BindView(R.id.login_1_button_next)
    Button button_next;
    @BindView(R.id.login_1_textView_need_help)
    TextView textView_need_help;


    private String mobileNumber = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_revised);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initCurrentLocation();
        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mobileNumber = "";
        } else {
            mobileNumber = extras.getString(key_mobile_number);
        }
    }

    private void initGUI() {

        //editText_mobile_number_plus.setVisibility(View.GONE);
        editText_mobile_number_plus.setVisibility(View.VISIBLE);

        if(preference.getString(key_last_logged_in_mobile_num, "") == null || preference.getString(key_last_logged_in_mobile_num, "").equalsIgnoreCase("")) {
            editText_mobile_number.setText(mobileNumber.replace("234", "").replaceAll("\\+", ""));
        } else {
             editText_mobile_number.setText(preference.getString(key_last_logged_in_mobile_num, "").replace("234", "").replaceAll("\\+", ""));

            if(mobileNumber == null || mobileNumber.equalsIgnoreCase("")) { } else {
                editText_mobile_number.setText(mobileNumber.replace("234", "").replaceAll("\\+", ""));
            }
        }


        if(editText_mobile_number.getText().toString().trim().equalsIgnoreCase("")){
            //editText_mobile_number_plus.setVisibility(View.GONE);
            editText_mobile_number_plus.setVisibility(View.VISIBLE);
        } else {
            editText_mobile_number_plus.setVisibility(View.VISIBLE);
        }

        editText_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editText_mobile_number.getText().toString().trim().equalsIgnoreCase("")){
                    //editText_mobile_number_plus.setVisibility(View.GONE);
                    editText_mobile_number_plus.setVisibility(View.VISIBLE);
                } else {
                    editText_mobile_number_plus.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        button_next.setOnClickListener(v -> {

            if(editText_mobile_number.getText().toString().trim().equalsIgnoreCase("")){
                editText_mobile_number.setError(getResources().getString(R.string.error_no_mobile_num));
                return;
            }
//            showToast_short("Hey");

            Pattern pattern;
            Matcher matcher;

            final String PASSWORD_PATTERN_NUMBER = "^[0-9]+$";
            char[] stringToCharArray = editText_mobile_number.getText().toString().trim().toCharArray();
            boolean isValidMobile = true;

            for (int i = 0; i < stringToCharArray.length; i++) {

                Log.d("MOBILENUM", "- " + stringToCharArray[i]);

                //number checker
                pattern = Pattern.compile(PASSWORD_PATTERN_NUMBER);
                matcher = pattern.matcher(String.valueOf(stringToCharArray[i]));


                    if (matcher.matches()) {
                        isValidMobile = true;
                    } else {
                        isValidMobile = false;
                        break;
                    }

            }



            if(isValidMobile){
                initAPICheckExistingMobileNum("+234" + editText_mobile_number.getText().toString().trim());
//                Intent intent=new Intent(Login1Activity.this, LoginMPinActivity.class);
//                intent.putExtra(key_mobile_number, "+" + editText_mobile_number.getText().toString().trim());
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                showDialog_APIError("", "", true, "ED72", "", "OK");
            }



            //showDialog_main("button_cancel", "button_register", false,"Mobile number is not registered", "Please register your mobile number.", "CANCEL", "Register");
        });


        textView_need_help.setOnClickListener(v -> {
            Intent intent=new Intent(Login1Activity.this, HelpActivity.class);
            intent.putExtra(key_prev_page, "login");
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

    private void initAPICheckExistingMobileNum(String mobileNumber) {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlCheckMobileNum,
                "mobile_number:"+ mobileNumber,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + mobileNumber + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    if (status.equals(status_success))
                    {
                        JsonObject data = jsonObj.get(key_data).getAsJsonObject();
                        String user_id = data.get("customer_id").getAsString();
                        String pin_verified = data.get("has_pin").getAsString();
                        String otp_verified = data.get("otp_verified").getAsString();
                        preference.putString(key_user_id, user_id);

                        if (otp_verified.equals("0"))
                        {
                            Intent intent=new Intent(Login1Activity.this, OTPActivity.class);
                            intent.putExtra(key_mobile_number, mobileNumber);
                            intent.putExtra(key_cust_id, user_id);
                            intent.putExtra(key_prev_page, "login");
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        if (pin_verified.equals("0"))
                        {
                            Intent intent=new Intent(Login1Activity.this, CreateMPinActivity.class);
                            intent.putExtra(key_cust_id, user_id);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        Intent intent=new Intent(Login1Activity.this, LoginMPinActivity.class);
                        intent.putExtra(key_mobile_number, mobileNumber);
                        intent.putExtra(key_cust_id, user_id);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();

                        if(error_code.equalsIgnoreCase("ED73")) {
                            showDialog_APIError("", "button_register", true, error_code, "CANCEL", "Register");
                        } else {
                            showDialog_APIError("", "", true, error_code, "", "OK");
                        }
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }




//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }




    private void initCurrentLocation() {
        getCurrentLocation(getContext());
        //Log.d("LOCATION", "Response Location: " + preference.getString(key_current_location, ""));
    }

    private void initPreviousActivity() {
        Intent intent = new Intent(getContext(), LoginOrRegAcivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "button_cancel":
                //showProgress();
                break;
            case "button_register":
                Intent intent = new Intent(Login1Activity.this, RegistrationAcivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
