//
//  LoginOrRegistrationVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/17/20.
//

import UIKit

class LoginOrRegistrationVC: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: DesignableButton!
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
    
    @IBAction func didTapButtonLogin(_ sender: Any) {
        self.performSegue(withIdentifier: "segueLogin", sender: nil)
    }
    
    @IBAction func didTapButtonRegister(_ sender: Any) {
        self.performSegue(withIdentifier: "segueRegister", sender: nil)
    }
    
}
