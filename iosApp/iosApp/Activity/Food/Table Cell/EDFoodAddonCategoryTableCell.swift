//
//  EDFoodAddonTableCell.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-zafyraminx on 2/2/20.
//

import UIKit

class EDFoodAddonCategoryTableCell: UITableViewCell {
    
    @IBOutlet weak var lblAddonCategory: UILabel!
    @IBOutlet weak var tableViewAddOns: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
