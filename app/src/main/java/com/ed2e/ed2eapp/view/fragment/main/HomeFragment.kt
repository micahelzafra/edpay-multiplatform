package com.ed2e.ed2eapp.view.fragment.main

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.R.layout.fragment_home
import com.ed2e.ed2eapp.util.*
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber


class HomeFragment : Fragment(fragment_home)
{
    lateinit var button_pay: Button
    lateinit var button_rewards: Button
    lateinit var button_top_up: Button
    lateinit var button_transfer: Button
    lateinit var textview_credits: TextView
    lateinit var button_edfood: LinearLayout
    lateinit var textview_see_all: TextView
    lateinit var imageview_flag: ImageView

    fun getActionbar() : ActionBar?
    {
        return (activity as AppCompatActivity).supportActionBar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {


        button_pay = view.findViewById(R.id.fragment_home_button_pay)
        button_rewards = view.findViewById(R.id.fragment_home_button_reward)
        button_top_up = view.findViewById(R.id.fragment_home_button_top_up)
        button_transfer = view.findViewById(R.id.fragment_home_button_transfer)
        button_edfood = view.findViewById(R.id.fragment_home_layout_edfood)
        textview_see_all = view.findViewById(R.id.fragment_home_textView_see_all)
            imageview_flag = view.findViewById(R.id.fragment_home_imageview_flag)

            button_pay.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_edpay_activity))
            }
            button_rewards.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_reward_activity))
            }
            button_top_up.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_topup_activity))
            }
            button_transfer.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_transfer_activity))
            }
            button_edfood.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_edfood_activity))
            }
            textview_see_all.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_intent_announcements_activity))
            }

            RxBus.publish(RxEvent.EventMainActivity(tag_load_fragment_home,this))
        }
    }
}