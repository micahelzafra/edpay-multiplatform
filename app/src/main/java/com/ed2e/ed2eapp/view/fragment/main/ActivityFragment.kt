package com.ed2e.ed2eapp.view.fragment.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.ed2e.ed2eapp.R.layout.fragment_activity
import com.ed2e.ed2eapp.util.tag_load_fragment_activity
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber


class ActivityFragment : Fragment(fragment_activity)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {
            RxBus.publish(RxEvent.EventMainActivity(tag_load_fragment_activity, this))
        }
    }
}