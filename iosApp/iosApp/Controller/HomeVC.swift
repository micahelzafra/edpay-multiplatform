//
//  HomeVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 12/27/19.
//

import Foundation
import UIKit
import SwiftyJSON
import app
import AlamofireImage
import SVProgressHUD

private let announcementIdentifier  = "announcementCollectionViewCell"

class HomeVC: BaseViewController, EventsDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
  
    internal var apiInterface = ApiInterface()

    @IBOutlet weak var cvAnnouncement: UICollectionView!
    @IBOutlet weak var imageViewPay: UIImageView!
    @IBOutlet weak var imageViewRewards: UIImageView!
    @IBOutlet weak var imageViewTopUp: UIImageView!
    @IBOutlet weak var imageViewTransfer: UIImageView!
    @IBOutlet weak var labelWallet: UILabel!
    @IBOutlet weak var viewEDFood: UIView!
    
    @IBOutlet weak var btnOrderNow: UIButton!
    @IBOutlet weak var lblSeeAll: UILabel!
   
    @IBOutlet weak var imageViewCountry: UIImageView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var lblNoAnnouncement: UILabel!
    
    
    let downloader = ImageDownloader()
    var announcementList = [JSON]()
    //Location
    var isParsingLocation = false
    
    var wallet = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventDelegate = self
        self.getLocation()
       
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageViewPay.isUserInteractionEnabled = true
        imageViewPay.addGestureRecognizer(tapGestureRecognizer)
       
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.delaysTouchesBegan = true
        imageViewRewards.isUserInteractionEnabled = true
        imageViewRewards.addGestureRecognizer(tapGestureRecognizer)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.delaysTouchesBegan = true
        imageViewTopUp.isUserInteractionEnabled = true
        imageViewTopUp.addGestureRecognizer(tapGestureRecognizer)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.delaysTouchesBegan = true
        imageViewTransfer.isUserInteractionEnabled = true
        imageViewTransfer.addGestureRecognizer(tapGestureRecognizer)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.delaysTouchesBegan = true
        viewEDFood.isUserInteractionEnabled = true
        viewEDFood.addGestureRecognizer(tapGestureRecognizer)
        
        //collection view
        cvAnnouncement.register(UINib(nibName: "AnnouncementCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: announcementIdentifier)
        
        //label actions
        let tapSeeAll = UITapGestureRecognizer(target: self, action: #selector(seeAll))
        lblSeeAll.isUserInteractionEnabled = true
        lblSeeAll.addGestureRecognizer(tapSeeAll)
        
        //underline label
        lblSeeAll.attributedText = NSAttributedString(string: "See All", attributes:
        [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        
        getAnnouncementList()
    }

    override func viewWillAppear(_ animated: Bool) {
        isParsingLocation = false
        
    }
    
    
    @objc func seeAll(sender:UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "segueAnnouncementDetails", sender: self)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if let tapView = tapGestureRecognizer.view
        {
            if tapGestureRecognizer.view is UIImageView {
                if let tapViewImage = tapGestureRecognizer.view as! UIImageView?
                {
                    if tapViewImage == imageViewPay {
                        performSegue(withIdentifier: "seguePay", sender: nil)
                    }
                    else if tapViewImage == imageViewRewards
                    {
                       print ("Rewards")
                    }
                    else if tapViewImage == imageViewTopUp
                    {
                        //handleError(displaytitle: "Temporary Unavailable", "Sorry. Transfer feature is temporarily unavailable but will be up and running soon. Stay tuned!")
                    }
                    else if tapViewImage == imageViewTransfer
                    {
                       handleError(displaytitle: "Temporary Unavailable", "Sorry. Transfer feature is temporarily unavailable but will be up and running soon. Stay tuned!")
                    }
                }
            }
            else
            {
                if tapView == viewEDFood {
                    performSegue(withIdentifier: "segueFood", sender: nil)
                }
            }
            
        }
    }
    
    @IBAction func didTapButtonOrder(_ sender: Any) {
    }
    
    
    @IBAction func buttonShowWallet(_ sender: UIButton) {
        if sender.isSelected
        {
            self.labelWallet.text = "******"
        }
        else
        {
            self.labelWallet.text = wallet
        }
        sender.isSelected = !sender.isSelected
    }
    
    func eventData(event: Int, data: AnyObject) {
        if event == CommonConstants.tag_load_current_location
        {
            let location = data as! Dictionary<String, Double>
//            self.log("location \(location[self.constant.key_latitude] ?? 0.00), \(location[self.constant.key_longitude] ?? 0.00)")
            if !isParsingLocation
            {
                getLocationAddress(longitude: "\(location[self.constant.key_longitude] ?? 0.00)", latitude: "\(location[self.constant.key_latitude] ?? 0.00)")
            }
        }
    }

    func getLocationAddress(longitude: String, latitude: String)
    {
        showLoading()
        isParsingLocation = true
        var sb = "https://maps.googleapis.com/maps/api/geocode/json"
        sb.append("?key=\("api_key".localized())")
        sb.append("&address=\(latitude),\(longitude)")
        var country_code = ""
        var country_name = ""
        
        apiInterface.parseGoogle(url: sb,
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    if let jsonArray = json[self.constant.key_results].array {
                        outerLoop: for jinx in jsonArray
                        {
                            if let address_components = jinx[self.constant.key_address_components].array
                            {
                                for component in address_components
                                {
                                    if let country = component[self.constant.key_types].array
                                    {
                                        if "\(country)".contains(self.constant.key_country)
                                        {
                                            country_code = component[self.constant.key_short_name].string!
                                            country_name = component[self.constant.key_long_name].string!
                                            self.log("country_name \(country_name), country_code \(country_code)")
                                            self.defaults.set(country_code, forKey: self.constant.key_country_code)
                                            self.defaults.set(country_name, forKey: self.constant.key_country)
                                            self.checkCurrentLocation()
                                            break outerLoop
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        self.log("jsonArray null")
                    }
                }
                catch let error
                {
                    print(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }

    func checkCurrentLocation()
    {
        
        if self.defaults.string(forKey: self.constant.key_country_code) != "NG" //must change to actual checking
        {
            self.handleError(displaytitle: "Country Unavailable", "Unfortunately, ED2E App is currently unavailable in your area.")
            imageViewCountry.image = UIImage(named: "ic_flag_NG")!
        } else {
            imageViewCountry.image = UIImage(named: "ic_flag_PH")!
        }
        self.getWallet(customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
                       country_name: self.defaults.string(forKey: self.constant.key_country)!)
        
        hideLoading()
    }
    
    private func getWallet(
        customer_id: String,
        country_name: String
    ) {
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlWallet,
            params: "customer_id:\(customer_id)||country_name:\(country_name)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    
                    let wallet = json[self.constant.key_edpoints].string
                    let currency = json[self.constant.key_currency_code].string

                    let value = "\(currency ?? "") \(commaSeparated(value: (wallet?.replacingOccurrences(of:",", with:""))!).asString())"
                    self.wallet = value
//                    self.labelWallet.text = wallet
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }

                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
        
    }
    
    private func getAnnouncementList() {
        showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlAnnouncementList,
            params: "",
            success: { data in
                    let jsonData = data.data(using: String.Encoding.utf8)!
                    let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                                              
                    let status = json[self.constant.key_status].string
                        if status == self.constant.status_success {
                            let jsonArray = json["announcement_records"].array
                                            
                            for data in jsonArray! {
                                self.announcementList.append(data)
                            }
                            if self.announcementList.count == 0 {
                                self.lblNoAnnouncement.isHidden = false
                            } else {
                                self.lblNoAnnouncement.isHidden = true
                            }
                            self.cvAnnouncement.reloadData()
                        }
                        else
                        {
                            let error = json[self.constant.key_code].string
                                self.handleError(error)
                        }
                self.hideLoading()
                return KotlinUnit()
            
        }, failure: {
                   self.handleError($0?.message)
                   return KotlinUnit()
               })
    }
    
    
    //MARK : Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return announcementList.count
       }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let collectionViewSize = collectionView.frame.size
        return CGSize(width: collectionViewSize.width / 3, height: 225)

        //return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewController = storyBoard.instantiateViewController(withIdentifier: "productServicesListIdentifier") as! ProductServicesListViewController
//            newViewController.categoryId = arrServices[indexPath.item].id!
//            self.navigationController?.pushViewController(newViewController, animated: true)
        
        //Segue to Announcement List
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = cvAnnouncement.dequeueReusableCell(withReuseIdentifier: announcementIdentifier, for: indexPath) as! AnnouncementCollectionViewCell
        
        let announcement = announcementList[indexPath.row]
        if let imageurl = announcement["image"].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
                downloader.download(urlRequest, filter: nil) { response in
                    if let image = response.result.value {
                            print(image)
                            cell.imageViewAnnouncement.image = image
                    }
                }
        }
                
        cell.lblAnnouncementName.text = announcement["title"].string
        
        return cell
    }

       
}
