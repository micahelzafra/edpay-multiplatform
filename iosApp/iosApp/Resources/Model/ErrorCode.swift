//
//  ErrorCode.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 1/23/20.
//
import Foundation

class ErrorCode :NSObject, NSCoding {
    var error_code:String = ""
    var error_title:String = ""
    var error_message:String = ""

    init(error_code: String = "", error_title: String = "", error_message: String = "") {
        self.error_code = error_code
        self.error_title = error_title
        self.error_message = error_message

    }

    required convenience init(coder aDecoder: NSCoder) {
        let error_code = aDecoder.decodeObject(forKey: "error_code") as! String
        let error_title = aDecoder.decodeObject(forKey: "error_title") as! String
        let error_message = aDecoder.decodeObject(forKey: "error_message") as! String
        self.init(error_code: error_code, error_title: error_title, error_message: error_message)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(error_code, forKey: "error_code")
        aCoder.encode(error_title, forKey: "error_title")
        aCoder.encode(error_message, forKey: "error_message")
    }
}
