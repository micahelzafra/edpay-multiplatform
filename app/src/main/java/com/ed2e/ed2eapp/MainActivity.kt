package com.ed2e.ed2eapp

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.View.*
import android.widget.Button
import android.widget.DatePicker
import androidx.annotation.IdRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ed2e.ed2eapp.adapter.AnnouncementListHomeAdapter
import com.ed2e.ed2eapp.adapter.MainPagerAdapter
import com.ed2e.ed2eapp.adapter.ViewPagerAdapter
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.model.Announcement
import com.ed2e.ed2eapp.model.MainScreen
import com.ed2e.ed2eapp.model.getMainScreenForMenuItem
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.EDPayActivity
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsActivity
import com.ed2e.ed2eapp.view.activity.help.HelpActivity
import com.ed2e.ed2eapp.view.activity.login.Login1Activity
import com.ed2e.ed2eapp.view.activity.main.MapActivity
import com.ed2e.ed2eapp.view.activity.main.home.edfood.EDFoodLogDetailsAcivity
import com.ed2e.ed2eapp.view.activity.main.home.edfood.EDFoodMerchantsActivity
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayLogDetailsActivity
import com.ed2e.ed2eapp.view.activity.main.profile.MyAccountActivity
import com.ed2e.ed2eapp.view.activity.referrals.ReferralsActivity
import com.ed2e.ed2eapp.view.activity.shakerewards.ShakeRewardsLogsActivity
import com.ed2e.ed2eapp.view.fragment.main.ActivityFragment
import com.ed2e.ed2eapp.view.fragment.main.HomeFragment
import com.ed2e.ed2eapp.view.fragment.main.ProfileFragment
import com.ed2e.ed2eapp.view.fragment.main.activity.FoodActivityFragment
import com.ed2e.ed2eapp.view.fragment.main.activity.PayActivityFragment
import com.ed2e.ed2eapp.view.fragment.main.activity.TopUpActivityFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_activity.*
import kotlinx.android.synthetic.main.fragment_activity_logs.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class MainActivity : BaseActivity(), CoroutineScope , BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference
    private lateinit var viewPager: ViewPager
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var mainPagerAdapter: MainPagerAdapter

    override val coroutineContext: CoroutineContext
        get() = job + Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        job = Job()
        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)
        initGUI()
        initListener()
    }

    private fun initGUI() {
        // Initialize components/views.
        viewPager = findViewById(R.id.main_view_pager)
        bottomNavigationView = findViewById(R.id.main_bottom_navigation_view)
        mainPagerAdapter = MainPagerAdapter(supportFragmentManager)

        // Set items to be displayed.
        mainPagerAdapter.setItems(arrayListOf(MainScreen.HOME, MainScreen.ACTIVITY, MainScreen.PROFILE))

        // Show the default screen.
        val defaultScreen = MainScreen.HOME
        scrollToScreen(defaultScreen)
        selectBottomNavigationViewMenuItem(defaultScreen.menuItemId)
        supportActionBar?.setTitle(defaultScreen.titleStringId)

        // Set the listener for item selection in the bottom navigation view.
        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        // Attach an adapter to the view pager and make it select the bottom navigation
        // menu item and change the title to proper values when selected.
        viewPager.adapter = mainPagerAdapter
//        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
//            override fun onPageSelected(position: Int) {
//                val selectedScreen = mainPagerAdapter.getItems()[position]
//                selectBottomNavigationViewMenuItem(selectedScreen.menuItemId)
//                supportActionBar?.setTitle(selectedScreen.titleStringId)
//
//                when (position) {
//                    0 -> {
//                        initHomePage()
//                    }
//                    1 -> {
//                        initAcivityPage()
//                    }
//                    else -> {
//                        initProfilePage()
//                    }
//                }
//            }
//        })

//        initHomePage()
    }

    private fun initHomePage(value: Any?)
    {
        getCurrentLocation(this@MainActivity)
        if (getCurrentLocationData(tag_current_location_country_code, this@MainActivity).equals(
                "NG",
                ignoreCase = true
            )
        ) {

        } else {
            showDialog_main(
                "",
                "tag_main_button_ok",
                true,
                getResources().getString(R.string.dialog_app_unavailable_title),
                getResources().getString(R.string.dialog_app_unavailable_message),
                "",
                "OK")
        }


        val fragment: HomeFragment = value as HomeFragment
        if(getCurrentLocationData(tag_current_location_country_code, this@MainActivity) == null || getCurrentLocationData(tag_current_location_country_code, this@MainActivity) == "") {
            fragment.fragment_home_textview_edcredits.text = "EDCredits"
        } else {
            fragment.fragment_home_textview_edcredits.text = "EDCredits (" + getCurrentLocationData(tag_current_location_country_code, this@MainActivity) + ")"
        }

        initAPIGetAnnouncementsList(value)
        getWallet(preference.getString(key_user_id), getCurrentLocationData(tag_current_location_country_name, this@MainActivity))
        initGetCountryFlags(value)
        initEDCreditsVisibility(value)
    }

    private fun initEDCreditsVisibility(value:Any?){
        val fragment: HomeFragment = value as HomeFragment

        fragment.fragment_home_imageview_eye.setOnClickListener {
            if (fragment.fragment_home_textview_credits.getVisibility() == View.VISIBLE) {
                fragment.fragment_home_textview_credits.visibility = GONE
                fragment.fragment_home_textview_stars.visibility = VISIBLE
                fragment.fragment_home_imageview_eye.setImageResource(R.drawable.ic_eye_hide)
            } else {
                fragment.fragment_home_textview_credits.visibility = VISIBLE
                fragment.fragment_home_textview_stars.visibility = GONE
                fragment.fragment_home_imageview_eye.setImageResource(R.drawable.ic_eye_show)
            }
        }

    }

    private fun initGetCountryFlags(value:Any?)
    {
        val fragment: HomeFragment = value as HomeFragment

            //showToast_short("Code: " + getCurrentLocationData(tag_current_location_country_code))
            try {
                when (getCurrentLocationData(tag_current_location_country_code, this@MainActivity)) {
                    "PH" -> Glide.with(context)
                        .load(R.drawable.philippines)
                        .apply(
                            RequestOptions().override(
                                256,
                                256
                            ).centerCrop().placeholder(R.drawable.ic_flag_default)
                        )
                        .into(
                            fragment.fragment_home_imageview_flag
                        )
                    "NG" -> Glide.with(context)
                        .load(R.drawable.nigeria)
                        .apply(
                            RequestOptions().override(
                                256,
                                256
                            ).centerCrop().placeholder(R.drawable.ic_flag_default)
                        )
                        .into(
                            fragment.fragment_home_imageview_flag
                        )
                    "MY" -> Glide.with(context)
                        .load(R.drawable.malaysia)
                        .apply(
                            RequestOptions().override(
                                256,
                                256
                            ).centerCrop().placeholder(R.drawable.ic_flag_default)
                        )
                        .into(
                            fragment.fragment_home_imageview_flag
                        )
                    else -> Glide.with(context)
                        .load(R.drawable.ic_flag_default)
                        .apply(
                            RequestOptions().override(
                                256,
                                256
                            ).centerCrop().placeholder(R.drawable.ic_flag_default)
                        )
                        .into(
                            fragment.fragment_home_imageview_flag
                        )
                }
            } catch (e: Exception) {
                Timber.e(e)
            }

    }

    private fun initAcivityPage(value: Any?)
    {
        val fragment: ActivityFragment = value as ActivityFragment
        getCurrentLocation(this@MainActivity)
//        val pager = MainScreen.ACTIVITY.fragment.fragment_activity_view_pager
        val pager = fragment.fragment_activity_view_pager
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PayActivityFragment(), getString(R.string.acivity_nav_pay))
        adapter.addFragment(FoodActivityFragment(), getString(R.string.acivity_nav_food))
        adapter.addFragment(TopUpActivityFragment(), getString(R.string.acivity_nav_topup))
        pager.adapter = adapter
//        MainScreen.ACTIVITY.fragment.fragment_activity_tablayout_view.setupWithViewPager(pager)
        fragment.fragment_activity_tablayout_view.setupWithViewPager(pager)
    }

    private fun initProfilePage(value: Any?)
    {

        val fragment: ProfileFragment = value as ProfileFragment
        getCurrentLocation(this@MainActivity)

//        val data = preference.getString(key_user_data)
//        //showToast_long(data)
//        val parser = JsonParser()
//        val jsonObj: JsonObject = parser.parse(data).getAsJsonObject()
//
//        val first_name = jsonObj[key_first_name].asString
//        val last_name = jsonObj[key_last_name].asString

        val first_name = preference.getString(key_first_name)
        val last_name = preference.getString(key_last_name)

        val name = first_name + " " + last_name
        try {
            MainScreen.PROFILE.fragment.fragment_profile_textview_name.text = name
            //fragment.fragment_profile_textview_name.text = name
        }
        catch (e:Exception)
        {
            Timber.e(e)
        }

        initAPIGetAccountDetails(fragment)
    }

    private fun initAPIGetAccountDetails(fragment: ProfileFragment) {
        showProgress()

        val data = preference.getString(key_user_data)
        //showToast_long(data)
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject

        val id = jsonObj["id"].asString

        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlGetAccountDetails,
            "customer_id:$id",
            { response: String ->
                hideProgress()
                Log.d("MainActivity", "DATA: | $id |")
                Log.d("MainActivity", "RESPONSE: $response")
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {

                    val data = jsonObj[key_data].toString()
                    Log.d("HELP","SUCCESS ACCOUNT: " + data)

                    val dataObj = parser.parse(data).asJsonObject

                    val first_name = dataObj[key_first_name].asString
                    val last_name = dataObj[key_last_name].asString
                    val name = first_name + " " + last_name
                    try {
                        fragment.fragment_profile_textview_name.text = name
                    }
                    catch (e:Exception)
                    {
                        Timber.e(e)
                    }

                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialog_APIError("", "", true, error_code, "", "OK")
                    //                        showDialog_APIError("", "tag_edpay_shake_reward_error", false, error_code, "", "OK");
                }
                null
            }
        ) { failure: Throwable? ->
            hideProgress()
            Log.d("MainActivity", "RESPONSE (ERROR): $failure")
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )
            null
        }
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag:Int,value:Any?)
    {
        when (tag) {
            tag_load_fragment_home -> {
                initHomePage(value)
            }
            tag_load_fragment_activity -> {
                initAcivityPage(value)
            }
            tag_load_fragment_profile -> {
                initProfilePage(value)
            }
            tag_intent_edpay_activity -> {

                if (getCurrentLocationData(tag_current_location_country_code, this@MainActivity).equals(
                        "NG",
                        ignoreCase = true
                    )
                ) {
                    val intent : Intent
                    intent = Intent(context, EDPayActivity::class.java)
                    startActivity(intent)
                } else {
                    showDialog_main(
                        "",
                        "tag_edpay_button_ok",
                        true,
                        getResources().getString(R.string.dialog_app_unavailable_title),
                        getResources().getString(R.string.dialog_app_unavailable_message),
                        "",
                        "OK"
                    )
                }



            }
            tag_intent_reward_activity -> {
                val intent : Intent
                intent = Intent(context, ShakeRewardsLogsActivity::class.java)
                startActivity(intent)
            }
            tag_intent_topup_activity -> {
                if (getCurrentLocationData(tag_current_location_country_code, this@MainActivity).equals(
                        "NG",
                        ignoreCase = true
                    )
                ) {
                    showDialog_main(
                        "",
                        "tag_topup_button_ok",
                        true,
                        "Temporarily Unavailable",
                        "Don\'t worry. Top Up is coming soon to your country",
                        "",
                        "OK"
                    )
                } else {
                    showDialog_main(
                        "",
                        "tag_topup_button_ok",
                        true,
                        getResources().getString(R.string.dialog_app_unavailable_title),
                        getResources().getString(R.string.dialog_app_unavailable_message),
                        "",
                        "OK"
                    )
                }


            }
            tag_intent_transfer_activity -> {
                showDialog_main("", "tag_transfer_button_ok",  true,"Temporarily Unavailable", "Sorry, Transfer feature is temporarily unavailable but will be up and running soon. Stay tuned!", "", "OK")
            }
            tag_intent_login_activity -> {
                showDialog_main("", "tag_log_out_button_ok", true, "Log Out", label_dialog_title_logout, "Cancel", "Log Out")

//                AlertDialog.Builder(this)
//    //                .setTitle(finalTitle)
//                    .setMessage(label_dialog_title_logout)
//                    .setPositiveButton(button_yes
//                    ) { dialogInterface: DialogInterface, i: Int ->
//                        dialogInterface.dismiss()
//                        val intent : Intent
//                        intent = Intent(context, Login1Activity::class.java)
//                        startActivity(intent)
//                        finish()
//                    }
//                    .setNegativeButton(button_no
//                    ) { dialogInterface: DialogInterface, i: Int ->
//                        dialogInterface.dismiss()
//                    }
//                    .setCancelable(false)
//                    .show()
            }
            tag_intent_my_account_activity -> {
                val intent : Intent
                intent = Intent(context, MyAccountActivity::class.java)
                startActivity(intent)
            }
            tag_intent_referrals_activity -> {
                val intent : Intent
                intent = Intent(context, ReferralsActivity::class.java)
                startActivity(intent)
            }
            tag_intent_help_activity -> {
                val intent : Intent
                intent = Intent(context, HelpActivity::class.java)
                startActivity(intent)
            }
            tag_intent_edfood_activity ->
            {

                if (getCurrentLocationData(tag_current_location_country_code, this@MainActivity).equals(
                        "NG",
                        ignoreCase = true
                    )
                ) {
                    val intent : Intent
                    intent = Intent(context, EDFoodMerchantsActivity::class.java)
                    startActivity(intent)
                } else {
                    showDialog_main(
                        "",
                        "tag_edpay_button_ok",
                        true,
                        getResources().getString(R.string.dialog_app_unavailable_title),
                        getResources().getString(R.string.dialog_app_unavailable_message),
                        "",
                        "OK"
                    )
                }
            }
            tag_intent_announcements_activity-> {
                val intent : Intent
                intent = Intent(context, AnnouncementsActivity::class.java)
                startActivity(intent)
            }
            tag_load_api_log_pay -> {
                val fragment: PayActivityFragment = value as PayActivityFragment
                getPayLogs(
                    fragment,
                    preference.getString(key_user_id),
                    "NG",
                    fragment.button_date_from.text.toString(),
                    fragment.button_date_to.text.toString())
            }
            tag_load_api_log_pay_from -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: PayActivityFragment = value as PayActivityFragment
                val date: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {
                            getPayLogs(
                                fragment,
                                preference.getString(key_user_id),
                                "NG",
                                setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_from),
                                fragment.button_date_to.text.toString())
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = dateto
                dpd.datePicker.maxDate = cal.timeInMillis
                dpd.show()
            }
            tag_load_api_log_pay_to -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: PayActivityFragment = value as PayActivityFragment
                val date: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {
                            getPayLogs(
                                fragment,
                                preference.getString(key_user_id),
                                "NG",
                                fragment.button_date_from.text.toString(),
                                setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_to))
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = dateto
                dpd.datePicker.minDate = cal.timeInMillis
                dpd.show()
            }
            tag_load_api_log_food -> {
                val fragment: FoodActivityFragment = value as FoodActivityFragment
                getFoodLogs(
                    fragment,
                    preference.getString(key_user_id),
                    fragment.button_date_from.text.toString(),
                    fragment.button_date_to.text.toString())
            }
            tag_load_api_log_food_from -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: FoodActivityFragment = value as FoodActivityFragment
                val date: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {
                            getFoodLogs(
                                fragment,
                                preference.getString(key_user_id),
                                setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_from),
                                fragment.button_date_to.text.toString())
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = dateto
                dpd.datePicker.maxDate = cal.timeInMillis
                dpd.show()
            }
            tag_load_api_log_food_to -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: FoodActivityFragment = value as FoodActivityFragment
                val date: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {
                            getFoodLogs(
                                fragment,
                                preference.getString(key_user_id),
                                fragment.button_date_from.text.toString(),
                                setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_to))
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = dateto
                dpd.datePicker.minDate = cal.timeInMillis
                dpd.show()
            }
            tag_load_api_log_topup -> {
                val fragment: TopUpActivityFragment = value as TopUpActivityFragment

                fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE

                //            getPayLogs(
    //                preference.getString(key_user_id),
    //                "NG",
    //                fragment.button_date_from.text.toString(),
    //                fragment.button_date_to.text.toString())
            }
            tag_load_api_log_topup_from -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: TopUpActivityFragment = value as TopUpActivityFragment
                val date: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {

                            fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                            fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE
    //                        getPayLogs(
    //                            preference.getString(key_user_id),
    //                            "NG",
    //                            setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_from),
    //                            fragment.button_date_to.text.toString())
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = dateto
                dpd.datePicker.maxDate = cal.timeInMillis
                dpd.show()
            }
            tag_load_api_log_topup_to -> {
                val cal = Calendar.getInstance()
                val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
                val fragment: TopUpActivityFragment = value as TopUpActivityFragment
                val date: Date = sdf.parse(fragment.button_date_to.text.toString())
                cal.time = date

                val dpd =  DatePickerDialog(this@MainActivity,
                    object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                               dayOfMonth: Int) {

                            fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                            fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE
    //                        getPayLogs(
    //                            preference.getString(key_user_id),
    //                            "NG",
    //                            fragment.button_date_from.text.toString(),
    //                            setDateToButton(cal, year,monthOfYear, dayOfMonth, fragment.button_date_to))
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                val dateto: Date = sdf.parse(fragment.button_date_from.text.toString())
                cal.time = dateto
                dpd.datePicker.minDate = cal.timeInMillis
                dpd.show()
            }
            tag_intent_order_history_detail -> {
                val intent : Intent
                intent = Intent(context, EDFoodLogDetailsAcivity::class.java)
                intent.putExtra(key_data,value.toString())
                startActivity(intent)
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
            tag_intent_track_activity -> {
                val intent : Intent
                intent = Intent(context, MapActivity::class.java)
                intent.putExtra(key_data,value.toString())
                startActivity(intent)
            }
            tag_intent_edpay_history_detail -> {
                val intent : Intent
                intent = Intent(context, EDPayLogDetailsActivity::class.java)
                intent.putExtra(key_data,value.toString())
                startActivity(intent)
            }
        }
    }

    private fun setDateToButton(cal: Calendar, year: Int, monthOfYear: Int,
                                dayOfMonth: Int, button: Button) : String
    {
        val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        button.text = sdf.format(cal.time)
        return  button.text.toString()
    }

    private fun getWallet(
        customer_id: String,
        country_name: String
    ) {
        Timber.d("params customer_id: %s, country_name: %s" ,customer_id ,country_name)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlWallet,
            "customer_id:" + customer_id + "||" +
                    "country_name:" + country_name,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    val wallet = jsonObj[key_edpoints].asString
                    val currency = jsonObj[key_currency_code].asString

                    val wallet_toDouble = wallet.replace(",", "").toDouble()

                    val formatter = DecimalFormat.getNumberInstance(Locale.US) as DecimalFormat /* new DecimalFormat("###,###.##");*/
                    formatter.applyPattern("###,##0.00")

//                    val value = currency+" "+ commaSeparated(wallet.replace(",","")).asString()
                    val value = currency+" "+ formatter.format(wallet_toDouble)


                        try {
                            //showToast_short("" + value)
                            this@MainActivity.runOnUiThread {
                                MainScreen.HOME.fragment.fragment_home_textview_credits.text = value
                            }
                        }
                        catch (e:Exception)
                        {
                            Timber.e(e)
                        }

                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun getAnnouncement(
        customer_id: String,
        country_name: String
    ) {
        Timber.d("params customer_id: %s, country_name: %s" ,customer_id ,country_name)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlWallet,
            "customer_id:" + customer_id + "||" +
                    "country_name:" + country_name,
            { response: String? ->
                launch(Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        val wallet = jsonObj[key_edpoints].asString
                        val currency = jsonObj[key_currency_code].asString

                        val value = currency+" "+ commaSeparated(wallet).asString()
                        try {
                            MainScreen.HOME.fragment.fragment_home_textview_credits.text = value
                        }
                        catch (e:Exception)
                        {
                            Timber.e(e)
                        }
                    } else {
                        val error_code = jsonObj[key_code].asString
                        showDialogError(request_error_dialog, error_code)
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun getPayLogs(
        fragment: PayActivityFragment,
        customer_id: String,
        country_code: String,
        date_from: String,
        date_to: String
    ) {
        val formatted_date_from = dateFormat(date_from, date_format_picker_display,date_format_server_api).asString()
        val formatted_date_to = dateFormat(date_to, date_format_picker_display,date_format_server_api).asString()
        val params = "customer_id:" + customer_id + "||" +
                "country_code:" + country_code + "||" +
                "date_from:" + formatted_date_from + "||" +
                "date_to:" + formatted_date_to
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlQRTransactionLogs,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {

                    runOnUiThread {
                        fragment.fragment_activity_logs_layout_recyclerView.visibility = VISIBLE
                        fragment.fragment_activity_logs_layout_no_data.visibility = GONE
                    }

                    val jsonArray = jsonObj[key_data].asJsonArray
                    val items = ArrayList<Any>()
                    for (data in jsonArray)
                    {
                        items.add(data)
                    }
                    fragment.displayList(items)
                } else {

                    runOnUiThread {
                        fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                        fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE
                    }

                    val error_code = jsonObj[key_code].asString

                    if(error_code == "ED12") {

                    } else {
                        showDialog_APIError("", "", true, error_code, "", "OK")
                    }
                    //showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun getFoodLogs(
        fragment: FoodActivityFragment,
        customer_id: String,
        date_from: String,
        date_to: String
    ) {
        val formatted_date_from = dateFormat(date_from, date_format_picker_display,date_format_server_api).asString()
        val formatted_date_to = dateFormat(date_to, date_format_picker_display,date_format_server_api).asString()
        val params = "customer_id:" + customer_id + "||" +
                "start_date:" + formatted_date_from + "||" +
                "end_date:" + formatted_date_to
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlOrderHistory,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {

                    if(jsonObj[key_booked] == null || jsonObj[key_booked].toString() == "") {

                        runOnUiThread {
                            fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                            fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE
                        }
                        //fragment_activity_logs_layout_recyclerView
                    } else {

                        runOnUiThread {
                            fragment.fragment_activity_logs_layout_recyclerView.visibility = VISIBLE
                            fragment.fragment_activity_logs_layout_no_data.visibility = GONE
                        }

                        val jsonArray_asString = "" + jsonObj[key_booked]
                        if(jsonArray_asString.equals("") || jsonArray_asString.equals("[]") || jsonArray_asString == null) {

                        } else {
                            val jsonArray = jsonObj[key_booked].asJsonArray
                            Log.d("Logs", "key_booked: $jsonArray")
                            val items = ArrayList<Any>()
                            for (data in jsonArray)
                            {
                                items.add(data)
                            }
                            fragment.displayList(items)
                        }
                    }


                } else {

                    runOnUiThread {
                        fragment.fragment_activity_logs_layout_recyclerView.visibility = GONE
                        fragment.fragment_activity_logs_layout_no_data.visibility = VISIBLE
                    }

                    val error_code = jsonObj[key_code].asString
                    showDialog_APIError("", "", true, error_code, "", "OK")
                    //showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private var modelList: List<Announcement>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: AnnouncementListHomeAdapter? = null

    private fun initAPIGetAnnouncementsList(jinx:Any?) {
        val fragment: HomeFragment = jinx as HomeFragment
        showProgress()

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL ,false)
        fragment.fragment_home_recyclerView_announcements.layoutManager = layoutManager
        fragment.fragment_home_recyclerView_announcements.setHasFixedSize(true)
        recyclerAdapter = AnnouncementListHomeAdapter(this, modelList)
        fragment.fragment_home_recyclerView_announcements.adapter = recyclerAdapter

        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlAnnouncementList, "",
            { response: String ->
                launch (Main) {

                    hideProgress()
                    Log.d(AnnouncementsActivity.TAG, "DATA: | ")
                    Log.d(AnnouncementsActivity.TAG, "RESPONSE: $response")
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    modelList = java.util.ArrayList<Announcement>()
                    (modelList as java.util.ArrayList<Announcement>).clear()
                    if (status == status_success) {
                        try {
                            val obj = JSONObject(response)
                            val announcement_data =
                                obj.getJSONArray("announcement_records")
                            //showToast_short("SUCCESS HOME: " + announcement_data.length());
                            runOnUiThread {
                                if (announcement_data.length() == 0) {
                                    fragment.fragment_home_textView_message.visibility = View.VISIBLE
                                    //fragment.fragment_home_recyclerView_announcements.visibility = View.INVISIBLE
                                } else {
                                    fragment.fragment_home_textView_message.visibility = INVISIBLE
                                    //fragment.fragment_home_recyclerView_announcements.visibility = View.VISIBLE
                                }
                            }
                            for (i in 0 until announcement_data.length()) {
                                val `object` = announcement_data.getJSONObject(i)
                                var announcement_id: String
                                announcement_id = try {
                                    "" + `object`["id"] as String
                                } catch (e: java.lang.Exception) {
                                    "" + `object`.getInt("id")
                                }
                                val title = `object`["title"] as String
                                val content = `object`["content"] as String
                                val event_date =
                                    `object`["event_date"] as String
                                val expiry_date =
                                    `object`["expiry_date"] as String
                                val image = `object`["image"] as String
                                //val countries = "\"" + `object`["countries"] + "\""
                                val countries = ""
                                var annc_status: String
                                annc_status = try {
                                    "" + `object`["annc_status"] as String
                                } catch (e: java.lang.Exception) {
                                    "" + `object`.getInt("annc_status")
                                }
                                val list = Announcement(
                                    announcement_id,
                                    title,
                                    content,
                                    event_date,
                                    expiry_date,
                                    image,
                                    countries,
                                    annc_status
                                )
                                (modelList as java.util.ArrayList<Announcement>).add(list)
                            }
                            runOnUiThread { recyclerAdapter!!.setModelList(modelList)

                                //fragment.fragment_home_recyclerView_announcements.adapter = recyclerAdapter
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        //                        JsonArray announcement_data = parser.parse(String.valueOf(jsonObj.get("announcement_records"))).getAsJsonArray();
//                        showToast_short("SUCCESS: " + announcement_data.size());
                    } else {
                        val error_code = jsonObj[key_code].asString
                        showDialog_APIError("", "", true, error_code, "", "OK")
                    }
                }
            }
        ) { failure: Throwable? ->
            hideProgress()
            Log.d(AnnouncementsActivity.TAG, "RESPONSE (ERROR): $failure")
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.message,
                "",
                "OK"
            )
            null
        }
    }

    private fun loadList() {
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlLogin,
            "email:+639477413159||" +
                    "password:carmel||" +
                    "login_type:1||" +
                    "device_id:aaaaa||" +
                    "country_name:nigeria",
            success = {
                launch(Main) {
//                    movieAdapter.list = it
                    android.util.Log.i("z- data", it.toString())
                }
            },
            failure = ::handleError
        )
    }

    fun onClickMovie(movie: MovieItemResponse) {
        android.util.Log.i("z- click", movie.toString())
    }

    private fun handleError(ex: Throwable?) {
        android.util.Log.i("z- error", ex?.printStackTrace().toString())
//        android.widget.Toast.makeText(this, ex?.message ?: "Unknown error" , android.widget.Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
        super.onDestroy()
    }

    override fun onResume() {
        initProfilePage(ProfileFragment())
        super.onResume()
    }

    /**
     * Scrolls `ViewPager` to show the provided screen.
     */
    private fun scrollToScreen(mainScreen: MainScreen) {
        val screenPosition = mainPagerAdapter.getItems().indexOf(mainScreen)
        if (screenPosition != viewPager.currentItem) {
            viewPager.currentItem = screenPosition
        }
    }

    /**
     * Selects the specified item in the bottom navigation view.
     */
    private fun selectBottomNavigationViewMenuItem(@IdRes menuItemId: Int) {
        bottomNavigationView.setOnNavigationItemSelectedListener(null)
        bottomNavigationView.selectedItemId = menuItemId
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    /**
     * Listener implementation for registering bottom navigation clicks.
     */
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        getMainScreenForMenuItem(menuItem.itemId)?.let {
            scrollToScreen(it)
            supportActionBar?.setTitle(it.titleStringId)
            return true
        }
        return false
    }


    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)

        when (requestTag) {
            "tag_log_out_button_ok" -> {
                val intent = Intent(
                    context,
                    Login1Activity::class.java
                )
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
            "tag_button_exit_app" -> {
                finishAffinity()
            }
            else -> {
            }
        }
    }

    override fun onBackPressed() {
        this@MainActivity.runOnUiThread(java.lang.Runnable {

            showDialog_main(
                "",
                "tag_button_exit_app",
                true,
                "",
                resources.getString(R.string.dialog_exit_app_message),
                "No",
                "Yes"
            )

//            AlertDialog.Builder(this)
////                .setTitle(finalTitle)
//                .setMessage(label_dialog_title_exit)
//                .setPositiveButton(button_yes
//                ) { dialogInterface: DialogInterface, i: Int ->
//                    dialogInterface.dismiss()
//                    finishAffinity()
//                }
//                .setNegativeButton(button_no
//                ) { dialogInterface: DialogInterface, i: Int ->
//                    dialogInterface.dismiss()
//                }
//                .setCancelable(false)
//                .show()
        })
        super.onBackPressed()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                this@MainActivity.runOnUiThread(java.lang.Runnable {

                    showDialog_main(
                        "",
                        "tag_button_exit_app",
                        true,
                        "",
                        resources.getString(R.string.dialog_exit_app_message),
                        "No",
                        "Yes"
                    )

//                    AlertDialog.Builder(this)
////                .setTitle(finalTitle)
//                        .setMessage(label_dialog_title_exit)
//                        .setPositiveButton(button_yes
//                        ) { dialogInterface: DialogInterface, i: Int ->
//                            dialogInterface.dismiss()
//                            finishAffinity()
//                        }
//                        .setNegativeButton(button_no
//                        ) { dialogInterface: DialogInterface, i: Int ->
//                            dialogInterface.dismiss()
//                        }
//                        .setCancelable(false)
//                        .show()
                })
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
