//
//  ConfirmMobilePINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/16/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app

class ConfirmMobilePINVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    var timer = Timer()
    var timer_count = 1
    
    @IBOutlet weak var confirmPINView: CBPinEntryView! {
        didSet {
                confirmPINView.delegate = self as CBPinEntryViewDelegate
            }
    }
    
    @IBOutlet weak var btnNext: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func didTapButtonNext(_ sender: Any) {
        initCheckPIN()
    }
    
    private func initCheckPIN()
    {
        let pin = self.segueData[self.constant.key_pin] as! String
        let confirm_pin = confirmPINView.getPinAsString()
        if confirm_pin.count < 6
        {
            self.handleError(displaytitle: "dialog_mpin_incomplete_title".localized(), "dialog_mpin_incomplete_message".localized())
            return
        }
        
        if pin != confirm_pin
        {
            self.handleError(displaytitle: "dialog_mpin_mismatch_title".localized(), "dialog_mpin_mismatch_message".localized())
            return
        }
        
        self.performSegue(withIdentifier: "segueLogin", sender: nil)
        self.initAPICreateMPin(customer_id: "\(self.segueData[self.constant.key_cust_id] as! Int)", pin: confirm_pin)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if timer_count < 1 {
            timer.invalidate()
            self.hideLoading()
            
            let okAction = UIAlertAction(title: self.constant.button_okay, style: UIAlertActionStyle.default) {
                UIAlertAction in
                let newStoryBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
                let VC = newStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    VC.modalPresentationStyle = .fullScreen
                    self.present(VC, animated: true, completion: nil)
            }
            let actions = [okAction]
//            self.showDialog(title: "dialog_mpin_success_title".localized(), message: "dialog_mpin_success_message".localized(), actions: actions)
            self.handleError(actions: actions, displaytitle: "dialog_mpin_success_title".localized(), "dialog_mpin_success_message".localized())
        } else {
            timer_count -= 1
        }
    }
                
    private func initAPICreateMPin(
        customer_id: String,
        pin:String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlPINCreate,
        params: "customer_id:\(customer_id)||pin:\(pin)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    self.runTimer()
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.hideLoading()
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}

extension ConfirmMobilePINVC: CBPinEntryViewDelegate {
    func entryCompleted(with entry: String?) {
        self.log("pinView \(confirmPINView.getPinAsString())")
        self.initCheckPIN()
    }

    func entryChanged(_ completed: Bool) {
        if completed {
        }
    }
}
