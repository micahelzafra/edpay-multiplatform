package com.ed2e.ed2eapp.view.activity.mpin;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlPINCreate;
import static com.ed2e.ed2eapp.util.ConstantKt.urlResetPIN;

public class ResetMPinActivity extends BaseActivity {

    public static final String TAG = ResetMPinActivity.class.getSimpleName();

    @BindView(R.id.mpin_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.mpin_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;


    @BindView(R.id.create_mpin_textView_title)
    TextView textView_title;
    @BindView(R.id.create_mpin_textView_desc)
    TextView textView_desc;
    @BindView(R.id.create_mpin_pinview)
    PinView pinview;
    @BindView(R.id.create_mpin_button_submit)
    Button button_submit;

    int currentPage = 1;
    String firstPin = "";
    String secondPin = "";
    String mobileNumber = "";
    String otpCode = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_mpin);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mobileNumber = "";
            otpCode = "";
        } else {
            mobileNumber = extras.getString(key_mobile_number);
            otpCode = extras.getString(key_otp_code);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        currentPage = 1;
        updatePage(currentPage);

        button_submit.setOnClickListener(v -> {
            if(currentPage == 1) {
                firstPin = pinview.getText().toString().trim();

                if(firstPin.length() < 6){
                    showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_incomplete_title), getResources().getString(R.string.dialog_mpin_incomplete_message), "", "OK");
                } else {
                    currentPage = 2;
                    updatePage(currentPage);
                }

            } else {
                secondPin = pinview.getText().toString().trim();

                if(secondPin.length() < 6){
                    showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_incomplete_title), getResources().getString(R.string.dialog_mpin_incomplete_message), "", "OK");
                } else {
                    if(firstPin.equalsIgnoreCase(secondPin)) {
                        initAPICreateMPin(mobileNumber, otpCode, secondPin);
                    } else {
                        showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_mismatch_title), getResources().getString(R.string.dialog_mpin_mismatch_message), "", "OK");
                    }
                }


//                currentPage = 1;
//                updatePage(currentPage);
            }
            //showDialog_main("", "button_ok",  true,"Mobile PIN incomplete", "Please enter a 6 digits Mobile PIN", "", "OK");
        });

        pinview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinview.getText().toString().trim().length() >= 6) {
                    if(currentPage == 1) {
                        firstPin = pinview.getText().toString().trim();

                        if(firstPin.length() < 6){
                            showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_incomplete_title), getResources().getString(R.string.dialog_mpin_incomplete_message), "", "OK");
                        } else {
                            currentPage = 2;
                            updatePage(currentPage);
                        }

                    } else {
                        secondPin = pinview.getText().toString().trim();

                        if(secondPin.length() < 6){
                            showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_incomplete_title), getResources().getString(R.string.dialog_mpin_incomplete_message), "", "OK");
                        } else {
                            if(firstPin.equalsIgnoreCase(secondPin)) {
                                initAPICreateMPin(mobileNumber, otpCode, secondPin);
                            } else {
                                showDialog_main("", "", true,getResources().getString(R.string.dialog_mpin_mismatch_title), getResources().getString(R.string.dialog_mpin_mismatch_message), "", "OK");
                            }
                        }
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void initAPICreateMPin(String mobile_number, String otp, String pin) {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlResetPIN,
                "mobile_number:" + mobile_number + "||" +
                        "otp_code:" + otp + "||" +
                        "new_pin:" + pin,
                response -> {
                    hideProgress();
                    Log.d(TAG, "URL: " + urlResetPIN );
                    Log.d(TAG, "DATA: | " + mobile_number + " | " + otp  + " | " + pin + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        showDialog_main("", "tag_mpin_success_button_log_in", false, getResources().getString(R.string.dialog_reset_mpin_success_title), getResources().getString(R.string.dialog_reset_mpin_success_message), "", "LOG IN");
                    }
                    else
                    {
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }


    private void updatePage(int current_page) {
        switch(current_page){
            case 1:
                textView_title.setText(getResources().getString(R.string.label_reset_pin_page_1_title));
                textView_desc.setText(getResources().getString(R.string.label_reset_pin_page_1_message));
                pinview.setText(firstPin);
                button_submit.setText(getResources().getString(R.string.button_next));
                LinearLayout_left.setVisibility(View.GONE);
                break;
            case 2:
                textView_title.setText(getResources().getString(R.string.label_confirm_pin_title));
                textView_desc.setText(getResources().getString(R.string.label_confirm_pin_message));
                pinview.setText("");
                button_submit.setText(getResources().getString(R.string.button_submit));
                LinearLayout_left.setVisibility(View.VISIBLE);
                break;
            default:
                textView_title.setText(getResources().getString(R.string.label_reset_pin_page_1_title));
                textView_desc.setText(getResources().getString(R.string.label_reset_pin_page_1_message));
                pinview.setText("");
                button_submit.setText(getResources().getString(R.string.button_next));
                LinearLayout_left.setVisibility(View.GONE);
                break;
        }
    }


//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }






    private void initPreviousActivity() {
//        if(currentPage == 1) {
//            Intent intent = new Intent(getContext(), Login1Activity.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//        } else {
            currentPage = 1;
            updatePage(currentPage);
//        }

    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "tag_mpin_success_button_log_in":
                intent = new Intent(getContext(), Login1Activity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
