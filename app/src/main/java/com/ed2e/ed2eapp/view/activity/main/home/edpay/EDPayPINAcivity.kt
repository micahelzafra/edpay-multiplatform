package com.ed2e.ed2eapp.view.activity.main.home.edpay


import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import com.bumptech.glide.Glide
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edpay_input_amount.*
import kotlinx.android.synthetic.main.activity_edpay_pin.*
import kotlinx.android.synthetic.main.activity_edpay_pin.edpay_pin_imageview_merchant_logo
import kotlinx.android.synthetic.main.activity_edpay_pin.edpay_pin_textview_merchant_name
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class EDPayPINAcivity : BaseActivity(), CoroutineScope {

    val TAG = EDPayPINAcivity::class.java.simpleName

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    private var soundPool: SoundPool? = null
    private var soundID = 0

    private var mAmountInput = "0"

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled: Boolean = false
    private var keyData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_pin)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        var promoCode = bundle!!.getString(key_promo_name)
        if(promoCode == null || promoCode == "" || promoCode == "null") {
            promoCode = ""
        }
        Log.d("Logs", "promoCode: " + bundle!!.getString(key_promo_name))

        keyData = data

        //val amount = bundle.getString(key_amount) // 1

        mAmountInput = bundle.getString(key_amount)
        Log.d("Logs", "mAmountInput: " + mAmountInput.replace(",",""))

        val discounted_amount = bundle.getString(key_discounted_amount)
        val currencyCode = bundle.getString(key_currency_code)
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)
        val trade_name = jsonObj[key_trade_name].asString
        val trade_id = jsonObj[key_merchant_code].asString

        var shop_facade = ""
        if(jsonObj[key_shop_facade] == null || jsonObj[key_shop_facade].toString() == "" || jsonObj[key_shop_facade].toString() == "{}" || jsonObj[key_shop_facade].toString() == "null") {
            Glide.with(this).load(R.drawable.ic_default_merchant).into(edpay_pin_imageview_merchant_logo)
        } else {
            shop_facade = jsonObj[key_shop_facade].asString
            Glide.with(this).load(shop_facade).into(edpay_pin_imageview_merchant_logo)
        }

//        val shop_facade = jsonObj[key_shop_facade].asString
//        Glide.with(this).load(shop_facade).into(edpay_pin_imageview_merchant_logo)

        edpay_pin_textview_merchant_name.text = trade_name
        edpay_pin_textview_currency.text = currencyCode

        val formatter = DecimalFormat.getNumberInstance(Locale.US) as DecimalFormat /* new DecimalFormat("###,###.##");*/
        formatter.applyPattern("###,##0.00")

        if(promoCode == null || promoCode == "" || promoCode == "null") {
            edpay_pin_textview_amount.text = formatter.format(mAmountInput.replace(",","").toDouble())
        } else {
            edpay_pin_textview_amount.text = formatter.format(discounted_amount.replace(",","").toDouble())
        }

//        edpay_pin_textview_amount.text = formatter.format(amount.replace(",","").toDouble())
        //edpay_pin_textview_amount.text = commaSeparated(amount).asString()

        edpay_pin_button_done.setOnClickListener {
            if (edpay_pin_input.text.toString().trim({ it <= ' ' }).length < 6)
            {
                showDialog_main(
                    "",
                    "",
                    true,
                    resources.getString(R.string.dialog_mpin_incomplete_title),
                    resources.getString(R.string.dialog_mpin_incomplete_message),
                    "",
                    "OK"
                )


                //showDialogError(request_error_dialog, "ED15")
                return@setOnClickListener
            }
            initTransaction(
                preference.getString(key_user_id),
                trade_id,
//                edpay_pin_textview_amount.text.toString().replace(",".toRegex(), ""),
                mAmountInput.replace(",",""),
                edpay_pin_input.text.toString(),
                promoCode)
        }

        edpay_pin_input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (edpay_pin_input.getText().toString().trim({ it <= ' ' }).length >= 6) {
                    initTransaction(
                        preference.getString(key_user_id),
                        trade_id,
                        //edpay_pin_textview_amount.text.toString(),
                        mAmountInput.replace(",",""),
                        edpay_pin_input.text.toString(),
                        promoCode)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })


        // Set the hardware buttons to control the music
        this.volumeControlStream = AudioManager.STREAM_MUSIC
        // Load the sound
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        soundPool!!.setOnLoadCompleteListener(object : SoundPool.OnLoadCompleteListener {
            override fun onLoadComplete(
                soundPool: SoundPool?, sampleId: Int,
                status: Int
            ) {

                //loaded = true
                //Timber.d("EDPayActivity %s %s %s")

                //isShaked = true
                //initCashBack(preference.getString(key_user_id),amount!!,paymentType(payment_type!!).asString())

//                Glide.with(this@EDPayShakeReward)
//                    .load(R.drawable.ic_gif_3)
//                    .into(edpay_pin_imageview_merchant_logo)
            }
        })
        soundID = soundPool!!.load(this, R.raw.transaction, 1)
    }

    private fun initTransaction(
        customer_id: String,
        merchant_code: String,
        amount: String,
        pin: String,
        promo_code: String
    ) {

        showProgress()

        val params = "customer_id:" + customer_id + "||" +
                "merchant_code:" + merchant_code + "||" +
                "amount:" + amount.replace(",", "") + "||" +
                "pin:" + pin + "||" +
                "promo_code:" + promo_code
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlQRTransaction,
            params,
            { response: String? ->
                hideProgress()
                Log.d("Logs", "DATA: | $params")
                Log.d("Logs", "RESPONSE: $response")
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    val data = jsonObj[key_data].asJsonObject
                    Timber.d("data: %s", data)

                    playTransactionSuccessSound()

                    val intent = Intent(this@EDPayPINAcivity, EDPaySuccess::class.java)
                    intent.putExtra(key_data, data.toString())
                    startActivity(intent)
                    finish()
                    overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)
                } else {
                    val error_code = jsonObj[key_code].asString
                    //showDialogError(request_error_dialog, error_code)

                    showDialog_APIError("", "", true, error_code, "", "OK")
                }
            }
        ) { failure: Throwable? ->
            hideProgress()
            println("Throwable:$failure")
            //showDialogError(request_error_dialog, failure!!.localizedMessage)

            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )

        }
    }


    private fun playTransactionSuccessSound() {
        Timber.d("playTransactionSuccessSound")
        val audioManager =
            getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = audioManager
            .getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = audioManager
            .getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val volume = actualVolume / maxVolume


        soundPool!!.play(soundID, volume, volume, 1, 0, 1f)


//        if (loaded) {
//            soundPool!!.play(soundID, volume, volume, 1, 0, 1f)
//            //isShaked = true
//        }
    }


    private fun initToolBar() {
        edpay_pin_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edpay_pin_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edpay_pin_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        val intent = Intent(this@EDPayPINAcivity, EDPayInputAmountActivity::class.java)
        intent.putExtra(key_data, keyData)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
