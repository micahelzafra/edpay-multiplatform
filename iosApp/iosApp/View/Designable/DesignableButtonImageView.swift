//
//  DesignableButtonImageView.swift
//  ed2eapp
//
//  Created by ED2E Technology iOS on 22/01/2019.
//  Copyright © 2019 ED2E Technology iOS. All rights reserved.
//

import UIKit

@IBDesignable class DesignableButtonImageView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    // Textfield border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    //Textfield border color
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var centerImage: UIImage? {
        
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var imageSize: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    
    func updateView () {
        
        if let image = centerImage {
            
            let imageView = UIImageView(frame: CGRect(x: (self.frame.size.width-imageSize)/2, y: (self.frame.size.height-imageSize)/2, width: imageSize, height: imageSize))
            imageView.image = image
            imageView.tintColor = tintColor
            self.addSubview(imageView)
            
        }
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.layer.backgroundColor = UIColor.gray.cgColor
//    }
//
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.layer.backgroundColor = UIColor.white.cgColor
//    }
}
