package com.ed2e.ed2eapp.view.activity.announcements;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;

public class AnnouncementsDetailsActivity extends BaseActivity {

    public static final String TAG = AnnouncementsDetailsActivity.class.getSimpleName();

    @BindView(R.id.announcement_details_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.announcement_details_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.activity_announcement_details_textview_title)
    TextView textView_title;
    @BindView(R.id.activity_announcement_details_textview_event_date)
    TextView textView_event_date;
    @BindView(R.id.activity_announcement_details_textview_content)
    TextView textView_content;
    @BindView(R.id.activity_announcement_details_imageView)
    ImageView imageView_content;

    private String annTitle = "";
    private String annDate = "";
    private String annContent = "";
    private String annImage = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_details);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            annTitle = "";
            annDate = "";
            annContent = "";
            annImage = "";
        } else {
            annTitle = extras.getString(key_announcement_title);
            annDate =  extras.getString(key_announcement_date);
            annContent =  extras.getString(key_announcement_content);
            annImage =  extras.getString(key_announcement_image);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        textView_title.setText(annTitle);
        textView_event_date.setText(annDate);
        textView_content.setText(annContent);

        Glide.with(AnnouncementsDetailsActivity.this)
                .load(annImage)
                .apply(new RequestOptions().override(1280, 710).centerCrop().placeholder(R.drawable.ic_eddy_2))
                .into(imageView_content);
    }

    private void initPreviousActivity() {
//        Intent intent = new Intent(getContext(), AnnouncementsActivity.class);
//        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
