package com.ed2e.ed2eapp.util;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ed2e.ed2eapp.base.BaseActivity;


public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final String TAG = NetworkChangeReceiver.class.getSimpleName();
    protected Dialog dialog_noNetwork;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            if (isOnline(context)) {
                //Log.e("keshav " + context.getApplicationInfo(), "Online Connect Intenet ");
//                hideDialog_noNetwork();
            } else {
                //Log.e("keshav " + context.getApplicationInfo(), "Connectivity Failure !!! ");
                showDialog_noNetwork(context);

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void showDialog_noNetwork(Context context) {
//        Log.d(TAG,"showDialog_noNetwork() triggered");
//
//        if(dialog_noNetwork != null){
//            hideDialog_noNetwork();
//        }
//
//        dialog_noNetwork = new Dialog(context, R.style.DialogTheme);
//        dialog_noNetwork.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        if (dialog_noNetwork.getWindow() != null)
//            dialog_noNetwork.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog_noNetwork.setContentView(R.layout.dialog_main);
//
//        TextView textView_title = dialog_noNetwork.findViewById(R.id.dialog_main_textView_title);
//        TextView textView_message = dialog_noNetwork.findViewById(R.id.dialog_main_textView_message);
//        ImageView imageView_image = dialog_noNetwork.findViewById(R.id.dialog_main_imageView_image);
//        CircleImageView circleImageView_image = dialog_noNetwork.findViewById(R.id.dialog_main_circleImageView_image);
//        Button button_1 = dialog_noNetwork.findViewById(R.id.dialog_main_button_1);
//        Button button_2 = dialog_noNetwork.findViewById(R.id.dialog_main_button_2);
//        LinearLayout linearLayout_x = dialog_noNetwork.findViewById(R.id.dialog_main_linearLayout_x);
//        ImageView imageView_x = dialog_noNetwork.findViewById(R.id.dialog_main_imageView_x);
//
//        linearLayout_x.setVisibility(View.GONE);
//        circleImageView_image.setVisibility(View.GONE);
//        button_1.setVisibility(View.GONE);
//
//        textView_title.setTextColor(context.getResources().getColor(R.color.color_red));
//        button_1.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_button_light_black_selector));
//        button_2.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_button_gold_solid_selector));
//
//        Typeface custom_font_NORMAL = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri.ttf");
//        Typeface custom_font_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri Bold.ttf");
//
//        textView_title.setTypeface(custom_font_BOLD);
//        textView_message.setTypeface(custom_font_NORMAL);
//        button_1.setTypeface(custom_font_BOLD);
//        button_2.setTypeface(custom_font_BOLD);
//
//        textView_title.setText(context.getResources().getString(R.string.dialog_no_internet));
//        textView_message.setText(context.getResources().getString(R.string.dialog_no_internet_message));
//        imageView_image.setImageResource(R.drawable.ic_no_internet);
//        button_1.setText("");
//        button_1.setTextColor(context.getResources().getColor(R.color.colorWhite));
//        button_2.setText(context.getResources().getString(R.string.button_exit).toUpperCase());
//        button_2.setTextColor(context.getResources().getColor(R.color.color_light_black));
//
//        imageView_x.setOnClickListener(v -> {
//            hideDialog_noNetwork();
//        });
//
//
//
//        button_1.setOnClickListener(v -> {
//            //context.
//            hideDialog_noNetwork();
//        });
//
//        button_2.setOnClickListener(v -> {
//            System.exit(0);
//            hideDialog_noNetwork();
//        });
//
//        dialog_noNetwork.setCancelable(false);
//        dialog_noNetwork.setCanceledOnTouchOutside(false);
//
//        try{
//            dialog_noNetwork.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

//    public void hideDialog_noNetwork() {
//        try {
//            dialog_noNetwork.dismiss();
//            BaseActivity ba = new BaseActivity() {
//                @Override
//                protected void onDialogDismiss(String requestTag, String input_data) {
////                    finish();
//                }
//            };
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}