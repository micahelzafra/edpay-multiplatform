package com.ed2e.ed2eapp.view.activity.main.home.edpay


import android.content.Context
import android.content.Intent
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.MainActivity
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.goodcodeforfun.seismik.ShakeDetector
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edpay_shake_reward.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext


class EDPayShakeReward : BaseActivity(), CoroutineScope, ShakeDetector.Listener {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled: Boolean = false
    private var isShaked: Boolean = false
    private lateinit var shakeDetector: ShakeDetector
    private var shake_count = 0
    private var mediaPlayer: MediaPlayer? = null
    private var soundPool: SoundPool? = null
    private var soundID = 0
    var loaded = false

    private var amount = ""
    private var payment_type = ""
    private var cashbackAmount = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_shake_reward)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initGUI()
    }



    private fun initGUI() {
        Timber.d("EDPayShakeReward initGUI")
        val bundle: Bundle? = intent.extras
//        val data = bundle!!.getString(key_data) // 1
        amount = bundle!!.getString(key_amount) // 1
        payment_type = bundle.getString(key_payment_method) // 1

        isShaked = false

//        val parser = JsonParser()
//        val jsonObj: JsonObject = parser.parse(data).getAsJsonObject()
//        Timber.d("jsonObj: %s", jsonObj)
//        Glide.with(this).load(shop_facade).into(edpay_pin_imageview_merchant_logo)

        shakeDetector = ShakeDetector(this)
        shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_LIGHT)
//        findViewById<RadioGroup>(R.id.radioGroupSensitivity).setOnCheckedChangeListener({ _, i ->
//            when (i) {
//                R.id.radioButtonLight -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_LIGHT)
//                R.id.radioButtonMedium -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_MEDIUM)
//                R.id.radioButtonHard -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_HARD)
//                R.id.radioButtonHarder -> shakeDetector.setSensitivity(ShakeDetector.Sensitivity.SENSITIVITY_HARDER)
//                else -> throw IllegalArgumentException()
//            }
//        })

        mediaPlayer = MediaPlayer.create(this, R.raw.sound_shake_2)
        mediaPlayer?.setOnPreparedListener {  }


        // Set the hardware buttons to control the music
        this.volumeControlStream = AudioManager.STREAM_MUSIC
        // Load the sound
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        soundPool!!.setOnLoadCompleteListener(object : SoundPool.OnLoadCompleteListener {
            override fun onLoadComplete(
                soundPool: SoundPool?, sampleId: Int,
                status: Int
            ) {

                loaded = true
                Timber.d("EDPayShakeReward %s %s %s",preference.getString(key_user_id),amount,payment_type)

                //isShaked = true
                //initCashBack(preference.getString(key_user_id),amount!!,paymentType(payment_type!!).asString())

//                Glide.with(this@EDPayShakeReward)
//                    .load(R.drawable.ic_gif_3)
//                    .into(edpay_pin_imageview_merchant_logo)
            }
        })
        soundID = soundPool!!.load(this, R.raw.sound_shake_2, 1)

//        Glide.with(this)
//            .load(R.drawable.ic_gif_2)
//            .into(edpay_pin_imageview_merchant_logo)

        startTimer()

        //edpay_shake_reward_button_keep
    }

    private fun initCashBack(
        customer_id: String,
        transaction_no: String,
        payment_type: String
    ) {
        Timber.d("EDPayShakeReward initCashBack")
        val params = "customer_id:" + customer_id + "||" +
                "transaction_no:" + transaction_no + "||" +
                "payment_type:" + payment_type
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCashback,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString


                if (status == status_success) {

                    val cashback_amount = jsonObj[key_cashback_amount].asString
//                    runOnUiThread(Runnable {
//                        Handler().postDelayed({
                            shakeDetector.stop()
//                            edpay_shake_reward_button_keep.visibility = VISIBLE
//                            edpay_shake_reward_layout_success.visibility = VISIBLE
                           // edpay_shake_reward_textView_amount.text = cashback_amount

                            cashbackAmount = cashback_amount

                                this@EDPayShakeReward.runOnUiThread {
                                    updateAnim(2)
                                }

                            edpay_shake_reward_button_keep.setOnClickListener {

//                                val intent = Intent(
//                                    this@EDPayShakeReward,
//                                    MainActivity::class.java
//                                )
//                                startActivity(intent)
                                finish()
                            }
//                        }, 4000)
//                    })
                } else {
                    shakeDetector.stop()
                    val error_code = jsonObj[key_code].asString
                    //showDialogError(request_error_dialog, error_code)
                    showDialog_APIError("", "tag_edpay_shake_reward_error", false, error_code, "", "OK")
                }
            }
        ) { failure: Throwable? ->
            shakeDetector.stop()
            println("Throwable:$failure")
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "tag_edpay_shake_reward_error",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure?.message,
                "",
                "OK"
            )

        }
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(
        requestTag: String?,
        input_data: String?
    ) {
        val intent: Intent
        when (requestTag) {
            "tag_edpay_shake_reward_error" -> {
                intent = Intent(
                    this@EDPayShakeReward,
                    MainActivity::class.java
                )
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
            else -> {
            }
        }
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hearShake() {
        Timber.d("hearShake")
        val audioManager =
            getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = audioManager
            .getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = audioManager
            .getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val volume = actualVolume / maxVolume
        if (loaded) {
            soundPool!!.play(soundID, volume, volume, 1, 0, 1f)
            isShaked = true
        }
    }



    private fun startTimer() {
        runOnUiThread {
            object : CountDownTimer(5 * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
//                    timerCount = millisUntilFinished / 1000
                    Log.d("TIMER", "Countdown in: " + millisUntilFinished / 1000)
                    updateAnim(1)
                }

                override fun onFinish() {
//                    if(isShaked) {

                    this@EDPayShakeReward.runOnUiThread {
                        initCashBack(
                            preference.getString(key_user_id),
                            amount!!,
                            paymentType(payment_type!!).asString()
                        )
                    }
//                    } else {
//                        showToast_short("EXIT")
//                    }
                }
            }.start()
        }
    }


    private fun updateAnim(animPage: Int) {
        when (animPage) {
            1 -> {
                Glide.with(this)
                    .load(R.drawable.ic_gif_3)
                    .into(edpay_pin_imageview_merchant_logo)

                edpay_shake_reward_textView_header.text = ""
                edpay_shake_reward_textView_desc.text = ""
                edpay_shake_reward_textView_amount.text = ""

                edpay_shake_reward_textView_header.visibility = GONE
                edpay_shake_reward_textView_desc.visibility = GONE
                edpay_shake_reward_textView_amount.visibility = GONE

                edpay_shake_reward_layout_button.visibility = GONE
            }
            2 -> {
                Glide.with(this)
                    .load(R.drawable.ic_gif_4)
                    .into(edpay_pin_imageview_merchant_logo)

                edpay_shake_reward_textView_header.text = "Congratulations!"
                edpay_shake_reward_textView_desc.text = "You\'ve earned"

                var currencyCode = ""

                    when (getCurrentLocationData(tag_current_location_country_code, this@EDPayShakeReward)) {
                        "PH" -> {
                            currencyCode = "PHP"
                        }
                        "NG" -> {
                            currencyCode = "NGN"
                        }
                        "MY" -> {
                            currencyCode = "MYR"
                        }
                        else -> {
                            currencyCode = ""
                        }
                    }

                if(currencyCode == "") {
                    edpay_shake_reward_textView_amount.text = cashbackAmount
                } else {
                    edpay_shake_reward_textView_amount.text = currencyCode + " " + cashbackAmount
                }

                edpay_shake_reward_textView_header.visibility = VISIBLE
                edpay_shake_reward_textView_desc.visibility = VISIBLE
                edpay_shake_reward_textView_amount.visibility = VISIBLE

                edpay_shake_reward_layout_button.visibility = VISIBLE
            }
            else -> {
                Glide.with(this)
                    .load(R.drawable.ic_gif_3)
                    .into(edpay_pin_imageview_merchant_logo)

                edpay_shake_reward_textView_header.text = ""
                edpay_shake_reward_textView_desc.text = ""
                edpay_shake_reward_textView_amount.text = ""

                edpay_shake_reward_textView_header.visibility = GONE
                edpay_shake_reward_textView_desc.visibility = GONE
                edpay_shake_reward_textView_amount.visibility = GONE

                edpay_shake_reward_layout_button.visibility = GONE
            }
        }
    }

    override fun onStart() {
        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        shakeDetector.start(sensorManager)
        super.onStart()
    }

    override fun onStop() {
        shakeDetector.stop()
        super.onStop()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                //initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
