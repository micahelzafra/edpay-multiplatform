//
//  CustomAlert.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/10/20.
//

import Foundation

import UIKit

class CustomAlert: UIView, Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    
    
    convenience init(title:String,image:UIImage,content:String) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title, image: image, content: content)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(title:String, image:UIImage, content:String){
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        backgroundView.alpha = 1
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-64
        
//        let titleLabel = UILabel(frame: CGRect(x: 8, y: 8, width: dialogViewWidth-16, height: 30))
//        titleLabel.text = title
//        titleLabel.textAlignment = .center
//        dialogView.addSubview(titleLabel)
//
//        let separatorLineView = UIView()
//        separatorLineView.frame.origin = CGPoint(x: 0, y: titleLabel.frame.height + 8)
//        separatorLineView.frame.size = CGSize(width: dialogViewWidth, height: 1)
//        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
//        dialogView.addSubview(separatorLineView)
//
        
        let imageView = UIImageView()
        imageView.frame.origin = CGPoint(x: 8, y: 8)
        imageView.frame.size = CGSize(width: dialogViewWidth - 16 , height: dialogViewWidth + 20)
        imageView.image = image
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        dialogView.addSubview(imageView)
        
        let contentLabel = UILabel(frame: CGRect(x: 8, y: imageView.frame.height + imageView.frame.origin.y + 8, width: dialogViewWidth-16, height: 30))
        contentLabel.text = content
        contentLabel.textAlignment = .center
        contentLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        contentLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedShakeLater)))
        contentLabel.isUserInteractionEnabled = true
        dialogView.addSubview(contentLabel)
        
        
        
        let dialogViewHeight =  imageView.frame.height + 8 + contentLabel.frame.height + 8
        
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-80, height: dialogViewHeight )
        dialogView.backgroundColor = UIColor.clear
        dialogView.layer.cornerRadius = 6
        addSubview(dialogView)
    }
    
    @objc func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
    
    @objc func didTappedShakeLater(){
        print("Shake Later")
    }
    
}
