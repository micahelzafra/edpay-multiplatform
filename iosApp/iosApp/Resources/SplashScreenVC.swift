//
//  SplashScreenVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/20/20.
//

import UIKit
import SwiftyGif
import SwiftyJSON
import Firebase
import app

class SplashScreenVC: BaseViewController, EventsDelegate {
    
    internal var apiInterface = ApiInterface()
    
    //XML
    var parser = XMLParser()
    var isStart = false
    var errorElement = ""
    var errorCode = ErrorCode()
    var errorCodearray = [ErrorCode]()
    
    //Timer
    var countdownTimer: Timer!
    var totalTime = 2
    
    //Location
    var isParsingLocation = false

    let logoAnimationView = LogoAnimationView()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            view.addSubview(logoAnimationView)
            logoAnimationView.pinEdgesToSuperView()
            logoAnimationView.logoGifImageView.delegate = self
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            self.defaults.register(defaults: [CommonConstants.KEY_FIRST_TIME_LAUNCH : true])
            logoAnimationView.logoGifImageView.startAnimatingGif()
            Messaging.messaging().delegate = self as? MessagingDelegate
            self.eventDelegate = self
            self.getLocation()
            checkInternet()
        }
    
    func eventData(event: Int, data: AnyObject) {
        if event == CommonConstants.tag_load_current_location
        {
            let location = data as! Dictionary<String, Double>
//            self.log("location \(location[self.constant.key_latitude] ?? 0.00), \(location[self.constant.key_longitude] ?? 0.00)")
            if !isParsingLocation
            {
                getLocationAddress(longitude: "\(location[self.constant.key_longitude] ?? 0.00)", latitude: "\(location[self.constant.key_latitude] ?? 0.00)")
            }
        }
    }

    func getLocationAddress(longitude: String, latitude: String)
    {
        isParsingLocation = true
        var sb = "https://maps.googleapis.com/maps/api/geocode/json"
        sb.append("?key=\("api_key".localized())")
        sb.append("&address=\(latitude),\(longitude)")
        var country_code = ""
        var country_name = ""
        
        apiInterface.parseGoogle(url: sb,
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    if let jsonArray = json[self.constant.key_results].array {
                        for jinx in jsonArray
                        {
                            if let address_components = jinx[self.constant.key_address_components].array
                            {
                                for component in address_components
                                {
                                    if let country = component[self.constant.key_types].array
                                    {
                                        if "\(country)".contains(self.constant.key_country)
                                        {
                                            country_code = component[self.constant.key_short_name].string!
                                            country_name = component[self.constant.key_long_name].string!
                                            self.log("country_name \(country_name), country_code \(country_code)")
                                            self.defaults.set(country_code, forKey: self.constant.key_country_code)
                                            self.defaults.set(country_name, forKey: self.constant.key_country)
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        self.log("jsonArray null")
                    }
                }
                catch let error
                {
                    print(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
        
    func checkInternet()
    {
        if Connectivity.isConnectedToInternet {
            print("Connected to Internet")
            //            FirebaseApp.configure()
            checkVersion()
            self.getLocation()
            
            // [START log_fcm_reg_token]
            let token = Messaging.messaging().fcmToken
            self.log("FCM token: \(token ?? "")")
            let bundle = Bundle.main.bundleIdentifier
            // [END log_fcm_reg_token]
            //            Messaging.messaging().subscribe(toTopic: GlobalConstants.FIREBASE_TOPIC)
            //            Messaging.messaging().subscribe(toTopic: "\(GlobalConstants.FIREBASE_TOPIC)TESTING")
            Messaging.messaging().subscribe(toTopic: bundle ?? "") { error in
                print("Subscribed to \(bundle ?? "")")
            }
            #if DEBUG
            Messaging.messaging().subscribe(toTopic: "\(bundle ?? "")TESTING") { error in
                print("Subscribed to \(bundle ?? "")TESTING_TOPIC")
            }
            #else
            print("Release App")
            #endif
            
            InstanceID.instanceID().instanceID { (result, error) in
              if let error = error {
                print("Error fetching remote instance ID: \(error)")
              } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.defaults.set(result.token, forKey: self.constant.key_device_token)
              }
            }
        }
        else
        {
            self.handleError("No Internet connection")
        }
    }

    private func initAccessCode(){
        apiInterface.parseAPI(access_code: constant.secretKeyCode, url: constant.urlAccessCode,
            params: "",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                if let accessCode = json[self.constant.key_data][self.constant.key_access_code].string {
                    self.defaults.set(accessCode, forKey: self.constant.key_access_code)
                }
                else
                {
                    self.log("accessCode null")
                }

                self.initErrorCode()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }

    private func initErrorCode(){

        let urlString = URL(string: constant.urlErrorXML)
        self.parser = XMLParser(contentsOf: urlString!)!
        self.parser.delegate = self
        let success:Bool = self.parser.parse()
        if success {
         print("success")
//                for errorCode in errorCodearray
//                {
//                    print("errorCode \(errorCode.error_code)")
//                }

            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: errorCodearray)
            self.defaults.set(encodedData, forKey: self.constant.key_error_list)
        } else {
            print("parse failure!")
        }
        
        self.startTimer()
    }
        
//        internal func handleError(_ error: String?){
//            self.log("handleError \(error ?? "")")
//            let message = error ?? "An unknown error occurred. Retry?"
//            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
//            self.present(alert, animated: true)
//        }
    
    func checkVersion()
    {
        let db = Firestore.firestore()
        
        db.collection("app_ver").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    if (document.get("ios") != nil)
                    {
                        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
                        let currentVersion = nsObject as! String
                        let storeVersion = document.get("ios") as? String
                        self.log("storeVersion: \(storeVersion!), currentVersion:\(currentVersion)")
                        self.log("")
                        if storeVersion?.compare(currentVersion, options: .numeric) == .orderedDescending {
                            print("store version is newer")
                            
                            let msg = "dialog_app_not_updated".localized()
                            if msg != ""
                            {
                                let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                                
                                let action = UIAlertAction(title: "Dismiss", style: .default) { (action:UIAlertAction) in
                                    print("You've pressed default");
                                    
                                    
                                    /* First create a URL, then check whether there is an installed app that can
                                     open it on the device. */
                                    if let url = URL(string: CommonConstants.URL_APP_STORE), UIApplication.shared.canOpenURL(url) {
                                        // Attempt to open the URL.
                                        UIApplication.shared.open(url, options: [:], completionHandler: {(success: Bool) in
                                            if success {
                                                self.dismiss(animated: false)
                                                print("Launching \(url) was successful")
                                            }})
                                    }
                                }
                                alert.addAction(action)
                                
                                self.present(alert, animated: true)
                            }
                            else
                            {
                                print("error parsing the string file")
                            }
                        }
                        else
                        {
                            print("app version is newer")
                            self.initAccessCode()
                        }
                    }
                }
            }
        }
    }
        
    func startTimer() {
        print("start timer")
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        if countdownTimer != nil
        {
            print("\(timeFormatted(totalTime))")
            if totalTime != 0 {
                totalTime -= 1
            } else {
                endTimer()
            }
        }
    }
    
    func endTimer() {
        print("end timer")
        countdownTimer.invalidate()
        countdownTimer = nil
        
        let value = defaults.bool(forKey: CommonConstants.KEY_FIRST_TIME_LAUNCH)
        if value == true
        {
            self.performSegue(withIdentifier: "segueOnBoarding", sender: nil)
//            let appearanceConfiguration = OnboardViewController.AppearanceConfiguration(
//                titleFont: UIFont.systemFont(ofSize: 22.0),
//                textFont: UIFont.systemFont(ofSize: 14.0))
//            let onboardingVC = OnboardViewController(pageItems: onboardingPages,
//                                                     appearanceConfiguration: appearanceConfiguration)
//            //            let onboardingVC = OnboardViewController(pageItems: onboardingPages)
//            onboardingVC.modalPresentationStyle = .formSheet
//            onboardingVC.presentFrom(self, animated: true)
        }
        else{
            
            if let autologin = defaults.string(forKey: constant.key_user_id)
            {
                print("autologin \(autologin)")
                self.performSegue(withIdentifier: "segueMain", sender: nil)
            }
            else
            {
                self.performSegue(withIdentifier: "segueLoginOrRegister", sender: nil)
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
}

extension SplashScreenVC: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        errorElement = elementName
        if(elementName=="error_details")
        {
            isStart = true
            errorCode = ErrorCode()
        }

    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if(elementName=="error_details")
        {
            print("errorElement: ", errorCode.error_code)
            errorCodearray.append(errorCode)
            isStart = false
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if (isStart)
        {
            switch errorElement {
                case "error_code":
                    errorCode.error_code = string
                break
                case "error_message":
                    errorCode.error_message = string
                break
                case "error_title":
                    errorCode.error_title = string
                break
              default:
                break
            }
            
        }
    }

    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("failure error: ", parseError)
    }
}

extension SplashScreenVC: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
//            self.performSegue(withIdentifier: "segueLoginOrRegister", sender: nil)
    }
}
