//
//  ActivityFoodVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/24/20.
//

import UIKit
import SwiftyJSON
import app

class ActivityFoodVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var tfDateFrom: DesignableTextField!
    @IBOutlet weak var tfDateTo: DesignableTextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datePicker : UIDatePicker?
    private var datePickerTo : UIDatePicker?
    var foodList = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePickerTo = UIDatePicker()
        datePickerTo?.datePickerMode = .date
        tfDateFrom.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(ActivityFoodVC.dateChanged(datePicker:)), for: .valueChanged)
        tfDateTo.inputView = datePickerTo
        datePickerTo?.addTarget(self, action: #selector(ActivityFoodVC.dateChangedTo(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActivityFoodVC.viewTapped(gestureRecognizer:)))
            view.addGestureRecognizer(tapGesture)
            
        //Write toolbar code for done button
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        tfDateFrom.inputAccessoryView = toolBar
        tfDateTo.inputAccessoryView = toolBar
    }
    
    //Toolbar done button function
    @objc func onClickDoneButton() {
        self.view.endEditing(true)
        getFoodLogs(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            date_from: dateFormat(date: self.tfDateFrom.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString(),
            date_to: dateFormat(date: self.tfDateTo.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString()
        )
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        getFoodLogs(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            date_from: dateFormat(date: self.tfDateFrom.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString(),
            date_to: dateFormat(date: self.tfDateTo.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString()
        )
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        
        let format = DateFormatter()
            format.dateFormat = constant.date_format_picker_display
        
        self.datePicker?.date = datePicker.date
        self.datePickerTo?.minimumDate = datePicker.date
        tfDateFrom.text = format.string(from: datePicker.date)
    }
    
    @objc func dateChangedTo(datePicker: UIDatePicker) {
        
        let format = DateFormatter()
            format.dateFormat = constant.date_format_picker_display
        
        self.datePickerTo?.date = datePicker.date
        self.datePicker?.maximumDate = datePicker.date
        tfDateTo.text = format.string(from: datePicker.date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = constant.date_format_server_api
        let formattedDate = format.string(from: date)
        print(formattedDate)
        
        let calendar = Calendar.current
        let lastMonthDate = Calendar.current.date(byAdding: .month, value: -1, to: date)
        calendar.component(.month, from:lastMonthDate!)

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = constant.date_format_server_api
        
        datePicker?.date = lastMonthDate!
        datePicker?.maximumDate = date
        datePickerTo?.date = date
        datePickerTo?.minimumDate = lastMonthDate!
        
        self.tfDateFrom.text = dateFormat(date: dateFormatterPrint.string(from: lastMonthDate!),from: constant.date_format_server_api,to: constant.date_format_picker_display).asString()
        self.tfDateTo.text = dateFormat(date: formattedDate,from: constant.date_format_server_api,to: constant.date_format_picker_display).asString()
        getFoodLogs(customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            date_from: dateFormatterPrint.string(from: lastMonthDate!),
            date_to: formattedDate
        )
    }
    
    // MARK: - Table view data source
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if foodList.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return foodList.count
      }
      
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityFoodTableViewCell", for: indexPath) as! ActivityFoodTableViewCell
            
            let data = foodList[indexPath.row]
            let booked_id = data[self.constant.key_booked_id].int
            let booked_date = data[self.constant.key_booked_date].string
            let booked_status = data[self.constant.key_booked_status].int
            let total = data[self.constant.key_total].string
            
            let food = ActivityFood(
            orderID: "ORDER ID - \(booked_id ?? -1)",
            status: "\(String(describing: getOrderStatus(status: "\(booked_status ?? 1)").asString())) - EDFOOD",
            totalAmount: "Total Order Amount - \(String(describing: commaSeparated(value: "\(total ?? "")").asString()))",
            date_and_time: getLocalTimeStamp(date: booked_date!).asString())
            cell.lblStatus.text = food.status
            cell.lblOrderID.text = food.orderID
            cell.lblTotalAmount.text = food.totalAmount
            cell.lblDateAndTime.text = food.date_and_time

            var segue = ""
            if (food.status?.contains("Delivered"))!
            {
                cell.lblStatus.textColor = #colorLiteral(red: 0.06666666667, green: 0.6901960784, blue: 0, alpha: 1)
                segue = "segueEDFoodDetails"
            }
            else if (food.status?.contains("Pending"))!
            {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9333333333, green: 0.7450980392, blue: 0.01960784314, alpha: 1)
                segue = "segueMap"
            }
            else
            {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9450980392, green: 0.04705882353, blue: 0.05882352941, alpha: 1)
                segue = "segueEDFoodDetails"
            }

            cell.buttonCallback = { [unowned self] in
              var segueData = Dictionary<String, AnyObject>()
              segueData[self.constant.key_data] = data as AnyObject
              self.performSegue(withIdentifier: segue, sender: segueData)
            }
            return cell
        }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 110
      }
      
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if (segue.identifier == "segueMap") {

              if let navController = segue.destination as? UINavigationController {
                  if let chidVC = navController.topViewController as? MapVC {
                          self.log("chidVC \(chidVC)")
                          chidVC.segueData = (sender as? [String:AnyObject])!
                  }
              }
          }
          else if (segue.identifier == "segueEDFoodDetails") {

              if let navController = segue.destination as? UINavigationController {
                  if let chidVC = navController.topViewController as? OrderHistoryVC {
                          self.log("chidVC \(chidVC)")
                          chidVC.segueData = (sender as? [String:AnyObject])!
                  }
              }
          }
      }
          
      private func getFoodLogs(
          customer_id: String,
          date_from: String,
          date_to: String
      ) {
        showLoading()
          apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlOrderHistory,
              params: "customer_id:\(customer_id)||start_date:\(date_from)||end_date:\(date_to)",
              success: { data in
                  let jsonData = data.data(using: String.Encoding.utf8)!
                  let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                  let json: JSON = JSON(jsonDic)
                  
                  let status = json[self.constant.key_status].string
                  if status == self.constant.status_success {

                    if let jsonArray = json[self.constant.key_booked].array
                    {
                        for data in jsonArray
                        {
                            self.foodList.append(data)
                        }
                        let sorted = self.foodList.sorted { $0[self.constant.key_created_date] < $1[self.constant.key_created_date] }
                        self.foodList = sorted
                    }
                    self.tableView.reloadData()
                    self.hideLoading()
                  }
                  else
                  {
                      let error = json[self.constant.key_code].string
                      self.handleError(error)
                        
                  }

                  return KotlinUnit()
          }, failure: {
              self.handleError($0?.message)
              return KotlinUnit()
          })
          
      }
}
