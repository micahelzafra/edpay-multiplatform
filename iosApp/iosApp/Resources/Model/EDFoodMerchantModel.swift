//
//  EDFoodMerchantModel.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/2/20.
//

import Foundation

struct EDFoodMerchant {
    
    var foodImage : String?
    var foodName : String?
    var foodRange : String?
    var foodLocation : String?
    var foodStatus : String?
    
}
