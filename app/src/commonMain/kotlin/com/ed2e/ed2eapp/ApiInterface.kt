package com.ed2e.ed2eapp

import com.rba.mpp.shared.ApplicationDispatcher
import io.ktor.client.HttpClient
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.url
import kotlinx.coroutines.*

class ApiInterface {

    private val httpClient = HttpClient()
    {
        expectSuccess = false
    }

    fun parseAPI(access_code: String, url: String, params: String ,success: (String) -> Unit, failure: (Throwable?) -> Unit) {

        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val paramList = ArrayList<Any>()
                val json = httpClient.post<String> {
                    url(url)
                    header("Secret-Key", access_code)
                    header("Access-Code", access_code)
                    if (!params.isBlank() || !params.isEmpty())
                    {
                        val parameters = params.split("||")
                        for(param in parameters)
                        {
                            val value = param.split(":".toRegex(),2)
                            parameter(value[0] , value[1])
                            paramList.add(param)
                        }
                    }
                }
                println("ApiInterface:"+"\nURL:"+url+"\nParams:$paramList"+"\nResponse:"+json)
                json.also(success)
            } catch (ex: Exception) {
                failure(ex)
            }
        }
    }

    fun parseGoogle(url: String,success: (String) -> Unit, failure: (Throwable?) -> Unit) {

        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val json = httpClient.post<String> {
                    url(url)
                }
                println("ApiInterface:"+"\nURL:"+url+"\nResponse:"+json)
                json.also(success)
            } catch (ex: Exception) {
                failure(ex)
            }
        }
    }
}