//
//  EDFoodPaymentSuccessVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/2/20.
//

import UIKit

class EDFoodPaymentSuccessVC: BaseViewController {
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblOrderAmount: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblNoteToRider: UILabel!
    
    
    @IBOutlet weak var btnTrackOrder: DesignableButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblOrderID.text = segueData[self.constant.key_booked_id] as? String
        self.lblOrderAmount.text = segueData[self.constant.key_total] as? String
        self.lblPaymentMethod.text = segueData[self.constant.key_payment_method] as? String
        self.lblNoteToRider.text = segueData[self.constant.key_special_notes] as? String
    }
    @IBAction func didTapButtonTrackOrder(_ sender: Any) {
        self.performSegue(withIdentifier: "segueMain", sender: nil)
        
    }
    
}
