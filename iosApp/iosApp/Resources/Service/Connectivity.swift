//
//  Connectivity.swift
//  iosApp
//
//  Created by ED2E Technology dev-michaeljinx on 1/24/20.
//

import Foundation
import Alamofire
class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
