//
//  IntegerExtension.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-MichaelJinx on 1/28/20.
//

import Foundation

extension Double
{
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? "0.00"
    }
}


