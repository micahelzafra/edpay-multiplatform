//
//  EDFoodAddToCartTableViewCell.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/2/20.
//

import UIKit

class EDFoodAddToCartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewAddOns: UIView!
    @IBOutlet weak var btnCheckUncheck: DesignableButton!
    @IBOutlet weak var lblAddOnsName: UILabel!
    @IBOutlet weak var lblAddOnsPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnCheckBox(_ sender: DesignableButton) {
//        sender.isSelected = !sender.isSelected
    }
}
