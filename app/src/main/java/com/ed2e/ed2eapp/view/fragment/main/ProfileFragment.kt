package com.ed2e.ed2eapp.view.fragment.main

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.R.layout.child_profile
import com.ed2e.ed2eapp.R.layout.fragment_profile
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.model.Profile
import com.ed2e.ed2eapp.util.*
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber


class ProfileFragment : Fragment(fragment_profile)
{
    lateinit var textviewName: TextView
    lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {
            recyclerView = view.findViewById(R.id.fragment_profile_recyclerview_list)
            val items:Array<String> = resources.getStringArray(R.array.tab_profile_items) //already array
            //val icons: IntArray = resources.getIntArray(R.array.tab_profile_items_ic) //already array
            val icons = resources.obtainTypedArray(R.array.tab_profile_items_ic)

            val list= ArrayList<Any>()
            for (i in 0..items.size-1)
            {
                list.add(Profile(items.get(i),icons.getResourceId(i, -1)))
            }
            val myAdapter = object : BaseRecyclerViewAdapter<Any>(list, object : ItemClickListener<Any>{
                override fun onItemClick(v: View, item: Any, position: Int) {

                    Timber.d("onItemClick %s",item)
                    if (position == 0)
                    {
                        RxBus.publish(RxEvent.EventMainActivity(tag_intent_my_account_activity))
                    }
                    else if (position == 1)
                    {
                        RxBus.publish(RxEvent.EventMainActivity(tag_intent_referrals_activity))
                    }
                    else if (position == 2)
                    {
                        RxBus.publish(RxEvent.EventMainActivity(tag_intent_help_activity))
                    }
                    else if (position == 3)
                    {
                        RxBus.publish(RxEvent.EventMainActivity(tag_intent_login_activity))
                    }
                    else
                    {
                        RxBus.publish(RxEvent.EventMainActivity(tag_intent_login_activity))
                    }
                }
            }) {
                override fun getLayoutId(position: Int, obj: Any): Int {
                    return child_profile
                }

                override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                    return ViewHolderFactory.create(view,viewType)
                }
            }
            recyclerView.layoutManager= LinearLayoutManager(context)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter=myAdapter
            RxBus.publish(RxEvent.EventMainActivity(tag_load_fragment_profile,this))
        }
    }
}