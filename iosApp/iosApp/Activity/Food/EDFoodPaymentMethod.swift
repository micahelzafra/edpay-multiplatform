//
//  EDFoodPaymentMethod.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/2/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class EDFoodPaymentMethod: BaseViewController {
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    
    //views
    @IBOutlet weak var viewCreditCard: UIView!
    @IBOutlet weak var viewCashOnDelivery: UIView!
    @IBOutlet weak var viewEDCredits: UIView!
    
    @IBOutlet weak var imageViewCreditCard: UIImageView!
    @IBOutlet weak var imageViewCashOnDelivery: UIImageView!
    @IBOutlet weak var imageViewEDCredits: UIImageView!
    
    @IBOutlet weak var btnPlaceOrder: DesignableButton!
    
    var selected = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapCreditCard = UITapGestureRecognizer(target: self, action: #selector(self.tapCreditCard(sender:)))
        viewCreditCard.addGestureRecognizer(tapCreditCard)
        let tapCOD = UITapGestureRecognizer(target: self, action: #selector(self.tapCOD(sender:)))
        viewCashOnDelivery.addGestureRecognizer(tapCOD)
        let tapEDCredits = UITapGestureRecognizer(target: self, action: #selector(self.tapEDCredits(sender:)))
        viewEDCredits.addGestureRecognizer(tapEDCredits)
        selectedPayment()
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: false)
    }
    @IBAction func didTapButtonPlaceOrder(_ sender: Any) {
        var title = ""
        var msg = ""
        if selected == 1
        {
            title = "\(self.constant.label_cod) Payment"
            msg = "\(self.constant.label_dialog_msg_cod_confirm.replacingOccurrences(of: "%s", with: segueData[self.constant.key_total] as! String)) Payment"
        }
        else
        {
            title = "\(self.constant.label_edcredits) Payment"
            msg = "\(self.constant.label_dialog_msg_edcredit_confirm.replacingOccurrences(of: "%s", with: segueData[self.constant.key_total] as! String)) Payment"
        }
        
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: self.constant.button_yes, style: .default, handler: { action in
            self.initUpdateOrderStatus(
                booked_id: self.segueData[self.constant.key_booked_id] as! String,
                country_name: self.segueData[self.constant.key_country] as! String,
                approver_id: self.defaults.string(forKey: self.constant.key_user_id)!,
                payment_type: "\(self.selected)"
            )
        }))
        alert.addAction(UIAlertAction(title: self.constant.button_no, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapCreditCard(sender: UITapGestureRecognizer) {
        selected = 0
//        selectedPayment()
    }
    
    @objc func tapCOD(sender: UITapGestureRecognizer) {
        selected = 1
        selectedPayment()
    }
    
    @objc func tapEDCredits(sender: UITapGestureRecognizer) {
        selected = 2
        selectedPayment()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueSuccess") {
            if let chidVC = segue.destination as? EDFoodPaymentSuccessVC {
                self.log("chidVC \(chidVC)")
                chidVC.segueData = (sender as? [String:AnyObject])!
            }
        }
    }
    
    private func selectedPayment()
    {
        if selected == 0
        {
            self.imageViewCreditCard.image = UIImage(named: "ordered_address")
            self.imageViewCashOnDelivery.image = UIImage(named: "delivery_address")
            self.imageViewEDCredits.image = UIImage(named: "delivery_address")
        }
        else if selected == 1
        {
            self.imageViewCreditCard.image = UIImage(named: "delivery_address")
            self.imageViewCashOnDelivery.image = UIImage(named: "ordered_address")
            self.imageViewEDCredits.image = UIImage(named: "delivery_address")
        }
        else
        {
            self.imageViewCreditCard.image = UIImage(named: "delivery_address")
            self.imageViewCashOnDelivery.image = UIImage(named: "delivery_address")
            self.imageViewEDCredits.image = UIImage(named: "ordered_address")
        }
    }
    
    private func initUpdateOrderStatus(
        booked_id: String,
        country_name: String,
        approver_id: String,
        payment_type: String)
    {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlUpdateOrderStatus,
                              params: "booked_id:\(booked_id)||country_name:\(country_name)||approver_id:\(approver_id)||payment_type:\(payment_type)||booked_status:\(self.constant.order_placed)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    
                    if status == self.constant.status_success
                    {
                        var payment_method = self.constant.label_credit_debit;
                        if payment_type == "1"
                        {
                            payment_method = self.constant.label_cod;
                        }
                        else if payment_type == "2"
                        {
                            payment_method = self.constant.label_edcredits;
                        }
                        
                        if let notifID = self.defaults.object(forKey: self.constant.pref_jinxnotif_bookedid)
                        {
                            var arrayList = [JSON]()
                            arrayList.append([
                                self.constant.key_booked_id: booked_id
                            ])
                            
                            if let jsonArray = JSON(notifID).array
                            {
                                for item in jsonArray
                                {
                                    arrayList.append(item)
                                }
                            }
                            self.defaults.set("\(arrayList)", forKey: self.constant.pref_jinxnotif_bookedid)
                        }
                        else
                        {
                            var arrayList = [JSON]()
                            arrayList.append([
                                self.constant.key_booked_id: booked_id
                            ])
                            self.defaults.set("\(arrayList)", forKey: self.constant.pref_jinxnotif_bookedid)
                        }
                        
                        var segueData = Dictionary<String, AnyObject>()
                        segueData[self.constant.key_booked_id] = self.segueData[self.constant.key_booked_id] as AnyObject
                        segueData[self.constant.key_total] = self.segueData[self.constant.key_total] as AnyObject
                        segueData[self.constant.key_payment_method] = payment_method as AnyObject
                        segueData[self.constant.key_special_notes] = self.segueData[self.constant.key_special_notes] as AnyObject
                        self.performSegue(withIdentifier: "segueSuccess", sender: segueData)
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                    }
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
}
