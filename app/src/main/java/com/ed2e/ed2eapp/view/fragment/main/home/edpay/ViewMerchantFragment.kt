package com.ed2e.ed2eapp.view.fragment.main.home.edpay

//import com.ed2e.ed2eapp.base.BaseActivity;
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.R.layout.fragment_view_merchant
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.EDPayActivity
import com.ed2e.ed2eapp.view.activity.help.HelpActivity
import com.ed2e.ed2eapp.view.activity.main.home.edfood.EDFoodMerchantItemActivity
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayMerchantDetailsActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonParser
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_edfood_merchants.*
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList


class ViewMerchantFragment : Fragment(fragment_view_merchant)
{
    lateinit var recyclerView: RecyclerView
    lateinit var editext_search: EditText
    lateinit var textView_country: TextView
    var isFragmentVisible = false
    private var disposables: CompositeDisposable? = null
    private var arraylist = ArrayList<Any>()
    private var arraylistSearch = ArrayList<Any>()
    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    fun getActionbar() : ActionBar?
    {
        return (activity as AppCompatActivity).supportActionBar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {
            recyclerView = view.findViewById(R.id.fragment_view_merchant_recyclerview)
            editext_search = view.findViewById(R.id.fragment_view_merchant_edittext_search)
            textView_country = view.findViewById(R.id.fragment_view_merchant_textview_location)



            val changeObservableSearch: Observable<CharSequence> = editext_search.textChanges()
            disposables = CompositeDisposable()
            disposables!!.add(changeObservableSearch.map { inputText: CharSequence -> inputText.length == 0 }
                .subscribe { isValid: Boolean ->
                    Timber.d("isValid:$isValid")
                    if (!isValid) {
                        val parser = JsonParser()
                        arraylistSearch.clear()
                        for (items in arraylist)
                        {
                            val jsonObj = parser.parse(items.toString()).asJsonObject
                            val name = jsonObj[key_trade_name].asString
                            val desc = jsonObj[key_product_services].asString
                            val business_address = jsonObj[key_business_address].asString

                            if (name.toLowerCase(Locale.US).contains(editext_search.text.toString().toLowerCase(Locale.US))||business_address.toLowerCase(Locale.US).contains(editext_search.text.toString().toLowerCase(Locale.US)))
                            {
                                arraylistSearch.add(items)
                            }
                        }
                        updateList()
                    } else {
                        try {
                            arraylistSearch.clear()
                            arraylistSearch.addAll(arraylist)
                            updateList()
                        }
                        catch (e:UninitializedPropertyAccessException)
                        {
                            Timber.d("UninitializedPropertyAccessException:$e")
                        }
                    }
                }
            )

            adapter = object : BaseRecyclerViewAdapter<Any>(arraylistSearch, object :
                ItemClickListener<Any> {
                override fun onItemClick(v: View, item: Any, position: Int) {

                    val parser = JsonParser()
                    val jsonObj = parser.parse(item.toString()).asJsonObject
                    val merchant_id = jsonObj[key_id].asString

                    Log.d("Logs", "Clicked: " + jsonObj)

                    val intent : Intent
                    intent = Intent(context, EDPayMerchantDetailsActivity::class.java)
                    intent.putExtra(key_id, merchant_id)
                    startActivity(intent)
                    //activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                }
            }) {
                override fun getLayoutId(position: Int, obj: Any): Int {
                    return R.layout.child_edpay_merchant
                }

                override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                    return ViewHolderFactory.create(view,viewType)
                }
            }
            recyclerView.layoutManager= LinearLayoutManager(context)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter=adapter
            if (isFragmentVisible)
            {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_edpay_merchant, this))
            }
        }
    }

    override fun setMenuVisibility(visible: Boolean) {
        super.setMenuVisibility(visible)
        isFragmentVisible = visible
    }

    fun displayList(list:ArrayList<Any>)
    {
        activity?.runOnUiThread {
            arraylist.clear()
            arraylist.addAll(list)
            arraylistSearch.clear()
            arraylistSearch.addAll(arraylist)
            updateList()
        }
    }

    private fun updateList()
    {
        val parser = JsonParser()
        Collections.sort(arraylistSearch,
            Comparator {
                    jinks, jinx -> parser.parse(jinx.toString()).asJsonObject[key_trade_name].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_trade_name].asString)
            })
        adapter.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView")

        if(disposables != null) {
            // Using clear will clear all, but can accept new disposable
            disposables!!.clear()
            // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
            disposables!!.dispose()
        }

    }
}