package com.ed2e.ed2eapp.view.activity.referrals;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_referral_code;

public class ReferralsActivity extends BaseActivity {

    @BindView(R.id.referrals_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.referrals_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.referrals_linearLayout_code)
    LinearLayout linearLayout_code;
    @BindView(R.id.referrals_textView_code)
    TextView textView_code;

    String referralCode;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referrals);
        ButterKnife.bind(this);

        preference = getInstance(this);

        initToolBar();
        initData();
        initGUI();
    }

    private void initData() {
        referralCode = preference.getString(key_referral_code, "");
    }

    private void initGUI() {

        textView_code.setText(referralCode);

        linearLayout_code.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("referral-code", textView_code.getText().toString().trim());
            clipboard.setPrimaryClip(clip);
            showToast_short("Copied to clipboard");
        });
    }


//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }






    private void initPreviousActivity() {
//        Intent intent = new Intent(getContext(), MainActivity.class);
//        startActivity(intent);
        finish();
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "button_ok":
                showToast_short("To Homescreen");
//                Intent intent = new Intent(getContext(), Login1Activity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
