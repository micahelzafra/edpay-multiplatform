//
//  ViewControllerExtension.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/22/20.
//

import Foundation
import UIKit
import SVProgressHUD

extension UIViewController  {

    func keyboardCheck() {
      NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        {
            
            if self.view.frame.origin.y == 0 {
                if UIDevice.current.screenType == .iPhones_5_5s_5c_SE  {
                     self.view.frame.origin.y -= (keyboardSize.height - 150)
                } else  if UIDevice.current.screenType == .iPhones_6_6s_7_8 {
                    self.view.frame.origin.y -= (keyboardSize.height - 210)
                } else {
                    self.view.frame.origin.y = 0
                }
               
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showLoading() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setContainerView(view)
        SVProgressHUD.setForegroundColor(#colorLiteral(red: 0.006476058159, green: 0.07246912271, blue: 0.476346612, alpha: 1))
        SVProgressHUD.show(withStatus: "Please wait...")
    }
    
    func hideLoading() {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
    }
    
    
//    func addDoneToDatePicker() {
//
//        //Write toolbar code for done button
//        let toolBar = UIToolbar()
//        toolBar.barStyle = .default
//        toolBar.isTranslucent = true
//        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
//        toolBar.setItems([space, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        toolBar.sizeToFit()
//    }
//    
//    //Toolbar done button function
//       @objc func onClickDoneButton() {
//           self.view.endEditing(true)
//       }

}
