package com.ed2e.ed2eapp.view.fragment.main.home.edpay

import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ed2e.ed2eapp.R
import com.google.android.gms.vision.barcode.Barcode
import com.notbytes.barcode_reader.BarcodeReaderFragment
import com.notbytes.barcode_reader.BarcodeReaderFragment.BarcodeReaderListener
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber

class ScanMerchantFragment : Fragment(), BarcodeReaderListener {
    private var barcodeReader: BarcodeReaderFragment? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_scan_merchant, container, false)
        if (userVisibleHint) {
            initGUI()
        }
        return view
    }

    override fun onScanned(barcode: Barcode) {
        Timber.d("onScanned: %s", barcode.displayValue)
//        barcodeReader!!.playBeep()
//        Toast.makeText(
//            activity,
//            "Barcode: " + barcode.displayValue,
//            Toast.LENGTH_SHORT
//        ).show()
        if (getView() != null) {
            if (userVisibleHint) {
                RxBus.publish(RxEvent.EventScanQR(barcode.displayValue))
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI()
            }
        }
    }

    private fun initGUI() {
        barcodeReader = childFragmentManager.findFragmentById(R.id.barcode_fragment) as BarcodeReaderFragment?
        barcodeReader!!.setListener(this)
    }

    override fun onScannedMultiple(barcodes: List<Barcode>) {
        Timber.e(
            TAG,
            "onScannedMultiple: %s", barcodes.size
        )
        var codes = ""
        for (barcode in barcodes) {
            codes += barcode.displayValue + ", "
        }
        val finalCodes = codes
        Toast.makeText(
            activity,
            "Barcodes: $finalCodes",
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>) {}
    override fun onScanError(errorMessage: String) {
        Timber.e(TAG, "onScanError: $errorMessage")
    }

    override fun onCameraPermissionDenied() {
        Toast.makeText(
            activity,
            "Camera permission denied!",
            Toast.LENGTH_LONG
        ).show()
    }

    companion object {
        private val TAG = ScanMerchantFragment::class.java.simpleName
        fun newInstance(): ScanMerchantFragment {
            val args = Bundle()
            val fragment =
                ScanMerchantFragment()
            fragment.arguments = args
            return fragment
        }
    }
}