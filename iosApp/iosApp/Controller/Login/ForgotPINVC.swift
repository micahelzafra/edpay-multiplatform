//
//  ForgotPINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/13/20.
//

import UIKit
import CBPinEntryView

class ForgotPINVC: UIViewController {
    
    @IBOutlet weak var forgotPinView: CBPinEntryView! {
        didSet {
                    forgotPinView.delegate = self as? CBPinEntryViewDelegate
                }
        }
    
    @IBOutlet weak var lbl60: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var btnSubmit: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
         performSegue(withIdentifier: "segueResetMobilePIN", sender: nil)
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
