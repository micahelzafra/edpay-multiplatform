//
//  DesignableButton.swift
//  EDPoints App
//
//  Created by ED2E Technology iOS on 19/12/2018.
//  Copyright © 2018 ED2E Technology. All rights reserved.
//

import UIKit

 @IBDesignable
class DesignableButton: UIButton {
    
    @IBInspectable var adjustsTitleFontSizeToFitWidth: Bool = true {
        didSet {
            self.titleLabel?.adjustsFontSizeToFitWidth = adjustsTitleFontSizeToFitWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var topLeft: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var topRight: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var bottomLeft: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var bottomRight: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    // Textfield border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    //Textfield border color
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    func refreshCR(_value: CGFloat) {
        layer.cornerRadius = _value
        var corners = CACornerMask()
        if topLeft
        {
            corners.insert(.layerMinXMinYCorner)
        }
        if topRight
        {
            corners.insert(.layerMaxXMinYCorner)
        }
        if bottomLeft
        {
            corners.insert(.layerMinXMaxYCorner)
        }
        if bottomRight
        {
            corners.insert(.layerMaxXMaxYCorner)
        }
        
        if #available(iOS 11.0, *) {
            layer.maskedCorners = corners
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBInspectable var customBGColor: UIColor = UIColor.init(red: 0, green: 122/255, blue: 255/255, alpha: 1) {
        didSet {
            refreshColor(_color: customBGColor)
        }
    }
    
    func refreshColor(_color: UIColor) {
        print("refreshColor(): \(_color)")
        let size: CGSize = CGSize(width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        _color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let bgImage: UIImage = (UIGraphicsGetImageFromCurrentImageContext())!
        UIGraphicsEndImageContext()
        setBackgroundImage(bgImage, for: UIControl.State.normal)
        clipsToBounds = true
    }
    
    override init(frame: CGRect) {
        print("init(frame:)")
        super.init(frame: frame);
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        print("init?(coder:)")
        super.init(coder: aDecoder)
        
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        print("prepareForInterfaceBuilder()")
        sharedInit()
    }
    
    func sharedInit() {
        refreshCR(_value: cornerRadius)
        refreshColor(_color: customBGColor)
        self.tintColor = UIColor.white
    }
    
    @IBInspectable var centerImage: UIImage? {
        
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var letfImage: UIImage? {
           
           didSet {
               updateView()
           }
       }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
           didSet {
               updateView()
           }
       }
    
    @IBInspectable var imageSize: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    
    func updateView () {
        
        if let image = centerImage {
            
//            let imageView = UIImageView(frame: CGRect(x: (self.frame.size.width-imageSize)/2, y: (self.frame.size.height-imageSize)/2, width: imageSize, height: imageSize))
//            imageView.image = image
//            imageView.tintColor = tintColor
//            self.addSubview(imageView)
            
            self.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
            self.setImage(image.withRenderingMode(.alwaysOriginal), for: .highlighted)
            
        }
        
    }

    override func setTitle(_ title: String?, for state: UIControlState) {
        UIView.performWithoutAnimation {
            super.setTitle(title, for: state)
            super.layoutIfNeeded()
        }
    }
}



