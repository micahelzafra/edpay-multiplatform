package com.ed2e.ed2eapp.util;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreference {
    private SharedPreferences prefs;
    private static AppPreference prefManager;


    private AppPreference(Context context) {
        String PREFS_FILE = context.getPackageName();
        prefs = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
    }

    public static AppPreference getInstance(Context context) {
        if (prefManager == null) {
            prefManager = new AppPreference(context);
        }
        return prefManager;
    }

    public void putString(String key, String value) {
        if (prefs != null) {
            prefs.edit().putString(key, value).apply();
        }
    }

    public String getString(String key, String... defValue) {
        String value = defValue.length > 0 ? defValue[0] : null;
        if (prefs != null) {
            return prefs.getString(key, value);
        }
        return value;
    }

    public void putLong(String key, long value) {
        if (prefs != null) {
            prefs.edit().putLong(key, value).apply();
        }
    }

    public long getLong(String key) {
        long value = 0;
        if (prefs != null) {
            return prefs.getLong(key, value);
        }
        return value;
    }

    public void putBoolean(String key, boolean value) {
        if (prefs != null) {
            prefs.edit().putBoolean(key, value).apply();
        }
    }

    public boolean getBoolean(String key, boolean... defValue) {
        boolean value = defValue.length > 0 && defValue[0];
        if (prefs != null) {
            return prefs.getBoolean(key, value);
        }
        return value;
    }
}
