//
//  RegisterVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/16/20.
//

import UIKit
import SwiftyJSON
import app


class RegisterVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var tfFirstName: DesignableTextField!
    @IBOutlet weak var tfLastName: DesignableTextField!
    @IBOutlet weak var tfPrefix: DesignableTextField!
    @IBOutlet weak var tfMobileNumber: DesignableTextField!
    @IBOutlet weak var tfEmailAddress: DesignableTextField!
    @IBOutlet weak var tfReferralCode: DesignableTextField!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnRegister: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfFirstName.delegate = self
        tfLastName.delegate = self
        tfPrefix.delegate = self
        tfMobileNumber.delegate = self
        tfEmailAddress.delegate = self
        tfReferralCode.delegate = self
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "LoginOrRegistrationVC") as! LoginOrRegistrationVC
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func didTapButtonRegister(_ sender: Any) {
        
        if tfFirstName.text == "" || tfLastName.text == "" || tfPrefix.text == "" || tfMobileNumber.text == "" || tfEmailAddress.text == ""  {
            
            let alert = UIAlertController(title: "Missing Field", message: "Please fill in the required fields", preferredStyle: .alert)
                   
                   alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                  
                   self.present(alert, animated: true)
            return
        }
        
        if !isValidEmail(tfEmailAddress.text!)
        {
            self.handleError("error_no_email_address".localized())
            return
        }
        
        let contact = "\(tfPrefix.text!)\(tfMobileNumber.text!)"

        self.initAPIRegister(
            email: tfEmailAddress.text!,
            lastName: tfLastName.text!,
            firstName: tfFirstName.text!,
            mobileNumber: contact,
            countryCode: "NG",
            referralCode: tfReferralCode.text!)
        
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueOTP") {
            if let chidVC = segue.destination as? OTPVC {
                self.log("chidVC \(chidVC)")
                chidVC.segueData = (sender as? [String:AnyObject])!
            }
        }
    }

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        self.log("textFieldShouldReturn nextTag \(nextTag)")

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
                
    private func initAPIRegister(
        email: String,
        lastName:String,
        firstName:String,
        mobileNumber:String,
        countryCode:String,
        referralCode:String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlRegister2,
            params: "email:\(email)||last_name:\(lastName)||first_name:\(firstName)||mobile_number:\(mobileNumber)||country_code:\(countryCode)||referral_code:\(referralCode)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    var segueData = Dictionary<String, AnyObject>()
                    segueData[self.constant.key_mobile_number] = mobileNumber as AnyObject
                    segueData[self.constant.key_cust_id] = json["customer_id"].int as AnyObject
                    segueData[self.constant.key_prev_page] = "registration" as AnyObject
                    self.performSegue(withIdentifier: "segueOTP", sender: segueData)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.hideLoading()
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}
