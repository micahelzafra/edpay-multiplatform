//
//  ActivityFoodModel.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/24/20.
//

import Foundation
import UIKit

struct ActivityFood {
    
    var orderID : String?
    var status : String?
    var totalAmount : String?
    var date_and_time : String?
    
}
