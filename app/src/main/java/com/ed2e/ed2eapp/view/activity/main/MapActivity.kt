package com.ed2e.ed2eapp.view.activity.main

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.main.home.edfood.EDFoodLogDetailsAcivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.bottom_sheet_map_tracking.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import kotlin.coroutines.CoroutineContext


class MapActivity : BaseActivity(), OnMapReadyCallback, CoroutineScope {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference


    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private lateinit var mMap: GoogleMap
    private var bookedId = ""
    private var restaurantDetails = JsonObject()
    private var riderDispatchDetails = JsonObject()
    private var bookedLoc:LatLng? = null
    private var merchantLoc:LatLng? = null
    private var riderLoc:LatLng? = null
    private var markers = ArrayList<MarkerOptions>()
    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>

    private var bookingDetails = JsonObject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
        initListener()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)

        bookedId = jsonObj[key_booked_id].asString
        getOrderDetails(bookedId)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map_mapview) as SupportMapFragment
        mapFragment.getMapAsync(this)


        sheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        bottom_sheet_map_tracking_textview_holder.setOnClickListener{
            expandCloseSheet()
        }
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //bottom_sheet_map_tracking_textview_holder.text = "Close Bottom Sheet"
                    }

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        //bottom_sheet_map_tracking_textview_holder.text = "Expand Bottom Sheet"
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })


        bottom_sheet_map_tracking_textview_view_order_details.setOnClickListener {
            //initAPIGetOrderDetails(bookedId)
            val intent : Intent
            intent = Intent(context, EDFoodLogDetailsAcivity::class.java)
            intent.putExtra(key_data,bookingDetails.toString())
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        bottom_sheet_map_tracking_layout_need_help.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://ed2e.com/contact-us/")
            )
            startActivity(intent)

        }
    }

    private fun getOrderDetails(
        booked_id: String
    ) {
        showProgress()
        Log.d("","getOrderDetails")
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlOrderHistoryDetails,
            "booked_id:" + booked_id,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    Log.d("","RESPONSE: " + response)
                    if (status == status_success) {
                        setData(jsonObj)
                    } else {
                        val error_code = jsonObj[key_code].asString
                        //showDialogError(request_error_dialog, error_code)
                        showDialog_APIError("", "", true, error_code, "", "OK")
                    }
                }
                hideProgress()
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            hideProgress()
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.message,
                "",
                "OK"
            )
            null
        }
    }

    //val orderID_fromData;

    private fun setData(jsonObj:JsonObject)
    {
        val parser = JsonParser()
        val jsonArray = jsonObj[key_booked].asJsonArray

        bookingDetails = parser.parse(jsonArray[0].toString()).asJsonObject
        Log.d("MapActivity", "RESPONSE (Booked Data): $bookingDetails")

        var data = JsonObject()
        for (json in jsonArray)
        {
            data = parser.parse(json.toString()).asJsonObject
        }
        val foodTotal = data[key_food_total].asJsonObject
        restaurantDetails = data[key_restaurant_details].asJsonObject
        riderDispatchDetails =  try
        {
            data[key_rider_dispatch_details].asJsonObject

        } catch (jinx:Exception) {
            JsonObject()
        }

        val orderStatus = data[key_booked_status].asString
        val date = data[key_booked_date].asString

//        val serviceFeePercentage = "${getString(R.string.label_service_fee,foodTotal[key_vat].asString)}%"

        val orderNo = "$label_logs_order_no ${data[key_booked_id].asString}"
        val total = commaSeparated(foodTotal[key_total].asString.replace(",","")).asString()
        val statusDate = "${getOrderStatus(orderStatus).asString()} ${getLocalTimeStamp(date).asString()}"
        val riderNote = try {
            data[key_special_notes].asString
        } catch (jinx: Exception) {
            "Rider notes"
        }

        bottom_sheet_map_tracking_textview_amount.text = total
        bottom_sheet_map_tracking_textview_merchant.text = restaurantDetails[key_restaurant_name].asString
        Timber.d("riderDispatchDetails: %s", riderDispatchDetails)
        if (riderDispatchDetails.toString().equals("{}"))
        {
            bottom_sheet_map_tracking_textview_no_rider.visibility = VISIBLE
            bottom_sheet_map_tracking_layout_rider_info.visibility = GONE
            bottom_sheet_map_tracking_textview_holder.text = "Still looking for your rider..."
        }
        else
        {

            val riderInfo = riderDispatchDetails[key_rider_info].asJsonObject
            val name = "${riderInfo[key_first_name].asString} ${riderInfo[key_last_name].asString}"
            val orderId = "$orderNo"
            bottom_sheet_map_tracking_textview_no_rider.visibility = GONE
            bottom_sheet_map_tracking_layout_rider_info.visibility = VISIBLE
            bottom_sheet_map_tracking_textview_plate.text = riderInfo[key_plate_no].asString
            bottom_sheet_map_tracking_textview_rider_name.text = name
            bottom_sheet_map_tracking_textview_order_id.text = orderId
            listenRiderLocation(riderInfo[key_id].asString)

            val distanceTime = riderInfo["distance_time"].asJsonObject
            val time = distanceTime["time"].asString

            Log.d("Logs", "Order time: " + time)


            var hours = 0
            var minutes = 0

            try {
                var delimiter = ":"
                val timeSplitted = time.split(delimiter).toTypedArray()

                hours = timeSplitted[0].toInt()
                minutes = timeSplitted[1].toInt()
            }
            catch (e:Exception)
            {
                Timber.e(e)
            }

            if(hours == 0 || hours == null) {
                bottom_sheet_map_tracking_textview_holder.text =
                    "Your order will arrive in $minutes minute(s)"
            } else {
                bottom_sheet_map_tracking_textview_holder.text =
                    "Your order will arrive in $hours hour(s) and $minutes minute(s)"
            }


        }

        if (orderStatus.equals("3"))
        {
            //0
            bottom_sheet_map_tracking_textview_status_1.isEnabled = true
            bottom_sheet_map_tracking_textview_status_2.isEnabled = false
            bottom_sheet_map_tracking_textview_status_3.isEnabled = false
            bottom_sheet_map_tracking_textview_status_4.isEnabled = false
        }
        else if (orderStatus.equals("4"))
        {
            //1
            bottom_sheet_map_tracking_textview_status_1.isEnabled = false
            bottom_sheet_map_tracking_textview_status_2.isEnabled = true
            bottom_sheet_map_tracking_textview_status_3.isEnabled = false
            bottom_sheet_map_tracking_textview_status_4.isEnabled = false
        }
        else if (orderStatus.equals("5"))
        {
            //2
            bottom_sheet_map_tracking_textview_status_1.isEnabled = false
            bottom_sheet_map_tracking_textview_status_2.isEnabled = false
            bottom_sheet_map_tracking_textview_status_3.isEnabled = true
            bottom_sheet_map_tracking_textview_status_4.isEnabled = false
        }
        else if (orderStatus.equals("17"))
        {
            //1
            bottom_sheet_map_tracking_textview_status_1.isEnabled = false
            bottom_sheet_map_tracking_textview_status_2.isEnabled = false
            bottom_sheet_map_tracking_textview_status_3.isEnabled = false
            bottom_sheet_map_tracking_textview_status_4.isEnabled = true
        }
        else
        {
            bottom_sheet_map_tracking_textview_status_1.isEnabled = false
            bottom_sheet_map_tracking_textview_status_2.isEnabled = false
            bottom_sheet_map_tracking_textview_status_3.isEnabled = false
            bottom_sheet_map_tracking_textview_status_4.isEnabled = false

            if (orderStatus.equals("6"))
            {
                val intent : Intent
                intent = Intent(context, EDFoodLogDetailsAcivity::class.java)
                intent.putExtra(key_data,data.toString())
                startActivity(intent)
            }
        }

        getLocationAddress("","",data[key_booked_location].asString)



        merchantLoc = LatLng(restaurantDetails[key_latitude].asDouble,restaurantDetails[key_longitude].asDouble)
        updateMarker()

    }

    private fun expandCloseSheet() {
        if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            //bottom_sheet_map_tracking_textview_holder.text = "Close Bottom Sheet"
        } else {
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            //bottom_sheet_map_tracking_textview_holder.text = "Expand Bottom Sheet"
        }
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        val parser = JsonParser()
        when (tag) {
            tag_reload_tracking_data -> {
                getOrderDetails(bookedId)
            }
        }
    }


    fun listenRiderLocation(riderId:String)
    {
        //If already initialize just let it be Thanks!!!
        try
        {
            //Searched by zafyrajinx
            //Manual override for getting the db of rider app for ref https://firebase.google.com/docs/projects/multiprojects
            val options = FirebaseOptions.Builder()
                .setApplicationId("1:722702005898:web:d1b7da2a00dcd92042df73") // Required for Analytics.
                .setApiKey("AIzaSyBLR-Kb7X6q-47jBDqnorTj8Hz3k0EZCrg") // Required for Auth.
                .setDatabaseUrl("https://ed2e-provider-1563245918372.firebaseio.com") // Required for RTDB.
                .setProjectId("ed2e-provider-1563245918372")
                .build()
            // Initialize with secondary app.
            FirebaseApp.initializeApp(this, options, "riderDB")
        }
        catch (jinx:Exception)
        {

        }

        // Access a Cloud Firestore instance from your Activity
        val db = FirebaseFirestore.getInstance(FirebaseApp.getInstance("riderDB"))
        val docRef = db.collection("coordinates")
            .document("ED2ENG-R:$riderId")
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
//                Log.w(TAG, "Listen failed.", e)
                Timber.e(e, "Listen failed.")
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
//                Log.d(TAG, "Current data: ${snapshot.data}")
                Timber.w( "Current data: ${snapshot.data.toString()}")
                Timber.w( "Current data: ${snapshot.data?.get(key_location)}")

                val geoPoint: GeoPoint = snapshot.data?.get(key_location) as GeoPoint
                riderLoc = LatLng(geoPoint.latitude,geoPoint.longitude)
                updateMarker()
            } else {
//                Log.d(TAG, "Current data: null")
                Timber.w( "Current data: null")
            }
        }
    }

    fun getLocationAddress(longitude: String, latitude: String, address:String?)
    {
        val apiKey = getString(R.string.api_key)
//        https://maps.googleapis.com/maps/api/geocode/json?address=14.6215107,121.0709483&key=AIzaSyDxzpoR7ZDsTUnv0_9pja97DJFhgdwF6DQ
        val parser = JsonParser()
        val sb = StringBuilder("https://maps.googleapis.com/maps/api/geocode/json")
        sb.append("?key=$apiKey")
        if (address==null)
            sb.append("&address=$latitude,$longitude")
        else
            sb.append("&address=$address")
        apiInterface.parseGoogle(sb.toString(),
            { response: String? ->
                launch(Dispatchers.Main) {
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
//                resultList.clear()
                    if (status == status_ok) {
                        val jsonArray = jsonObj[key_results].asJsonArray
                        loop@ for (i in 0 until jsonArray.size()) {
                            val jinx = jsonArray[i].asJsonObject
                            val formatted_address = jinx[key_formatted_address].asString
                            val address_components = jinx[key_address_components].asJsonArray

                            val geometry = jinx[key_geometry].asJsonObject
                            val location = geometry[key_location].asJsonObject
                            val lat = location[key_lat].asDouble
                            val lng = location[key_lng].asDouble
                            bookedLoc = LatLng(lat,lng)
                            updateMarker()
                        }

                    } else {
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    fun updateMarker()
    {
        markers.clear()
        mMap.clear()
        if (bookedLoc != null)
        {
            mMap.addMarker(MarkerOptions().position(bookedLoc!!).title("Delivery Address").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_user)))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bookedLoc,15.0f))
            markers.add(MarkerOptions().position(bookedLoc!!).title("Delivery Address").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_user)))

        }

        if (merchantLoc != null)
        {
            mMap.addMarker(MarkerOptions().position(merchantLoc!!).title(restaurantDetails[key_restaurant_name].asString).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_merchant)))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(merchantLoc,15.0f))
            markers.add(MarkerOptions().position(merchantLoc!!).title(restaurantDetails[key_restaurant_name].asString).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_merchant)))
        }

        if (riderLoc != null)
        {
            mMap.addMarker(MarkerOptions().position(riderLoc!!).title("Rider").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_rider)))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(riderLoc,15.0f))
            markers.add(MarkerOptions().position(riderLoc!!).title("Rider").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_rider)))
        }

        val builder = LatLngBounds.Builder()
        for (marker in markers) {
            builder.include(marker.position)
        }
        val bounds = builder.build()
        val padding = 50 // offset from edges of the map in pixels

        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        mMap.animateCamera(cu)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }




    private fun initToolBar() {
        maps_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    maps_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    maps_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {

    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {

    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}