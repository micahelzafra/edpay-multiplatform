//
//  ProfileHelpVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/28/20.
//

import UIKit

class ProfileHelpVC: UIViewController {

    //UIView
    @IBOutlet weak var vContactNumber: UIView!
    @IBOutlet weak var vEmailAddress: UIView!
    @IBOutlet weak var vWebsite: UIView!
    @IBOutlet weak var vLocation: UIView!
    
    
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    var helpDetailListPH = ProfileHelp(contactNumber: "+6323506089", emailAddress: "support@ed2e", website: "www.ed2e.com", location: "@14.62145,121.0613163,17z")
    var helpDetailListNG = ProfileHelp(contactNumber: "+2347087216653", emailAddress: "support@ed2e.com", website: "www.ed2e.com", location: "@6.6100301,3.3430559,17z")
    
    var currentLocation : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapContactNumber = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            vContactNumber.isUserInteractionEnabled = true
            vContactNumber.addGestureRecognizer(tapContactNumber)
       
        let tapEmailAddress = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            vEmailAddress.isUserInteractionEnabled = true
            vEmailAddress.addGestureRecognizer(tapEmailAddress)
        
        let tapWebsite = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            vWebsite.isUserInteractionEnabled = true
            vWebsite.addGestureRecognizer(tapWebsite)
        
        let tapLocation = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            vLocation.isUserInteractionEnabled = true
            vLocation.addGestureRecognizer(tapLocation)

    }
    
    //TODO based on location //current Location
    func location() {
        if currentLocation == "NG" {
            lblContactNumber.text = helpDetailListNG.contactNumber
            lblEmailAddress.text = helpDetailListNG.emailAddress
            lblWebsite.text = helpDetailListNG.website
            lblLocation.text = helpDetailListNG.location
        } else if currentLocation == "PH" {
            lblContactNumber.text = helpDetailListPH.contactNumber
            lblEmailAddress.text = helpDetailListPH.emailAddress
            lblWebsite.text = helpDetailListPH.website
            lblLocation.text = helpDetailListPH.location
        }
        
    }
    @IBAction func didTapButtonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
              //action
        print("Tap")
        if sender.view == vContactNumber {
            
            makeAPhoneCall()
        } else if sender.view == vEmailAddress {
             print("Tap Email Address")
            emailTo()
        } else if sender.view == vWebsite {
            goToURL()
            print("Tap Website")
        } else {
            goToMapAddress()
            print("Tap Location")
        }
        
    }
    
    func makeAPhoneCall()  {
        guard let number = URL(string: "tel://" + (helpDetailListPH.contactNumber ?? "") ) else { return }
        UIApplication.shared.open(number)
    }
    
    func emailTo() {
        guard let emailAdd = URL(string: "mailto:" + (helpDetailListPH.emailAddress ?? "") ) else { return }
        UIApplication.shared.open(emailAdd)
    }
    
    func goToURL() {
           guard let emailAdd = URL(string: "https://" + (helpDetailListPH.website ?? "") ) else { return }
           UIApplication.shared.open(emailAdd)
       }
    
    func goToMapAddress() {
        guard let location = URL(string: "https://www.google.com/maps/" + (helpDetailListPH.location ?? "") ) else { return }
              UIApplication.shared.open(location)
          }

}
