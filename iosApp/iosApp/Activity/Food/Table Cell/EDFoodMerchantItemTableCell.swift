//
//  EDFoodMerchantItemTableCell.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-zafyraminx on 2/2/20.
//

import UIKit

class EDFoodMerchantItemTableCell: UITableViewCell {
    @IBOutlet weak var imageViewFood: UIImageView!
    
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var buttonAddOutlet: UIButton!
    
    var buttonCallback : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
     self.buttonAddOutlet.addTarget(self, action: #selector(buttonAdd(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonAdd(_ sender: UIButton) {
        buttonCallback?()
    }
}
