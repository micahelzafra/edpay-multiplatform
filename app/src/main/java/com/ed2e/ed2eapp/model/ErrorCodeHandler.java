package com.ed2e.ed2eapp.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


public class ErrorCodeHandler {

    private List<errorCode> errorCodes = new ArrayList<errorCode>();
    private errorCode errorCode;
    private String text;

    public List<errorCode> getErrorCodes() {
        return errorCodes;
    }

    public com.ed2e.ed2eapp.model.errorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(com.ed2e.ed2eapp.model.errorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<errorCode> parse(String xml) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput( new StringReader( xml) ); // pass input whatever xml you have
//            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//            factory.setNamespaceAware(true);
//            XmlPullParser  parser = factory.newPullParser();
//
//            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("error_details")) {
                            // create a new instance of errorCode
                            errorCode = new errorCode();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("error_details")) {
                            // add errorCode object to list
                            errorCodes.add(errorCode);
                        }else if (tagname.equalsIgnoreCase("error_code")) {
                            errorCode.setError_code(text);
                        }  else if (tagname.equalsIgnoreCase("error_title")) {
                            errorCode.setError_title(text);
                        } else if (tagname.equalsIgnoreCase("error_message")) {
                            errorCode.setError_message(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {e.printStackTrace();}
        catch (IOException e) {e.printStackTrace();}

        return errorCodes;
    }
}
