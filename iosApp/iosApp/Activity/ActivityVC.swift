//
//  ActivityVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 12/27/19.
//

import Foundation
import UIKit
import SnapKit

class AcivityVC: UIViewController {

    @IBOutlet weak var segmentActivity: UISegmentedControl!
    @IBOutlet weak var viewContainerActivity: UIView!
    
    let segmentindicator: UIView = {

        let v = UIView()

        v.translatesAutoresizingMaskIntoConstraints = false
        v.isOpaque = false
        v.contentMode = .scaleAspectFit
        v.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "four_pillars_3_segment"))

        return v
    }()
    
    var payActivitySegment = ActivityPayVC()
    var foodActivitySegment = ActivityFoodVC()
    var topUpActivitySegment = ActivityTopUpVC()
     
    
     private lazy var payActivity: ActivityPayVC = {
         let storyboard = UIStoryboard(name: "Activity", bundle: Bundle.main)
         var viewController = storyboard.instantiateViewController(withIdentifier: "payIdentifier") as! ActivityPayVC
         self.add(asChildViewController: viewController)
         
         return viewController
     }()
     
     private lazy var foodActivity: ActivityFoodVC = {
         let storyboard = UIStoryboard(name: "Activity", bundle: Bundle.main)
         var viewController = storyboard.instantiateViewController(withIdentifier: "foodIdentifier") as! ActivityFoodVC
         self.add(asChildViewController: viewController)
         
         return viewController
     }()
    
    private lazy var topUpActivity: ActivityTopUpVC = {
         let storyboard = UIStoryboard(name: "Activity", bundle: Bundle.main)
         var viewController = storyboard.instantiateViewController(withIdentifier: "topUpIdentifier") as! ActivityTopUpVC
         self.add(asChildViewController: viewController)
         
         return viewController
     }()
    
      override func viewDidLoad() {
         super.viewDidLoad()

          updateView()
         
        
         let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
             UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
             UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected )
         // End of customizing the segmented control
         
         self.view.addSubview(segmentindicator)
         
         setupLayout()
     }
    
    func setupLayout() {
        segmentindicator.snp.makeConstraints { (make) in
            
            make.top.equalTo(segmentActivity.snp.bottom).offset(-10)
            make.height.equalTo(10)
            make.width.equalTo(75 + segmentActivity.titleForSegment(at: 0)!.count * 8)
            make.centerX.equalTo(segmentActivity.snp.centerX).dividedBy(segmentActivity.numberOfSegments)
            
        }
        
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        DispatchQueue.main.async(execute: {
            (sender as? UIMenuController)?.setMenuVisible(false, animated: false)
        })
        return false
    }
    
    
    @IBAction func switchSegmentActivity(_ sender: Any) {
        updateView()
              
              let numberOfSegments = CGFloat(segmentActivity.numberOfSegments)
              let selectedIndex = CGFloat((sender as AnyObject).selectedSegmentIndex)
              segmentindicator.snp.remakeConstraints { (make) in
                  
                  make.top.equalTo(segmentActivity.snp.bottom).offset(-10)
                  make.height.equalTo(10)
                  make.width.equalTo(75 + segmentActivity.titleForSegment(at: 0)!.count * 8)
                  make.centerX.equalTo(segmentActivity.snp.centerX).dividedBy(numberOfSegments / CGFloat(3.0 + CGFloat(selectedIndex-1.0)*2.0))
                  
              }
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        viewContainerActivity.addSubview(viewController.view)
        viewController.view.frame = viewContainerActivity.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    private func updateView() {
        if segmentActivity.selectedSegmentIndex == 0 {
            remove(asChildViewController: foodActivity)
            remove(asChildViewController: topUpActivity)
            add(asChildViewController: payActivity)
        } else if segmentActivity.selectedSegmentIndex == 1 {
            add(asChildViewController: foodActivity)
            remove(asChildViewController: topUpActivity)
            remove(asChildViewController: payActivity)
        } else if  segmentActivity.selectedSegmentIndex == 2 {
            remove(asChildViewController: foodActivity)
            add(asChildViewController: topUpActivity)
            remove(asChildViewController: payActivity)
        }
    }


}
