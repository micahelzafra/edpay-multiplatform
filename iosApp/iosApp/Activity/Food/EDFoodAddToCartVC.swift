//
//  EDFoodAddToCartVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/2/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class EDFoodAddToCartVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var imageViewFood: UIImageView!
    
    //view food merchant
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblFoodNameDetails: UILabel!
    @IBOutlet weak var lblFoodPrice: UILabel!
    
    
    @IBOutlet weak var lineDivider: UIView!
    @IBOutlet weak var lblToppings: UILabel!
    @IBOutlet weak var tableViewAddOns: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tfSpecialNotes: DesignableTextField!
    
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var imageViewLessQuantity: UIImageView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var imageViewAddQuantity: UIImageView!
    
    @IBOutlet weak var btnAddToCart: DesignableButton!
    
    var selectedAddons = [JSON]()

    let downloader = ImageDownloader()
    
    var bookedId = ""
    var country_name = ""
    var booked_location = ""
    var restaurant_id = ""
    private var quantity = 1
    private var addon_amount = 0.00
    
    var addons = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      let tapAdd = UITapGestureRecognizer(target: self, action: #selector(tapAddQuantity(tapGestureRecognizer:)))
      self.imageViewAddQuantity.isUserInteractionEnabled = true
      self.imageViewAddQuantity.addGestureRecognizer(tapAdd)

      let tapMinus = UITapGestureRecognizer(target: self, action: #selector(tapMinusQuantity(tapGestureRecognizer:)))
      self.imageViewLessQuantity.isUserInteractionEnabled = true
      self.imageViewLessQuantity.addGestureRecognizer(tapMinus)
        
    //self.keyboardCheck()
                
    }
        
    override func viewWillAppear(_ animated: Bool) {
        //self.keyboardCheck()
        self.hideKeyboardWhenTappedAround()
        
        let data = segueData[self.constant.key_data] as! JSON
        booked_location = segueData[self.constant.key_booked_location] as! String
        bookedId = segueData[self.constant.key_booked_id] as! String
        restaurant_id = segueData[self.constant.key_restaurant_id] as! String
        self.log("passed data: \(data)")
        
        if let imageurl = data[self.constant.key_food_photo].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewFood.image = image
                }
            }
        }
        
        let foodName = data[self.constant.key_food_name].string
        let foodDescription = data[self.constant.key_food_description].string
        let foodPrice = data[self.constant.key_food_price].string
        self.lblFoodName.text = foodName
        self.lblFoodNameDetails.text = foodDescription
        self.lblFoodPrice.text = foodPrice
        
        let add_ons_category = data[self.constant.key_add_ons_category].array
        if add_ons_category!.count == 0
        {
            tableViewAddOns.isHidden = true
            //lineDivider.isHidden = true
            
            self.tableViewHeight.constant = 0
        }
        else
        {
            tableViewAddOns.isHidden = false
            //lineDivider.isHidden = false
            var itemCount = 0
            for addons in add_ons_category!
            {
                self.addons.append(addons)
                let addonList = addons[self.constant.key_addons].array
                itemCount = itemCount + addonList!.count
            }
            self.log("addons count \(self.addons.count)")
            self.tableViewAddOns.reloadData()
            self.tableViewAddOns.layoutIfNeeded()
            let height = CGFloat((self.addons.count * 28)) + CGFloat((itemCount * 36))
            self.log("height \(height)")
            self.tableViewHeight.constant = height
            self.tableViewAddOns.reloadData()
            
            self.tableViewHeight.constant = self.tableViewAddOns.contentSize.height
            self.tableViewAddOns.isScrollEnabled = false
            self.view.layoutIfNeeded()
        }
        computeTotal()
    }
    
    private func computeTotal()
    {
        self.lblQuantity.text = "\(quantity)"
        let amount = Double(self.lblFoodPrice.text!.replacingOccurrences(of: ",", with: ""))
        self.lblTotalAmount.text = commaSeparated(value: "\((Double(quantity) * ( amount! + self.addon_amount)))").asString()
    }

    @objc func tapAddQuantity(tapGestureRecognizer: UITapGestureRecognizer) {
        self.quantity += 1
        computeTotal()
    }

    @objc func tapMinusQuantity(tapGestureRecognizer: UITapGestureRecognizer) {
        if self.quantity >= 2
        {
            self.quantity -= 1
        }
        computeTotal()
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: false, completion: {})
    }
    
    @IBAction func didTapButtonAddToCart(_ sender: Any) {
        var min_addon = 0
        var max_addon = 0
        var max_addon_optional = 0
        var addon_count = 0
        
        if addons.count != 0
        {
            for addonCat in addons
            {
                let mandatory = addonCat["mandatory"].int
                let addons = addonCat["addons"].array
                let min = addonCat[self.constant.key_min].int
                let max = addonCat[self.constant.key_max].int
                if mandatory == 1
                {
                    min_addon += min!
                    max_addon += max!
                    for addon in addons!
                    {
                        for selected in selectedAddons
                        {
                            if selected == addon
                            {
                                addon_count += 1
                            }
                        }
                    }
                }
                else
                {
                    max_addon_optional += max!
                }
            }

            if addon_count < min_addon
            {
                self.handleError(displaytitle: self.constant.label_dialog_title_addon_required, self.constant.label_dialog_msg_addon_required)
                return
            }

            if max_addon != 0
            {
                if addon_count > max_addon
                {
                        self.handleError(displaytitle: self.constant.label_dialog_title_addon_required, self.constant.label_dialog_msg_addon_exceed)
                    return
                }
            }

            if max_addon_optional != 0
            {
                if addon_count > max_addon_optional
                {
                    self.handleError(displaytitle: self.constant.label_dialog_title_addon_required, self.constant.label_dialog_msg_addon_exceed)
                    return
                }
            }
        }
                    
        var arralist = [JSON]()
        let data = segueData[self.constant.key_data] as! JSON
        self.log("key_data \(data)")
        for selected in selectedAddons
        {
            let addon_name = selected["addon_name"].string
            let addon_price = selected["addon_price"].string
            let id = selected["id"].int
            let addon_type = data[self.constant.key_category].string
            let food_id = data[self.constant.key_food_id].int
            let jsonObject: JSON = [
                "food_addon": addon_name!,
                "addon_price": addon_price!,
                "addon_type": addon_type!,
                "food_id": food_id!,
                "id": "\(id ?? -1)",
            ]
//                jsonObject.jsonObject.setValue("food_addon",addon_name)
//                jsonObject.jsonObject.setValue("addon_price",addon_price)
//                jsonObject.jsonObject.setValue("addon_type",this.jsonObj[key_category].asString)
//                jsonObject.jsonObject.setValue("food_id",this.jsonObj[key_food_id].asString)
//                jsonObject.jsonObject.setValue("id",id)
            arralist.append(jsonObject)
        }

        self.initCreateItemCart(
            booked_location: self.booked_location,
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            restaurant_id: self.restaurant_id,
            food_id: "\(data[self.constant.key_food_id].int ?? -1)",
            food_quantity: "\(self.quantity)",
            food_price: data[self.constant.key_food_price].string!,
            booked_id: self.bookedId,
            category_name: data[self.constant.key_category].string!,
            container_price: data[self.constant.key_container_price].string!,
            special_notes: self.tfSpecialNotes.text!,
            addons: "\(arralist)")
    }
   
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
          
         if addons.count == 0
          {
              tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
          }
          else
          {
              self.log("sections addons.count \(addons.count)")
              tableView.restore()
          }
          return addons.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if addons.count == 0
        {
            return ""
        }
        else
        {
            let data = addons[section]
            let name = data[self.constant.key_addon_category_name].string
            let max = data[self.constant.key_max].int
            let min = data[self.constant.key_min].int
            return "\(name ?? "") (min \(min ?? 0) max \(max ?? 0))"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if addons.count == 0
        {
            return 0
        }
        else
        {
            let data = addons[section]
            let addonList = data[self.constant.key_addons].array
            return addonList!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EDFoodAddonCategoryTableCell", for: indexPath)
                        as! EDFoodAddToCartTableViewCell
        
        let data = addons[indexPath.section]
        let addonList = data[self.constant.key_addons].array
        let addon = addonList![indexPath.row].self
//        cell.lblAddonCategory.text = "\(addon[self.constant.key_addon_name] )"
        cell.lblAddOnsName.text = addon[self.constant.key_addon_name].string
        cell.lblAddOnsPrice.text = "\(addon[self.constant.key_addon_price].string ?? "0.00")"
        self.tableViewAddOns.sizeToFit()
        cell.btnCheckUncheck.setTitle(addon[self.constant.key_addon_name].string, for: .normal)
        cell.btnCheckUncheck.tag = (indexPath.section*100)+indexPath.row
        cell.btnCheckUncheck.addTarget(self, action: #selector(checkboxClicked(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? EDFoodAddToCartTableViewCell {
            cell.btnCheckUncheck.sendActions(for: .touchUpInside)
         }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = #colorLiteral(red: 0.006476058159, green: 0.07246912271, blue: 0.476346612, alpha: 1)
        header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    // MARK : Text field Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -150, up: true)
      
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -150, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    @objc func checkboxClicked(_ sender: UIButton) {
        let section = sender.tag / 100
        let row = sender.tag % 100
        let indexPath = NSIndexPath(row: row, section: section)
        sender.isSelected = !sender.isSelected
        self.log("cell.btnCheckUncheck.isSelected \(sender.isSelected) \(section) \(row) \(indexPath)")
        if sender.isSelected
        {
            let data = addons[indexPath.section]
            let addonList = data[self.constant.key_addons].array
            let addon = addonList![indexPath.row].self
            selectedAddons.append(addon)
        }
        else
        {
            
            let data = addons[indexPath.section]
            let addonList = data[self.constant.key_addons].array
            let addon = addonList![indexPath.row].self
            
            var index = 0
            for selected in selectedAddons
            {
                if selected == addon
                {
                    index = index + 1
                }
            }
            selectedAddons.remove(at: index)
        }
    }
                
    private func initCreateItemCart(
        booked_location: String,
        customer_id:String,
        restaurant_id:String,
        food_id:String,
        food_quantity:String,
        food_price:String,
        booked_id:String,
        category_name:String,
        container_price:String,
        special_notes:String,
        addons:String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartCreateItem,
            params: "booked_location:\(booked_location)||customer_id:\(customer_id)||restaurant_id:\(restaurant_id)||food_id:\(food_id)||food_quantity:\(food_quantity)||food_price:\(food_price)||booked_id:\(booked_id)||category_name:\(category_name)||container_price:\(container_price)||special_notes:\(special_notes)||addons:\(addons)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    self.dismiss(animated: false)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.hideLoading()
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
    
}
