//
//  ViewMerchantsVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/21/20.
//

import UIKit

class ViewMerchantsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var viewMerchant = [
        ViewMerchants(id: 1, image: "food", storeName: "KFC", storeType:  "Chicken, Fastfood",storeLocation: "Cubao Ibabaw"),
        ViewMerchants(id: 1, image: "food", storeName: "MacDonalds", storeType:  "Chicken, Fastfood",storeLocation: "Cubao Ilalim"),
        ViewMerchants(id: 1, image: "food", storeName: "Jollibee", storeType:  "Chicken, Fastfood",storeLocation: "Cubao Main"),
           ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
    }
    
    // MARK: - Table view data source

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewMerchant.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewMerchantCell", for: indexPath)
                        as! ViewMerchantsTableViewCell
        
        let merchant = viewMerchant[indexPath.row]
        cell.imageViewMerchants.image = UIImage(named: merchant.image!)
        cell.labelStoreName.text = merchant.storeName
        cell.lblStoreType.text = merchant.storeType
        cell.lblStoreLocation.text = merchant.storeLocation
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
