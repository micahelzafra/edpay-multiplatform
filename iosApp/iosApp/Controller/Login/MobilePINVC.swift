//
//  MobilePINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/10/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app

class MobilePINVC: BaseViewController, EventsDelegate {

    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var btnLogin: DesignableButton!
    @IBOutlet weak var lblForgotPIN: UILabel!
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var pinView: CBPinEntryView!{
    didSet {
                pinView.delegate = self as? CBPinEntryViewDelegate
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        lblForgotPIN.isUserInteractionEnabled = true
        lblForgotPIN.addGestureRecognizer(tap)
        self.hideKeyboardWhenTappedAround()
        self.eventDelegate = self
        self.getLocation()

    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Forgot Mobile PIN", message: "Do you wish to reset Mobile PIN?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: {
            action in
            print("Yay! You brought your towel!")
            self.performSegue(withIdentifier: "segueVerifyYourPhoneNumber", sender: nil)
        }))
        
        self.present(alert, animated: true)
    }
    
   
    @IBAction func btnBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
   @IBAction func didTapLogin(_ sender: Any) {
//        let alert = UIAlertController(title: "Incorrect Mobile PIN", message: "Please make sure to enter the correct PIN", preferredStyle: .alert)
//
//        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//
//        self.present(alert, animated: true)
//
    self.log("pinView \(pinView.getPinAsString())")
        if pinView.getPinAsString().count < 6
        {
            self.handleError(displaytitle: "dialog_missing_field_title".localized(), "dialog_missing_field_message".localized())
            return
        }
//        performSegue(withIdentifier: "segueLogin", sender: nil)
        initAPILogin(
            mobile:self.segueData[self.constant.key_mobile_number] as! String,
            pin: pinView.getPinAsString(),
            device_token: self.defaults.string(forKey: self.constant.key_device_token)!,
            country: self.defaults.string(forKey: self.constant.key_country) ?? "NG")
    
    }
    
    func eventData(event: Int, data: AnyObject) {
        if event == CommonConstants.tag_load_current_location
        {
            let location = data as! Dictionary<String, Double>
//            self.log("location \(location[self.constant.key_latitude] ?? 0.00), \(location[self.constant.key_longitude] ?? 0.00)")
        }
    }
    
    private func initAPILogin(mobile:String,pin:String,device_token:String,country:String){
        self.showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlLogin2,
            params: "mobile_number:\(mobile)||pin:\(pin)||device_id:\(device_token)||country_name:\(country)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    let data = json[self.constant.key_data].self
                    let user_id = data[self.constant.key_id].int
                    self.log("data \(data)")
                    if user_id != nil
                    {
                        self.log("login user_id \(String(describing: user_id))")
                        self.defaults.set(user_id, forKey: self.constant.key_user_id)
                    }
                    self.defaults.set("\(data)", forKey: self.constant.key_user_data)
                    self.performSegue(withIdentifier: "segueLogin", sender: nil)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
}

extension MobilePINVC: CBPinEntryViewDelegate {
    func entryCompleted(with entry: String?) {
        self.log("pinView \(pinView.getPinAsString())")
        initAPILogin(
            mobile:self.segueData[self.constant.key_mobile_number] as! String,
            pin: pinView.getPinAsString(),
            device_token: self.defaults.string(forKey: self.constant.key_device_token)!,
            country: self.defaults.string(forKey: self.constant.key_country) ?? "NG")
    }

    func entryChanged(_ completed: Bool) {
        if completed {
        }
    }
}
