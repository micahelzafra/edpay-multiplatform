package com.ed2e.ed2eapp.model;

import java.util.List;

public class Announcement {

    private String id;
    private String title;
    private String content;
    private String event_date;
    private String expiry_date;
    private String image;
    private String countries;
    private String annc_status;


    public Announcement(String id, String title, String content, String event_date, String expiry_date, String image, String countries, String annc_status) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.event_date = event_date;
        this.expiry_date = expiry_date;
        this.image = image;
        this.countries = countries;
        this.annc_status = annc_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public String getAnnc_status() {
        return annc_status;
    }

    public void setAnnc_status(String annc_status) {
        this.annc_status = annc_status;
    }
}
