//
//  EDFoodCartTableCell.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-zafyrajinx on 2/6/20.
//

import UIKit

class EDFoodCartTableCell: UITableViewCell {
    
    @IBOutlet weak var imageViewFood: UIImageView!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var imageViewLessQuantity: UIImageView!
    @IBOutlet weak var imageViewAddQuantity: UIImageView!
    @IBOutlet weak var buttonRemove: UIButton!
    @IBOutlet weak var layoutAddon: UIStackView!
    
    var buttonCallback : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
