//
//  AnnouncementListCVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/7/20.
//

import Foundation
import UIKit
import SwiftyJSON
import app
import AlamofireImage

private let reuseIdentifier = "announcementListCollectionViewCell"

class AnnouncementListCVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{


    @IBOutlet weak var cvAnnouncementList: UICollectionView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    
    internal var apiInterface = ApiInterface()
    let downloader = ImageDownloader()
    var announcementDetailList = [JSON]()
    
    //Location
    var isParsingLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        cvAnnouncementList.register(UINib(nibName: "AnnouncementListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        getAnnouncementList()
    }

    private func getAnnouncementList() {
        showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlAnnouncementList,
            params: "",
            success: { data in
                    let jsonData = data.data(using: String.Encoding.utf8)!
                    let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                                              
                    let status = json[self.constant.key_status].string
                        if status == self.constant.status_success {
                            let jsonArray = json["announcement_records"].array
                                            
                            for data in jsonArray! {
                                self.announcementDetailList.append(data)
                            }
                            self.cvAnnouncementList.reloadData()
                        }
                        else
                        {
                            let error = json[self.constant.key_code].string
                                self.handleError(error)
                        }
                self.hideLoading()
                return KotlinUnit()
            
        }, failure: {
                   self.handleError($0?.message)
                   return KotlinUnit()
               })
    }

    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDataSource


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
       return announcementDetailList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width , height: 150)

      }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! AnnouncementListCollectionViewCell
    
        // Configure the cell
        let announcement = announcementDetailList[indexPath.row]
        if let imageurl = announcement["image"].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
                downloader.download(urlRequest, filter: nil) { response in
                    if let image = response.result.value {
                            print(image)
                            cell.imageViewAnnouncementList.image = image
                    }
                }
        }
                
        cell.lblAnnouncementListTitle.text = announcement["title"].string
        cell.lblAnnouncementListDate.text = announcement["event_date"].string
        cell.lblAnnouncementListDetails.text = announcement["content"].string
        
        return cell
    }

}
