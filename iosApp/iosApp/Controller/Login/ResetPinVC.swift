//
//  ResetPinVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/13/20.
//

import UIKit
import CBPinEntryView

class ResetPinVC: UIViewController {
    @IBOutlet weak var resetPinView: CBPinEntryView! {
    didSet{
                resetPinView.delegate = self as? CBPinEntryViewDelegate
            }
        }
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSubmit: DesignableButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
        
         performSegue(withIdentifier: "segueConfirmNewMobilePIN", sender: nil)
    }
    
}
