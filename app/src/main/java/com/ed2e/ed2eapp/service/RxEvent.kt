package therajanmaurya.rxbus.kotlin

/**
 * @author Zafyrajinx
 * On 13/12/19.
 */
class RxEvent {
    data class EventAddPerson(val personName: String)
    data class EventScanQR(val qrCode: String)
    data class EventMainActivity(val tag: Int, val value : Any? = null)
}