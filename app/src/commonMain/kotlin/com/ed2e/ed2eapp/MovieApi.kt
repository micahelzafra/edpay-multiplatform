package com.ed2e.ed2eapp

import com.rba.mpp.shared.ApplicationDispatcher
import io.ktor.client.HttpClient
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.url
import kotlinx.coroutines.*

class MovieApi {

    private val httpClient = HttpClient()

    fun getMovieList(access_code: String, url: String ,success: (String) -> Unit, failure: (Throwable?) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            try {
//                val url =
//                    "http://167.99.65.221:8085/api/announcement/records"
                val json = httpClient.post<String> {
                    url(url)
//                    header("Accept", "application/json")
//                    header("Content-Type", "application/json")
                    header("Secret-Key", access_code)
                    header("Access-Code", access_code)
                }
//                Json.nonstrict.parse(MovieResponse.serializer(), json)
//                    .results
//                    .also(success)

                println(json)
                json.also(success)
//                Json.nonstrict.parse(MovieResponse.serializer(), json)
//                    .status
//                    .also(success)
            } catch (ex: Exception) {
                failure(ex)
            }
        }
    }
}