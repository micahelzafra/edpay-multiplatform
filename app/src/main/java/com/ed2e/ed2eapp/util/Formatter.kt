package com.ed2e.ed2eapp.util

import java.text.SimpleDateFormat
import java.util.*

actual class KMPDate actual constructor(formatString: String) { // 1

    private val dateFormat = SimpleDateFormat(formatString,Locale.US) // 2

    actual fun asString(): String {
        return dateFormat.format(Date()) // 3
    }
}

actual class commaSeparated actual constructor(value: String) {

    private val numberFormat = value

    actual fun asString(): String {
        return String.format("%,.2f", numberFormat.toBigDecimal())
    }
}

actual class paymentType actual constructor(value: String) {
    private var paymentType = value
    actual fun asString(): String {
        var type = "1"
        if (paymentType.equals(key_payment_type_edpay) || paymentType.equals("0"))
        {
            type = "0"
        }
        return type
    }
}

actual class dateFormat actual constructor(
    date: String,
    from: String,
    to: String
) {
    private val datevalue = date
    private val fromformat = from
    private val toformat = to
    actual fun asString(): String {
        val parser = SimpleDateFormat(fromformat,Locale.US)
        val formatter = SimpleDateFormat(toformat,Locale.US)
        return formatter.format(parser.parse(datevalue))
    }
}

actual class get30DaysBeforeDate actual constructor() {
    actual fun asString(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, - 30)

        val destDf = SimpleDateFormat(date_format_picker_display,Locale.US)

        return destDf.format(calendar.time)
    }
}

actual class getLocalTimeStamp actual constructor(date: String) {
    private val datevalue = date
    actual fun asString(): String {
        val df = SimpleDateFormat(date_format_server, Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("UTC")
        val date = df.parse(datevalue)
        val displayformat = SimpleDateFormat(date_format_logs_display, Locale.ENGLISH)
        displayformat.timeZone = TimeZone.getDefault()
        val formattedDate = displayformat.format(date)
        return formattedDate
    }
}

actual class getCurrentDay actual constructor() {
    actual fun asString(): String {
        val calendar = Calendar.getInstance()
        val day = calendar[Calendar.DAY_OF_WEEK]

        var currentDay = ""
        when (day) {
            Calendar.SUNDAY -> {
                currentDay = "Sunday"
            }
            Calendar.MONDAY -> {
                currentDay = "Monday"
            }
            Calendar.TUESDAY -> {
                currentDay = "Tuesday"
            }
            Calendar.WEDNESDAY -> {
                currentDay = "Wednesday"
            }
            Calendar.THURSDAY -> {
                currentDay = "Thursday"
            }
            Calendar.FRIDAY -> {
                currentDay = "Friday"
            }
            Calendar.SATURDAY -> {
                currentDay = "Saturday"
            }
        }
        return currentDay
    }
}

actual class getOrderStatus actual constructor(status: String) {
    var orderStatus = status

    actual fun asString(): String {
        val status: String
        if (orderStatus == "6")
        {
            status = "Delivered"
        }
        else if (orderStatus == "10")
        {
            status = "Cancelled"
        }
        else
        {
            status = "Pending"
        }
        return status
    }
}