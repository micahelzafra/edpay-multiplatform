package com.ed2e.ed2eapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.SearchLocation;
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsDetailsActivity;
import com.ed2e.ed2eapp.view.activity.main.home.edfood.EDFoodMerchantsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import therajanmaurya.rxbus.kotlin.RxBus;
import therajanmaurya.rxbus.kotlin.RxEvent;

import static com.ed2e.ed2eapp.util.CommonConstantKt.tag_intent_my_account_activity;
import static com.ed2e.ed2eapp.util.CommonConstantKt.tag_load_google_search_location;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;

public class SearchLocationAdapter extends RecyclerView.Adapter<SearchLocationAdapter.MyViewHolder> {

    Activity activity;
    List<SearchLocation> modelList;

    public SearchLocationAdapter(Activity activity, List<SearchLocation> modelList) {
        this.activity = activity;
        this.modelList = modelList;
    }

    public void setModelList(List<SearchLocation> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchLocationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.child_search_location, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchLocationAdapter.MyViewHolder myViewHolder, int position) {

        String address;
        try {
            JSONObject obj = new JSONObject(modelList.get(position).getResult());

            try {
                address = "" + (String) obj.get("description");
            } catch (Exception e) {
                address = "" + obj.getInt("description");
            }

        } catch (JSONException e) {
            e.printStackTrace();
            address = "";
        }
        myViewHolder.textview_address.setText("" + address);

        String finalAddress = address;
        myViewHolder.linearLayout_container.setOnClickListener(v -> {
            //Toast.makeText(v.getContext(), "@" + position + " | " + finalAddress, Toast.LENGTH_SHORT).show();

            ((EDFoodMerchantsActivity)activity).getLocationDetails(modelList.get(position).getResult());
//            EDFoodMerchantsActivity edFoodMerchantsActivity = new EDFoodMerchantsActivity();
//            edFoodMerchantsActivity.getLocationDetails(modelList.get(position).getResult());


//            Intent intent = new Intent (v.getContext(), AnnouncementsDetailsActivity.class);
//            intent.putExtra(key_announcement_title, modelList.get(position).getTitle());
//            intent.putExtra(key_announcement_content, modelList.get(position).getContent());
//            intent.putExtra(key_announcement_date, modelList.get(position).getEvent_date());
//            intent.putExtra(key_announcement_image, modelList.get(position).getImage());
//            activity.startActivity(intent);
//            //activity.finish();
//            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_address;
        LinearLayout linearLayout_container;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_address = itemView.findViewById(R.id.child_search_location_textview_address);
            linearLayout_container = itemView.findViewById(R.id.child_search_location_layout_main);
        }
    }
}
