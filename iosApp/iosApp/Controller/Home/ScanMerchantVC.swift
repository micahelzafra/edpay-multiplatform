//
//  ScanMerchantVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/21/20.
//

import UIKit
import AVFoundation
import SwiftyJSON
import app

class ScanMerchantVC: BaseViewController,  AVCaptureMetadataOutputObjectsDelegate
{
    
    internal var apiInterface = ApiInterface()
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var avaudioPlayer: AVAudioPlayer?
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewFrame: UIView!
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor = UIColor.green.cgColor
        codeFrame.layer.borderWidth = 2
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()
    
    func createFrame() -> CAShapeLayer {
        let height: CGFloat = self.viewFrame.frame.size.height
        let width: CGFloat = self.viewFrame.frame.size.width
        let x = (self.view.frame.width/2) - (self.viewFrame.frame.width/2)
        let barFrame = UIApplication.shared.statusBarFrame.height*2
        let y = ((self.view.frame.height/2) - (self.viewFrame.frame.height/2)) - barFrame
        let path = UIBezierPath()
        path.move(to: CGPoint(x: x+5, y: y+50))
        path.addLine(to: CGPoint(x: x+5, y: y+5))
        path.addLine(to: CGPoint(x: x+50, y: y+5))
        path.move(to: CGPoint(x: x+(height - 55), y: y+5))
        path.addLine(to: CGPoint(x: x+(height - 5), y: y+5))
        path.addLine(to: CGPoint(x: x+(height - 5), y: y+55))
        path.move(to: CGPoint(x: x+5, y: y+(width - 55)))
        path.addLine(to: CGPoint(x: x+5, y: y+(width - 5)))
        path.addLine(to: CGPoint(x: x+55, y: y+(width - 5)))
        path.move(to: CGPoint(x: x+(width - 55), y: y+(height - 5)))
        path.addLine(to: CGPoint(x: x+(width - 5), y: y+(height - 5)))
        path.addLine(to: CGPoint(x: x+(width - 5), y: y+(height - 55)))
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.strokeColor = UIColor.red.cgColor
        shape.lineWidth = 5
        shape.fillColor = UIColor.clear.cgColor
        return shape
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.avaudioPlayer?.delegate = self
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr]
        } else {
            failed()
            return
        }
        
        let frame = self.createFrame()
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.layer.addSublayer(frame)
        
        let maskLayer = CAShapeLayer()
        // Initialize the path.
        let path = UIBezierPath(rect: previewLayer.bounds)
        
        let x = (self.view.frame.width/2) - (self.viewFrame.frame.width/2)
        let barFrame = UIApplication.shared.statusBarFrame.height*2
        let y = ((self.view.frame.height/2) - (self.viewFrame.frame.height/2)) - barFrame
        
        // Specify the point that the path should start get drawn.
        path.move(to: CGPoint(x: x, y: y))
        
        // Create a line between the starting point and the bottom-left side of the view.
        path.addLine(to: CGPoint(x: x, y: y+self.viewFrame.frame.size.height))
        
        // Create the bottom line (bottom-left to bottom-right).
        path.addLine(to: CGPoint(x: x+self.viewFrame.frame.size.width, y: y+self.viewFrame.frame.size.height))
        
        // Create the vertical line from the bottom-right to the top-right side.
        path.addLine(to: CGPoint(x: x+self.viewFrame.frame.size.width, y: y))
        
        // Close the path. This will create the last line automatically.
        path.close()
        maskLayer.path = path.cgPath //freeform.cgPath
        maskLayer.fillRule = kCAFillRuleEvenOdd
        let whiteView = UIView(frame: previewLayer.bounds)
        whiteView.layer.mask = maskLayer
        whiteView.clipsToBounds = true
        whiteView.alpha = 0.3
        whiteView.backgroundColor = UIColor.black
        view.addSubview(whiteView)
        
        captureSession.startRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
    
    func found(code: String) {
        print(code)
        //self.playAudio(file:"Scanned.mp3")
        self.scanQR(merchant_code: code, country_code: "NG")
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePayment" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? PaymentVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
    
    func playAudio(file:String) {
        print("playAudioFile file \(file)")
        let path = Bundle.main.path(forResource: file, ofType:nil)!
        let url = URL(fileURLWithPath: path)
        do {
            avaudioPlayer = try AVAudioPlayer(contentsOf: url)
            avaudioPlayer?.play()
        } catch {
            // couldn't load file :(
            print("playAudioFile file \(file) not found")
        }
    }
        
    private func scanQR(
        merchant_code: String,
        country_code: String
    ) {
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlOfflineMerchant,
            params: "merchant_code:\(merchant_code)||country_code:\(country_code)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {

                    let data = json[self.constant.key_data].array

                    var segueData = Dictionary<String, AnyObject>()
                    segueData[self.constant.key_data] = data![0].self as AnyObject
                    self.performSegue(withIdentifier: "seguePayment", sender: segueData)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                    self.captureSession.startRunning()
                }

                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.captureSession.startRunning()
            return KotlinUnit()
        })
    }
}
