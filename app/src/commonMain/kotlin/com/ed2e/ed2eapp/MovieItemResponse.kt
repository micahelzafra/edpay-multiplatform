package com.ed2e.ed2eapp

import kotlinx.serialization.Serializable

@Serializable
data class MovieItemResponse(
    val id: Int,
    val title: String,
    val poster_path: String
)