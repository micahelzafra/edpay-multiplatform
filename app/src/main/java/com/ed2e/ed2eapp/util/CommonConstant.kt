package com.ed2e.ed2eapp.util

//EVENT TAGS intent
const val tag_intent_edpay_activity                       = 1000
const val tag_intent_reward_activity                      = 1001
const val tag_intent_topup_activity                       = 1002
const val tag_intent_login_activity                       = 1003
const val tag_intent_my_account_activity                  = 1004
const val tag_intent_referrals_activity                   = 1005
const val tag_intent_shake_activity                       = 1006
const val tag_intent_edfood_activity                      = 1007
const val tag_intent_edfood_menu_details_activity         = 1008
const val tag_intent_help_activity                        = 1009
const val tag_intent_transfer_activity                    = 1010
const val tag_intent_announcements_activity               = 1011
const val tag_intent_order_history_detail                 = 1012
const val tag_intent_edpay_history_detail                 = 1013
const val tag_intent_track_activity                       = 1014
const val tag_intent_edfood_close_prev_act                = 1015

//EVENT TAGS load API
const val tag_load_api_log_pay                            = 2000
const val tag_load_api_log_pay_from                       = 2001
const val tag_load_api_log_pay_to                         = 2002
const val tag_load_api_log_food                           = 2003
const val tag_load_api_log_food_from                      = 2004
const val tag_load_api_log_food_to                        = 2005
const val tag_load_api_log_topup                          = 2006
const val tag_load_api_log_topup_from                     = 2007
const val tag_load_api_log_topup_to                       = 2008
const val tag_load_api_edpay_merchant                     = 2009
const val tag_load_addon                                  = 2010
const val tag_load_addon_remove                           = 2011
const val tag_load_quantity_increase                      = 2012
const val tag_load_quantity_decrease                      = 2013
const val tag_load_order_edit                             = 2014
const val tag_load_order_remove                           = 2015
const val tag_load_fragment_home                          = 2016
const val tag_load_fragment_activity                      = 2017
const val tag_load_fragment_profile                       = 2018
const val tag_reload_tracking_data                        = 2019
const val tag_load_google_search_location                 = 2020


const val FIREBASE_KEY_APP_VER                            = "app_ver"
const val FIREBASE_TOPIC                                  = "com.ed2e.edpay"
const val device_type                                     = "android"