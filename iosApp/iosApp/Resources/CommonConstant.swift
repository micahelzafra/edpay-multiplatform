//
//  CommonConstant.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 1/24/20.
//

import Foundation


struct CommonConstants {
    
    // App Store URL.
    static let URL_APP_STORE                            = "https://itunes.apple.com/us/app/edpoints-app/id1259730468?ls=1&mt=8"
    static let DEVICE_TYPE                              = "ios";
    
    static let KEY_FIRST_TIME_LAUNCH                    = "KEY_FIRST_TIME_LAUNCH";
    
    
    static let tag_load_current_location                = 1000;
    static let tag_load_data_order                      = 1001;
    
    
    static let tag_notification                         = "tag_notification";
}
