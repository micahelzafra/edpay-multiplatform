//
//  LoginVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/10/20.
//

import UIKit
import SwiftyJSON
import app

class LoginVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var stackViewMain: UIStackView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tfMobileNumber: DesignableTextField!
    @IBOutlet weak var btnNext: DesignableButton!
    
    var isPlaceHolder = true

    override func viewDidLoad() {
        super.viewDidLoad()
            self.keyboardCheck()
            self.tfMobileNumber.text = "+234 "
            self.tfMobileNumber.delegate = self
        initialState()
    }
    
    private func initialState()
    {
        let string = "+234 0123456789"
        let attributedString = NSMutableAttributedString.init(string: string)
        let range = (string as NSString).range(of: "+234 ")
        let range2 = (string as NSString).range(of: "0123456789")
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: range2)
        self.tfMobileNumber.attributedText = attributedString
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.keyboardCheck()
        self.hideKeyboardWhenTappedAround()
    }
 
    @IBAction func didTapBackButton(_ sender: Any) {
        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let VC = newStoryBoard.instantiateViewController(withIdentifier: "LoginOrRegistrationVC") as! LoginOrRegistrationVC
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)

    }
    
    @IBAction func didTapHelpButton(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let navigationController = storyBoard.instantiateViewController(withIdentifier: "ProfileHelpVC") as! UINavigationController
        _ = navigationController.viewControllers.first as! ProfileHelpVC
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func didTapNextButton(_ sender: Any) {
        if tfMobileNumber.text == "" {
                   let alert = UIAlertController(title: "Mobile number is empty", message: "Please enter your mobile number", preferredStyle: .alert)

                   alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

                   self.present(alert, animated: true)
               }
        
        if tfMobileNumber.text == "0" {
            let alert = UIAlertController(title: "Mobile number is not registered", message: "Please register your mobile number", preferredStyle: .alert)

                              alert.addAction(UIAlertAction(title: "Register", style: .default, handler: nil))
                              alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

                              self.present(alert, animated: true)
        }
        
        initAPICheckExistingMobileNum(mobile: tfMobileNumber.text!.replacingOccurrences(of: " ", with: ""))
//        if tfMobileNumber.text != "0" {
//                performSegue(withIdentifier: "segueMobilePIN", sender: nil)
//        }
    }
    
    //MARK: - text field masking
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        //MARK:- If Delete button click
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        print("\(char) was pressed \(string)")

                
        let allowedCharacters = CharacterSet(charactersIn: " +0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        print("allowedCharacters \(allowedCharacters.isSuperset(of: characterSet))")
        if  !allowedCharacters.isSuperset(of: characterSet)
        {
            return false
        }
        
        if (isBackSpace == -92) {
            print("Backspace was pressed")
            if (textField.text?.count)! >= 6
            {
                if !isPlaceHolder
                {
                    textField.text!.removeLast()
                }
            }
            else
            {
                if !isPlaceHolder
                {
                    isPlaceHolder = true
                    initialState()
                }
            }
            return false
        }
        
        if string.count >= 2 && string.count >= 10
        {
            
            textField.text = "+234 \(String(string.replacingOccurrences(of: " ", with: "").suffix(10)))" //there we are ading - in textfield
            isPlaceHolder = false
            self.tfMobileNumber.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return false
        }

        if textField == self.tfMobileNumber
        {
            if isPlaceHolder
            {
                textField.text = "+234 \(string)" //there we are ading - in textfield
                isPlaceHolder = false
                self.tfMobileNumber.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                return false
            }
            else
            {
                if (textField.text?.count)! <= 5
                {
                    textField.text = "+234 "  //There we are ading () and space two things
                }
                else if (textField.text?.count)! > 14
                {
                    return false
                }
            }
            return true
        }
        return false
    }

    private func initAPICheckExistingMobileNum(mobile:String){
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCheckMobileNum,
            params: "mobile_number:\(mobile)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    let user_id = json[self.constant.key_data]["customer_id"].int
                    let pin_verified = json[self.constant.key_data]["has_pin"].int
                    let otp_verified = json[self.constant.key_data]["otp_verified"].int
                    self.log("user_id \(user_id ?? -1)")
//                    self.defaults.set(user_id, forKey: self.constant.key_user_id)
                    if otp_verified == 0
                    {
                        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Registration", bundle:nil)
                        let VC = newStoryBoard.instantiateViewController(withIdentifier: "otpVC") as! OTPVC
                        var segueData = Dictionary<String, AnyObject>()
                        segueData[self.constant.key_mobile_number] = mobile as AnyObject
                        segueData[self.constant.key_cust_id] = user_id as AnyObject
                        segueData[self.constant.key_prev_page] = "login" as AnyObject
                        VC.segueData = segueData
                        VC.modalPresentationStyle = .fullScreen
                        self.present(VC, animated: true, completion: nil)
                        return KotlinUnit()
                    }

                    if pin_verified == 0
                    {
                        let newStoryBoard : UIStoryboard = UIStoryboard(name: "Registration", bundle:nil)
                        let VC = newStoryBoard.instantiateViewController(withIdentifier: "CreatePINVC") as! CreateMobilePIN
                        var segueData = Dictionary<String, AnyObject>()
                        segueData[self.constant.key_mobile_number] = mobile as AnyObject
                        segueData[self.constant.key_cust_id] = user_id as AnyObject
                        segueData[self.constant.key_prev_page] = "login" as AnyObject
                        VC.segueData = segueData
                        VC.modalPresentationStyle = .fullScreen
                        self.present(VC, animated: true, completion: nil)
                        return KotlinUnit()
                    }
                    var segueData = Dictionary<String, AnyObject>()
                    segueData[self.constant.key_mobile_number] = mobile as AnyObject
                    segueData[self.constant.key_cust_id] = user_id as AnyObject?
                    self.performSegue(withIdentifier: "segueMobilePIN", sender: segueData)
                    
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }

                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueMobilePIN") {
            if segue.destination is MobilePINVC
            {
                if let chidVC = segue.destination as? MobilePINVC {
                    self.log("chidVC \(chidVC)")
                    chidVC.segueData = (sender as? [String:AnyObject])!
                }
            }
        }
    }
}


