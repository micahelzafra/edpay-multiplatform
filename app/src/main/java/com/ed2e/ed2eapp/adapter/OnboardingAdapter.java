package com.ed2e.ed2eapp.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ed2e.ed2eapp.R;

public class OnboardingAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;

    public String[] onboarding_titles;
    public int[] onboarding_images;
    public String[] onboarding_descriptions;


    public OnboardingAdapter(Context context) {
        this.context = context;

        onboarding_titles = context.getResources().getStringArray(R.array.array_onboarding_titles);
        onboarding_images = context.getResources().getIntArray(R.array.array_onboarding_images);
        onboarding_descriptions = context.getResources().getStringArray(R.array.array_onboarding_descriptions);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == o);
    }

    @Override
    public int getCount() {
        return onboarding_titles.length;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.child_onboarding, container, false);

        //LinearLayout linearLayout = view.findViewById(R.id.child_onboarding_linearLayout_main);
        TextView textView_title = view.findViewById(R.id.child_onboarding_textView_title);
        ImageView imageView = view.findViewById(R.id.child_onboarding_imageView_image);
        TextView textView_desc = view.findViewById(R.id.child_onboarding_textView_desc);


        textView_title.setText(onboarding_titles[position]);
        textView_desc.setText(onboarding_descriptions[position]);

        TypedArray onboarding_images= context.getResources().obtainTypedArray(R.array.array_onboarding_images);
        onboarding_images.getResourceId(position, -1);
        imageView.setImageResource(onboarding_images.getResourceId(position, -1));
        onboarding_images.recycle();


        container.addView(view);
        return view;
    }

}
