//
//  EDPaySuccessVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 1/28/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app
import AlamofireImage
import CoreMotion

class EDPaySuccessVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    var merchantData = JSON()
    
    var motionManager = CMMotionManager()
    
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblMerchantAmount: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblTransactionNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnSubmit: DesignableButton!
    @IBOutlet weak var imageViewMerchant: RoundableImageView!
    let downloader = ImageDownloader()
    var xInPositiveDirection = 0.0
    var xInNegativeDirection = 0.0
    var shakeCount = 0

    var tempVariable = 0
    
    var isStartShake = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.initGUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        motionManager.stopAccelerometerUpdates()
    }
    
    func initGUI()
    {
        motionManager = CMMotionManager()
        
        merchantData = segueData[self.constant.key_data] as! JSON
        lblMerchantName.text = merchantData[self.constant.key_trade_name].string
        lblMerchantAmount.text = "\(merchantData[self.constant.key_currency_code].string ?? "") \(merchantData[self.constant.key_paid_amount].string ?? "")"
        lblPaymentMethod.text = merchantData[self.constant.key_payment_method].string
        lblTransactionNo.text = merchantData[self.constant.key_transaction_no].string
        lblDate.text = merchantData[self.constant.key_transaction_date].string
        
        self.log("merchantData \(merchantData)")
                
        if let imageurl = merchantData[self.constant.key_shop_facade].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewMerchant.image = image
                }
            }
        }
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
        
        let alert = CustomAlert(title: "", image: UIImage.gifImageWithName("ic_gif_1")!, content: "Shake Later")
        alert.show(animated: true)
        
        //TODO
//        let alert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Shake Later", style: .default, handler: { action in
//            self.performSegue(withIdentifier: "segueMain", sender: nil)
//        }))
//
//        self.present(alert, animated: true)
        
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: OperationQueue.main) { (data, error) in
//                print("motionManager \(data)")
                if data!.acceleration.x > 1.0 || data!.acceleration.x < -1.0 {



                    if data!.acceleration.x > 1.0 {
                        self.xInPositiveDirection = data!.acceleration.x
                    }

                    if data!.acceleration.x < -1.0 {
                        self.xInNegativeDirection = data!.acceleration.x
                    }

                    if self.xInPositiveDirection != 0.0 && self.xInNegativeDirection != 0.0 {
                        self.shakeCount = self.shakeCount + 1
                        self.xInPositiveDirection = 0.0
                        self.xInNegativeDirection = 0.0
                    }

                    if self.shakeCount > 2 {
                        self.tempVariable = self.tempVariable + 1
                        print("EDPaySuccessVC Shaken! \(self.tempVariable)")
                        self.shakeCount = 0
                    }
                    
                    if self.tempVariable == 2 && !self.isStartShake
                    {
                        self.isStartShake = true
                        alert.dismiss(animated: true)
                        var segueData = Dictionary<String, AnyObject>()
                        segueData[self.constant.key_amount] = self.lblTransactionNo.text as AnyObject
                        segueData[self.constant.key_payment_method] = self.lblPaymentMethod.text as AnyObject
                        self.performSegue(withIdentifier: "segueShake", sender: segueData)
                    }
                }
            }
        }
    }
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
       print(motion)
       if motion == .motionShake {
          print("shake was detected")
       }
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
       // Code you want to implement.
        if motion == .motionShake {
            print("motionEnded Shake Gesture Detected")
            //show some alert here
        }
    }
    
    override func motionCancelled(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
       // code you want to implement.
    }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueShake" {
            if segue.destination is EDPayShakeRewardVC
            {
                if let chidVC = segue.destination as? EDPayShakeRewardVC {
                    self.log("chidVC \(chidVC)")
                    chidVC.segueData = (sender as? [String:AnyObject])!
                }
            }
        }
    }
}
