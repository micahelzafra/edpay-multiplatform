//
//  ReportSelectionVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS dev-zafyrajinx on 2/19/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class ReportSelectionVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()

    var segueData = Dictionary<String, AnyObject>()
    
    let downloader = ImageDownloader()
    
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblStatusDate: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    
    @IBOutlet weak var lblRiderName: UILabel!
    @IBOutlet weak var lblPlateNo: UILabel!
    
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGUI()
    }
    
    func initGUI()
    {
        self.log("passed data: \(segueData)")
    }

    @IBAction func didTapBackButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: false)
    }
    
    @IBAction func didTapRiderReportButton(_ sender: DesignableButton) {
        var segueData = Dictionary<String, AnyObject>()
        segueData[self.constant.key_data] = self.segueData as AnyObject
        segueData[self.constant.key_amount] = "" as AnyObject
        self.performSegue(withIdentifier: "segueReport", sender: segueData)
    }

    @IBAction func didTapMerchantReportButton(_ sender: DesignableButton) {
        var segueData = Dictionary<String, AnyObject>()
        segueData[self.constant.key_data] = self.segueData as AnyObject
        segueData[self.constant.key_amount] = "" as AnyObject
        self.performSegue(withIdentifier: "segueReport", sender: segueData)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueReport" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? ReportVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
}
