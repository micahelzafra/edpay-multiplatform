package com.ed2e.ed2eapp.view.activity.main.home

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.RewardsLogAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.model.Cashback
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsActivity
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayShakeReward
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edpay_shake_reward_logs.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class RewardActivity : BaseActivity(), CoroutineScope {

    private val modelList: List<Cashback>? = null
    var recyclerView: RecyclerView? = null
    var recyclerAdapter: RewardsLogAdapter? = null

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference
    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    private var arraylist = ArrayList<Any>()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_shake_reward_logs)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
        initListener()
    }

    private fun initGUI() {


        adapter = object : BaseRecyclerViewAdapter<Any>(arraylist, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {

            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.child_rewards_logs
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }
        activity_edpay_shake_reward_logs_recyclerview.layoutManager= LinearLayoutManager(context)
        activity_edpay_shake_reward_logs_recyclerview.setHasFixedSize(true)
        activity_edpay_shake_reward_logs_recyclerview.adapter=adapter

        val c = Calendar.getInstance().time
        val df = SimpleDateFormat(date_format_picker_display, Locale.US)
        val formattedDate = df.format(c)

        activity_edpay_shake_reward_logs_button_date_from.text = get30DaysBeforeDate().asString()
        activity_edpay_shake_reward_logs_button_date_to.text = formattedDate

        activity_edpay_shake_reward_logs_button_date_from.setOnClickListener {

            val cal = Calendar.getInstance()
            val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
            val date: Date = sdf.parse(activity_edpay_shake_reward_logs_button_date_from.text.toString())
            cal.time = date

            val dpd =  DatePickerDialog(this@RewardActivity,
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                           dayOfMonth: Int) {
                        initRewardsLogs(
                            preference.getString(key_user_id),
                            "NG",
                            setDateToButton(cal, year,monthOfYear, dayOfMonth, activity_edpay_shake_reward_logs_button_date_from),
                            "${activity_edpay_shake_reward_logs_button_date_to.text}")
                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH))
            val dateto: Date = sdf.parse(activity_edpay_shake_reward_logs_button_date_to.text.toString())
            cal.time = dateto
            dpd.getDatePicker().maxDate = cal.getTimeInMillis()
            dpd.show()

        }

        activity_edpay_shake_reward_logs_button_date_to.setOnClickListener {

            val cal = Calendar.getInstance()
            val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
            val date: Date = sdf.parse(activity_edpay_shake_reward_logs_button_date_to.text.toString())
            cal.time = date

            val dpd =  DatePickerDialog(this@RewardActivity,
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                           dayOfMonth: Int) {
                        initRewardsLogs(
                            preference.getString(key_user_id),
                            "NG",
                            "${activity_edpay_shake_reward_logs_button_date_from.text}",
                            setDateToButton(cal, year,monthOfYear, dayOfMonth, activity_edpay_shake_reward_logs_button_date_to))
                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH))
            val dateto: Date = sdf.parse(activity_edpay_shake_reward_logs_button_date_from.text.toString())
            cal.time = dateto
            dpd.getDatePicker().minDate = cal.timeInMillis
            dpd.show()

        }


        initRewardsLogs(
            preference.getString(key_user_id),
            "NG",
            "${activity_edpay_shake_reward_logs_button_date_from.text}",
            "${activity_edpay_shake_reward_logs_button_date_to.text}")

        initAPIGetRewardsHistory(
            preference.getString(key_user_id),
            "NG",
            "${activity_edpay_shake_reward_logs_button_date_from.text}",
            "${activity_edpay_shake_reward_logs_button_date_to.text}")
    }

    private fun initAPIGetRewardsHistory(
        customer_id: String,
        country_code: String,
        date_from: String,
        date_to: String
    ) {
        //arraylist.clear()

        val formatted_date_from = dateFormat(date_from, date_format_picker_display,date_format_server_api).asString()
        val formatted_date_to = dateFormat(date_to, date_format_picker_display,date_format_server_api).asString()
        val params = "customer_id:" + customer_id + "||" +
                "country_code:" + country_code + "||" +
                "date_from:" + formatted_date_from + "||" +
                "date_to:" + formatted_date_to
        Timber.d("PARAMS: %s", params)


        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlRewardsHistory,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString

                if (status == status_success) {

                    Timber.d("SUCCESS CASHBACK 2: %s", response)
                    val jsonArray = jsonObj[key_data].asJsonArray
////                    arraylist.clear()
//                    var amount: Double = 0.00
//                    var currency = ""
//                    for (data in jsonArray)
//                    {
//                        arraylist.add(data)
//                    }
//
//                    for (i in 0 until jsonArray.size()) {
//                        try {
//                            val jsonItem = jsonArray[i].asJsonObject
//                            val cashback = jsonItem[key_cashback].asJsonObject
//                            val cashback_amount = cashback[key_cashback].asFloat
//                            val country = jsonItem[key_country].asJsonObject
//                            currency = country[key_currency_code].asString
//                            amount += cashback_amount
//                        }
//                        catch (e : Exception)
//                        {
//                            Timber.e("cashback Exception $e ")
//                        }
//
//                    }
//                    val displayed_amount = "$currency ${commaSeparated(amount.toString().replace(",","")).asString()}"
//                    activity_edpay_shake_reward_logs_textview_reward_amount.text = displayed_amount
//                    updateList()
                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                    //updateList()
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
            //updateList()
        }
    }

    private fun initRewardsLogs(
        customer_id: String,
        country_code: String,
        date_from: String,
        date_to: String
    ) {
        arraylist.clear()

        val formatted_date_from = dateFormat(date_from, date_format_picker_display,date_format_server_api).asString()
        val formatted_date_to = dateFormat(date_to, date_format_picker_display,date_format_server_api).asString()
        val params = "customer_id:" + customer_id + "||" +
                "country_code:" + country_code + "||" +
                "date_from:" + formatted_date_from + "||" +
                "date_to:" + formatted_date_to
        Timber.d("PARAMS: %s", params)


        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlQRTransactionLogs,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString

                if (status == status_success) {

                    Timber.d("SUCCESS CASHBACK: %s", response)
                    val jsonArray = jsonObj[key_data].asJsonArray
//                    arraylist.clear()
                    var amount: Double = 0.00
                    var currency = ""
                    for (data in jsonArray)
                    {
                        arraylist.add(data)
                    }

                    for (i in 0 until jsonArray.size()) {
                        try {
                            val jsonItem = jsonArray[i].asJsonObject
                            val cashback = jsonItem[key_cashback].asJsonObject
                            val cashback_amount = cashback[key_cashback].asFloat
                            val country = jsonItem[key_country].asJsonObject
                            currency = country[key_currency_code].asString
                            amount += cashback_amount
                        }
                        catch (e : Exception)
                        {
                            Timber.e("cashback Exception $e ")
                        }

                    }
                    val displayed_amount = "$currency ${commaSeparated(amount.toString().replace(",","")).asString()}"
                    activity_edpay_shake_reward_logs_textview_reward_amount.text = displayed_amount
                    updateList()
                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                    updateList()
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
            updateList()
        }
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        when (tag) {
            tag_intent_shake_activity -> {
                val parser = JsonParser()
                val jsonObj = parser.parse(value.toString()).asJsonObject
                val transaction_no = jsonObj[key_transaction_no].asString

                val intent : Intent
                intent = Intent(context, EDPayShakeReward::class.java)
                intent.putExtra(key_amount, transaction_no)
                try {
                    intent.putExtra(key_payment_method, jsonObj[key_payment_type].asString)
                }
                catch (e: Exception)
                {
                    intent.putExtra(key_payment_method, "1")
                }
                startActivity(intent)
            }
        }
    }


    private fun setDateToButton(cal: Calendar, year: Int, monthOfYear: Int,
                                dayOfMonth: Int, button: Button
    ) : String
    {
        val sdf = SimpleDateFormat(date_format_picker_display,Locale.US)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        button.text = sdf.format(cal.time)
        return  button.text.toString()
    }

    private fun updateList()
    {
        this@RewardActivity.runOnUiThread {
            val parser = JsonParser()
            Collections.sort(arraylist,
                Comparator {
                        jinks, jinx -> parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
                })
            adapter.notifyDataSetChanged()
        }
    }


    private fun initToolBar() {
        edpay_shake_reward_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edpay_shake_reward_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edpay_shake_reward_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
    }


    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
