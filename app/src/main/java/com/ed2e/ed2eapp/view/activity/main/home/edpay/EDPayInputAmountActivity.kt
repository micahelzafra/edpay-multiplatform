package com.ed2e.ed2eapp.view.activity.main.home.edpay

import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.EDPayActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edfood_cart.*
import kotlinx.android.synthetic.main.activity_edpay_input_amount.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import java.text.DecimalFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class EDPayInputAmountActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isEnabled:Boolean = false
    private var mAmountInput = "0"
    private var mAmountDiscount = "0"
    private var isVoucherCodeValid = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_input_amount)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
    }

    private fun initGUI()
    {
        getCountryCurrency()

        val bundle :Bundle ?=intent.extras
        val data = bundle!!.getString(key_data) // 1
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        val trade_name = jsonObj[key_trade_name].asString
        val merchantID = jsonObj[key_id].asString

        var shop_facade = ""
        if(jsonObj[key_shop_facade] == null || jsonObj[key_shop_facade].toString() == "" || jsonObj[key_shop_facade].toString() == "{}" || jsonObj[key_shop_facade].toString() == "null") {
            Glide.with(this@EDPayInputAmountActivity).load(R.drawable.ic_default_merchant).into(edpay_pin_imageview_merchant_logo)
        } else {
            shop_facade = jsonObj[key_shop_facade].asString
            Glide.with(this@EDPayInputAmountActivity).load(shop_facade).into(edpay_pin_imageview_merchant_logo)
        }

        edpay_pin_textview_merchant_name.text = trade_name

//        Glide.with(this@EDPayInputAmountActivity)
//            .load(shop_facade)
//            .apply(
//                RequestOptions().override(
//                    256,
//                    256
//                ).centerCrop().placeholder(R.drawable.logo_edpay)
//            )
//            .into(
//                edpay_pin_imageview_merchant_logo
//            )

        //Log.d("Logs", "Merchant Data: " + data)
        //showToast_short("Merchant: " + edpay_pin_textview_merchant_name.text.toString())
        enableNext(isEnabled)

        edpay_input_amount_button_confirm.setOnClickListener {
            if (edpay_input_amount_edittext_amount.text.toString().trim().replace(
                    ",",
                    ""
                ).equals("", ignoreCase = true)
                || edpay_input_amount_edittext_amount.text.toString().trim { it <= ' ' }.toDouble() == 0.0
            ) {

                showDialog_main(
                    "",
                    "",
                    true,
                    resources.getString(R.string.dialog_invalid_amount_title),
                    resources.getString(R.string.dialog_invalid_amount_message),
                    "",
                    "OK"
                )

            } else {
                enableNext(isEnabled)
                hideKeyboard(this@EDPayInputAmountActivity)
            }
        }

        edpay_input_amount_button_apply.setOnClickListener {
            if ( edpay_input_amount_edittext_promo.text.toString().trim().replace(
                    ",",
                    ""
                ).equals("", ignoreCase = true)) {

//                showDialog_APIError("", "", true, "ED41", "", "OK")
                showPromoStatus(true, false, "Invalid promo code", "")

            } else {
                showPromoStatus(false, false, "", "")
                initAPICheckPromoCode(preference.getString(key_user_id), merchantID,  edpay_input_amount_edittext_promo.text.toString(), edpay_input_amount_edittext_amount.text.toString().trim().replace(",", ""  ))
            }

            }

        edpay_input_amount_button_slide_to_pay.setOnFinishListener {
            edpay_input_amount_button_slide_to_pay.reset()

            if (edpay_input_amount_textview_pay.text.toString().trim().replace(
                    ",",
                    ""
                ).equals("", ignoreCase = true)
                || edpay_input_amount_textview_pay.text.toString().trim { it <= ' ' }.replace(
                    ",",
                    ""
                ).toDouble() == 0.0
            ) {

                showDialog_main(
                    "",
                    "",
                    true,
                    resources.getString(R.string.dialog_invalid_amount_title),
                    resources.getString(R.string.dialog_invalid_amount_message),
                    "",
                    "OK"
                )

            } else if(!edpay_input_amount_edittext_promo.text.toString().equals("") && !isVoucherCodeValid) {
//                showDialog_main(
//                    "",
//                    "",
//                    true,
//                    resources.getString(R.string.dialog_invalid_voucher_title),
//                    resources.getString(R.string.dialog_invalid_voucher_message),
//                    "",
//                    "OK"
//                )

                //showDialog_APIError("", "", true, "ED41", "", "OK")
                showPromoStatus(true, false, "Invalid promo code", "")
            } else {

                val intent = Intent(this@EDPayInputAmountActivity, EDPayPINAcivity::class.java)
                intent.putExtra(key_data, data)
                intent.putExtra(key_amount, mAmountInput )
                intent.putExtra(key_currency_code, edpay_input_amount_textview_currency.text )
                intent.putExtra(key_promo_name, edpay_input_amount_edittext_promo.text.toString())
                intent.putExtra(key_discounted_amount, mAmountDiscount)
                startActivity(intent)
                finish()
                overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)

            }
        }

        edpay_input_amount_edittext_promo.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isVoucherCodeValid = false
                showPromoStatus(false, false, "", "")
            }
        })

        enableNext(false)
        showPromoStatus(false, false, "", "")
    }

    private fun initAPICheckPromoCode(customer_id: String, merchant_id: String, promo_code: String, total_purchased: String) {
        showProgress()

        val params = "customer_id:" + customer_id + "||" +
                "merchant_id:" + merchant_id + "||" +
                "promo_code:" + promo_code + "||" +
                "total_purchased:" + total_purchased

        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlEDPayPromoCode,
            params,
            { response: String ->
                hideProgress()
                Log.d("Logs", "DATA: | ")
                Log.d("Logs", "RESPONSE: $response")
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {

                    isVoucherCodeValid = true

                    val data = jsonObj[key_data].asJsonObject
                    val total_purchased_with_discount = data["total_purchased_with_discount"].asString
                    val discount_value = data["discount_value"].asString
                    val promo_name = data["promo_name"].asString

                    val formatter = DecimalFormat.getNumberInstance(Locale.US) as DecimalFormat /* new DecimalFormat("###,###.##");*/
                    formatter.applyPattern("###,##0.00")
                    mAmountDiscount = formatter.format(total_purchased_with_discount.replace(",","").toDouble())

                    runOnUiThread {
                        //showToast_short("Promo code valid")
                        edpay_input_amount_textview_pay.text = mAmountDiscount

                        showPromoStatus(true, true, promo_name, discount_value.replace(",",""))
                    }

                     /*val data = jsonObj[key_data].asJsonObject
                     val trade_name = data["trade_name"].asString
                     var product_services = ""
                     product_services = try {
                         "" + data["product_services"].asString
                     } catch (e: Exception) {
                         ""
                     }
                     var description = ""
                     description = try {
                         "" + data["description"].asString
                     } catch (e: Exception) {
                         ""
                     }
                     val business_address = data["business_address"].asString
                     val finalProduct_services = product_services
                     val finalDescription = description
                     runOnUiThread {
                         var shop_facade = ""
                         //showToast_short("show: " + data.get(key_shop_facade));
                         if (data[key_shop_facade] == null || data[key_shop_facade].toString().equals(
                                 "",
                                 ignoreCase = true
                             ) || data[key_shop_facade].toString().equals(
                                 "{}",
                                 ignoreCase = true
                             ) || data[key_shop_facade].toString().equals(
                                 "null",
                                 ignoreCase = true
                             )
                         ) { //showToast_short("null");
                             Glide.with(this@EDPayMerchantDetailsActivity)
                                 .load(R.drawable.ic_default_merchant).into(imageview_merchant_logo)
                         } else {
                             shop_facade =
                                 data[key_shop_facade].toString().replace("\"".toRegex(), "")
                             Glide.with(this@EDPayMerchantDetailsActivity).load(shop_facade)
                                 .into(imageview_merchant_logo)
                         }
                         textview_name.setText("" + trade_name)
                         textview_products.setText("" + finalProduct_services)
                         textview_desc.setText("" + finalDescription)
                         textview_business_address.setText(resources.getString(R.string.label_address).toString() + business_address)
                     }*/
                } else {
                    isVoucherCodeValid = false
                    val error_code = jsonObj[key_code].asString
                    val error_title = jsonObj["title"].asString

                    if(error_code == "ED41"){ } else {
                        showDialog_APIError("", "", true, error_code, "", "OK")
                    }


                    runOnUiThread {
                        showPromoStatus(true, false, error_title, "")
                    }
                    mAmountDiscount = "0"

                }
                null
            }
        ) { failure: Throwable? ->
            hideProgress()
            Log.d(EDPayMerchantDetailsActivity.TAG, "RESPONSE (ERROR): $failure")
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.message,
                "",
                "OK"
            )
            isVoucherCodeValid = false
            mAmountDiscount = "0"
            runOnUiThread {
                showPromoStatus(false, false, "", "")
            }
            null
        }
    }

    private fun getCountryCurrency() {
        var currencyCode = ""
        currencyCode = when (getCurrentLocationData(tag_current_location_country_code, this@EDPayInputAmountActivity)) {
            "PH" -> "PHP"
            "NG" -> "NGN"
            "MY" -> "RM"
            else -> ""
        }
        edpay_input_amount_textview_currency.text = currencyCode
    }


    private fun showPromoStatus(isPromoShow:Boolean, isSuccess:Boolean, promoName:String, promoDiscount:String)
    {
        if(isPromoShow){
            if(isSuccess) {
                val formatter = DecimalFormat.getNumberInstance(Locale.US) as DecimalFormat /* new DecimalFormat("###,###.##");*/
                formatter.applyPattern("###,##0.00")
                var final_promoDiscount = formatter.format(promoDiscount.replace(",", "").toDouble())
                edpay_input_amount_textView_promo_status.text = "($promoName) less discount - $final_promoDiscount"

                try {
                    edpay_input_amount_textView_promo_status.setTextColor(getResources().getColor(R.color.greenColor))
                } catch (e: Exception) {
                }
            } else {

                edpay_input_amount_textView_promo_status.text = promoName
                try {
                    edpay_input_amount_textView_promo_status.setTextColor(getResources().getColor(R.color.ed2e_red))
                } catch (e: Exception) {
                }
            }

            edpay_input_amount_textView_promo_status.visibility = VISIBLE
        } else {
            edpay_input_amount_textView_promo_status.visibility = GONE
        }
    }

    private fun enableNext(isEnable:Boolean)
    {
        Timber.d("enableNext %s",isEnable)
        edpay_input_amount_button_slide_to_pay.isEnabled = isEnable

        //edpay_input_amount_view_divider.isVisible = isEnable
        edpay_input_amount_textview_pay_title.isVisible = isEnable
        edpay_input_amount_textview_currency.isVisible = isEnable
        edpay_input_amount_textview_pay.isVisible = isEnable
        edpay_input_amount_button_slide_to_pay.isVisible = isEnable

        if (isEnable)
        {
            edpay_input_amount_edittext_amount.isEnabled = false
            edpay_input_amount_button_confirm.text = "Edit"
            edpay_input_amount_edittext_promo.isEnabled = true
            edpay_input_amount_button_apply.isEnabled = true
            //edpay_input_amount_button_confirm.setBackgroundColor(R.color.green)

            //editText_amount.setSelection(editText_amount.getText().length());
            val formatter = DecimalFormat.getNumberInstance(Locale.US) as DecimalFormat /* new DecimalFormat("###,###.##");*/
            formatter.applyPattern("###,##0.00")
            mAmountInput = formatter.format(edpay_input_amount_edittext_amount.text.toString().replace(",","").toDouble())
            edpay_input_amount_textview_pay.text = mAmountInput


            showPromoStatus(false, false, "", "")

        }
        else
        {
            edpay_input_amount_edittext_amount.isEnabled = true
            edpay_input_amount_button_confirm.text = "Confirm"
            edpay_input_amount_edittext_promo.isEnabled = false
            edpay_input_amount_button_apply.isEnabled = false

            edpay_input_amount_edittext_amount.requestFocus()

            edpay_input_amount_edittext_promo.text.clear()
            mAmountDiscount = "0"
            showPromoStatus(false, false, "", "")
        }
        isEnabled = !isEnable
    }



    private fun initToolBar() {
        edpay_input_amount_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edpay_input_amount_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edpay_input_amount_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        val intent = Intent(this@EDPayInputAmountActivity, EDPayActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}