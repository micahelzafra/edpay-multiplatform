//
//  EDFoodMerchantsVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-Zafyrajinx on 2/1/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class EDFoodMerchantsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var viewDelivery: DesignableCardView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfAutoComplete: DesignableTextField!
    @IBOutlet weak var tfSearch: UITextField!

    @IBOutlet weak var btnBack: UIBarButtonItem!
    private var PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
    private var GEO_API_BASE = "https://maps.googleapis.com/maps/api/geocode"
    private var TYPE_AUTOCOMPLETE = "/autocomplete"
    private var DETAILS = "/details"
    private var OUT_JSON = "/json"
    private var country_code = "NG"
    private var country_name = "Nigeria"
    private var isSelectPlace = false
    
    var resultList = [JSON]()
    var foodMerchantList = [JSON]()
    var searchList = [JSON]()
    var displayList = [String]()
    let dropDown = VPAutoComplete()

    let downloader = ImageDownloader()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDown.onTextField = self.tfAutoComplete // Your TextField
        dropDown.onView = self.view
        
        // ViewController's Viewx
        //dropDownTop.showAlwaysOnTop = true //To show dropdown always on top.
        dropDown.show { (str, index) in
            print("string : \(str) and Index : \(index)")
            self.tfAutoComplete.text = str[self.constant.key_description].string
            self.getLocationDetails(data: str)
        }
        tfAutoComplete.delegate = self
        tfAutoComplete.addTarget(self, action: #selector(textFieldShouldEndEditing(_:)), for: UIControlEvents.editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        initMerchantList(country_code: "NG", search: "", longitude: "", latitude: "")
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonClear(_ sender: UIButton) {
        self.tfAutoComplete.text = ""
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.resultList.removeAll()
        self.displayList.removeAll()
        self.log(textField.text)
        
        let escapedString = tfAutoComplete.text?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        var sb = "\(PLACES_API_BASE)\(TYPE_AUTOCOMPLETE)\(OUT_JSON)"
        sb.append("?key=\("api_key".localized())")
        sb.append("&components=country:NG")
        sb.append("&input=\(escapedString ?? "")")
        apiInterface.parseGoogle(url: sb,
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_ok {
                        if let jsonArray = json[self.constant.key_predictions].array {
                            for jinx in jsonArray
                            {
                                self.resultList.append(jinx)
                                let description = jinx[self.constant.key_description].string
                                self.displayList.append(description!)
                            }
                        }
                        else
                        {
                            self.log("jsonArray null")
                        }
                        self.dropDown.dataSource = self.resultList
                    }
                }
                catch let error
                {
                    print(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
        return true
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchList.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return searchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EDFoodMerchantTableViewCell", for: indexPath)
                        as! EDFoodMerchantTableViewCell

        let data = searchList[indexPath.row]
        let desc = String(format: "%.2f", data[self.constant.key_distance_from_customer].double!)
        let operating_info = data[self.constant.key_operating_info].array
        var merchantStatus = self.constant.label_close_today
        for jinx in operating_info!
        {
            if "\(jinx)".contains(getCurrentDay().asString())
            {
                merchantStatus = self.constant.label_open_today
            }
        }
        let food = EDFoodMerchant(
            foodImage: data[self.constant.key_restaurant_photo].string,
            foodName: data[self.constant.key_restaurant_name].string,
            foodRange: "\(desc) mi",
            foodLocation: data[self.constant.key_address].string,
            foodStatus: merchantStatus
        )
        if let imageurl = food.foodImage
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    cell.imageViewFood.image = image
                }
            }
        }
        cell.lblFoodName.text = food.foodName
        cell.lblFoodRange.text = food.foodRange
        cell.lblFoodLocation.text = food.foodLocation
        cell.lblFoodStatus.text = food.foodStatus
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = searchList[indexPath.row]
        let operating_info = data[self.constant.key_operating_info].array
        var merchantStatus = self.constant.label_close_today
        for jinx in operating_info!
        {
            if "\(jinx)".contains(getCurrentDay().asString())
            {
                merchantStatus = self.constant.label_open_today
            }
        }
        if merchantStatus == self.constant.label_close_today
        {
            self.handleError(
                displaytitle: self.constant.label_dialog_title_merchant_close, self.constant.label_dialog_msg_merchant_close)
            return
        }
        else
        {
            self.initUplacedOrderChecker(merchantdata: data, customer_id: self.defaults.string(forKey: self.constant.key_user_id)!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "segueMerchantItem") {
//
//            if let navController = segue.destination as? UINavigationController {
//                if let chidVC = navController.topViewController as? EDFoodMerchantItemVC {
//                        self.log("chidVC \(chidVC)")
//                        chidVC.segueData = (sender as? [String:AnyObject])!
//                }
//            }
//        }
        
        if (segue.identifier == "segueMerchantItem") {
            if let chidVC = segue.destination as? EDFoodMerchantItemVC {
                self.log("chidVC \(chidVC)")
                chidVC.segueData = (sender as? [String:AnyObject])!
            }
        }
    }
        
    private func initMerchantList(
        country_code: String,
        search: String,
        longitude: String,
        latitude: String
    ) {
      self.dismissKeyboard()
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlRestaurantList,
            params: "country_code:\(country_code)||search:\(search)||longitude:\(longitude)||latitude:\(latitude)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_success {

                      let jsonArray = json[self.constant.key_data].array
                      for data in jsonArray!
                      {
                        self.foodMerchantList.append(data)
                      }
                        self.searchList = self.foodMerchantList
                      self.tableView.reloadData()
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                          
                    }
                }
                catch let error
                {
                    print(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }

    func getLocationDetails(data: JSON)
    {
        let place_id = data[self.constant.key_place_id].string
        var sb = "\(PLACES_API_BASE)\(DETAILS)\(OUT_JSON)"
        sb.append("?key=\("api_key".localized())")
        sb.append("&place_id=\(place_id ?? "")")
        
        apiInterface.parseGoogle(url: sb,
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_ok {
                        let result = json[self.constant.key_result].self
                        let geometry = result[self.constant.key_geometry].self
                        let location = geometry[self.constant.key_location].self
                        let lat = location[self.constant.key_lat].double
                        let lng = location[self.constant.key_lng].double
                        self.initMerchantList(country_code: "NG", search: "", longitude: "\(lng ?? 0.00)", latitude: "\(lat ?? 0.00)")
                    }
                }
                catch let error
                {
                    print(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }

    func initUplacedOrderChecker(
        merchantdata: JSON,
        customer_id: String
    )
    {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCheckUnplacedOrder,
            params: "customer_id:\(customer_id)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_success {

                        let bookedRecord = json[self.constant.key_booked_record].string
                        if bookedRecord == "1"
                        {
                            let bookedId = json[self.constant.key_booked_id].string
                            self.initDeleteCartOrder(merchantdata: merchantdata, booked_id: bookedId!)
                        }
                        else
                        {
                            var segueData = Dictionary<String, AnyObject>()
                            segueData[self.constant.key_data] = merchantdata as AnyObject
                            segueData[self.constant.key_booked_location] = self.tfAutoComplete.text as AnyObject
                            segueData[self.constant.key_country_code] = self.country_code as AnyObject
                            segueData[self.constant.key_country] = self.country_name as AnyObject
                            self.performSegue(withIdentifier: "segueMerchantItem", sender: segueData)
                        }
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                          
                    }
                }
                catch let error
                {
                    print(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }

    func initDeleteCartOrder(
        merchantdata: JSON,
        booked_id: String
    )
    {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartDeleteOrder,
            params: "booked_id:\(booked_id)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_success {
                        self.initUplacedOrderChecker(merchantdata: merchantdata,customer_id: self.defaults.string(forKey: self.constant.key_user_id)!)
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                          
                    }
                }
                catch let error
                {
                    print(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}

