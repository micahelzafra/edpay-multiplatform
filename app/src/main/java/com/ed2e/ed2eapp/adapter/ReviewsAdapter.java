package com.ed2e.ed2eapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.Reviews;
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsDetailsActivity;

import java.util.List;

import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    Activity activity;
    List<Reviews> modelList;

    public ReviewsAdapter(Activity activity, List<Reviews> modelList) {
        this.activity = activity;
        this.modelList = modelList;
    }

    public void setModelList(List<Reviews> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReviewsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.child_edfood_merchant_more_info_reviews, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsAdapter.MyViewHolder myViewHolder, int position) {


        myViewHolder.textview_name.setText("" + modelList.get(position).getCustomer());
        myViewHolder.textview_date.setText("" + modelList.get(position).getReview_date());

        if(modelList.get(position).getReview() == null || modelList.get(position).getReview().equalsIgnoreCase("null") || modelList.get(position).getReview().equalsIgnoreCase("")) {
            myViewHolder.textView_review.setText("");
        } else {
            myViewHolder.textView_review.setText("" + modelList.get(position).getReview());
        }

        myViewHolder.ratingBar.setRating(Float.parseFloat("" + modelList.get(position).getRating()));

//        String image_path, image;
//        int index = modelList.get(position).getImagesList().size() - 1;
//
//        try {
//            if(index <= 0) {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            } else {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            }
//        } catch (Exception e) {
//            image_path = "";
//            image = "";
//        }
//
//
//        Glide.with(activity)
//                .load( modelList.get(position).getImage())
//                .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_ed2e_logo))
//                .into(myViewHolder.imageView);
//
//
//        String finalImage_path = image_path;
//        String finalImage = image;
//        myViewHolder.linearLayout_view_details.setOnClickListener(v -> {
//            //Toast.makeText(v.getContext(), /*"@" + position + " | " + */"View Details", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent (v.getContext(), AnnouncementDetailsActivity.class);
//            intent.putExtra(KEY_TITLE, modelList.get(position).getTitle());
//            intent.putExtra(KEY_EVENT_DATE, modelList.get(position).getEvent_date());
//            intent.putExtra(KEY_CREATED_DATE, formatDate_MMMMDDYYYY(modelList.get(position).getCreated_date()));
//            intent.putExtra(KEY_CONTENT, modelList.get(position).getContent());
//            intent.putExtra(KEY_IMAGE, "http://" + hostname + "/" + finalImage_path + "/" + finalImage);
//            activity.startActivity(intent);
//        });
//
//        myViewHolder.linearLayout_container.setOnClickListener(v -> {
//            //Toast.makeText(v.getContext(), "@" + position + " | " + "Share", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent (v.getContext(), AnnouncementsDetailsActivity.class);
//            intent.putExtra(key_announcement_title, modelList.get(position).getTitle());
//            intent.putExtra(key_announcement_content, modelList.get(position).getContent());
//            intent.putExtra(key_announcement_date, modelList.get(position).getEvent_date());
//            intent.putExtra(key_announcement_image, modelList.get(position).getImage());
//            activity.startActivity(intent);
//            //activity.finish();
//            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//        });

    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_name, textview_date, textView_review;
        RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_name = itemView.findViewById(R.id.child_edfood_merchant_more_info_textview_name);
            textview_date = itemView.findViewById(R.id.child_edfood_merchant_more_info_textview_date);
            textView_review = itemView.findViewById(R.id.child_edfood_merchant_more_info_textview_review);
            ratingBar = itemView.findViewById(R.id.child_edfood_merchant_more_info_ratingbar_review);

        }
    }
}
