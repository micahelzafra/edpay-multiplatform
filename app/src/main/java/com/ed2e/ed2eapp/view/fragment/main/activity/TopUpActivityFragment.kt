package com.ed2e.ed2eapp.view.fragment.main.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.R.layout.*
import com.ed2e.ed2eapp.util.*
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class TopUpActivityFragment : Fragment(fragment_activity_logs)
{
    lateinit var button_date_from: Button
    lateinit var button_date_to: Button
    fun getActionbar() : ActionBar?
    {
        return (activity as AppCompatActivity).supportActionBar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {

            button_date_from = view.findViewById(R.id.fragment_activity_logs_button_date_from)
            button_date_to = view.findViewById(R.id.fragment_activity_logs_button_date_to)


            val c = Calendar.getInstance().time
            val df = SimpleDateFormat(date_format_picker_display, Locale.US)
            val formattedDate = df.format(c)

            button_date_from.text = get30DaysBeforeDate().asString()
            button_date_to.text = formattedDate
            RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_topup, this))

            button_date_from.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_topup_from, this))
            }

            button_date_to.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_topup_to, this))
            }
        }
    }
}