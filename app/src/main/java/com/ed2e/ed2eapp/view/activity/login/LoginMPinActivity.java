package com.ed2e.ed2eapp.view.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginActivity;
import com.ed2e.ed2eapp.view.PinCreateAcivity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.ed2e.ed2eapp.view.activity.otp.ResetPINOTPActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_current_location;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_device_token;
import static com.ed2e.ed2eapp.util.ConstantKt.key_email_address;
import static com.ed2e.ed2eapp.util.ConstantKt.key_first_name;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_last_logged_in_mobile_num;
import static com.ed2e.ed2eapp.util.ConstantKt.key_last_name;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_referral_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.request_error_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_name;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;

public class LoginMPinActivity extends BaseActivity {

    public static final String TAG = LoginMPinActivity.class.getSimpleName();

    @BindView(R.id.login_mpin_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.login_mpin_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.login_mpin_pinview)
    PinView pinview;
    @BindView(R.id.login_mpin_button_log_in)
    Button button_log_in;

    @BindView(R.id.login_mpin_textView_forgot_pin)
    TextView textView_forgot_pin;


    private String mobileNumber = "";
    private String customerID = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_mpin);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initCurrentLocation();
        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            mobileNumber = "";
            customerID = "";
        } else {
            mobileNumber = extras.getString(key_mobile_number);
            customerID = extras.getString(key_cust_id);
        }



//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        textView_forgot_pin.setOnClickListener(v -> {
            showDialog_main("", "tag_login_mpin_button_forgot_pin", true, "Forgot Mobile PIN", "Do you wish to reset your Mobile PIN?", "CANCEL", "YES");
        });

        button_log_in.setOnClickListener(v -> {

            if(pinview.getText().toString().trim().length() < 6) {
                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
            } else {
                //showToast_short(pinview.getText().toString().trim());
                initAPILogin(mobileNumber, pinview.getText().toString().trim(), preference.getString(key_device_token), getCurrentLocationData(tag_current_location_country_name, LoginMPinActivity.this));
                //showDialog_main("", "button_ok", true,"Incorrect Mobile PIN", "Please make sure to enter the correct PIN.", "", "OK");
            }

        });


        pinview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(pinview.getText().toString().trim().length() >= 6) {
                    initAPILogin(mobileNumber, pinview.getText().toString().trim(), preference.getString(key_device_token), getCurrentLocationData(tag_current_location_country_name, LoginMPinActivity.this));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void initAPILogin(String mobileNumber, String mobilePIN, String device_token, String country)
    {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlLogin2,
                "mobile_number:"+ mobileNumber + "||" +
                        "pin:" + mobilePIN + "||" +
                        "device_id:" + device_token + "||" +
                        "country_name:" + country,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + mobileNumber + " | " + mobilePIN + " | " + device_token + " | " + country + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        JsonObject data = jsonObj.get(key_data).getAsJsonObject();
                        String user_id = data.get(key_id).getAsString();
                        String mobile_number = data.get(key_mobile_number).getAsString();
                        String pin = data.get(key_pin).getAsString();
                        String otp_verified = data.get(key_otp_verified).getAsString();
                        String referral_code = data.get(key_referral_code).getAsString();

                        String first_name = data.get("first_name").getAsString();
                        String last_name = data.get("last_name").getAsString();
                        String email_address = data.get("email_address").getAsString();

                        preference.putString(key_user_id, user_id);
                        preference.putString(key_user_data, data.toString());
                        preference.putString(key_referral_code, referral_code);

                        preference.putString(key_first_name, first_name);
                        preference.putString(key_last_name, last_name);
                        preference.putString(key_email_address, email_address);
                        preference.putString(key_mobile_number, mobile_number);

                        //showToast_short("Data Login: " + preference.getString(key_user_data, ""));

                        if (otp_verified.equals("0"))
                        {
                            Intent intent=new Intent(LoginMPinActivity.this, OTPActivity.class);
                            intent.putExtra(key_mobile_number, mobile_number);
                            intent.putExtra(key_cust_id, user_id);
                            intent.putExtra(key_prev_page, "login");
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        if (pin.equals("0"))
                        {
                            Intent intent=new Intent(LoginMPinActivity.this, CreateMPinActivity.class);
                            intent.putExtra(key_cust_id, user_id);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            return null;
                        }

                        preference.putString(key_last_logged_in_mobile_num, mobileNumber);

                        Intent intent=new Intent(LoginMPinActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();

                        if(error_code.equalsIgnoreCase("ED73")) {
                            showDialog_APIError("", "button_register", true, error_code, "CANCEL", "Register");
                        } else {
                            showDialog_APIError("", "", true, error_code, "", "OK");
                        }
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }



//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }




    private void initCurrentLocation() {
        getCurrentLocation(getContext());
        //Log.d("LOCATION", "Response Location: " + preference.getString(key_current_location, ""));
    }

    private void initPreviousActivity() {
        Intent intent = new Intent(getContext(), Login1Activity.class);
        intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {

        Intent intent;
        switch (requestTag) {
            case "button_register":
                intent = new Intent(LoginMPinActivity.this, RegistrationAcivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
                break;
            case "tag_login_mpin_button_forgot_pin":
                intent = new Intent(LoginMPinActivity.this, ResetPINOTPActivity.class);
                intent.putExtra(key_mobile_number, mobileNumber);
                intent.putExtra(key_cust_id, customerID);
                intent.putExtra(key_prev_page, "login");
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
