//
//  FormatterExtension.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-MichaelJinx on 1/28/20.
//

import Foundation

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
}
