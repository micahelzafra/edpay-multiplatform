//
//  LogoAnimationShakeView.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/11/20.
//

import Foundation

import UIKit
import SwiftyGif

class LogoAnimationShakeView: UIView {
    
    let logoGifImageView: UIImageView = {
        guard let gifImage = try? UIImage(gifName: "ic_gif_4.gif") else {
            return UIImageView()
        }
        return UIImageView(gifImage: gifImage, loopCount: 10)
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor(white: 246.0 / 255.0, alpha: 1)
        addSubview(logoGifImageView)
        logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
        logoGifImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoGifImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        logoGifImageView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        logoGifImageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
    }
}
