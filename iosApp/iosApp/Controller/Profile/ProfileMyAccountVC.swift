//
//  ProfileMyAccountVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/28/20.
//

import UIKit
import SwiftyJSON
import app

class ProfileMyAccountVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()

    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var svSaveCancel: UIStackView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    //textfield
    @IBOutlet weak var tfName: DesignableTextField!
    @IBOutlet weak var tfLastName: DesignableTextField!
    @IBOutlet weak var tfMobileNumber: DesignableTextField!
    @IBOutlet weak var tfEmail: DesignableTextField!
    
    //UIView
    @IBOutlet weak var vChangeMobilePIN: UIView!
    
    var alert = UIAlertController()
    
    var json: JSON = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        vChangeMobilePIN.isUserInteractionEnabled = true
        vChangeMobilePIN.addGestureRecognizer(tap)
        initGUI()
    }
    
    private func initGUI()
    {
        let userData = self.defaults.string(forKey: self.constant.key_user_data)
        let jsonData = userData!.data(using: String.Encoding.utf8)!
        let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
        json = JSON(jsonDic)
        
        self.log(userData)
        tfName.text = json[self.constant.key_first_name].string
        tfLastName.text = json[self.constant.key_last_name].string
        tfMobileNumber.text = json[self.constant.key_mobile_number].string
        tfEmail.text = json[self.constant.key_email_address].string
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
            performSegue(withIdentifier: "segueChangeMobilePIN", sender: nil)
        }

    @IBAction func didTapButtonBack(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapButtonEdit(_ sender: UIButton) {
        
        //if sender.isSelected
        //{
            self.svSaveCancel.isHidden = false
            self.btnEdit.isHidden = true
            self.tfName.isUserInteractionEnabled = true
            self.tfLastName.isUserInteractionEnabled = true
            self.tfEmail.isUserInteractionEnabled = true
            self.tfMobileNumber.isUserInteractionEnabled = true
//        }
//        else
//        {
//            self.tfName.isUserInteractionEnabled = true
//            self.tfLastName.isUserInteractionEnabled = true  
//            self.tfEmail.isUserInteractionEnabled = true
//            self.tfMobileNumber.isUserInteractionEnabled = true
//        }
//        sender.isSelected = !sender.isSelected
    }
    @IBAction func didTapButtonSave(_ sender: Any) {
//        self.tfName.isUserInteractionEnabled = true
//        self.tfLastName.isUserInteractionEnabled = true
//        self.tfEmail.isUserInteractionEnabled = true
        if self.tfName.text == json[self.constant.key_first_name].string
        && self.tfLastName.text == json[self.constant.key_last_name].string
        && self.tfMobileNumber.text == json[self.constant.key_mobile_number].string
        && self.tfEmail.text == json[self.constant.key_email_address].string
        {
            handleError("No information has been changed. Please try again.")
        }
        else
        {
            alert = UIAlertController(title: "label_dialog_title_update_account_info".localized(), message: "label_dialog_msg_update_account_info".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: self.constant.button_yes, style: .default, handler: { action in
                self.initAccountInfoUpdate(
                    customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
                    last_name: self.tfLastName.text!,
                    first_name: self.tfName.text!,
                    email_address: self.tfEmail.text!,
                    mobile_number: self.tfMobileNumber.text!,
                    prev_mobile_number: self.json[self.constant.key_mobile_number].string!
                )
            }))
            alert.addAction(UIAlertAction(title: self.constant.button_no, style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func didTapButtonCancel(_ sender: Any) {
        self.svSaveCancel.isHidden = true
        self.btnEdit.isHidden = false
        self.tfName.isUserInteractionEnabled = false
        self.tfLastName.isUserInteractionEnabled = false
        self.tfEmail.isUserInteractionEnabled = false
        self.tfMobileNumber.isUserInteractionEnabled = false
    }
    
    private func initAccountInfoUpdate(
        customer_id: String,
        last_name: String,
        first_name: String,
        email_address: String,
        mobile_number: String,
        prev_mobile_number: String
    ) {
        showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlUpdateUserInfo,
            params: "customer_id:\(customer_id)||last_name:\(last_name)||first_name:\(first_name)||email_address:\(email_address)||mobile_number:\(mobile_number)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    let data = json[self.constant.key_data].self

                    self.json[self.constant.key_first_name].string = data[self.constant.key_first_name].string
                    self.json[self.constant.key_last_name].string = data[self.constant.key_last_name].string
                    self.json[self.constant.key_mobile_number].string = data[self.constant.key_mobile_number].string
                    self.json[self.constant.key_email_address].string = data[self.constant.key_email_address].string
                    self.defaults.set("\(self.json)", forKey: self.constant.key_user_data)
                    self.dismiss(animated: true)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
    
}
