package com.ed2e.ed2eapp.view.activity.report;

import com.ed2e.ed2eapp.util.getOrderStatus;
import com.ed2e.ed2eapp.util.getLocalTimeStamp;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_country;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_dispute_type;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_merchant_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_order_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_order_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_reported_desc;
import static com.ed2e.ed2eapp.util.ConstantKt.key_reported_name;
import static com.ed2e.ed2eapp.util.ConstantKt.key_rider_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;

public class ReportSelectionActivity extends BaseActivity {

    public static final String TAG = ReportSelectionActivity.class.getSimpleName();

    @BindView(R.id.edfood_report_selection_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edfood_report_selection_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.edfood_report_selection_textview_transaction_id)
    TextView textview_transaction_id;
    @BindView(R.id.edfood_report_selection_textview_status_date)
    TextView textview_status_date;
    @BindView(R.id.edfood_report_selection_textview_amount)
    TextView textview_amount;
    @BindView(R.id.edfood_report_selection_textview_payment_type)
    TextView textview_payment_type;

    @BindView(R.id.edfood_report_selection_layout_rider_info)
    LinearLayout layout_rider_info;
    @BindView(R.id.edfood_report_selection_layout_merchant_info)
    LinearLayout layout_merchant_info;

    @BindView(R.id.edfood_report_selection_textview_rider_name)
    TextView textview_rider_name;
    @BindView(R.id.edfood_report_selection_textview_rider_plate_no)
    TextView textview_rider_plate_no;
    @BindView(R.id.edfood_report_selection_button_report_rider)
    Button button_report_rider;

    @BindView(R.id.edfood_report_selection_imageview_merchant_picture)
    ImageView imageview_merchant_picture;
    @BindView(R.id.edfood_report_selection_textview_merchant_name)
    TextView textview_merchant_name;
    @BindView(R.id.edfood_report_selection_textview_merchant_address)
    TextView textview_merchant_address;
    @BindView(R.id.edfood_report_selection_button_report_merchant)
    Button button_report_merchant;

    private String orderData = "";
    private String riderData = "";
    private String merchantData = "";
    private String orderID = "";
    private String riderID = "";
    private String merchantID = "";
    private String riderName = "";
    private String riderDesc = "";
    private String merchantName = "";
    private String merchantDesc = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edfood_report_selection);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            orderData = "";
            riderData = "";
            merchantData = "";
        } else {
            orderData = extras.getString(key_order_data);
            riderData = extras.getString(key_rider_data);
            merchantData = extras.getString(key_merchant_data);
        }

        Log.d(TAG, "orderData: " + orderData);
        Log.d(TAG, "riderData: " + riderData);
        Log.d(TAG, "merchantData: " + merchantData);

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        //initialize textviews from orderData
        String booked_id = "";
        String booked_status = "";
        String booked_date = "";
        String payment_type = "";
        String total = "";

        if(orderData == null || orderData.equalsIgnoreCase("") || orderData.equalsIgnoreCase("{}")) {

        } else {
            try {
                JSONObject obj = new JSONObject(orderData);
                JSONArray booked_data = obj.getJSONArray("booked");
                JSONObject order_details = booked_data.getJSONObject(0);

                //Log.d(TAG, "(NEW) orderData: " + order_details);

                try {
                    booked_id = "" + (String) order_details.get("booked_id");
                    orderID = booked_id;
                } catch (Exception e) {
                    booked_id = "" + order_details.getInt("booked_id");
                    orderID = booked_id;
                }

                try {
                    booked_status = "" + (String) order_details.get("booked_status");
                } catch (Exception e) {
                    booked_status = "" + order_details.getInt("booked_status");
                }
                booked_date = (String) order_details.get("booked_date");
                payment_type = (String) order_details.get("payment_type");

                JSONObject food_total = order_details.getJSONObject("food_total");
                total = (String) food_total.get("total");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        textview_transaction_id.setText("ORDER # " + booked_id);
        textview_status_date.setText(new getOrderStatus(booked_status).asString() + " - " + new getLocalTimeStamp(booked_date).asString());
        textview_amount.setText("" + total);
        textview_payment_type.setText("" + payment_type);

        //initialize textviews from riderData
        String rider_first_name = "";
        String rider_last_name = "";
        String rider_plate_no = "";

        if(riderData == null || riderData.equalsIgnoreCase("") || riderData.equalsIgnoreCase("{}")) {
            layout_rider_info.setVisibility(View.GONE);
        } else {
            layout_rider_info.setVisibility(View.VISIBLE);
            try {
                JSONObject obj = new JSONObject(riderData);
                JSONObject rider_details = obj.getJSONObject("rider_info");

                try {
                    riderID = "" + (String) rider_details.get("id");
                } catch (Exception e) {
                    riderID = "" + rider_details.getInt("id");
                }

                rider_first_name = (String) rider_details.get("first_name");
                rider_last_name = (String) rider_details.get("last_name");
                rider_plate_no = (String) rider_details.get("plate_no");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        riderName = rider_first_name + " " + rider_last_name;
        riderDesc = "" + rider_plate_no;
        textview_rider_name.setText(riderName);
        textview_rider_plate_no.setText(riderDesc);


        //initialize textviews from merchantData
        String merchant_name = "";
        String merchant_address = "";

        if(merchantData == null || merchantData.equalsIgnoreCase("") || merchantData.equalsIgnoreCase("{}")) {
            layout_merchant_info.setVisibility(View.GONE);
        } else {
            layout_merchant_info.setVisibility(View.VISIBLE);
            try {
                JSONObject merchant_details = new JSONObject(merchantData);

                try {
                    merchantID = "" + (String) merchant_details.get("id");
                } catch (Exception e) {
                    merchantID = "" + merchant_details.getInt("id");
                }

                merchant_name = (String) merchant_details.get("restaurant_name");
                merchant_address = (String) merchant_details.get("address");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        merchantName = "" + merchant_name;
        merchantDesc = "" + merchant_address;
        textview_merchant_name.setText(merchantName);
        textview_merchant_address.setText(merchantDesc);




//        try {
//            JSONObject obj = new JSONObject(response);
//            JSONArray announcement_data = obj.getJSONArray("announcement_records");
//
//            //showToast_short("SUCCESS pO: " + announcement_data.length());
//
//
//            for(int i = 0; i < announcement_data.length(); i++)
//            {
//
//                JSONObject object = announcement_data.getJSONObject(i);
//
//                String announcement_id;
//                try {
//                    announcement_id = "" + (String) object.get("id");
//                } catch (Exception e) {
//                    announcement_id = "" + object.getInt("id");
//                }
//
//                String title = (String) object.get("title");
//                String content = (String) object.get("content");
//                String event_date = (String) object.get("event_date");
//                String expiry_date = (String) object.get("expiry_date");
//                String image = (String) object.get("image");
//                String countries = "\"" + object.get("countries") + "\"";
//
//                String annc_status;
//                try {
//                    annc_status = "" + (String) object.get("annc_status");
//                } catch (Exception e) {
//                    annc_status = "" + object.getInt("annc_status");
//                }
//
//                Announcement list = new Announcement(
//                        announcement_id,
//                        title,
//                        content,
//                        event_date,
//                        expiry_date,
//                        image,
//                        countries,
//                        annc_status
//                );
//                modelList.add(list);
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        //initAPIGetAnnouncementsList();


        button_report_rider.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ReportActivity.class);
            intent.putExtra(key_order_id, orderID);
            intent.putExtra(key_dispute_type, "RIDER");
            intent.putExtra(key_id, riderID);
            intent.putExtra(key_reported_name, riderName);
            intent.putExtra(key_reported_desc, riderDesc);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        button_report_merchant.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ReportActivity.class);
            intent.putExtra(key_order_id, orderID);
            intent.putExtra(key_dispute_type, "RESTAURANT");
            intent.putExtra(key_id, merchantID);
            intent.putExtra(key_reported_name, merchantName);
            intent.putExtra(key_reported_desc, merchantDesc);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

    private void initAPIGetAnnouncementsList() {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlAnnouncementList,"",
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | ");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    if (status.equals(status_success))
                    {

//                        try {
//                            JSONObject obj = new JSONObject(response);
//                            JSONArray announcement_data = obj.getJSONArray("announcement_records");
//
//                            //showToast_short("SUCCESS pO: " + announcement_data.length());
//
//                            runOnUiThread(() -> {
//                                if (announcement_data.length() == 0) {
//                                    textview_message.setVisibility(View.VISIBLE);
//                                } else {
//                                    textview_message.setVisibility(View.GONE);
//                                }
//                            });
//
//                            for(int i = 0; i < announcement_data.length(); i++)
//                            {
//
//                                JSONObject object = announcement_data.getJSONObject(i);
//
//                                String announcement_id;
//                                try {
//                                    announcement_id = "" + (String) object.get("id");
//                                } catch (Exception e) {
//                                    announcement_id = "" + object.getInt("id");
//                                }
//
//                                String title = (String) object.get("title");
//                                String content = (String) object.get("content");
//                                String event_date = (String) object.get("event_date");
//                                String expiry_date = (String) object.get("expiry_date");
//                                String image = (String) object.get("image");
//                                String countries = "\"" + object.get("countries") + "\"";
//
//                                String annc_status;
//                                try {
//                                    annc_status = "" + (String) object.get("annc_status");
//                                } catch (Exception e) {
//                                    annc_status = "" + object.getInt("annc_status");
//                                }
//
//                                Announcement list = new Announcement(
//                                        announcement_id,
//                                        title,
//                                        content,
//                                        event_date,
//                                        expiry_date,
//                                        image,
//                                        countries,
//                                        annc_status
//                                );
//                                modelList.add(list);
//                            }
//
//                            runOnUiThread(() -> {
//                                recyclerAdapter.setModelList(modelList);
//                            });
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }

    private void initPreviousActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "button_ok":
//                showToast_short("To Homescreen");
//                intent = new Intent(getContext(), CreateMPinActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;

            default:
                break;
        }
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
