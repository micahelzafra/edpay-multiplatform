//
//  BaseViewController.swift
//  ed2eapp
//
//  Created by ED2E Technology iOS on 17/01/2019.
//  Copyright © 2019 ED2E Technology iOS. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
import CoreLocation
import app

class BaseViewController: UIViewController, AVAudioPlayerDelegate, UITextFieldDelegate, CLLocationManagerDelegate {
    
    var audioPlayer: AVAudioPlayer?
    internal var constant = ConstantKt.self
    internal var defaults = UserDefaults.standard
    var locationManager: CLLocationManager!
    var eventDelegate: EventsDelegate!
    
    
    weak var returndelegate: ReturnButtonDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        audioPlayer?.delegate = self
//        getLocation()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addBackButton(_ imageName:String, _ transparent:Bool? = false) {
        let backButton = UIButton(type: .custom)
        let image = UIImage(named: imageName)!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        backButton.setImage(image, for: .normal) // Image can be downloaded from here below link
        //        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        if transparent!
        {
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }
    
    func addBackButton(navigation: UINavigationItem) {
        let backButton = UIButton(type: .custom)
        let image = UIImage(named: "toolbar_back.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        backButton.setImage(image, for: .normal) // Image can be downloaded from here below link
        //        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        
        navigation.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        print("backAction")
        let _ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true) { }
    }
    
    func showProgressImage()
    {
//        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        if tabBarController != nil
//        {
//            blurEffectView.frame = tabBarController!.view.bounds
//        }
//        else
//        {
//            blurEffectView.frame = view.bounds
//        }
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//        let gif = UIImage.gif(asset: "ic_loading") //for GIF
//        let imageView = UIImageView(image: gif)
////        let screenSize: CGRect = UIScreen.main.bounds
//        let screenSize: CGRect = UIScreen.main.bounds
//        imageView.frame = CGRect(x: 0, y: 0, width: screenSize.width-64, height: screenSize.height-64)
//        imageView.frame = CGRect(x: 0, y: 0, width: screenSize.width/2, height: screenSize.height/2)
//        imageView.contentMode = .scaleAspectFit
//        imageView.center = blurEffectView.contentView.center
//        blurEffectView.contentView.addSubview(imageView)
//        if tabBarController != nil
//        {
//            print("tabBarController")
//            tabBarController!.view.addSubview(blurEffectView)
//        }
//        else
//        {
//            view.addSubview(blurEffectView)
//        }
    }
    
    func hideProgressImage()
    {
        if tabBarController != nil
        {
            tabBarController!.view.subviews.compactMap {  $0 as? UIVisualEffectView }.forEach { $0.removeFromSuperview() }
        }
        else
        {
            view.subviews.compactMap {  $0 as? UIVisualEffectView }.forEach { $0.removeFromSuperview() }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func showDialog(title:String, message:String, actions:[UIAlertAction] = [UIAlertAction]())
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions
        {
            alert.addAction(action)
        }
        
        self.present(alert, animated: true)
    }
    
    func playAudioFile(file:String) {
        print("playAudioFile file \(file)")
        let path = Bundle.main.path(forResource: file, ofType:nil)!
        let url = URL(fileURLWithPath: path)
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
        } catch {
            // couldn't load file :(
            print("playAudioFile file \(file) not found")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            textField.resignFirstResponder()
            nextResponder.becomeFirstResponder()
        } else {
            if let nextResponder = textField.superview?.superview!.viewWithTag(nextTag) {
                textField.resignFirstResponder()
                nextResponder.becomeFirstResponder()
            } else {
                if let nextResponder = textField.superview?.superview!.superview!.viewWithTag(nextTag) {
                    textField.resignFirstResponder()
                    nextResponder.becomeFirstResponder()
                } else {
                    //            textField.superview?.viewWithTag(textField.tag)!.resignFirstResponder()
                    textField.resignFirstResponder()
                }
            }
        }
        
//        self.delegate?.returnKey(textField)
        
        if let returndelegate: ReturnButtonDelegate = self.returndelegate {
            returndelegate.returnKey(textField)
        }
        return true
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification){
        adjustingHeight(show: true, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        adjustingHeight(show: false, notification: notification)
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        // to be override in each use
    }
    
    func log(_ log:String?)
    {
        #if DEBUG
        print(log!)  // original URL request
        #endif
    }
    
    func handleError(actions: [UIAlertAction] = [UIAlertAction](),displaytitle: String = "",_ error: String?){
        var title = constant.label_dialog_title_error_1
        if displaytitle != ""
        {
            title = displaytitle
        }
        var message = error
        if let errorList = defaults.data(forKey: self.constant.key_error_list)
        {
            let decode = NSKeyedUnarchiver.unarchiveObject(with: errorList) as! [ErrorCode]
            for errorCode in decode
            {
                if errorCode.error_code == error
                {
                    title = errorCode.error_title
                    message = errorCode.error_message
                    break
                }
            }
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions
        {
            alert.addAction(action)
        }
        
        if actions.count == 0
        {
            let okAction = UIAlertAction(title: self.constant.button_okay, style: UIAlertActionStyle.default) {
                UIAlertAction in
            }
            alert.addAction(okAction)
        }
        
        self.present(alert, animated: true)
    }
    
    func getLocation()
    {
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        var data = Dictionary<String, Double>()
        data[self.constant.key_latitude] = location.coordinate.latitude
        data[self.constant.key_longitude] = location.coordinate.longitude
        if self.eventDelegate != nil{
//            self.log("locationManager \(data)")
            self.eventDelegate.eventData(event: CommonConstants.tag_load_current_location, data: data as AnyObject)
        }
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
    }
    
    func publish(delegate: EventsDelegate?,event:Int, data:AnyObject)
    {
        if delegate != nil{
            self.log("publish \(data)")
            delegate!.eventData(event: event, data: data as AnyObject)
        }
    }
}

protocol ReturnButtonDelegate: class {
    func returnKey(_ textField: UITextField)
}
