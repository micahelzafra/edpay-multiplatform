package com.ed2e.ed2eapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsDetailsActivity;

import java.util.List;

import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;

public class AnnouncementListHomeAdapter extends RecyclerView.Adapter<AnnouncementListHomeAdapter.MyViewHolder> {

    Activity activity;
    List<Announcement> modelList;

    public AnnouncementListHomeAdapter(Activity activity, List<Announcement> modelList) {
        this.activity = activity;
        this.modelList = modelList;
    }

    public void setModelList(List<Announcement> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AnnouncementListHomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.child_announcements_home, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnouncementListHomeAdapter.MyViewHolder myViewHolder, int position) {
//        edpay_input_amount_textview_pay
//        if( edpay_input_amount_textview_pay.getText().toString().trim().replaceAll(",", "").equalsIgnoreCase("") || Double.parseDouble(myViewHolder.textview_title.getText().toString().trim()) == 0){
//
//        } else {
//
//        }

        myViewHolder.textview_title.setText("" + modelList.get(position).getTitle());
        myViewHolder.textview_content.setText("" + modelList.get(position).getContent());
        myViewHolder.textView_event_date.setText("" + modelList.get(position).getEvent_date());

        ViewGroup.LayoutParams params =  myViewHolder.linearLayout_main_container.getLayoutParams();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        params.width = (width/3);
        myViewHolder.linearLayout_main_container.setLayoutParams(params);

//        String image_path, image;
//        int index = modelList.get(position).getImagesList().size() - 1;
//
//        try {
//            if(index <= 0) {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            } else {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            }
//        } catch (Exception e) {
//            image_path = "";
//            image = "";
//        }
//
//
        Glide.with(activity)
                .load( modelList.get(position).getImage())
                .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_ed2e_logo))
                .into(myViewHolder.imageView);
//
//
//        String finalImage_path = image_path;
//        String finalImage = image;
//        myViewHolder.linearLayout_view_details.setOnClickListener(v -> {
//            //Toast.makeText(v.getContext(), /*"@" + position + " | " + */"View Details", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent (v.getContext(), AnnouncementDetailsActivity.class);
//            intent.putExtra(KEY_TITLE, modelList.get(position).getTitle());
//            intent.putExtra(KEY_EVENT_DATE, modelList.get(position).getEvent_date());
//            intent.putExtra(KEY_CREATED_DATE, formatDate_MMMMDDYYYY(modelList.get(position).getCreated_date()));
//            intent.putExtra(KEY_CONTENT, modelList.get(position).getContent());
//            intent.putExtra(KEY_IMAGE, "http://" + hostname + "/" + finalImage_path + "/" + finalImage);
//            activity.startActivity(intent);
//        });
//
        myViewHolder.linearLayout_container.setOnClickListener(v -> {
            //Toast.makeText(v.getContext(), "@" + position + " | " + "Share", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent (v.getContext(), AnnouncementsDetailsActivity.class);
            intent.putExtra(key_announcement_title, modelList.get(position).getTitle());
            intent.putExtra(key_announcement_content, modelList.get(position).getContent());
            intent.putExtra(key_announcement_date, modelList.get(position).getEvent_date());
            intent.putExtra(key_announcement_image, modelList.get(position).getImage());
            activity.startActivity(intent);
            //activity.finish();
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_title, textview_content, textView_event_date;
        ImageView imageView;
        LinearLayout linearLayout_container, linearLayout_main_container;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_title = itemView.findViewById(R.id.child_announcement_textview_title);
            textview_content = itemView.findViewById(R.id.child_announcement_textview_desc);
            textView_event_date = itemView.findViewById(R.id.child_announcement_textview_date);
            imageView = itemView.findViewById(R.id.child_announcement_imageview_logo);

            linearLayout_container = itemView.findViewById(R.id.child_announcement_inearLayout_container);
            linearLayout_main_container = itemView.findViewById(R.id.child_announcement_linearLayout_main_container);
        }
    }
}
