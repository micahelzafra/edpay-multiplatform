package com.ed2e.ed2eapp.view.activity.onboarding;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.OnboardingAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity;
import com.ed2e.ed2eapp.view.activity.registration.RegistrationAcivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.KEY_FIRST_TIME_LAUNCH;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin2;

public class OnboardingActivity extends BaseActivity {

    public static final String TAG = OnboardingActivity.class.getSimpleName();

    @BindView(R.id.activity_onboarding_linearLayout_main)
    LinearLayout linearLayout_main;
    @BindView(R.id.activity_onboarding_viewPager)
    ViewPager viewPager_onboarding;
    @BindView(R.id.activity_onboarding_linearLayout_2)
    LinearLayout linearLayout_2;
    @BindView(R.id.activity_onboarding_textView_skip)
    TextView button_skip;
    @BindView(R.id.activity_onboarding_textView_next)
    TextView button_next;
    @BindView(R.id.activity_onboarding_imageView_dots)
    ImageView imageView_dots;

    OnboardingAdapter onboardingAdapter;
    private TypedArray onboarding_dots;
    private String[] bgColor;
    private AppPreference preference;
    private CountDownTimer mCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);

        preference = getInstance(this);

        getCurrentLocation(OnboardingActivity.this);
        //initData();
        //initToolBar();
        initGUI();
    }

//    private void initData() {
//        Log.d(TAG, "initData() triggered");
//
//        Bundle extras = getIntent().getExtras();
//        if (extras == null) {
//            mobileNumber = "";
//        } else {
//            mobileNumber = extras.getString(key_mobile_number);
//        }
//
////        showToast_long(
////              "initData:\n" +
////                      "(" + mobileNumber + ")"
////        );
//    }

    private void initGUI() {
        Log.d(TAG,"initGUI() triggered");



//        if (Build.VERSION.SDK_INT < 16) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            View decorView = getWindow().getDecorView();
//            // Hide the status bar.
//            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(uiOptions);
//            // Remember that you should never show the action bar if the
//            // status bar is hidden, so hide that too if necessary.
//            ActionBar actionBar = getActionBar();
//            actionBar.hide();
//        }

        bgColor = getResources().getStringArray(R.array.array_onboarding_colors);
        linearLayout_main.setBackgroundColor(Color.parseColor(bgColor[0]));

        //set pager adapter
        onboardingAdapter = new OnboardingAdapter(this);
        viewPager_onboarding.setAdapter(onboardingAdapter);

        //set to first page of onboarding
        onboarding_dots= getResources().obtainTypedArray(R.array.array_onboarding_dots);
        imageView_dots.setImageResource(onboarding_dots.getResourceId(0, -1));

        viewPager_onboarding.setCurrentItem(0);
        updateButtons();

        button_skip.setOnClickListener(v -> {
            initLoginOrRegActivity();
        });

        button_next.setOnClickListener(v -> {
            if(viewPager_onboarding.getCurrentItem() == 3) {
                initLoginOrRegActivity();
            } else {
                moveNext();
            }


        });

        viewPager_onboarding.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                String nextValue = bgColor[i];
                linearLayout_main.setBackgroundColor(Color.parseColor(nextValue));
                updateButtons();
                imageView_dots.setImageResource(onboarding_dots.getResourceId(i, -1));
                startTimer();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        startTimer();
    }

    private long timerCount;

    private void startTimer() {

        runOnUiThread(() ->
        {
            if (mCountDownTimer != null) {
                mCountDownTimer.cancel();
            }

            mCountDownTimer = new CountDownTimer((3 * 1000), 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    moveNext();
                }

            };

            mCountDownTimer.start();


        });
    }


    private void initLoginOrRegActivity() {
        preference.putBoolean(KEY_FIRST_TIME_LAUNCH, false);

        Intent intent=new Intent(OnboardingActivity.this, LoginOrRegAcivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
    }

    public void moveNext() {
        Log.d(TAG,"moveNext() triggered");
        viewPager_onboarding.setCurrentItem(viewPager_onboarding.getCurrentItem() + 1);
        updateButtons();
    }

    private void updateButtons() {
        Log.d(TAG,"updateButtons() triggered");

        if(viewPager_onboarding.getCurrentItem() == 3) {
            button_skip.setVisibility(View.GONE);
            button_next.setText(getResources().getString(R.string.button_done));
        } else {
            button_skip.setVisibility(View.VISIBLE);
            button_next.setText(getResources().getString(R.string.button_next));
        }
    }






    private void initPreviousActivity() {
        Intent intent = new Intent(getContext(), Login1Activity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag) {
            case "button_register":
                Intent intent = new Intent(OnboardingActivity.this, RegistrationAcivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.no_anim);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

//    private void initToolBar() {
//        LinearLayout_left.setOnTouchListener((v, event) -> {
//            switch(event.getAction()) {
//                case MotionEvent.ACTION_DOWN:
//                    // PRESSED
//                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
//                    return true; // if you want to handle the touch event
//                case MotionEvent.ACTION_UP:
//                    // RELEASED
//                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
//                    initPreviousActivity();
//                    return true; // if you want to handle the touch event
//            }
//            return false;
//        });
//    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
