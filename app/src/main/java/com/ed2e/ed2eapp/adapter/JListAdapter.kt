package com.ed2e.ed2eapp.adapter


import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.util.key_description
import com.google.gson.JsonParser

class JListAdapter(private val context: Activity, private val data: ArrayList<Any>)
    : ArrayAdapter<Any>(context, R.layout.child_autocomplete_jinks, data) {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.child_autocomplete_jinks, null, true)

        val titleText = rowView.findViewById(R.id.child_autocomplete_jinks_textview) as TextView

        val parser = JsonParser()
        val jsonObj = parser.parse(data[position].toString()).asJsonObject
//            val predictionjsonObj = jsonArray[i].asJsonObject
        val description = jsonObj[key_description].asString
        titleText.text = description

        return rowView
    }
}