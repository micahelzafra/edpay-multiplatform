//
//  MapVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS dev-zafyrajinx on 2/11/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage
import GoogleMaps
import MapKit
import CoreLocation
import Firebase
import SwiftEventBus

class MapVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var mapView: MKMapView!
    
    private var bookedId = ""
    private var restaurantDetails = JSON()
    private var riderDispatchDetails = JSON()
    private var bookedLoc: CLLocationCoordinate2D? = nil
    private var merchantLoc: CLLocationCoordinate2D? = nil
    private var riderLoc: CLLocationCoordinate2D? = nil

    private var markers = [MKPointAnnotation]()
    
    var merchantData = JSON()
    
    let downloader = ImageDownloader()
        
    // 1- Init bottomSheetVC
    let bottomSheetVC = BottomSheetViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGUI()
        
        SwiftEventBus.onMainThread(self, name: CommonConstants.tag_notification) { result in
            // UI thread
            self.log("SwiftEventBus \(result?.object as! NSDictionary)")
            self.getOrderDetails(booked_id: self.bookedId)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self, name: CommonConstants.tag_notification)
    }
    
    func initGUI()
    {
//        // 1
//        let location = CLLocationCoordinate2D(latitude: 51.50007773,
//            longitude: -0.1246402)
//
//        // 2
//        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
//        let region = MKCoordinateRegion(center: location, span: span)
//            mapView.setRegion(region, animated: true)
//
//        //3
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = location
//        annotation.title = "Big Ben"
//        annotation.subtitle = "London"
//        mapView.addAnnotation(annotation)

        addBottomSheetView()
        
        let jsonObj = segueData[self.constant.key_data] as! JSON
        self.log("passed data \(jsonObj)")
        self.bookedId = "\(jsonObj[self.constant.key_booked_id].int ?? -1)"
        self.getOrderDetails(booked_id: bookedId)
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    func addBottomSheetView() {

        // 2- Add bottomSheetVC as a child view
        self.addChildViewController(bottomSheetVC)
        self.view.addSubview(bottomSheetVC.view)
        bottomSheetVC.didMove(toParentViewController: self)

        // 3- Adjust bottomSheet frame and initial position.
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    private func updateMarker()
    {
        self.markers.removeAll()
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        if (bookedLoc != nil)
        {
            let annotation = MKPointAnnotation()
            annotation.coordinate = bookedLoc!
            annotation.title = "Delivery Address"
            annotation.subtitle = "Delivery Address"
            self.markers.append(annotation)
            mapView.addAnnotation(annotation)

        }
        
        if (merchantLoc != nil)
        {
            let annotation = MKPointAnnotation()
            annotation.coordinate = merchantLoc!
            annotation.title = self.restaurantDetails[self.constant.key_restaurant_name].string
            annotation.subtitle = self.restaurantDetails[self.constant.key_restaurant_name].string
            self.markers.append(annotation)
            mapView.addAnnotation(annotation)

        }
        
        if (riderLoc != nil)
        {
            let annotation = MKPointAnnotation()
            annotation.coordinate = riderLoc!
            annotation.title = "Rider"
            annotation.subtitle = "Rider"
            self.markers.append(annotation)
            mapView.addAnnotation(annotation)

        }
        
        mapView.showAnnotations(self.markers, animated: true)
    }
    
    private func setData(jsonObj:JSON)
    {
        let jsonArray = jsonObj[self.constant.key_booked].array
        var data = JSON()
        for json in jsonArray!
        {
            data = json
        }
//        let foodTotal = data[self.constant.key_food_total].self
        restaurantDetails = data[self.constant.key_restaurant_details].self
        getLocationAddress(longitude: "",latitude: "",address: data[self.constant.key_booked_location].string)
        
        var lat: Double?
        if let doubleVal = restaurantDetails[self.constant.key_latitude].object as? Double {
            lat = doubleVal
        }
        else {
            lat = Double(restaurantDetails[self.constant.key_latitude].string ?? "0.00")
        }
        var lng: Double?
        if let doubleVal = restaurantDetails[self.constant.key_longitude].object as? Double {
            lng = doubleVal
        }
        else {
            lng = Double(restaurantDetails[self.constant.key_longitude].string ?? "0.00")
        }
        
        self.merchantLoc = CLLocationCoordinate2D(
            latitude: CLLocationDegrees(lat!),
            longitude: CLLocationDegrees(lng!
        ))
        
        
        if let json = data[self.constant.key_rider_dispatch_details].object as? Array<Any> {
            self.log("json \(json)")
            riderDispatchDetails = JSON()
        }
        else {
            riderDispatchDetails = data[self.constant.key_rider_dispatch_details].self
        }
        print("riderDispatchDetails \(riderDispatchDetails)")
        if !riderDispatchDetails.isEmpty
        {
            let riderInfo = riderDispatchDetails[self.constant.key_rider_info].self
            self.listenRiderLocation(riderId: "\(riderInfo[self.constant.key_id].int ?? -1)")
        }
        
        updateMarker()

        self.publish(delegate: bottomSheetVC.eventDelegate, event: CommonConstants.tag_load_data_order, data: jsonObj as AnyObject)
    }
    
    private func listenRiderLocation(riderId:String)
    {
        self.log("listenRiderLocation: \(riderId)")
        //Manual override for getting the db of rider app for ref https://firebase.google.com/docs/projects/multiprojects
        // Configure with manual options.
        let secondaryOptions = FirebaseOptions(googleAppID: "1:722702005898:ios:18124835b72d7fb8", gcmSenderID: "722702005898")
        secondaryOptions.bundleID = "com.ed2e.provider"
        secondaryOptions.apiKey = "AIzaSyBu9uvaX1wWBZws4Vdlpkodc3St_xNhWew"
        secondaryOptions.clientID = "722702005898-hem9rj9difvcdnlkfjc41lib48dmjh5m.apps.googleusercontent.com"
        secondaryOptions.databaseURL = "https://ed2e-provider-1563245918372.firebaseio.com"
        secondaryOptions.projectID = "ed2e-provider-1563245918372"

        // Configure an alternative FIRApp.
        if FirebaseApp.app(name: "riderDB") == nil {
            FirebaseApp.configure(name: "riderDB", options: secondaryOptions)
        }

        // Retrieve a previous created named app.
        guard let secondary = FirebaseApp.app(name: "riderDB")
          else { assert(false, "Could not retrieve riderDB") }
        
        self.log("listenRiderLocation: \(riderId)")

        // Retrieve a Real Time Database client configured against a specific app.
//        let secondaryDb = Database.database(app: secondary)

        let db = Firestore.firestore(app: secondary)
        db.collection("coordinates").document("ED2ENG-R:\(riderId)").addSnapshotListener { documentSnapshot, error in
            guard let document = documentSnapshot else {
                self.log("Error fetching document: \(error!)")
                return
            }
            guard let data = document.data() else {
                self.log("Document data was empty.")
                return
            }
            self.log("Current data: \(data)")
            
            let geoPoint: GeoPoint = data[self.constant.key_location] as! GeoPoint
            self.riderLoc = CLLocationCoordinate2D(
                latitude: geoPoint.latitude,
                longitude: geoPoint.longitude
            )
            self.updateMarker()
        }
    }
    
    private func getOrderDetails(
      booked_id: String
    ) {
    showLoading()
      apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlOrderHistoryDetails,
          params: "booked_id:\(booked_id)",
          success: { data in
              let jsonData = data.data(using: String.Encoding.utf8)!
              let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
              let json: JSON = JSON(jsonDic)
              
              let status = json[self.constant.key_status].string
              if status == self.constant.status_success {
                self.setData(jsonObj: json)
              }
              else
              {
                  let error = json[self.constant.key_code].string
                  self.handleError(error)
                    
              }

            self.hideLoading()
            return KotlinUnit()
      }, failure: {
        self.hideLoading()
        self.handleError($0?.message)
        return KotlinUnit()
      })
    }

    func getLocationAddress(longitude: String, latitude: String, address:String?)
    {
        showLoading()
        var sb = "https://maps.googleapis.com/maps/api/geocode/json"
        sb.append("?key=\("api_key".localized())")
        
        if address==nil
        {
            sb.append("&address=\(latitude),\(longitude)")
        }
        else
        {
            sb.append("&address=\(address ?? "")")
        }
        
        apiInterface.parseGoogle(url: sb,
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_ok {
                        if let jsonArray = json[self.constant.key_results].array {
                            for jinx in jsonArray
                            {
//                                self.resultList.append(jinx)
//                                let description = jinx[self.constant.key_description].string
//                                self.displayList.append(description!)
//                                let jinx = jsonArray[jinx].asJsonObject
//                                let formatted_address = jinx[self.constant.key_formatted_address].string
//                                let address_components = jinx[self.constant.key_address_components].array

                                let geometry = jinx[self.constant.key_geometry].self
                                let location = geometry[self.constant.key_location].self
                                let lat = location[self.constant.key_lat].double
                                let lng = location[self.constant.key_lng].double
                                self.bookedLoc = CLLocationCoordinate2D(latitude: lat!,
                                                                   longitude: lng!)
                                self.updateMarker()
                            }
                        }
                        else
                        {
                            self.log("jsonArray null")
                        }
                    }
                }
                catch let error
                {
                    print(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}
