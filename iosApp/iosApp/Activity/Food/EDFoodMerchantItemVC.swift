//
//  EDFoodMerchantItemVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS Dev-Zafyraminx on 2/1/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class EDFoodMerchantItemVC: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    //Details
    @IBOutlet weak var imageViewFoodMerchantItem: UIImageView!
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblClosingTime: UILabel!
    
    @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var titleBar: UIView!
    
    @IBOutlet weak var segmentedControl: ScrollableSegmentedControl!
    
    //Cart
    @IBOutlet weak var lblCartItems: BadgeSwift!
    @IBOutlet weak var lblCartTotalAmount: UILabel!
    @IBOutlet weak var viewCart: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    let downloader = ImageDownloader()
    
    var childScrollView: UIScrollView {
        return tableView
    }
    
    let segmentindicator: UIView = {

           let v = UIView()

           v.translatesAutoresizingMaskIntoConstraints = false
           v.isOpaque = false
           v.contentMode = .scaleAspectFit
           v.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "four_pillars_3_segment"))

           return v
       }()
        
    var goingUp: Bool?
    var childScrollingDownDueToParent = false
    
    var foodList = [JSON]()
    var categories = [String]()
    var bookedId = ""
    var country_name = ""
    var booked_location = ""
    var restaurant_id = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        parentScrollView.delegate = self
        self.tableView.isScrollEnabled = true

        let imageButton = #imageLiteral(resourceName: "ic_back_arrow").withRenderingMode(.alwaysTemplate) // use your UIImage here
            btnBack.setImage(imageButton, for: .normal) // assign it to your UIButton
            btnBack.tintColor = UIColor.white // set a color
        
        tableView.reloadData()
        
        self.view.addSubview(segmentindicator)

        let tapViewCart = UITapGestureRecognizer(target: self, action: #selector(tapViewCart(tapGestureRecognizer:)))
        self.viewCart.isUserInteractionEnabled = true
        self.viewCart.addGestureRecognizer(tapViewCart)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        setupView()
        foodList.removeAll()
        categories.removeAll()
        let data = segueData[self.constant.key_data] as! JSON
        self.log("passed data: \(data)")
        self.country_name = segueData[self.constant.key_country] as! String
        self.booked_location = segueData[self.constant.key_booked_location] as! String
        self.restaurant_id = "\(data[self.constant.key_restaurant_id].int ?? 0)"
        self.lblMerchantName.text = data[self.constant.key_restaurant_name].string
        self.lblDistance.text = "\(data[self.constant.key_distance_from_customer].double ?? 0.00) mi"
        self.lblDescription.text = data[self.constant.key_short_description].string
        
        if let imageurl = data[self.constant.key_restaurant_photo].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewFoodMerchantItem.image = image
                }
            }
        }
        
        let operatingInfo = data[self.constant.key_operating_info].array
        var closing = ""
        
        for info in operatingInfo!
        {
            if "\(info)".contains(getCurrentDay().asString())
            {
                let operatingInfoObj = info.self
                closing = operatingInfoObj["closing"].string!
            }
        }
        
        self.lblClosingTime.text = "Closing at \(closing)"
        self.initGetOrderID(
            customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
            restaurant_id: restaurant_id
        )
    }
    
    private func setupView() {
        setupSegmentedControl()
    }
    
    private func setupSegmentedControl() {
        
        // Configure Segmented Control
        segmentedControl.segmentStyle = .textOnly
            for n in 0...self.categories.count-1
            {
                self.segmentedControl.insertSegment(withTitle: self.categories[n], at: n)
            }

        segmentedControl.underlineSelected = true
        
        // change some colors
        segmentedControl.selectedSegmentContentColor = #colorLiteral(red: 0.006476058159, green: 0.07246912271, blue: 0.476346612, alpha: 1)
        segmentedControl.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        segmentedControl.tintColor = #colorLiteral(red: 0.006476058159, green: 0.07246912271, blue: 0.476346612, alpha: 1)
        
        // Select First Segment
        segmentedControl.selectedSegmentIndex = 0
    }
    
    func setupLayout() {
          segmentindicator.snp.makeConstraints { (make) in

              make.top.equalTo(segmentedControl.snp.bottom).offset(-10)
              make.height.equalTo(10)
              make.width.equalTo(75 + segmentedControl.titleForSegment(at: 0)!.count * 8)
              make.centerX.equalTo(segmentedControl.snp.centerX).dividedBy(segmentedControl.numberOfSegments)

          }

      }
    
//    IBAction func switchSegmentActivity(_ sender: Any){
//        updateView()
//
//              let numberOfSegments = CGFloat(segmentActivity.numberOfSegments)
//              let selectedIndex = CGFloat((sender as AnyObject).selectedSegmentIndex)
//              segmentindicator.snp.remakeConstraints { (make) in
//
//                  make.top.equalTo(segmentActivity.snp.bottom).offset(-10)
//                  make.height.equalTo(10)
//                  make.width.equalTo(75 + segmentActivity.titleForSegment(at: 0)!.count * 8)
//                  make.centerX.equalTo(segmentActivity.snp.centerX).dividedBy(numberOfSegments / CGFloat(3.0 + CGFloat(selectedIndex-1.0)*2.0))
//
//              }
//    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func tapViewCart(tapGestureRecognizer: UITapGestureRecognizer) {
        var segueData = Dictionary<String, AnyObject>()
        segueData[self.constant.key_booked_location] = self.booked_location as AnyObject
        //        segueData[self.constant.key_data] = food as AnyObject
        segueData[self.constant.key_country] = self.country_name as AnyObject
        segueData[self.constant.key_booked_id] = self.bookedId as AnyObject
        self.performSegue(withIdentifier: "segueViewCart", sender: segueData)
    }

    
    // MARK: - Table view data source
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if categories.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            self.log("sections addons.count \(categories.count)")
            tableView.restore()
        }
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if categories.count == 0
        {
            return ""
        }
        else
        {
            let data = categories[section]
            //let name = data[self.constant.key_category]
            return "\(data )"
        }
    }

    
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var categoryArr = [JSON]()
          
        for data in foodList
        {
            let category = data[self.constant.key_category].string
            if category == categories[section] {
                categoryArr.append(data)
            }
        }
        
        if categoryArr.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return categoryArr.count
      }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "EDFoodMerchantItemTableCell", for: indexPath) as! EDFoodMerchantItemTableCell
          
        var categoryArr = [JSON]()
        
        for data in foodList
        {
            let category = data[self.constant.key_category].string
            if category == categories[indexPath.section] {
                categoryArr.append(data)
            }
        }
        
          let food = categoryArr[indexPath.row]
            cell.lblFoodName.text = food[self.constant.key_food_name].string
            cell.lblPrice.text = food[self.constant.key_food_price].string

            cell.buttonCallback = { [unowned self] in
                var segueData = Dictionary<String, AnyObject>()
                segueData[self.constant.key_data] = food as AnyObject
                segueData[self.constant.key_booked_location] = self.booked_location as AnyObject
                segueData[self.constant.key_booked_id] = self.bookedId as AnyObject
                segueData[self.constant.key_restaurant_id] = self.restaurant_id as AnyObject
                self.performSegue(withIdentifier: "segueItemDetails", sender: segueData)
            }

            if let imageurl = food[self.constant.key_food_photo].string
            {
                let urlRequest = URLRequest(url: URL(string: imageurl)!)
                downloader.download(urlRequest, filter: nil) { response in
                    if let image = response.result.value {
                        print(image)
                        cell.imageViewFood.image = image
                    }
                }
            }
          
          return cell
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 110
      }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
          view.tintColor = #colorLiteral(red: 0.8457748294, green: 0.8457748294, blue: 0.8457748294, alpha: 1)
          let header = view as! UITableViewHeaderFooterView
          header.textLabel?.textColor = #colorLiteral(red: 0.006476058159, green: 0.07246912271, blue: 0.476346612, alpha: 1)
          header.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
      }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if parentScrollView == self.parentScrollView {
            tableView.isScrollEnabled = (self.parentScrollView.contentOffset.y >= self.parentScrollView.contentOffset.y)
        }

        if parentScrollView == self.tableView {
            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)
        }

        
        let alpha = scrollView.contentOffset.y/320*2 ; // 0.8
        self.titleBar.alpha = CGFloat(alpha)
      // determining whether scrollview is scrolling up or down
    goingUp = scrollView.panGestureRecognizer.translation(in: scrollView).y < 0
      
      // maximum contentOffset y that parent scrollView can have
      let parentViewMaxContentYOffset = parentScrollView.contentSize.height - parentScrollView.frame.height
      
      // if scrollView is going upwards
      if goingUp! {
          // if scrollView is a child scrollView
          if scrollView == childScrollView {
              // if parent scroll view is't scrolled maximum (i.e. menu isn't sticked on top yet)
              if parentScrollView.contentOffset.y < parentViewMaxContentYOffset && !childScrollingDownDueToParent {
                  
                  // change parent scrollView contentOffset y which is equal to minimum between maximum y offset that parent scrollView can have and sum of parentScrollView's content's y offset and child's y content offset. Because, we don't want parent scrollView go above sticked menu.
                  // Scroll parent scrollview upwards as much as child scrollView is scrolled
                  // Sometimes parent scrollView goes in the middle of screen and stucks there so max is used.
                  parentScrollView.contentOffset.y = max(min(parentScrollView.contentOffset.y + childScrollView.contentOffset.y, parentViewMaxContentYOffset), 0)
                  
                  // change child scrollView's content's y offset to 0 because we are scrolling parent scrollView instead with same content offset change.
                  childScrollView.contentOffset.y = 0
              }
          }
      }
          // Scrollview is going downwards
      else {
          
          if scrollView == childScrollView {
              // when child view scrolls down. if childScrollView is scrolled to y offset 0 (child scrollView is completely scrolled down) then scroll parent scrollview instead
              // if childScrollView's content's y offset is less than 0 and parent's content's y offset is greater than 0
              if childScrollView.contentOffset.y < 0 && parentScrollView.contentOffset.y > 0 {
                  
                  // set parent scrollView's content's y offset to be the maximum between 0 and difference of parentScrollView's content's y offset and absolute value of childScrollView's content's y offset
                  // we don't want parent to scroll more that 0 i.e. more downwards so we use max of 0.
                  parentScrollView.contentOffset.y = max(parentScrollView.contentOffset.y - abs(childScrollView.contentOffset.y), 0)
              }
          }
          
          // if downward scrolling view is parent scrollView
          if scrollView == parentScrollView {
              // if child scrollView's content's y offset is greater than 0. i.e. child is scrolled up and content is hiding up
              // and parent scrollView's content's y offset is less than parentView's maximum y offset
              // i.e. if child view's content is hiding up and parent scrollView is scrolled down than we need to scroll content of childScrollView first
              if childScrollView.contentOffset.y > 0 && parentScrollView.contentOffset.y < parentViewMaxContentYOffset {
                  // set if scrolling is due to parent scrolled
                  childScrollingDownDueToParent = true
                  // assign the scrolled offset of parent to child not exceding the offset 0 for child scroll view
                  childScrollView.contentOffset.y = max(childScrollView.contentOffset.y - (parentViewMaxContentYOffset - parentScrollView.contentOffset.y), 0)
                  // stick parent view to top coz it's scrolled offset is assigned to child
                  parentScrollView.contentOffset.y = parentViewMaxContentYOffset
                  childScrollingDownDueToParent = false
              }
          }
      }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueItemDetails" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? EDFoodAddToCartVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
        else if segue.identifier == "segueViewCart" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? EDFoodCartVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
          
    private func initMerchantMenu(
      restaurant_id: String
    ) {
    showLoading()
      apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlRestaurantMenu,
          params: "restaurant_id:\(restaurant_id)",
          success: { data in
              let jsonData = data.data(using: String.Encoding.utf8)!
              let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
              let json: JSON = JSON(jsonDic)
              
              let status = json[self.constant.key_status].string
              if status == self.constant.status_success {
                self.categories.removeAll()
                let jsonArray = json[self.constant.key_data].array
                for data in jsonArray!
                {
                    let category = data[self.constant.key_category].string
                    self.categories.append(category!)
                    self.foodList.append(data)
                }
                self.categories = self.removeDuplicates(array: self.categories)
                self.setupSegmentedControl()
    //                    let sorted = self.foodList.sorted(by: {
    //                        $0.date_and_time!.compare($1.date_and_time!) == .orderedDescending
    //                    })
    //                    self.foodList = sorted
                
                self.tableView.reloadData()
                self.hideLoading()

                if self.bookedId != ""
                {
                    self.initGetCartOrderList(
                        customer_id: self.defaults.string(forKey: self.constant.key_user_id)!,
                        booked_id: self.bookedId,
                        country_name: self.country_name)
                }
              }
              else
              {
                  let error = json[self.constant.key_code].string
                  self.handleError(error)
                    
              }

              return KotlinUnit()
      }, failure: {
          self.handleError($0?.message)
          return KotlinUnit()
      })
    }
                
    private func initGetOrderID(
        customer_id: String,
        restaurant_id: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCheckOrderID,
            params: "customer_id:\(customer_id)||restaurant_id:\(restaurant_id)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)

                self.initMerchantMenu(restaurant_id: restaurant_id)
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success {
                    
                    self.viewCart.alpha = 1.0
                    let bookData = json[self.constant.key_data].self
                    self.bookedId = "\(bookData[self.constant.key_booked_order_id].int!)"
                }
                else
                {
                    self.viewCart.alpha = 0.0
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
                
    private func initGetCartOrderList(
        customer_id: String,
        booked_id: String,
        country_name: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCartOrderList,
            params: "customer_id:\(customer_id)||booked_id:\(booked_id)||distance_km:1||country_name:\(country_name)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                var msg = ""
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    msg = status
                }

                if msg != self.constant.status_fail
                {
                    var original_sub_total = ""
                    if (json[self.constant.key_original_sub_total].string == nil) {
                        original_sub_total = ""
                    } else {
                        original_sub_total = json[self.constant.key_original_sub_total].string!
                    }
                    self.lblCartTotalAmount.text = commaSeparated(value: original_sub_total).asString()
                    
                    let jsonArray = json[self.constant.key_order].array
                    var quantities = 0
                    for json in jsonArray!
                    {
                        let jsonItem = json.self
                        let foodQuantity = jsonItem[self.constant.key_food_quantity].int
                        quantities = quantities + foodQuantity!
                    }
                    self.lblCartItems.text = "\(quantities)"
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
}
