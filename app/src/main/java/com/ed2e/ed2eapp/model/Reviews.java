package com.ed2e.ed2eapp.model;

public class Reviews {

    private String customer;
    private String rating;
    private String review_date;
    private String review;

    public Reviews(String customer, String rating, String review_date, String review) {
        this.customer = customer;
        this.rating = rating;
        this.review_date = review_date;
        this.review = review;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview_date() {
        return review_date;
    }

    public void setReview_date(String review_date) {
        this.review_date = review_date;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
