//
//  ActivityPayVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/24/20.
//

import UIKit
import SwiftyJSON
import app

class ActivityPayVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    internal var apiInterface = ApiInterface()
    
    @IBOutlet weak var tfDateFrom: DesignableTextField!
    @IBOutlet weak var tfDateTo: DesignableTextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datePicker : UIDatePicker?
    private var datePickerTo : UIDatePicker?
    var payList = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePickerTo = UIDatePicker()
        datePickerTo?.datePickerMode = .date
        tfDateFrom.inputView = datePicker
        datePicker?.addTarget(self, action: #selector(ActivityFoodVC.dateChanged(datePicker:)), for: .valueChanged)
        tfDateTo.inputView = datePickerTo
        datePickerTo?.addTarget(self, action: #selector(ActivityFoodVC.dateChangedTo(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActivityFoodVC.viewTapped(gestureRecognizer:)))
            view.addGestureRecognizer(tapGesture)
            
        //Write toolbar code for done button
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        tfDateFrom.inputAccessoryView = toolBar
        tfDateTo.inputAccessoryView = toolBar
    }
    
    //Toolbar done button function
    @objc func onClickDoneButton() {
        self.view.endEditing(true)
        getPayLogs(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            country_code: "NG",
            date_from: dateFormat(date: self.tfDateFrom.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString(),
            date_to: dateFormat(date: self.tfDateTo.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString()
        )
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        getPayLogs(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            country_code: "NG",
            date_from: dateFormat(date: self.tfDateFrom.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString(),
            date_to: dateFormat(date: self.tfDateTo.text!,from: constant.date_format_picker_display,to: constant.date_format_server_api).asString()
        )
    }

    @objc func dateChanged(datePicker: UIDatePicker) {
       
       let format = DateFormatter()
           format.dateFormat = constant.date_format_picker_display
       
       self.datePicker?.date = datePicker.date
       self.datePickerTo?.minimumDate = datePicker.date
       tfDateFrom.text = format.string(from: datePicker.date)
    }

    @objc func dateChangedTo(datePicker: UIDatePicker) {
       
       let format = DateFormatter()
           format.dateFormat = constant.date_format_picker_display
       
       self.datePickerTo?.date = datePicker.date
       self.datePicker?.maximumDate = datePicker.date
       tfDateTo.text = format.string(from: datePicker.date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = constant.date_format_server_api
        let formattedDate = format.string(from: date)
        print(formattedDate)
        
        let calendar = Calendar.current
        let lastMonthDate = Calendar.current.date(byAdding: .month, value: -1, to: date)
        calendar.component(.month, from:lastMonthDate!)

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = constant.date_format_server_api
        
        datePicker?.date = lastMonthDate!
        datePicker?.maximumDate = date
        datePickerTo?.date = date
        datePickerTo?.minimumDate = lastMonthDate!
        
        self.tfDateFrom.text = dateFormat(date: dateFormatterPrint.string(from: lastMonthDate!),from: constant.date_format_server_api,to: constant.date_format_picker_display).asString()
        self.tfDateTo.text = dateFormat(date: formattedDate,from: constant.date_format_server_api,to: constant.date_format_picker_display).asString()
        getPayLogs(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            country_code: "NG",
            date_from: dateFormatterPrint.string(from: lastMonthDate!),
            date_to: formattedDate
        )
    }
  
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if payList.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return payList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityPayTableViewCell", for: indexPath)
                        as! ActivityPayTableViewCell

        let data = payList[indexPath.row]
        let merchant_info = data[self.constant.key_offline_merchant].self
        let name = merchant_info[self.constant.key_trade_name].string
        let booked_date = data[self.constant.key_created_date].string
        let transaction_no = "\(self.constant.label_transaction_number) \(data[self.constant.key_transaction_no])"
        let paid_amount = data[self.constant.key_paid_amount].string

        let pay = ActivityPay(
                id: data[self.constant.key_id].int,
                name: name,
                amount: paid_amount,
                transaction_number: transaction_no,
                date_and_time:  getLocalTimeStamp(date: booked_date!).asString())
        
        cell.lblMerchantName.text = pay.name
        cell.lblAmount.text = pay.amount
        cell.lblTransactionNumber.text = pay.transaction_number
        cell.lblDateAndTime.text = pay.date_and_time
        
        cell.buttonCallback = { [unowned self] in
            var segueData = Dictionary<String, AnyObject>()
            segueData[self.constant.key_data] = data as AnyObject
            self.performSegue(withIdentifier: "segueEDPayDetails", sender: segueData)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueEDPayDetails") {

            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? ActivityPayDetailsVC {
                        self.log("chidVC \(chidVC)")
                        chidVC.segueData = (sender as? [String:AnyObject])!
                }
            }
        }
    }
        
    private func getPayLogs(
        customer_id: String,
        country_code: String,
        date_from: String,
        date_to: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlQRTransactionLogs,
            params: "customer_id:\(customer_id)||country_code:\(country_code)||date_from:\(date_from)||date_to:\(date_to)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                
                do{
                    let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                    let json: JSON = JSON(jsonDic)
                    
                    let status = json[self.constant.key_status].string
                    if status == self.constant.status_success {

                      let jsonArray = json[self.constant.key_data].array
                      for data in jsonArray!
                      {
                        self.payList.append(data)
                      }
                        let sorted = self.payList.sorted { $0[self.constant.key_created_date] < $1[self.constant.key_created_date] }
                        
                      self.payList = sorted
                      self.tableView.reloadData()
                    }
                    else
                    {
                        let error = json[self.constant.key_code].string
                        self.handleError(error)
                          
                    }
                }
                catch let error
                {
                    print(error)
                }
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
        
    }
}
