//
//  OnBoardingVC.swift
//  ed2eapp
//
//  Created by ED2E Technology iOS on 15/01/2019.
//  Copyright © 2019 ED2E Technology iOS. All rights reserved.
//

import UIKit
import app

class OnBoardingVC: UIPageViewController
{
    internal var defaults = UserDefaults.standard
    internal var constant = ConstantKt.self
    
    var countdownTimer: Timer!
    var totalTime = 3
//    var customPageControl = UIPageControl()
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIdentifier: "Page1"),
            self.getViewController(withIdentifier: "Page2"),
            self.getViewController(withIdentifier: "Page3"),
            self.getViewController(withIdentifier: "Page4")
        ]
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let value = defaults.bool(forKey: CommonConstants.KEY_FIRST_TIME_LAUNCH)
        if value == true
        {
            self.dataSource = self
            self.delegate   = self
            if let firstVC = pages.first
            {
                setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
                self.setupButton(viewController: firstVC)
            }
        }
        else
        {
            DispatchQueue.main.async(){
                self.dismiss(animated: false, completion: {})
                let autologin = self.defaults.string(forKey: self.constant.key_user_id)
                if autologin != ""
                {
                    self.performSegue(withIdentifier: "segueOnboardingMain", sender: self)
                }
                else
                {
                    self.performSegue(withIdentifier: "segueOnboardingLogin", sender: self)
                }
            }
        }
        startTimer()
    }
    
    @objc func buttonAction(sender:UIButton!) {
        let btnsendtag: UIButton = sender
        switch btnsendtag.tag {
        case 1:
            print("label_go_to_login".localized())
            self.performSegue(withIdentifier: "segueOnboardingLogin", sender: nil)
            UserDefaults.standard.set(false, forKey: CommonConstants.KEY_FIRST_TIME_LAUNCH)
        case 2:
            print("label_go_to_reg".localized())
            self.performSegue(withIdentifier: "segueOnboardingRegistration", sender: nil)
            UserDefaults.standard.set(false, forKey: CommonConstants.KEY_FIRST_TIME_LAUNCH)
        default:
            self.goToNextPage()
        }
    }
    
    func setupButton(viewController: UIViewController)
    {
        for view in viewController.view.subviews as [UIView] {
            if let button = view as? UIButton {
                print("button name: \(button.titleLabel?.text ?? "")")
                button.addTarget(self, action: #selector(buttonAction), for: UIControl.Event.touchUpInside)
            }
            if let stackview = view as? UIStackView {
                for view in stackview.subviews as [UIView] {
                    if let button = view as? UIButton {
                        print("button name: \(button.titleLabel?.text ?? "")")
                        button.addTarget(self, action: #selector(buttonAction), for: UIControl.Event.touchUpInside)
                    }
                }
            }
        }
    }
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {
        //timerLabel.text = "\(timeFormatted(totalTime))"
        print("\(timeFormatted(totalTime))")

        if totalTime != 0 {
            totalTime -= 1
        } else {
            if let currentViewController = viewControllers?[0] {
                if ((dataSource?.pageViewController(self, viewControllerAfter: currentViewController)) == nil) {
                    endTimer()
                    self.performSegue(withIdentifier: "segueOnboardingLogin", sender: nil)
                    UserDefaults.standard.set(false, forKey: CommonConstants.KEY_FIRST_TIME_LAUNCH)
                }
                else
                {
                    self.goToNextPage()
                    totalTime = 3
                }
            }
        }
    }

    func endTimer() {
        countdownTimer.invalidate()
    }

    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
}

extension UIPageViewController {
    func goToNextPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if let currentViewController = viewControllers?[0] {
            if let nextPage = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) {
                setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
            }
        }
    }
}

extension OnBoardingVC: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
//        guard previousIndex >= 0          else { return pages.last }//unli loop
        guard previousIndex >= 0          else { return nil }
        guard pages.count > previousIndex else { return nil }
        
        self.setupButton(viewController: pages[previousIndex])
        return pages[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
//        guard nextIndex < pages.count else { return pages.first }//unli loop
        guard nextIndex < pages.count else { return nil }
        guard pages.count > nextIndex else { return nil }
        
        self.setupButton(viewController: pages[nextIndex])
        return pages[nextIndex]
    }
}
extension OnBoardingVC: UIPageViewControllerDelegate { }
