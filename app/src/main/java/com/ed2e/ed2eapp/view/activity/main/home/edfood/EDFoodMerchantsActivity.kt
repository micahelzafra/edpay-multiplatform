package com.ed2e.ed2eapp.view.activity.main.home.edfood


import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.ListPopupWindow
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.JListAdapter
import com.ed2e.ed2eapp.adapter.SearchLocationAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.model.SearchLocation
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsActivity
import com.google.gson.JsonParser
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edfood_merchants.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class EDFoodMerchantsActivity : BaseActivity(), CoroutineScope {

    val TAG = EDFoodMerchantsActivity::class.java.simpleName

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private var apiInterface: ApiInterface = ApiInterface()
    private lateinit var preference: AppPreference
    private var disposables: CompositeDisposable? = null
    private var arraylist = ArrayList<Any>()
    private var arraylistSearch = ArrayList<Any>()
    lateinit var adapter: BaseRecyclerViewAdapter<Any>

    var resultList: ArrayList<Any> = ArrayList()


    private var PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
    private var GEO_API_BASE = "https://maps.googleapis.com/maps/api/geocode"
//    https://maps.googleapis.com/maps/api/place/details/json?placeid=EilMYWdvcyAtIEliYWRhbiBFeHByZXNzd2F5LCBMYWdvcywgTmlnZXJpYSIuKiwKFAoSCTvqwwWilDsQEQNfINqu4TNHEhQKEgnDz4G3dZQ7EBEf30TSVZT_uQ&key=AIzaSyDxzpoR7ZDsTUnv0_9pja97DJFhgdwF6DQ
    private var TYPE_AUTOCOMPLETE = "/autocomplete"
    private var DETAILS = "/details"
    private var OUT_JSON = "/json"
    private var apiKey = ""
    private var country_code = ""
    private var country_name = ""
    private var isSelectPlace = false

    private var clickedData = ""
    private var finalLong = ""
    private var finalLat = ""

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var modelList: List<SearchLocation>? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerAdapter: SearchLocationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_merchants)

        job = Job()

        //apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        getLastLocation(this)
        initListener()
        initGUI()

    }

    private fun initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.edfood_merchants_recycleview_search_location_results)
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        recyclerAdapter = SearchLocationAdapter(this@EDFoodMerchantsActivity, modelList)
        recyclerView.adapter = recyclerAdapter

        edfood_merchants_layout_search_location_results.visibility = GONE
        edfood_merchants_layout_merchant_list.visibility = VISIBLE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
           // window.setStatusBarColor(R.color.primaryRedColor)
        }

        apiKey = getString(R.string.api_key)

        adapter = object : BaseRecyclerViewAdapter<Any>(arraylistSearch, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {

                val parser = JsonParser()
                val jsonObj = parser.parse(item.toString()).asJsonObject
                val operating_info = jsonObj[key_operating_info].asJsonArray

                var merchantStatus = label_close_today
                for (i in 0 until operating_info.size()) {
                    if (operating_info[i].asJsonObject.toString().contains(getCurrentDay().asString()))
                    {
                        merchantStatus = label_open_today
                    }
                }

                if (merchantStatus.equals(label_close_today))
                {
                    showDialog_main(
                        "",
                        "",
                        true,
                        label_dialog_title_merchant_close,
                        label_dialog_msg_merchant_close,
                        "",
                        "OK"
                    )

//                    AlertDialog.Builder(this@EDFoodMerchantsActivity)
//                        .setTitle(label_dialog_title_merchant_close)
//                        .setMessage(label_dialog_msg_merchant_close)
//                        .setPositiveButton(button_okay
//                        ) { dialogInterface: DialogInterface, i: Int ->
//                            dialogInterface.dismiss()
//                        }
//                        .setCancelable(false)
//                        .show()
                }
                else
                {
                    initUnplacedOrderChecker(item,preference.getString(key_user_id))
                }
            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.child_edfood_merchant
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }

        edfood_merchants_recycleview.layoutManager= LinearLayoutManager(context)
        edfood_merchants_recycleview.setHasFixedSize(true)
        edfood_merchants_recycleview.adapter=adapter

        //load search results to textview and set onClick
        val listPopupWindow = ListPopupWindow(this)
        listPopupWindow.setAdapter(ArrayAdapter (this@EDFoodMerchantsActivity, R.layout.child_results_list, resultList))
        listPopupWindow.anchorView = edfood_merchants_autocomplete_fragment
        listPopupWindow.width = ListPopupWindow.WRAP_CONTENT
        listPopupWindow.height = 400
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            listPopupWindow.dismiss()
            val parser = JsonParser()
            val predictionjsonObj = parser.parse(resultList[position].toString()).asJsonObject
            val description = predictionjsonObj[key_description].asString
            isSelectPlace = true
            edfood_merchants_autocomplete_fragment.setText(description)
            edfood_merchants_autocomplete_fragment.clearFocus()
            getLocationDetails(resultList[position])
            closeKeyBoard()
        }

        //listener - on searching of merchants
        edfood_merchants_autocomplete_fragment.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                if(edfood_merchants_autocomplete_fragment.text.toString() == "") {
                    edfood_merchants_layout_search_location_results.visibility = GONE
                    edfood_merchants_layout_merchant_list.visibility = VISIBLE
                } else {
                    edfood_merchants_layout_search_location_results.visibility = VISIBLE
                    edfood_merchants_layout_merchant_list.visibility = GONE
                }


                this@EDFoodMerchantsActivity.runOnUiThread {
                    if (isSelectPlace)
                    {
                        isSelectPlace = false
                    }
                    else
                    {
                        val sb = StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON)
//                        val sb = StringBuilder(GEO_API_BASE + OUT_JSON)
                        sb.append("?key=$apiKey")
                        sb.append("&components=country:"+"NG")
                        sb.append("&input=" + URLEncoder.encode(edfood_merchants_autocomplete_fragment.text.toString(), "utf8"))

                        apiInterface.parseGoogle(sb.toString(),
                            { response: String? ->

                                modelList = java.util.ArrayList()
                                (modelList as java.util.ArrayList<SearchLocation>).clear()

                                val parser = JsonParser()
                                val jsonObj = parser.parse(response).asJsonObject
                                val status = jsonObj[key_status].asString
                                resultList.clear()
                                if (status == status_ok) {
                                    val jsonArray = jsonObj[key_predictions].asJsonArray
                                    for (i in 0 until jsonArray.size()) {
                                        try {
                                            val predictionjsonObj = jsonArray[i].asJsonObject
                                            val description = predictionjsonObj[key_description].asString
                                            val placeID = predictionjsonObj[key_place_id].asString
                                            Log.d("Logs", "resultList: " + resultList)
                                            resultList.add(jsonArray[i])

                                            Log.d("Logs", "predictionjsonObj: " +  predictionjsonObj.toString())

                                            val list = SearchLocation(
                                                predictionjsonObj.toString()
                                            )
                                            (modelList as java.util.ArrayList<SearchLocation>).add(list)

                                        }
                                        catch (e : Exception)
                                        {
                                            Timber.e("parseGoogle Exception $e ")
                                        }

                                    }

                                    Timber.d("edfood_merchants_autocomplete_fragment: $isSelectPlace")
                                    this@EDFoodMerchantsActivity.runOnUiThread {
                                        //listPopupWindow.setAdapter(JListAdapter (this@EDFoodMerchantsActivity, resultList))
                                        //listPopupWindow.show()
                                        Log.d("Logs", "resultList_final: " + resultList.size + " | " + resultList)

                                        recyclerAdapter.setModelList(modelList)

                                    }



                                } else {
                                    this@EDFoodMerchantsActivity.runOnUiThread {
                                        //listPopupWindow.dismiss()
                                    }
                                }
                            }
                        ) { failure: Throwable? ->
                            println("Throwable:$failure")
                            showDialogError(request_error_dialog, failure!!.localizedMessage)
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        //listener - on searching merchant tags
        edfood_merchants_editText_search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {
                getLocationAddress(finalLong, finalLat)
                edfood_merchants_editText_search.requestFocus()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        //listener - on searching merchant tags
//        val changeObservableSearch: Observable<CharSequence> = edfood_merchants_editText_search.textChanges()
//        disposables = CompositeDisposable()
//        disposables!!.add(changeObservableSearch.map { inputText: CharSequence -> inputText.length == 0 }
//            .subscribe { isValid: Boolean ->
//                Timber.d("isValid:$isValid")
////                if (!isValid) {
//
//                    //getLocationDetails(clickedData)
//                    getLocationAddress(finalLong, finalLat)
//
////                    val parser = JsonParser()
////                    arraylistSearch.clear()
////                    for (items in arraylist)
////                    {
//////                        val jsonObj = parser.parse(items.toString()).asJsonObject
//////                        val name = jsonObj[key_trade_name].asString
//////                        val desc = jsonObj[key_product_services].asString
//////                        val business_address = jsonObj[key_business_address].asString
//////
//////                        if (name.toLowerCase(Locale.US).contains(edfood_merchants_editText_search.text.toString().toLowerCase(
//////                                Locale.US))||business_address.toLowerCase(Locale.US).contains(edfood_merchants_editText_search.text.toString().toLowerCase(
//////                                Locale.US)))
//////                        {
//////                            arraylistSearch.add(items)
//////                        }
////                    }
////                    updateList()
////                } else {
////                    try {
////                        arraylistSearch.clear()
////                        arraylistSearch.addAll(arraylist)
////                        updateList()
////                    }
////                    catch (e:UninitializedPropertyAccessException)
////                    {
////                        Timber.d("UninitializedPropertyAccessException:$e")
////                    }
////                }
//            }
//        )




        edfood_merchants_button_clear.setOnClickListener {
            edfood_merchants_autocomplete_fragment.setText("")
            edfood_merchants_autocomplete_fragment.requestFocus()
        }

        val view = this.currentFocus
        view?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    fun getLocationDetails(data:Any)
    {
        clickedData = data.toString()
        Log.d("Logs", "getLocationDetails() triggered | clickedData: $clickedData")

        edfood_merchants_editText_search.text.clear()

        val parser = JsonParser()
        val dataJsonObj = parser.parse(data.toString()).asJsonObject
        val place_id = dataJsonObj[key_place_id].asString
        val sb = StringBuilder(PLACES_API_BASE + DETAILS + OUT_JSON)
        //apiKey = resources.getString(R.string.api_key)
        sb.append("?key=$apiKey")
        sb.append("&place_id=$place_id")
        //apiInterface = ApiInterface()
        apiInterface.parseGoogle(sb.toString(),
            { response: String? ->
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                resultList.clear()
                if (status == status_ok) {

                    val result = jsonObj[key_result].asJsonObject
                    val geometry = result[key_geometry].asJsonObject
                    val location = geometry[key_location].asJsonObject
                    val lat = location[key_lat].asBigDecimal
                    val lng = location[key_lng].asBigDecimal

                    finalLong = lng.toString()
                    finalLat = lat.toString()
                    getLocationAddress(finalLong, finalLat)
                } else {
                    this@EDFoodMerchantsActivity.runOnUiThread {
//                        listPopupWindow.dismiss()
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    fun getLocationAddress(longitude: String, latitude: String)
    {
        Log.d("Logs", "getLocationAddress() triggered | Longitude: $longitude | Latitude: $latitude")

//        https://maps.googleapis.com/maps/api/geocode/json?address=14.6215107,121.0709483&key=AIzaSyDxzpoR7ZDsTUnv0_9pja97DJFhgdwF6DQ
        val parser = JsonParser()
        val sb = StringBuilder("https://maps.googleapis.com/maps/api/geocode/json")
        sb.append("?key=$apiKey")
        sb.append("&address=$latitude,$longitude")
        apiInterface.parseGoogle(sb.toString(),
            { response: String? ->

                Log.d(TAG, "URL: | $sb")
                Log.d(TAG, "RESPONSE: $response")

                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                resultList.clear()
                if (status == status_ok) {

                    val jsonArray = jsonObj[key_results].asJsonArray
                    loop@ for (i in 0 until jsonArray.size()) {
                        val jinx = jsonArray[i].asJsonObject
                        val formatted_address = jinx[key_formatted_address].asString
                        val address_components = jinx[key_address_components].asJsonArray
                        for (j in 0 until address_components.size()) {
                            val component = address_components[j].asJsonObject
                            Timber.w("component hahaha $component")
                            if (component[key_types].asJsonArray.toString().contains(key_country))
                            {
                                country_code = component[key_short_name].asString
                                country_name = component[key_long_name].asString
                                this@EDFoodMerchantsActivity.runOnUiThread {
                                    edfood_merchants_autocomplete_fragment.setText(formatted_address)
                                    isSelectPlace = true
                                }
                                initMerchantList(country_code, edfood_merchants_editText_search.text.toString(), longitude, latitude)
                                break@loop
                            }
                        }
                    }

                } else {
                    this@EDFoodMerchantsActivity.runOnUiThread {
                        //                        listPopupWindow.dismiss()
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }


    private fun initMerchantList(
        country_code: String,
        search: String,
        longitude: String,
        latitude: String
    ) {
        //showProgress()
        Log.d(TAG, "initMerchantList() triggered")
        val params = "country_code:" + country_code + "||" +
                "search:" + search + "||" +
                "longitude:" + longitude + "||" +
                "latitude:" + latitude

        this@EDFoodMerchantsActivity.runOnUiThread {
            edfood_merchants_layout_search_location_results.visibility = GONE
            edfood_merchants_layout_merchant_list.visibility = VISIBLE
        }

        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlRestaurantList,
            params,
            { response: String? ->
                //hideProgress()
                arraylist.clear()
                arraylistSearch.clear()

                Log.d(TAG, "URL: | $urlRestaurantList")
                Log.d(TAG, "DATA: | $params")
                Log.d(TAG, "RESPONSE: $response")
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    val jsonArray = jsonObj[key_data].asJsonArray
                    Timber.d("data: %s", jsonArray)
//                    arraylist.clear()
//                    arraylistSearch.clear()
                    for (data in jsonArray)
                    {
                        arraylist.add(data)
                    }
                    arraylistSearch.addAll(arraylist)
                    updateList()
                } else {

                    if(jsonObj[key_code].toString() == null || jsonObj[key_code].toString() == "" || jsonObj[key_code].toString() == "null") {

                    } else {
//                        val error_code = jsonObj[key_code].asString
//
////                        if(error_code == "ED12") {
////
////                        } else {
//                            showDialogError(request_error_dialog, error_code)
////                        }
                    }

                    updateList()

                }
            }
        ) { failure: Throwable? ->
            //hideProgress()
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun updateList()
    {
        this@EDFoodMerchantsActivity.runOnUiThread {
            val parser = JsonParser()
            arraylistSearch.sortWith(Comparator { jinks, jinx -> parser.parse(jinx.toString()).asJsonObject[key_distance_from_customer].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_distance_from_customer].asString)
            })
            adapter.notifyDataSetChanged()
        }
    }








    private fun initUnplacedOrderChecker(
        data: Any,
        customer_id: String
    ) {
        val params = "customer_id:" + customer_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCheckUnplacedOrder,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    val bookedRecord = jsonObj[key_booked_record].asString
                    if (bookedRecord.equals("1"))
                    {
                        val bookedId = jsonObj[key_booked_id].asString
                        initDeleteCartOrder(data,bookedId)
                    }
                    else
                    {
                        val intent : Intent
                        intent = Intent(context, EDFoodMerchantItemActivity::class.java)
                        intent.putExtra(key_data, data.toString())
                        intent.putExtra(key_booked_location, edfood_merchants_autocomplete_fragment.text.toString())
                        intent.putExtra(key_country_code, country_code)
                        intent.putExtra(key_country, country_name)
                        startActivity(intent)
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                    }
                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initDeleteCartOrder(
        data: Any,
        booked_id: String
    ) {
        val params = "booked_id:" + booked_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartDeleteOrder,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {
                    initUnplacedOrderChecker(data,preference.getString(key_user_id))
                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }





    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        val parser = JsonParser()
        when (tag) {
            tag_intent_edfood_close_prev_act -> {
                finish()
            }
        }
    }


    private fun initToolBar() {
        edfood_merchants_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edfood_merchants_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edfood_merchants_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        // Using clear will clear all, but can accept new disposable
        //disposables!!.clear()
        // Using dispose will clear all and set isDisposed = true, so it will not accept any new disposable
        //disposables!!.dispose()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        Timber.d("onUpdateLocation %s, %s", latitude,longitude)

        finalLong = longitude.toString()
        finalLat = latitude.toString()
        getLocationAddress(finalLong, finalLat)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
