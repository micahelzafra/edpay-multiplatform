package com.ed2e.ed2eapp.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.ErrorCodeHandler;
import com.ed2e.ed2eapp.model.errorCode;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.util.NetworkChangeReceiver;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

import timber.log.Timber;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.KEY_FIRST_TIME_LAUNCH;
import static com.ed2e.ed2eapp.util.ConstantKt.button_okay;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_current_location;
import static com.ed2e.ed2eapp.util.ConstantKt.key_error_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_error_list;
import static com.ed2e.ed2eapp.util.ConstantKt.key_error_message;
import static com.ed2e.ed2eapp.util.ConstantKt.key_error_title;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.label_dialog_title_error_1;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_name;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_latitude;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_longitude;

public abstract class BaseActivity extends AppCompatActivity {

    int PERMISSION_ID_LOCATION = 44;

    protected Dialog progressDialog;
    protected Dialog dialog;
    protected Dialog dialog2;

    protected Dialog dialog_main;

    String input_data = "Hello";
    private BroadcastReceiver mNetworkReceiver = null;
    FusedLocationProviderClient mFusedLocationClient;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNetworkReceiver = new NetworkChangeReceiver();


        mNetworkReceiver = new NetworkChangeReceiver();


        preference = getInstance(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        //showToast_short("Broadcast CREATED");
//        registerNetworkBroadcastForNougat();

        runOnUiThread(() ->
        {
            progressDialog = new Dialog(getContext(), android.R.style.Theme_Light_NoTitleBar);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (progressDialog.getWindow() != null)
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.dialog_progress_loading);
            ImageView imageView_progress = progressDialog.findViewById(R.id.dialog_progress_loading_imageview);
            progressDialog.setCancelable(false);

            Glide.with(getContext())
                    .load(R.drawable.ic_loading)
                    .into(imageView_progress);
        });

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //showToast_short("onDestroy");
//        unregisterNetworkChanges();
//        Intent i = new Intent(this, NetworkCheckerService.class);
//        stopService(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //showToast_short("onResume");
//        registerNetworkBroadcastForNougat();
//        Intent i = new Intent(this, NetworkCheckerService.class);
//        startService(i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //showToast_short("onDestroy");
//        unregisterNetworkChanges();
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    /*@Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(networkCheckerReceiver);
    }*/

    public Context getContext() {
        return this;
    }

    public Toolbar setupToolBar(int id) {
        Toolbar toolbar = findViewById(id);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_left_arrow_white));
            toolbar.setNavigationOnClickListener(v -> onToolbarBackPress());
        }
        return toolbar;
    }

    protected void onToolbarBackPress() {
        //superclass implemention does nothing
    }

    public void showProgress() {
        if (getContext() != null) {
            if (progressDialog != null) {
                try {
                    runOnUiThread(() -> progressDialog.show());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void hideProgress() {
        if (getContext() != null) {
            if (progressDialog != null) {
                if (progressDialog.isShowing())
                    try {
                        progressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    public void showToast_short(String message) {
        runOnUiThread(() ->
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show());
    }

    public void showToast_long(String message) {
        runOnUiThread(() ->
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show());
    }

    private void inputAddTextChangedListener(EditText editText_input) {
        editText_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                input_data = editText_input.getText().toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    public static String getHash(String str) {
        MessageDigest digest = null;
        byte[] input = null;

        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            input = digest.digest(str.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return convertToHex(input);
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }


    @SuppressLint("MissingPermission")
    public void getLastLocation(Context context){
        Timber.d("checkPermissions %s",checkPermissions());
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        task -> {
                            Location location = task.getResult();
                            if (location == null) {
                                requestNewLocationData();
                            } else {
                                //showToast_short("Location: " + location.getLatitude() + " " + location.getLongitude());
                                Log.d("LASTLOCATION", "getCountryName() to trigger");
                                getCountryName(context, location.getLatitude(), location.getLongitude());
                                onUpdateLocation(location.getLatitude()+"",location.getLongitude()+"");
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    public void getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address result;

            if (addresses != null && !addresses.isEmpty()) {
                Log.d("LASTLOCATION", "LASTLOCATION: " + addresses.get(0).getCountryName());
                Log.d("LASTLOCATION", "LASTLOCATION: " + addresses.get(0).getCountryCode());
            }
        } catch (Exception error) {
            //do something
        }

    }

    double locLatitude = 0;
    double locLongitude = 0;
    String countryName = "";
    String countryCode = "";



    String finalLocation = "";

    @SuppressLint("MissingPermission")
    public void getCurrentLocation(Context context){

        locLatitude = 0;
        locLongitude = 0;
        countryName = "";
        countryCode = "";

        finalLocation = "";

        Timber.d("checkPermissions %s",checkPermissions());
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        task -> {
                            Location location = task.getResult();
                            if (location == null) {
                                requestNewLocationData();
                            } else {
                                locLatitude = location.getLatitude();
                                locLongitude = location.getLongitude();

                                //showToast_short("Location: " + locLatitude + " " + locLongitude);
                                //onUpdateLocation(locLatitude+"",locLongitude+"");

                                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                                List<Address> addresses = null;


                                try {
                                    addresses = geocoder.getFromLocation(locLatitude, locLongitude, 1);
                                    Address result;

                                    if (addresses != null && !addresses.isEmpty()) {
                                        finalLocation = "{\"latitude\":\"" + locLatitude + "\",\"longitude\":\"" + locLongitude + "\",\"country_name\":\"" + addresses.get(0).getCountryName() + "\",\"country_code\":\"" + addresses.get(0).getCountryCode() + "\"}";
                                        Log.d("getCurrentLocation", "Current Location: " + finalLocation);
                                        preference.putString(key_current_location, finalLocation);
                                        Log.d("getCurrentLocation", "Current Location: (Pref) " + preference.getString(key_current_location, ""));
                                    }
                                } catch (Exception error) {

                                    //do something
                                }

                            }

                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    public String getCurrentLocationData(String tag, Activity activity) {

        //preference = getInstance(activity);
        String mLatitude = "", mLongitude = "", mCountryName = "", mCountryCode = "";

        if(preference.getString(key_current_location, "") == null || preference.getString(key_current_location, "").equalsIgnoreCase("")){
            return null;
        } else {

            Log.d("getCurrentLocationData", "Current Location Data: (Pref) " + preference.getString(key_current_location, ""));

            JsonParser parser = new JsonParser();
            JsonObject jsonObj = parser.parse(preference.getString(key_current_location, "")).getAsJsonObject();
            mLatitude = jsonObj.get("latitude").getAsString();
            mLongitude = jsonObj.get("longitude").getAsString();
            mCountryName = jsonObj.get("country_name").getAsString();
            mCountryCode = jsonObj.get("country_code").getAsString();

            switch (tag) {
                case tag_current_location_latitude:
                    return mLatitude;
                case tag_current_location_longitude:
                    return mLongitude;
                case tag_current_location_country_name:
                    return mCountryName;
                case tag_current_location_country_code:
                    return mCountryCode;
                default:
                    return null;
            }
        }
    }


    @SuppressLint("MissingPermission")
    public void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            onUpdateLocation(mLastLocation.getLatitude()+"",mLastLocation.getLongitude()+"");
        }
    };

    public boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID_LOCATION
        );
    }

    public boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //getLastLocation();
            }
        }
    }

    public void showDialogError(String request_type,String error_code)
    {
        JsonParser parser = new JsonParser();
        //Log.d("HELLO", "Error List: |" + preference.getString(key_error_list) + "|");
        JsonArray jsonArray = parser.parse(preference.getString(key_error_list)).getAsJsonArray();
        String title = label_dialog_title_error_1;
        String message = error_code;
        for (int i=0; i < jsonArray.size(); i++) {
            JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
            String code = jsonObj.get(key_error_code).getAsString();
            if(error_code.equals(code))
            {
                Timber.d("errorCodeHandler jsonObj %s",jsonObj);
                title = jsonObj.get(key_error_title).getAsString();
                message = jsonObj.get(key_error_message).getAsString();
            }
        }

        String finalTitle = title;
        String finalMessage = message;
        new Handler(Looper.getMainLooper()).post(() ->
        {
            new AlertDialog.Builder(this)
                    .setTitle(finalTitle)
                    .setMessage(finalMessage)
                    .setPositiveButton(button_okay,
                            (dialog, which) -> onDialogDismiss(request_type,""))
//                    .setNegativeButton(android.R.string.no,
//                            (dialog, which) -> Toast.makeText(activityCtx, "\uD83D\uDE22",Toast.LENGTH_SHORT).show())
                    .setCancelable(false)
                    .show();
        });
    }

    public void showDialog_APIError(String requestTag_button1, String requestTag_button2, boolean isCancellable, String error_code, String button1, String button2) {



        runOnUiThread(() ->
        {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(preference.getString(key_error_list)).getAsJsonArray();

            String error_title = label_dialog_title_error_1;
            String error_message = error_code;

            for (int i=0; i < jsonArray.size(); i++) {
                JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                String code = jsonObj.get(key_error_code).getAsString();

                if(error_code.equals(code))
                {
                    Timber.d("errorCodeHandler jsonObj %s",jsonObj);
                    error_title = jsonObj.get(key_error_title).getAsString();
                    error_message = jsonObj.get(key_error_message).getAsString();
                }
            }

            String finalTitle = error_title;
            String finalMessage = error_message;

            if(dialog != null){
                hideDialog();
            }

            if(dialog2 != null){
                hideDialog2();
            }

            dialog = new Dialog(this, R.style.DialogTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_main);

            TextView textView_title = dialog.findViewById(R.id.dialog_main_textView_title);
            TextView textView_message = dialog.findViewById(R.id.dialog_main_textView_message);
            TextView textView_cancel = dialog.findViewById(R.id.dialog_main_textView_cancel);
            TextView textView_confirm = dialog.findViewById(R.id.dialog_main_textView_confirm);

            textView_title.setVisibility(View.VISIBLE);
            textView_message.setVisibility(View.VISIBLE);
            textView_cancel.setVisibility(View.VISIBLE);
            textView_confirm.setVisibility(View.VISIBLE);

            if(finalTitle.equalsIgnoreCase("")){
                textView_title.setVisibility(View.GONE);
            }

            if(finalMessage.equalsIgnoreCase("")){
                textView_message.setVisibility(View.GONE);
            }

            if(button1.equalsIgnoreCase("")){
                textView_cancel.setVisibility(View.GONE);
            }

            if(button2.equalsIgnoreCase("")){
                textView_confirm.setVisibility(View.GONE);
            }

            textView_title.setText(finalTitle);
            textView_message.setText(finalMessage);
            textView_cancel.setText(button1.toUpperCase());
            textView_confirm.setText(button2.toUpperCase());

            textView_cancel.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button1, input_data);
                hideDialog();
            });

            textView_confirm.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button2, input_data);
                hideDialog();
            });

            dialog.setCancelable(isCancellable);
            dialog.setCanceledOnTouchOutside(false);


            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void showDialog_APIError2(String requestTag_button1, String requestTag_button2, boolean isCancellable, String error_code, String button1, String button2) {



        runOnUiThread(() ->
        {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(preference.getString(key_error_list)).getAsJsonArray();

            String error_title = label_dialog_title_error_1;
            String error_message = error_code;

            for (int i=0; i < jsonArray.size(); i++) {
                JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                String code = jsonObj.get(key_error_code).getAsString();

                if(error_code.equals(code))
                {
                    Timber.d("errorCodeHandler jsonObj %s",jsonObj);
                    error_title = jsonObj.get(key_error_title).getAsString();
                    error_message = jsonObj.get(key_error_message).getAsString();
                }
            }

            String finalTitle = error_title;
            String finalMessage = error_message;

            if(dialog != null){
                hideDialog();
            }

            if(dialog2 != null){
                hideDialog2();
            }

            dialog2 = new Dialog(this, R.style.DialogTheme);
            dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog2.getWindow() != null)
                dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog2.setContentView(R.layout.dialog_main);

            TextView textView_title = dialog2.findViewById(R.id.dialog_main_textView_title);
            TextView textView_message = dialog2.findViewById(R.id.dialog_main_textView_message);
            TextView textView_cancel = dialog2.findViewById(R.id.dialog_main_textView_cancel);
            TextView textView_confirm = dialog2.findViewById(R.id.dialog_main_textView_confirm);

            textView_title.setVisibility(View.VISIBLE);
            textView_message.setVisibility(View.VISIBLE);
            textView_cancel.setVisibility(View.VISIBLE);
            textView_confirm.setVisibility(View.VISIBLE);

            if(finalTitle.equalsIgnoreCase("")){
                textView_title.setVisibility(View.GONE);
            }

            if(finalMessage.equalsIgnoreCase("")){
                textView_message.setVisibility(View.GONE);
            }

            if(button1.equalsIgnoreCase("")){
                textView_cancel.setVisibility(View.GONE);
            }

            if(button2.equalsIgnoreCase("")){
                textView_confirm.setVisibility(View.GONE);
            }

            textView_title.setText(finalTitle);
            textView_message.setText(finalMessage);
            textView_cancel.setText(button1.toUpperCase());
            textView_confirm.setText(button2.toUpperCase());

            textView_cancel.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button1, input_data);
                hideDialog2();
            });

            textView_confirm.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button2, input_data);
                hideDialog2();
            });

            dialog2.setCancelable(isCancellable);
            dialog2.setCanceledOnTouchOutside(false);


            try {
                dialog2.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }



    public void showDialog_main(String requestTag_button1, String requestTag_button2, boolean isCancellable, String title, String message, String button1, String button2) {

        runOnUiThread(() ->
        {
            if (dialog != null) {
                hideDialog();
            }

            input_data = "";

            dialog = new Dialog(this, R.style.DialogTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_main);


            TextView textView_title = dialog.findViewById(R.id.dialog_main_textView_title);
            TextView textView_message = dialog.findViewById(R.id.dialog_main_textView_message);
            TextView textView_cancel = dialog.findViewById(R.id.dialog_main_textView_cancel);
            TextView textView_confirm = dialog.findViewById(R.id.dialog_main_textView_confirm);

            textView_title.setVisibility(View.VISIBLE);
            textView_message.setVisibility(View.VISIBLE);
            textView_cancel.setVisibility(View.VISIBLE);
            textView_confirm.setVisibility(View.VISIBLE);

            if (title.equalsIgnoreCase("")) {
                textView_title.setVisibility(View.GONE);
            }

            if (message.equalsIgnoreCase("")) {
                textView_message.setVisibility(View.GONE);
            }

            if (button1.equalsIgnoreCase("")) {
                textView_cancel.setVisibility(View.GONE);
            }

            if (button2.equalsIgnoreCase("")) {
                textView_confirm.setVisibility(View.GONE);
            }

            textView_title.setText(title);
            textView_message.setText(message);
            textView_cancel.setText(button1.toUpperCase());
            textView_confirm.setText(button2.toUpperCase());

            textView_cancel.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button1, input_data);
                hideDialog();
            });

            textView_confirm.setOnClickListener(v -> {
                onDialogDismiss(requestTag_button2, input_data);
                hideDialog();
            });

            dialog.setCancelable(isCancellable);
            dialog.setCanceledOnTouchOutside(false);

            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void hideDialog() {
        try {
            dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideDialog2() {
        try {
            dialog2.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isEmailValid(String s) {
        if(s.isEmpty()){
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(s).matches()){
            return false;
        } else {
            return true;
        }
    }

    public static boolean isNameValid(String s) {
        if(s.isEmpty()){
            return false;
        } else if (!Pattern.matches("[a-zA-Z]+",s)){
            return false;
        } else {
            return true;
        }
    }

    public void closeKeyBoard(){
        View view = this.getCurrentFocus();
        if (view != null){
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void setHideKeyboardOnTouch(final Context context, View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        try {
            //Set up touch listener for non-text box views to hide keyboard.
            if (!(view instanceof EditText || view instanceof ScrollView)) {

                view.setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        return false;
                    }

                });
            }
            //If a layout container, iterate over children and seed recursion.
            if (view instanceof ViewGroup) {

                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                    View innerView = ((ViewGroup) view).getChildAt(i);

                    setHideKeyboardOnTouch(context, innerView);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected abstract void onUpdateLocation(String latitude, String longitude);

    protected abstract void onDialogDismiss(String requestTag, String input_data);
}
