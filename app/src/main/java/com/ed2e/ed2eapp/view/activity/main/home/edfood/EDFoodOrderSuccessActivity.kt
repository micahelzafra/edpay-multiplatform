package com.ed2e.ed2eapp.view.activity.main.home.edfood

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.main.MapActivity
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_edfood_order_success.*
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent

class EDFoodOrderSuccessActivity : BaseActivity() {
//    @JvmField
//    @BindView(R.id.edfood_order_success_textView_orderID)
//    var textView_orderID: TextView? = null
//    @JvmField
//    @BindView(R.id.edfood_order_success_textView_orderAmount)
//    var textView_orderAmount: TextView? = null
//    @JvmField
//    @BindView(R.id.edfood_order_success_textView_paymentMethod)
//    var textView_paymentMethod: TextView? = null
//    //    @BindView(R.id.edfood_order_success_textView_orderDetail)
////    TextView textView_orderDetail;
//    @JvmField
//    @BindView(R.id.edfood_order_success_textView_noteToRider)
//    var textView_noteToRider: TextView? = null
//    @JvmField
//    @BindView(R.id.edfood_order_success_button_track_order)
    var button_track_order: Button? = null
    var orderID = ""
    var orderAmount = ""
    var paymentMethod = ""
    var orderDetail = ""
    var noteToRider = ""
    var apiInterface: ApiInterface? = null
    var preference: AppPreference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_order_success)
//        ButterKnife.bind(this)
        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)
        initData()
    }

    private fun initData() {
        Log.d(TAG, "initData() triggered")
        val extras = intent.extras
        if (extras == null) {
            orderID = ""
            orderAmount = ""
            paymentMethod = ""
            orderDetail = ""
            noteToRider = ""
        } else {
            orderID = extras.getString(key_booked_id)
            orderAmount = extras.getString(key_total)
            paymentMethod = extras.getString(key_payment_method)
//            orderDetail = extras.getString(key_cust_id)
            noteToRider = extras.getString(key_special_notes)
        }
        initGUI()
    }

    private fun initGUI() {
        edfood_order_success_textView_orderID.text = orderID
        edfood_order_success_textView_orderAmount.text = orderAmount
        edfood_order_success_textView_paymentMethod.text = paymentMethod
        //        textView_orderDetail.setText(orderDetail);
        edfood_order_success_textView_noteToRider.text = noteToRider
        edfood_order_success_button_track_order.setOnClickListener { v: View? ->
            val jsonObject = JsonObject()
            jsonObject.addProperty(key_booked_id, orderID)
            val intent =
                Intent(this@EDFoodOrderSuccessActivity, MapActivity::class.java)
            intent.putExtra(key_data, jsonObject.toString())
            startActivity(intent)
            val returnIntent = Intent()
            setResult(Activity.RESULT_OK, returnIntent)
            RxBus.publish(RxEvent.EventMainActivity(tag_intent_edfood_close_prev_act,this))
            finish()
        }
    }

    private fun initPreviousActivity() { //        if(currentPage == 1) {
//            Intent intent = new Intent(getContext(), Login1Activity.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//        } else {
//            currentPage = 1;
//            updatePage(currentPage);
//        }
    }

    override fun onDialogDismiss(
        requestTag: String,
        input_data: String
    ) {
        var intent: Intent
        //        switch (requestTag) {
//            case "tag_mpin_success_button_log_in":
//                intent = new Intent(getContext(), Login1Activity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//            default:
//                break;
//        }
    }

    override fun onUpdateLocation(
        latitude: String,
        longitude: String
    ) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK ->  //initPreviousActivity();
                true
            else -> super.onKeyUp(keyCode, event)
        }
    }

    companion object {
        val TAG = EDFoodOrderSuccessActivity::class.java.simpleName
    }
}