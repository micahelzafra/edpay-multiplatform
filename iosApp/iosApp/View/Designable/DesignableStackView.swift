//
//  DesignableStackView.swift
//  EDPoints App
//
//  Created by ED2E Technology iOS on 19/12/2018.
//  Copyright © 2018 ED2E Technology. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableStackView: UIStackView {
    
    @IBInspectable var cornerRadius: CGFloat = 0
        {
        didSet {
//            backgroundLayer.cornerRadius = cornerRadius
            refreshCR(_value: cornerRadius)
        }
    }
    
//    @IBInspectable var cornerRadius: CGFloat = 15 {
//        didSet {
//            refreshCR(_value: cornerRadius)
//        }
//    }
    
    @IBInspectable var topLeft: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var topRight: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var bottomLeft: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    @IBInspectable var bottomRight: Bool = true {
        didSet {
            refreshCR(_value: cornerRadius)
        }
    }
    
    func refreshCR(_value: CGFloat) {
        backgroundLayer.cornerRadius = _value
        var corners: UIRectCorner = []
        if topLeft
        {
            corners.insert(.topLeft)
        }
        if topRight
        {
            corners.insert(.topRight)
        }
        if bottomLeft
        {
            corners.insert(.bottomLeft)
        }
        if bottomRight
        {
            corners.insert(.bottomRight)
        }
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: self.cornerRadius, height: self.cornerRadius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        backgroundLayer.mask = mask
    }
    
    
    @IBInspectable private var color: UIColor?
    override var backgroundColor: UIColor? {
        get { return color }
        set {
            color = newValue
            self.setNeedsLayout() // EDIT 2017-02-03 thank you @BruceLiu
        }
    }
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.cornerRadius).cgPath
        
        var corners: UIRectCorner = []
        if topLeft
        {
            corners.insert(.topLeft)
        }
        if topRight
        {
            corners.insert(.topRight)
        }
        if bottomLeft
        {
            corners.insert(.bottomLeft)
        }
        if bottomRight
        {
            corners.insert(.bottomRight)
        }
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: self.cornerRadius, height: self.cornerRadius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        backgroundLayer.mask = mask
        backgroundLayer.fillColor = self.backgroundColor?.cgColor
    }
}


