//
//  ProfileHelpModel.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/29/20.
//

import Foundation
import UIKit

struct ProfileHelp {
    
    var contactNumber : String?
    var emailAddress : String?
    var website : String?
    var location : String?
    
}
