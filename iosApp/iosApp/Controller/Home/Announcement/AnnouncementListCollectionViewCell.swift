//
//  AnnouncementListCollectionViewCell.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/7/20.
//

import UIKit

class AnnouncementListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAnnouncementList: UIImageView!
    @IBOutlet weak var lblAnnouncementListTitle: UILabel!
    @IBOutlet weak var lblAnnouncementListDate: UILabel!
    @IBOutlet weak var lblAnnouncementListDetails: UILabel!
    @IBOutlet weak var viewAnnouncementList: DesignableCardView!
}
