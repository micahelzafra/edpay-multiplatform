package com.ed2e.ed2eapp.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.widget.EditText;
import android.widget.ImageView;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.BuildConfig;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.ErrorCodeHandler;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.util.ConstantKt;
import com.ed2e.ed2eapp.util.InternetUtils;
import com.ed2e.ed2eapp.view.activity.onboarding.OnboardingActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import timber.log.Timber;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.CommonConstantKt.FIREBASE_KEY_APP_VER;
import static com.ed2e.ed2eapp.util.CommonConstantKt.FIREBASE_TOPIC;
import static com.ed2e.ed2eapp.util.CommonConstantKt.device_type;
import static com.ed2e.ed2eapp.util.ConstantKt.KEY_FIRST_TIME_LAUNCH;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_device_token;
import static com.ed2e.ed2eapp.util.ConstantKt.key_error_list;
import static com.ed2e.ed2eapp.util.ConstantKt.key_remembered;
import static com.ed2e.ed2eapp.util.ConstantKt.request_error_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.request_update_app_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.secretKeyCode;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAccessCode;
import static com.ed2e.ed2eapp.util.ConstantKt.urlErrorXML;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.splash_imageView_splash_logo)
    ImageView imageView_splash_logo;
    public static final int SPLASH_SCREEN_TIME                               = 4 * 1000;
    Handler handler;

    ApiInterface apiInterface;
    AppPreference preference;

    boolean isSecondTime = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        getCurrentLocation(SplashActivity.this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);


        Glide.with(getContext())
                .load(R.drawable.ic_splash_intro)
                .into(imageView_splash_logo);

        checkInternetConnection();
        //initErrorCode();
        //initAccessCode();
    }

    private void checkInternetConnection() {
        InternetUtils internetChecker = new InternetUtils();
        if (internetChecker.isInternetOn(SplashActivity.this)) {
            initErrorCode();
            //showToast_short("CONNECTED");
//            FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_TOPIC);
//            if(BuildConfig.DEBUG) {
//                FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_TOPIC + "TESTING_TOPIC");
//            }
//            initErrorCode();
//            //initGUI();
        } else {


            if(isSecondTime) {
                isSecondTime = false;
                //showToast_short("" + isSecondTime);
                showDialog_APIError2("tag_request_no_internet_button_exit", "tag_request_no_internet_button_retry", false, "ED14", "Exit EDPAY", "Retry");
            } else {
                isSecondTime = true;
                //showToast_short("" + isSecondTime);
                showDialog_APIError("tag_request_no_internet_button_exit", "tag_request_no_internet_button_retry", false, "ED14", "Exit EDPAY", "Retry");
            }
            //showDialog_main("tag_request_no_internet_button_exit", "tag_request_no_internet_button_retry", false, getResources().getString(R.string.error_connection_title), getResources().getString(R.string.error_connection), "Exit EDPAY", "Retry");

            //showToast_short("NOT CONNECTED");
//            showDialog_main_2("", REQUEST_TAG_EXIT, false,true, false, getResources().getString(R.string.dialog_no_internet), getResources().getString(R.string.dialog_no_internet_message), R.drawable.ic_no_internet, "", getResources().getString(R.string.button_exit));
//            showDialogError(request_error_dialog,"ED14");
        }
    }

    private void initErrorCode()
    {
        apiInterface.parseAPI(
                secretKeyCode,
                urlErrorXML,
                "",
                response -> {
//                      saveXML();
                    Timber.d("APIResponse %s",response);
//                        parseXml(response);
                    ErrorCodeHandler errorCodeHandler = new ErrorCodeHandler();
                    Gson gson = new Gson();
                    String error_list = gson.toJson(errorCodeHandler.parse(response));
                    preference.putString(key_error_list, error_list);
                    Timber.d("errorCodeHandler %s",errorCodeHandler.parse(response));

                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SplashActivity.this, instanceIdResult ->
                    {
                        String newToken = instanceIdResult.getToken();
                        Timber.d("newToken %s",newToken);
                        preference.putString(key_device_token, newToken);
                    });
                    //checkVersion();
                    initAccessCode();

                    return null;
                }
                , failure -> {
                    System.out.println("Throwable:"+ failure);
                    //checkVersion();
                    initAccessCode();
                    return null;
                });
    }

    private void initAccessCode()
    {
        apiInterface.parseAPI(
                secretKeyCode,
                urlAccessCode,
                "",
                response -> {
//                      saveXML();
                    Timber.d("APIResponse %s",response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    JsonObject data = jsonObj.get(ConstantKt.key_data).getAsJsonObject();
                    String access_code = data.get(ConstantKt.key_access_code).getAsString();
                    preference.putString(ConstantKt.key_access_code, access_code);

                    runOnUiThread(() -> {
                        startTimer();
                    });
//                    Intent intent = new Intent(SplashActivity.this, LoginOrRegAcivity.class);
//                    startActivity(intent);
//                    finish();
                    return null;
                }
                , failure -> {
                    System.out.println("Throwable:"+ failure);
                    //TODO Error message
                    return null;
                });
    }

    private void startTimer() {
        new CountDownTimer(SPLASH_SCREEN_TIME, 1000) {

            public void onTick(long millisUntilFinished) {
                //Log.d("TIMER", "Countdown in: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                Log.d("Logs", "Country: " + getCurrentLocationData(tag_current_location_country_code, SplashActivity.this));
                //showToast_short("Country: " + getCurrentLocationData(tag_current_location_country_code));
                if(getCurrentLocationData(tag_current_location_country_code, SplashActivity.this) == null || getCurrentLocationData(tag_current_location_country_code, SplashActivity.this).equalsIgnoreCase("")) {
                    Intent intent;

                    if (preference.getBoolean(KEY_FIRST_TIME_LAUNCH, true)) {
                        intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.no_anim);
                    } else {
                        intent = new Intent(SplashActivity.this, LoginOrRegAcivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.no_anim);
                    }
                    //showDialog_main("", "tag_request_button_ok_splash", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
                } else {

                    if (getCurrentLocationData(tag_current_location_country_code, SplashActivity.this).equalsIgnoreCase("NG") || (getCurrentLocationData(tag_current_location_country_code, SplashActivity.this) == null || getCurrentLocationData(tag_current_location_country_code, SplashActivity.this).equalsIgnoreCase(""))) {
                        Intent intent;

                        if (preference.getBoolean(KEY_FIRST_TIME_LAUNCH, true)) {
                            intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.no_anim);
                        } else {
                            intent = new Intent(SplashActivity.this, LoginOrRegAcivity.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.no_anim);
                        }
                    } else {
                        showDialog_main("", "tag_request_button_ok_splash", false, getResources().getString(R.string.dialog_app_unavailable_title), getResources().getString(R.string.dialog_app_unavailable_message), "", "OK");
                    }

                }


            }

        }.start();
    }

    private void checkVersion() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(FIREBASE_KEY_APP_VER)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            Timber.d(document.getId() + " => " + document.getData());
                        }
                        //direct document
                        for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                            if (doc.get(device_type) != null) {
                                Timber.d("App version:" + doc.getId() + " => " + doc.getString(device_type));
                                String app_ver = doc.getString(device_type);
                                if (app_ver != null) {
                                    Timber.d("compareVersionNames:%s", compareVersionNames(app_ver, BuildConfig.VERSION_NAME));
                                    if(compareVersionNames(app_ver, BuildConfig.VERSION_NAME)>0) //if 1 outdated app
                                    {
//                                        showToast_short("Outdated App");
//                                        Log.w(TAG, "Version Result: " + "OUTDATED");
//                                        showDialog_main(REQUEST_TAG_UPDATE_APP, "", false,false, false, getResources().getString(R.string.dialog_app_not_updated), "", 0, getResources().getString(R.string.button_continue), "");
                                        showDialogError(request_update_app_dialog,getResources().getString(R.string.dialog_app_not_updated));
                                    } else {
                                        final Handler handler = new Handler();
                                        handler.postDelayed(() -> {
//                                            showToast_short("PROCEED");
//                                            Log.w(TAG, "Version Result: " + "PROCEED");

                                            if(preference.getBoolean(key_remembered)){
                                                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                            } else if (preference.getBoolean(KEY_FIRST_TIME_LAUNCH, true)) {
//                                                startActivity(new Intent(SplashActivity.this, OnboardingActivity.class));
                                                //TODO onboarding
                                                initAccessCode();
                                            } else {
                                                initAccessCode();
                                            }
                                            finish();
                                        }, SPLASH_SCREEN_TIME);
                                    }
                                }
                            }
                        }

                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            initAccessCode();
//                            showToast_short("PROCEED");
                            /*startActivity(new Intent(SplashActivity.this, OnboardingActivity.class));
                            finish();*/
                        }, SPLASH_SCREEN_TIME);
                    }
                });
    }

    public static int compareVersionNames(String oldVersionName, String newVersionName) {
        int res = 0;

        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");

        // To avoid IndexOutOfBounds
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);

        for (int i = 0; i < maxIndex; i++) {
            int oldVersionPart = Integer.valueOf(oldNumbers[i]);
            int newVersionPart = Integer.valueOf(newNumbers[i]);

            if (oldVersionPart < newVersionPart) {
                res = -1;
                break;
            } else if (oldVersionPart > newVersionPart) {
                res = 1;
                break;
            }
        }

        // If versions are the same so far, but they have different length...
        if (res == 0 && oldNumbers.length != newNumbers.length) {
            res = (oldNumbers.length > newNumbers.length) ? 1 : -1;
        }

        return res;
    }

    private void initGooglePlay()
    {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        finish();
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        switch (requestTag){
            case request_error_dialog:
                finish();
                break;

            case request_update_app_dialog:
                initGooglePlay();
                break;

            case "tag_request_button_ok_splash":
                finish();
                break;

            case "tag_request_no_internet_button_exit":
                finish();
                break;

            case "tag_request_no_internet_button_retry":
                checkInternetConnection();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    public void parseXml(String xml){
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput( new StringReader( xml) ); // pass input whatever xml you have
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if(eventType == XmlPullParser.START_DOCUMENT) {
                    Timber.d("Start document");
                } else if(eventType == XmlPullParser.START_TAG) {
                    Timber.d("Start tag " + xpp.getName());
                } else if(eventType == XmlPullParser.END_TAG) {
                    Timber.d("End tag " + xpp.getName());
                } else if(eventType == XmlPullParser.TEXT) {
                    Timber.d("Text " + xpp.getText()); // here you get the text from xml
                }
                eventType = xpp.next();
            }
            Timber.d("End document");

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
