
//  File.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/6/20.
//

struct Announcement {
    
    var announcementImage : String?
    var announcementTitle : String?
    var announcementDateFrom : String?
    var announcementDateTo : String?
    var announcementDetails : String?
    
}
