package com.ed2e.ed2eapp.view.activity.main.profile


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.myaccount.ChangePasswordActivity
import com.ed2e.ed2eapp.view.activity.otp.OTPActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_my_account.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class MyAccountActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var first_name = ""
    private var last_name = ""
    private var contact = ""
    private var email_address = ""

    private var global_mobile_number = ""
    private var global_prev_mobile_number = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initData()
        initToolBar()
        initGUI()
    }

    private fun initData() {
        val data = preference.getString(key_user_data)
        //showToast_short("DATA: $data")
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).getAsJsonObject()
        Timber.d("jsonObj: %s", jsonObj)

        first_name = preference.getString(key_first_name)
        last_name = preference.getString(key_last_name)
        contact = preference.getString(key_mobile_number)
        email_address = preference.getString(key_email_address)

        my_account_edittext_first_name.setText(first_name)
        my_account_edittext_last_name.setText(last_name)
        my_account_edittext_contact.setText(contact)
        my_account_edittext_email.setText(email_address)
    }

    private fun initGUI() {

        my_account_button_edit.setOnClickListener {
            my_account_button_edit.visibility = GONE
            my_account_button_save.visibility = VISIBLE
            my_account_button_cancel.visibility = VISIBLE

            my_account_edittext_first_name.isEnabled = true
            my_account_edittext_last_name.isEnabled = true
            my_account_edittext_contact.isEnabled = true
            my_account_edittext_email.isEnabled = true
        }

        my_account_button_save.setOnClickListener {
            Timber.d("name %s : %s",my_account_edittext_first_name.text.toString(),first_name)
            if(my_account_edittext_first_name.text.toString().equals(first_name)
                && my_account_edittext_last_name.text.toString().equals(last_name)
                && my_account_edittext_contact.text.toString().equals(contact)
                && my_account_edittext_email.text.toString().equals(email_address)
            )
            {
//                showDialogError(request_error_dialog, "No information has been changed. Please try again.")
                showDialog_main(
                    "",
                    "",
                    true,
                    "",
                    resources.getString(R.string.dialog_no_info_change_message),
                    "",
                    "OK"
                )

            }
            else
            {

                showDialog_main(
                    "",
                    "tag_request_button_yes",
                    true,
                    label_dialog_title_update_account_info,
                    label_dialog_msg_update_account_info,
                    "No",
                    "Yes"
                )

//                AlertDialog.Builder(this)
//                    .setTitle(label_dialog_title_update_account_info)
//                    .setMessage(label_dialog_msg_update_account_info)
//                    .setPositiveButton(button_yes
//                    ) { dialogInterface: DialogInterface, i: Int ->
//                        dialogInterface.dismiss()
////                        if (!my_account_edittext_contact.text.toString().equals(contact))
////                        {
//////                                TODO("intent to OTP")
////
////                            val intent = Intent(this@MyAccountActivity, OTPActivity::class.java)
////                            intent.putExtra(key_mobile_number, my_account_edittext_contact.text.toString())
////                            intent.putExtra(key_cust_id, preference.getString(key_user_id))
////                            intent.putExtra(key_prev_page, "login")
////                            startActivity(intent)
////                            finish()
////                            overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)
////                        }
////                        else
////                        {
//                            this@MyAccountActivity.runOnUiThread {
//                                initAccountInfoUpdate(
//                                    preference.getString(key_user_id),
//                                    my_account_edittext_last_name.text.toString(),
//                                    my_account_edittext_first_name.text.toString(),
//                                    my_account_edittext_email.text.toString(),
//                                    my_account_edittext_contact.text.toString(),
//                                    contact)
//                            }
////                        }
//                    }
//                    .setNegativeButton(button_no
//                    ) { dialogInterface: DialogInterface, i: Int ->
//                        dialogInterface.dismiss()
//                    }
//                    .setCancelable(false)
//                    .show()
            }
        }

        my_account_button_cancel.setOnClickListener {
            my_account_button_edit.visibility = VISIBLE
            my_account_button_save.visibility = GONE
            my_account_button_cancel.visibility = GONE

            my_account_edittext_first_name.isEnabled = false
            my_account_edittext_last_name.isEnabled = false
            my_account_edittext_contact.isEnabled = false
            my_account_edittext_email.isEnabled = false

            initData()
        }

        my_account_button_change_password.setOnClickListener {
            val intent = Intent(this@MyAccountActivity, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        my_account_button_change_pin.setOnClickListener {
            val intent = Intent(this@MyAccountActivity, ChangePINActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }


//        change_pin_textview_forgot_pin.setOnClickListener {
//            //TODO intent to change PIN page
//        }
//        change_pin_button_save.setOnClickListener {
//            val current_pin = change_pin_edittext_current_pin.text
//            val new_pin = change_pin_edittext_new_pin.text
//            val confirm_pin = change_pin_edittext_retype_new_pin.text
//            if (new_pin.isNullOrEmpty() || confirm_pin.isNullOrEmpty())
//            {
//                showDialogError(request_error_dialog, "ED94")
//                return@setOnClickListener
//            }
//
//            if ("$new_pin".length <= 5 || "$confirm_pin".length <= 5)
//            {
//                showDialogError(request_error_dialog, "ED94")
//                return@setOnClickListener
//            }
//
//            if ("$new_pin".equals("$confirm_pin"))
//            {
//                showDialogError(request_error_dialog, "ED100")
//                return@setOnClickListener
//            }
//            initPINUpdate(preference.getString(key_user_id), "$current_pin","$new_pin")
//        }
    }

    private fun initAccountInfoUpdate(
        customer_id: String,
        last_name: String,
        first_name: String,
        email_address: String,
        mobile_number: String,
        prev_mobile_number: String
    ) {

        showProgress()
        val params = "customer_id:" + customer_id + "||" +
                "last_name:" + last_name + "||" +
                "first_name:" + first_name + "||" +
                "email_address:" + email_address + "||" +
                "mobile_number:" + mobile_number
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlUpdateUserInfo,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString

                Log.d("Tag", "Response Account: " + response)

                if (status == status_success) {
                    hideProgress()
                    val data = jsonObj[key_data].asJsonObject
                    Timber.d("data: %s", data)

                    val new_first_name = data["first_name"].asString
                    val new_last_name = data["last_name"].asString
                    val new_email_address = data["email_address"].asString
                    val new_mobile_number = data["mobile_number"].asString

                    preference.putString(key_first_name, new_first_name)
                    preference.putString(key_last_name, new_last_name)
                    preference.putString(key_email_address, new_email_address)
                    preference.putString(key_mobile_number, new_mobile_number)





                    this@MyAccountActivity.runOnUiThread {

                        initData()
                        global_mobile_number = mobile_number
                        global_prev_mobile_number = prev_mobile_number

                        showDialog_main("", "tag_changes_saved_button_ok", false, label_dialog_title_change_save, "Your information has been updated.", "", "OK");

//                        AlertDialog.Builder(this)
//                            .setTitle(label_dialog_title_change_save)
//                            .setMessage("Your information has been updated.")
//                            .setPositiveButton(
//                                button_okay
//                            ) { dialogInterface: DialogInterface, i: Int ->
//                                dialogInterface.dismiss()
//                                if (!mobile_number.equals(prev_mobile_number))
//                                {
//                                    val intent = Intent(this@MyAccountActivity, OTPActivity::class.java)
//                                    intent.putExtra(key_mobile_number, my_account_edittext_contact.text.toString())
//                                    intent.putExtra(key_cust_id, preference.getString(key_user_id))
//                                    intent.putExtra(key_prev_page, "login")
//                                    startActivity(intent)
//                                    finish()
//                                    overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)
//
//                                }
//                                else {
//                                    finish()
//                                }
//                            }
//                            .setCancelable(false)
//                            .show()
                    }
                } else {
                    hideProgress()
                    my_account_button_edit.visibility = VISIBLE
                    my_account_button_save.visibility = GONE
                    my_account_button_cancel.visibility = GONE

                    my_account_edittext_first_name.isEnabled = false
                    my_account_edittext_last_name.isEnabled = false
                    my_account_edittext_contact.isEnabled = false
                    my_account_edittext_email.isEnabled = false
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            hideProgress()
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initToolBar() {
        my_account_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    my_account_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    my_account_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
    }

    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)

        when (requestTag) {
            "tag_changes_saved_button_ok" -> {
                if (!global_mobile_number.equals(global_prev_mobile_number))
                {
                    val intent = Intent(this@MyAccountActivity, OTPActivity::class.java)
                    intent.putExtra(key_mobile_number, my_account_edittext_contact.text.toString())
                    intent.putExtra(key_cust_id, preference.getString(key_user_id))
                    intent.putExtra(key_prev_page, "login")
                    startActivity(intent)
                    finish()
                    overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)

                }
                else {
                    finish()
                }
            }
            "tag_request_button_yes" -> {
//                        if (!my_account_edittext_contact.text.toString().equals(contact))
//                        {
////                                TODO("intent to OTP")
//
//                            val intent = Intent(this@MyAccountActivity, OTPActivity::class.java)
//                            intent.putExtra(key_mobile_number, my_account_edittext_contact.text.toString())
//                            intent.putExtra(key_cust_id, preference.getString(key_user_id))
//                            intent.putExtra(key_prev_page, "login")
//                            startActivity(intent)
//                            finish()
//                            overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)
//                        }
//                        else
//                        {
                            this@MyAccountActivity.runOnUiThread {
                                initAccountInfoUpdate(
                                    preference.getString(key_user_id),
                                    my_account_edittext_last_name.text.toString(),
                                    my_account_edittext_first_name.text.toString(),
                                    my_account_edittext_email.text.toString(),
                                    my_account_edittext_contact.text.toString(),
                                    contact)
                            }
//                        }
            }
            else -> {
            }
        }


    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}