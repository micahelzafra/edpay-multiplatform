package com.ed2e.ed2eapp.model

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.view.fragment.main.ActivityFragment
import com.ed2e.ed2eapp.view.fragment.main.HomeFragment
import com.ed2e.ed2eapp.view.fragment.main.ProfileFragment

/**
 * Screens available for display in the main screen, with their respective titles,
 * icons, and menu item IDs and fragments.
 */
enum class MainScreen(@IdRes val menuItemId: Int,
                      @DrawableRes val menuItemIconId: Int,
                      @StringRes val titleStringId: Int,
                      val fragment: Fragment
) {
    HOME(R.id.bottom_navigation_item_home, R.drawable.ic_home, R.string.bottom_nav_home,
        HomeFragment()
    ),
    ACTIVITY(R.id.bottom_navigation_item_activity, R.drawable.ic_activity, R.string.bottom_nav_activity,
        ActivityFragment()
    ),
    PROFILE(R.id.bottom_navigation_item_profile, R.drawable.ic_profile, R.string.bottom_nav_profile,
        ProfileFragment()
    )
}

fun getMainScreenForMenuItem(menuItemId: Int): MainScreen? {
    for (mainScreen in MainScreen.values()) {
        if (mainScreen.menuItemId == menuItemId) {
            return mainScreen
        }
    }
    return null
}