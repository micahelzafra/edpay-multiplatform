//
//  EDPaySuccessVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 1/28/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage
import CoreMotion
import AVFoundation
import SwiftyGif

class EDPayShakeRewardVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    var merchantData = JSON()
    
    var motionManager = CMMotionManager()
    
    @IBOutlet weak var btnSubmit: DesignableButton!
    
    @IBOutlet weak var lblShakePoints: UILabel!
    @IBOutlet weak var viewCongratulations: UIView!
    @IBOutlet weak var btnKeepIt: DesignableButton!
    
    var xInPositiveDirection = 0.0
    var xInNegativeDirection = 0.0
    var shakeCount = 0
    var tempVariable = 0
    
    //Timer
    var countdownTimer: Timer!
    var totalTime = 4

    var player: AVAudioPlayer?
    let logoAnimationView = LogoAnimationShakeView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.logoAnimationView)
        self.logoAnimationView.pinEdgesToSuperView()
        self.logoAnimationView.logoGifImageView.delegate = self as? SwiftyGifDelegate
        
        self.initGUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       logoAnimationView.logoGifImageView.startAnimatingGif()
        
        viewCongratulations.isUserInteractionEnabled = true
              viewCongratulations.isHidden = true
    }
    
    func initGUI()
    {
        motionManager = CMMotionManager()
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: OperationQueue.main) { (data, error) in
//                print("motionManager \(data)")
                if data!.acceleration.x > 1.0 || data!.acceleration.x < -1.0 {

                    if data!.acceleration.x > 1.0 {
                        self.xInPositiveDirection = data!.acceleration.x
                    }

                    if data!.acceleration.x < -1.0 {
                        self.xInNegativeDirection = data!.acceleration.x
                    }

                    if self.xInPositiveDirection != 0.0 && self.xInNegativeDirection != 0.0 {
                        self.shakeCount = self.shakeCount + 1
                        self.xInPositiveDirection = 0.0
                        self.xInNegativeDirection = 0.0
                    }

                    if self.shakeCount > 1 {
                        self.tempVariable = self.tempVariable + 1
                        print("EDPayShakeRewardVC Shaken! \(self.tempVariable)")
                        self.shakeCount = 0
                        self.playSound()
                    }
                    
                }
            }
        }
        startTimer()
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
    }
    
    
    @IBAction func didTapButtonKeepIt(_ sender: Any) {
        self.performSegue(withIdentifier: "segueMain", sender: nil)
    }
    
    func startTimer() {
        print("start timer")
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        if countdownTimer != nil
        {
            print("\(timeFormatted(totalTime))")
            if totalTime != 0 {
                totalTime -= 1
            } else {
                endTimer()
            }
        }
    }
    
    func endTimer() {
        print("end timer")
        countdownTimer.invalidate()
        countdownTimer = nil
        self.initCashBack(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            transaction_no: segueData[self.constant.key_amount] as! String,
            payment_type: paymentType(value: segueData[self.constant.key_payment_method] as! String).asString()
        )
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "sound_shake_2", withExtension: "mp3") else {
            print("url not found")
            return
        }

        do {
            /// this codes for making this app ready to takeover the device audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)

            /// change fileTypeHint according to the type of your audio file (you can omit this)

            /// for iOS 11 onward, use :
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /// else :
            /// player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3)

            // no need for prepareToPlay because prepareToPlay is happen automatically when calling play()
            player!.play()
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }
    }
    
    private func initCashBack(
        customer_id: String,
        transaction_no: String,
        payment_type: String
    ){
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCashback,
            params: "customer_id:\(customer_id)||transaction_no:\(transaction_no)||payment_type:\(payment_type)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    self.motionManager.stopAccelerometerUpdates()
                    self.view.addSubview(self.viewCongratulations)
                    self.viewCongratulations.isUserInteractionEnabled = true
                    self.viewCongratulations.isHidden = false
                    
                    self.lblShakePoints.text = "NGN  \(json[self.constant.key_cashback_amount].string ?? "")"
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePaymentSuccess" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? PaymentMobilePINVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
}
