package com.ed2e.ed2eapp.view.activity.announcements;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.Cashback;
import com.ed2e.ed2eapp.model.MainScreen;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_prev_page;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegisterCheckOTP;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRegisterSendOTP;

public class AnnouncementsActivity extends BaseActivity {

    public static final String TAG = AnnouncementsActivity.class.getSimpleName();

    @BindView(R.id.announcement_list_home_recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.announcement_list_home_textview_message)
    TextView textview_message;

    @BindView(R.id.announcements_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.announcements_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;



//    @BindView(R.id.otp_pinview)
//    PinView pinview;
//    @BindView(R.id.otp_textView_timer)
//    TextView textView_timer;
//    @BindView(R.id.otp_button_submit)
//    Button button_submit;

    private String mobileNumber = "";
    private String customerID = "";
    private String prevPage = "";

    private List<Announcement> modelList;
    static RecyclerView recyclerView;
    AnnouncementListAdapter recyclerAdapter;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_list);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

//        Bundle extras = getIntent().getExtras();
//        if (extras == null) {
//            mobileNumber = "";
//            customerID = "";
//            prevPage = "";
//        } else {
//            mobileNumber = extras.getString(key_mobile_number);
//            customerID = extras.getString(key_cust_id);
//            prevPage = extras.getString(key_prev_page);
//        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.announcement_list_home_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new AnnouncementListAdapter(AnnouncementsActivity.this, modelList);
        recyclerView.setAdapter(recyclerAdapter);


        initAPIGetAnnouncementsList();

        //updateButton(false);

        //mobileNumber = "";

//        textView_timer.setOnClickListener(v -> {
//            if(timerCount <= 0) {
//                timerCount = 60;
//                updateButton(false);
//            } else {
//                return;
//            }
//        });
//
//        button_submit.setOnClickListener(v -> {
//            if(pinview.getText().toString().trim().length() < 6) {
//                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
//            } else {
//                initAPICheckOTP(mobileNumber, pinview.getText().toString().trim());
//            }
//
//            //showDialog_main("", "button_ok", true,"Registration Successful", "You may use your registered mobile number to login and enjoy EDPAY App features.", "", "OK");
//        });
//
//        if(mobileNumber.equalsIgnoreCase("") || mobileNumber == null) {
//            showDialog_APIError("", "tag_button_ok", true, "ED72", "", "OK");
//        } else {
//            initAPISendOTP(mobileNumber);
//            //startTimer();
//        }
    }

    private void initAPIGetAnnouncementsList() {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlAnnouncementList,"",
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | ");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    modelList = new ArrayList<>();
                    modelList.clear();

                    if (status.equals(status_success))
                    {

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray announcement_data = obj.getJSONArray("announcement_records");

                            //showToast_short("SUCCESS pO: " + announcement_data.length());

                            runOnUiThread(() -> {
                                if (announcement_data.length() == 0) {
                                    textview_message.setVisibility(View.VISIBLE);
                                } else {
                                    textview_message.setVisibility(View.GONE);
                                }
                            });

                            for(int i = 0; i < announcement_data.length(); i++)
                            {



                                JSONObject object = announcement_data.getJSONObject(i);



                                String announcement_id;
                                try {
                                    announcement_id = "" + (String) object.get("id");
                                } catch (Exception e) {
                                    announcement_id = "" + object.getInt("id");
                                }

                                String title = (String) object.get("title");
                                String content = (String) object.get("content");
                                String event_date = (String) object.get("event_date");
                                String expiry_date = (String) object.get("expiry_date");
                                String image = (String) object.get("image");
                                String countries = "";
                                //String countries = "\"" + object.get("countries") + "\"";


//                                if(title.equalsIgnoreCase(barcode)) {
//                                    showToast_short("Data Found");
//                                    break;
//                                } else {
//                                    continue;
//                                }
//
//                                showDialog_main("", "", true, "Invalid Merchant", "This merchant is not available in your area.", "","OK");


                                String annc_status;
                                try {
                                    annc_status = "" + (String) object.get("annc_status");
                                } catch (Exception e) {
                                    annc_status = "" + object.getInt("annc_status");
                                }

                                Announcement list = new Announcement(
                                        announcement_id,
                                        title,
                                        content,
                                        event_date,
                                        expiry_date,
                                        image,
                                        countries,
                                        annc_status
                                );
                                modelList.add(list);
                            }

                            runOnUiThread(() -> {
                                recyclerAdapter.setModelList(modelList);
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



//                        JsonArray announcement_data = parser.parse(String.valueOf(jsonObj.get("announcement_records"))).getAsJsonArray();
//                        showToast_short("SUCCESS: " + announcement_data.size());



                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;



                });
    }
//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }


    private void initGetCountryFlag() {
//        switch(getCurrentLocationData(tag_current_location_country_code)) {
//            case "PH":
//                Glide.with(MainActivity.this)
//                        .load(R.drawable.philippines)
//                        .apply(new RequestOptions().override(256, 256).centerCrop().placeholder(R.drawable.ic_flag_default))
//                        .into( MainScreen.HOME.fragment.fragment_home_imageview_flag);
//                break;
//            case "NG":
//                break;
//            case "MY":
//                break;
//            default:
//                break;
//        }
    }


    private void initPreviousActivity() {

        finish();

    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "button_ok":
//                showToast_short("To Homescreen");
                intent = new Intent(getContext(), CreateMPinActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case "tag_register_success_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(AnnouncementsActivity.this, CreateMPinActivity.class);
                    intent.putExtra(key_cust_id, customerID);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(AnnouncementsActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    intent = new Intent(AnnouncementsActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }

                break;
            case "tag_otp_incomplete_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(getContext(), LoginOrRegAcivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }

                break;


            default:
                break;
        }
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
