package com.ed2e.ed2eapp

import kotlinx.serialization.Serializable

@Serializable
data class MovieResponse(
    val status: String,
//    val code: List<Result>,
//    val code: String,
//    val title: String,
    val message: String
)