package com.ed2e.ed2eapp.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_device_token;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_otp_verified;
import static com.ed2e.ed2eapp.util.ConstantKt.key_pin;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.request_error_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlLogin;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.login_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.login_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.login_edittext_phone)
    EditText editText_phone;
    @BindView(R.id.login_edittext_password)
    EditText editText_password;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        //setupToolBar(R.id.login_toolbar);
        getLastLocation(LoginActivity.this);
        initToolBar();
        initGUI();
    }

    private void initGUI() {

    }

    @OnClick(R.id.login_button_default_login) void submit() {
        String password = editText_password.getText().toString();
        String email = editText_phone.getText().toString();
        if (password.equals(""))
        {
//            this.errorMsg('ED1')
            showDialogError(request_error_dialog,"ED1");
            return;
        }

        initLogin(email, password, preference.getString(key_device_token), "nigeria");
    }

    private void initLogin(String email, String password, String device_token, String country)
    {
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlLogin,
            "email:"+email+"||" +
                    "password:"+password+"||" +
                    "login_type:1||" +
                    "device_id:"+device_token+"||" +
                    "country_name:"+country,
            response -> {

                JsonParser parser = new JsonParser();
                JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                String status = jsonObj.get(key_status).getAsString();
                if (status.equals(status_success))
                {
                    JsonObject data = jsonObj.get(key_data).getAsJsonObject();
                    String user_id = data.get(key_id).getAsString();
                    String pin = data.get(key_pin).getAsString();
                    String otp_verified = data.get(key_otp_verified).getAsString();
                    preference.putString(key_user_id, user_id);

                    if (otp_verified.equals("0"))
                    {
                        //TODO OTP PAGE
//                        Intent intent=new Intent(LoginActivity.this, PinCreateAcivity.class);
//                        startActivity(intent);
//                        finish();
                        return null;
                    }

                    if (pin.equals("0"))
                    {
                        Intent intent=new Intent(LoginActivity.this, PinCreateAcivity.class);
                        startActivity(intent);
                        finish();
                        return null;
                    }
                    Intent intent=new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    String error_code = jsonObj.get(key_code).getAsString();
                    //showDialog_main("button_cancel", "button_register", false,"Mobile number is not registered", "Please register your mobile number.", "CANCEL", "Register");
                    //showDialog_APIError("", "button_register", true, error_code, "", "OK");
                    showDialogError(request_error_dialog,error_code);
                }

                return null;
            }
            , failure -> {
                System.out.println("Throwable:"+ failure);
                showDialogError(request_error_dialog,failure.getLocalizedMessage());
                return null;
            });
    }







    private void initPreviousActivity() {
        Intent intent = new Intent(getContext(), LoginOrRegAcivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {

    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {
        Timber.d("onUpdateLocation %s : %s",latitude,longitude);

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed_blue);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
