//
//  ActivityTopUpVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/24/20.
//

import UIKit

class ActivityTopUpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tfDateFrom: DesignableTextField!
    @IBOutlet weak var tfDateTo: DesignableTextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datePicker : UIDatePicker?
    var topUpList = [ActivityTopUp]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
            
        tfDateFrom.inputView = datePicker
        tfDateTo.inputView = datePicker
            datePicker?.addTarget(self, action: #selector(ActivityTopUpVC.dateChanged(datePicker:)), for: .valueChanged)
            
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActivityFoodVC.viewTapped(gestureRecognizer:)))
                view.addGestureRecognizer(tapGesture)
                
        //Write toolbar code for done button
        let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
            toolBar.setItems([space, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            toolBar.sizeToFit()
            tfDateFrom.inputAccessoryView = toolBar
            tfDateTo.inputAccessoryView = toolBar
        }
        
        //Toolbar done button function
        @objc func onClickDoneButton() {
            self.view.endEditing(true)
        }
        
        @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
            view.endEditing(true)
        }
        
        @objc func dateChanged(datePicker: UIDatePicker) {
            
            let format = DateFormatter()
                format.dateFormat = "MM/dd/yyyy"
            
            tfDateFrom.text = format.string(from: datePicker.date)
            tfDateTo.text = format.string(from: datePicker.date)
        }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if topUpList.count == 0
         {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
         }
         else
         {
             tableView.restore()
         }
         return topUpList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTopUpTableViewCell", for: indexPath)
                        as! ActivityTopUpTableViewCell
        
//        let topUp = topUpList[indexPath.row]
//        cell.lblMerchantName.text = topUp.name
//        cell.lblAmount.text = topUp.amount
//        cell.lblTransactionNumber.text = topUp.transaction_number
//        cell.lblDateAndTime.text = topUp.date_and_time
//        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
   
}
