//
//  AnnouncementCollectionViewCell.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/6/20.
//

import UIKit

class AnnouncementCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewAnnouncement: UIImageView!
    @IBOutlet weak var lblAnnouncementName: UILabel!
    
}
