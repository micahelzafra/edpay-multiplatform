//
//  ActivityPayModel.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/24/20.
//

import Foundation
import UIKit

struct ActivityPay {
    
    var id : Int?
    var name : String?
    var amount : String?
    var transaction_number : String?
    var date_and_time : String?
    
}
