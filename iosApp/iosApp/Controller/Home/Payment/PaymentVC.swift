//
//  PaymentVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/23/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class PaymentVC: BaseViewController, SlideButtonDelegate {
    
    internal var apiInterface = ApiInterface()

    var segueData = Dictionary<String, AnyObject>()

    @IBOutlet weak var imageViewMerchant: RoundableImageView!
    @IBOutlet weak var lblMerchantName: UILabel!
    
    @IBOutlet weak var tfAmount: DesignableTextField!
    @IBOutlet weak var tfPromocode: DesignableTextField!
    @IBOutlet weak var btnConfirmEdit: DesignableButton!
    @IBOutlet weak var btnApply: DesignableButton!
    @IBOutlet weak var slideToPay: MMSlidingButton!
    
    //Voucher
    @IBOutlet weak var lblVoucherResult: UILabel!
    
    @IBOutlet weak var viewSlide: UIView!

    private var bookedId = ""
    private var countryName :String? = ""
    private var bookedLocation :String? = ""
    private var restaurantId = ""
    private var promoId = ""
    private var promoName = ""
    private var promoAmount = ""
    private var isVoucherCodeValid = true
    
    
    var currencyCode = ""
    let downloader = ImageDownloader()
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.slideToPay.delegate = self
        tfAmount.delegate = self
        btnConfirmEdit.isSelected = false
        btnConfirmEdit.isSelected = false
        viewSlide.isUserInteractionEnabled = true
        viewSlide.isHidden = true
        
        let data = segueData[self.constant.key_data] as! JSON
        lblMerchantName.text = data[self.constant.key_trade_name].string

                
        if let imageurl = data[self.constant.key_shop_facade].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewMerchant.image = image
                }
            }
        }

        currencyCode = getCountryCurrency()
    }
    
    @IBAction func didTapButtonApply(_ sender: Any) {
        
    }
    
    @IBAction func didTapButtonConfirmEdit(_ sender: Any) {
        
        if tfAmount.text == "" {
            let alert = UIAlertController(title: "Invalid Amount", message: "Please input a valid amount that is greater than zero(0)", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

            self.present(alert, animated: true)
        } else {
            if btnConfirmEdit.isSelected  == false {
                btnConfirmEdit.isSelected = true
                viewSlide.isUserInteractionEnabled = true
                viewSlide.isHidden = false
                
                let formatter: NumberFormatter = NumberFormatter()
                formatter.locale = NSLocale(localeIdentifier: "EN") as Locale
                let final = formatter.number(from: tfAmount.text ?? "0.00")
                let doubleNumber = Double(truncating: final!)
                print("\(doubleNumber.formattedWithSeparator)")
                lblAmount.text = doubleNumber.formattedWithSeparator
                dismissKeyboard()
                self.tfAmount.isUserInteractionEnabled = false
            } else {
                btnConfirmEdit.isSelected = false
                viewSlide.isUserInteractionEnabled = true
                viewSlide.isHidden = true
                self.tfAmount.isUserInteractionEnabled = true
            }
        }
     
        
    }

    private func getCountryCurrency() -> String {
        let country = self.defaults.string(forKey: self.constant.key_country)
        switch country {
        case "PH":
            return "PHP"
        case "NG":
            return "NGN"
        case "MY":
            return "RM"
        default:
            return ""
        }
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        var segueData = Dictionary<String, AnyObject>()
        segueData[self.constant.key_data] = self.segueData[self.constant.key_data] as AnyObject
        segueData[self.constant.key_amount] = self.lblAmount.text as AnyObject?
        segueData[self.constant.key_currency_code] = self.currencyCode as AnyObject
        self.performSegue(withIdentifier: "seguePaymentMobilePIN", sender: segueData)
    }
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool
    {
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "EN") as Locale
        let final = formatter.number(from: textField.text ?? "0.00")
        let doubleNumber = Double(truncating: final!)
        
        let countdots = (doubleNumber.formattedWithSeparator.components(separatedBy: ".").count) - 1

        if countdots > 0 && string == "."
        {
            return false
        }
        return true
    }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePaymentMobilePIN" {
            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? PaymentMobilePINVC {
                    chidVC.segueData = (sender as? [String:AnyObject])!
                    print("Entering\(chidVC)")
                }
            }
        }
    }
    
    //For checking the voucher code
    private func initAPICheckPromoCode(
        customer_id: String,
        merchant_id: String,
        promo_code: String,
        total_purchased: String
    ) {
      showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlEDPayPromoCode,
            params: "customer_id:\(customer_id)||merchant_id:\(merchant_id)||promo_code:\(promo_code)||total_purchased:\(total_purchased)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)


                self.lblVoucherResult.alpha = 1.0
                if let status = json[self.constant.key_status].string {
                    self.log("status \(status)")
                    if status == self.constant.status_success {
                        self.isVoucherCodeValid = true
                        let jsonObject = json[self.constant.key_data].self

                        let total_purchased_with_discount = jsonObject["total_purchased_with_discount"].string
//                        let promoName = jsonObject[self.constant.key_promo_name].string
//                        let promoID = jsonObject["promo_id"].int
//                        let discountValue = jsonObject[self.constant.key_discount_value].string
//                        self.promoName = promoName!
//                        self.promoAmount = discountValue!
//                        self.promoId = "\(promoID ?? -1)"
//                        self.lblVoucherResult.text = "\(promoName ?? "") less discount - \(commaSeparated(value: discountValue!).asString())"
//                        self.lblVoucherResult.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                        
//                        let additionalFee = Double(containerfee!.replacingOccurrences(of: ",", with: ""))! + Double(deliveryfee!.replacingOccurrences(of: ",", with: ""))! + Double(vatamount!.replacingOccurrences(of: ",", with: ""))! + Double(servicefee!.replacingOccurrences(of: ",", with: ""))!
//                        let totalVoucher = totalVoucher + additionalFee
//                        self.lblTotal.text = commaSeparated(value: "\(totalVoucher)").asString()
                    }
                    else
                    {
                        self.isVoucherCodeValid = false
                        let title = json[self.constant.key_title].string
                        self.lblVoucherResult.text = "\(title ?? "")"
                        self.lblVoucherResult.textColor = #colorLiteral(red: 0.9621763825, green: 0.01609125175, blue: 0, alpha: 1)
                    }
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                
                self.hideLoading()
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            self.hideLoading()
            return KotlinUnit()
        })
    }
}
