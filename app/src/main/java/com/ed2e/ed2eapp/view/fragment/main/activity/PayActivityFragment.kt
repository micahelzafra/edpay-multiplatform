package com.ed2e.ed2eapp.view.fragment.main.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.R.layout.fragment_activity_logs
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonParser
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates


class PayActivityFragment : Fragment(fragment_activity_logs)
{
    lateinit var button_date_from: Button
    lateinit var button_date_to: Button
    lateinit var recyclerView: RecyclerView
    var isFragmentVisible = false

    fun getActionbar() : ActionBar?
    {
        return (activity as AppCompatActivity).supportActionBar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (userVisibleHint) {
            initGUI(view)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Timber.d("setUserVisibleHint(): %s %s" , isVisibleToUser , getView())
        if (getView() != null) {
            if (isVisibleToUser) {
                initGUI(getView())
            }
        }
    }

    private fun initGUI(view: View?) {
        if (view != null) {
            button_date_from = view.findViewById(R.id.fragment_activity_logs_button_date_from)
            button_date_to = view.findViewById(R.id.fragment_activity_logs_button_date_to)
            recyclerView = view.findViewById(R.id.fragment_activity_logs_recyclerview)

            val c = Calendar.getInstance().time
            val df = SimpleDateFormat(date_format_picker_display, Locale.US)
            val formattedDate = df.format(c)

            button_date_from.text = get30DaysBeforeDate().asString()
            button_date_to.text = formattedDate
            if (isFragmentVisible)
            {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_pay, this))
            }

            button_date_from.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_pay_from, this))
            }

            button_date_to.setOnClickListener {
                RxBus.publish(RxEvent.EventMainActivity(tag_load_api_log_pay_to, this))
            }
        }
    }

    override fun setMenuVisibility(visible: Boolean) {
        super.setMenuVisibility(visible)
        isFragmentVisible = visible
    }

    fun displayList(list:ArrayList<Any>)
    {
        activity?.runOnUiThread {
            val parser = JsonParser()
            Collections.sort(list,
                Comparator {
                        jinks, jinx -> parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
                })
            val adapter = object : BaseRecyclerViewAdapter<Any>(list, object :
                ItemClickListener<Any> {
                override fun onItemClick(v: View, item: Any, position: Int) {

                }
            }) {
                override fun getLayoutId(position: Int, obj: Any): Int {
                    return R.layout.child_log_pay
                }

                override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                    return ViewHolderFactory.create(view,viewType)
                }
            }
            recyclerView.layoutManager= LinearLayoutManager(context)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter=adapter
        }
    }
}