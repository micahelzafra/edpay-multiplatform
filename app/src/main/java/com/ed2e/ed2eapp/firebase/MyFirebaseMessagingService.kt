package com.ed2e.ed2eapp.firebase

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.model.NotificationData
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.LoginOrRegAcivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Timber.d("onNewToken: %s", s)
    }

    override fun onMessageSent(msgId: String) {
        Timber.d("onMessageSent: %s", msgId)
    }

    override fun onSendError(msgId: String, e: Exception) {
        Timber.e("onSendError: %s", msgId)
        Timber.e(e, "Exception")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) { // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Timber.w("From: %s", remoteMessage.from)
        val parser = JsonParser()
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            remoteMessage.data
            Timber.w("Message data payload: %s", remoteMessage.data)
            if (remoteMessage.data[key_booked_id] != null) {
                val preference = AppPreference.getInstance(this)
                if (preference.getString(pref_jinxnotif_bookedid, "").isNotEmpty()) {
                    val jsonArray =
                        parser.parse(preference.getString(pref_jinxnotif_bookedid)).asJsonArray
                    for (j in 0 until jsonArray.size()) {
                        val jsonObject =
                            jsonArray[j].asJsonObject
                        val bookedId = jsonObject[key_booked_id].asString
                        if (bookedId == remoteMessage.data[key_booked_id]) {
                            getImage(remoteMessage)
                        }
                    }
                }
            }
            else
            {
                if (remoteMessage.data[key_announcement_image] != null) {
                    getImage(remoteMessage)
                }
            }
        }
        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Timber.w("Message Notification Body: %s", remoteMessage.notification!!.body)
        }
    }

    private fun sendNotification(bitmap: Bitmap) {
        val style =
            NotificationCompat.BigPictureStyle()
        style.bigPicture(bitmap)
        val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val intent = Intent(applicationContext, LoginOrRegAcivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val jsonObject = JsonObject()
        intent.putExtra(key_data,jsonObject.addProperty(key_booked_id,NotificationData.booked_id).toString())
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "101"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") val notificationChannel =
                NotificationChannel(
                    channelId,
                    "Notification",
                    NotificationManager.IMPORTANCE_MAX
                )
            //Configure Notification Channel
            notificationChannel.description = "Game Notifications"
            notificationChannel.enableLights(true)
            notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        @Suppress("DEPRECATION") val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(NotificationData.title)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentText(NotificationData.content)
                .setContentIntent(pendingIntent)
//                .setStyle(style) //Uncomment for showing the image
                .setLargeIcon(bitmap)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX)
        notificationManager.notify(1, notificationBuilder.build())
    }

    private fun getImage(remoteMessage: RemoteMessage) {
        val data = remoteMessage.data
        NotificationData.title = data["title"]
        NotificationData.content = data["body"]
        NotificationData.booked_id = data["booked_id"]
//        NotificationData.imageUrl = data[key_announcement_image]
        //Create thread to fetch image from notification
        remoteMessage.data
        val uiHandler = Handler(Looper.getMainLooper())
        uiHandler.post {
            val icon = BitmapFactory.decodeResource(
                resources,
                R.drawable.ic_ed2e_logo
            )
            sendNotification(icon)
            RxBus.publish(RxEvent.EventMainActivity(tag_reload_tracking_data, this))
        }
    }
}
