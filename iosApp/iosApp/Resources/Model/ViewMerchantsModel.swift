//
//  ViewMerchantsModel.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/22/20.
//

import Foundation
import UIKit

struct ViewMerchants {
    
    var id : Int?
    var image : String?
    var storeName : String?
    var storeType : String?
    var storeLocation :  String
    
}
