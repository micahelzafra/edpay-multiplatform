//
//  CreateMobilePIN.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/16/20.
//

import UIKit
import CBPinEntryView

class CreateMobilePIN: BaseViewController {
    
    var segueData = Dictionary<String, AnyObject>()

    @IBOutlet weak var createMobilePINView: CBPinEntryView! {
    didSet {
                createMobilePINView.delegate = self as CBPinEntryViewDelegate
            }
    }
    
    @IBOutlet weak var btnNext: DesignableButton!
    
    var timer = Timer()
    var timer_count = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func didTapButtonNext(_ sender: Any) {
        initNextPage()
    }
    
    func initNextPage()
    {
        let pin = createMobilePINView.getPinAsString()
        if pin.count < 6
        {
            self.handleError(displaytitle: "dialog_mpin_incomplete_title".localized(), "dialog_mpin_incomplete_message".localized())
            return
        }
        
        self.showLoading()
        self.runTimer()
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if timer_count < 1 {
            timer.invalidate()
            self.hideLoading()
            var segueData = Dictionary<String, AnyObject>()
            segueData[self.constant.key_pin] = createMobilePINView.getPinAsString() as AnyObject
            segueData[self.constant.key_cust_id] = (self.segueData[self.constant.key_cust_id] as! Int) as AnyObject
            self.performSegue(withIdentifier: "segueConfirmMobilePIN", sender: segueData)
        } else {
            timer_count -= 1
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueConfirmMobilePIN") {
            if let chidVC = segue.destination as? ConfirmMobilePINVC {
                self.log("chidVC \(chidVC)")
                chidVC.segueData = (sender as? [String:AnyObject])!
            }
        }
    }
}

extension CreateMobilePIN: CBPinEntryViewDelegate {
    func entryCompleted(with entry: String?) {
        self.log("pinView \(createMobilePINView.getPinAsString())")
        self.initNextPage()
    }

    func entryChanged(_ completed: Bool) {
        if completed {
        }
    }
}
