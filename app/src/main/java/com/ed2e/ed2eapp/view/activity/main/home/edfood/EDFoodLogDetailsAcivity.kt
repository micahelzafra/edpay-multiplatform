package com.ed2e.ed2eapp.view.activity.main.home.edfood

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.report.ReportSelectionActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edfood_log_details.*
import kotlinx.android.synthetic.main.dialog_rating_review.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class EDFoodLogDetailsAcivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    private var arraylist = ArrayList<Any>()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var isRiderReviewed = false
    private var riderRating = 0.0
    private var isMerchantReviewed = false
    private var merchantRating = 0.0
    private var bookedId = ""

    private var orderDetails = JsonObject()
    private var restaurantDetails = JsonObject()
    private var riderDispatchDetails = JsonObject()

    private var currentOrderStatus = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_log_details)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)

        bookedId = jsonObj[key_booked_id].asString
        getOrderDetails(bookedId)




        adapter = object : BaseRecyclerViewAdapter<Any>(arraylist, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {
            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.child_edfood_log_details_item
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }
        edfood_log_details_recyclerview_orders.layoutManager= LinearLayoutManager(context)
        edfood_log_details_recyclerview_orders.setHasFixedSize(true)
        edfood_log_details_recyclerview_orders.adapter=adapter
        edfood_log_details_button_rate_merchant.setOnClickListener { showRatingDialog("RESTAURANT") }
        edfood_log_details_button_rate_rider.setOnClickListener { showRatingDialog("RIDER") }

        edfood_log_details_button_report_issue.setOnClickListener {
            val intent = Intent(
                context,
                ReportSelectionActivity::class.java
            )
            intent.putExtra(key_order_data, orderDetails.toString())
            intent.putExtra(key_rider_data, riderDispatchDetails.toString())
            intent.putExtra(key_merchant_data, restaurantDetails.toString())
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    private fun getOrderDetails(
        booked_id: String
    ) {
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlOrderHistoryDetails,
                    "booked_id:" + booked_id,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        setData(jsonObj)

                        Log.d("Logs", "Order status: " + currentOrderStatus)
                        //|| currentOrderStatus.equals("10") || currentOrderStatus.equals("Cancelled")
                        if (currentOrderStatus.equals("6") || currentOrderStatus.equals("Delivered"))
                        {
                            getRiderReview(preference.getString(key_user_id), booked_id)
                        } else {
                            hideProgress()
                        }
                        //getRiderReview(preference.getString(key_user_id), booked_id)
                    } else {
                        val error_code = jsonObj[key_code].asString
                        //showDialogError(request_error_dialog, error_code)
                        showDialog_APIError("", "", true, error_code, "", "OK")
                        hideProgress()
                    }
                }

            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            hideProgress()
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )
        }
    }

    private fun setData(jsonObj:JsonObject)
    {
        val parser = JsonParser()
        val jsonArray = jsonObj[key_booked].asJsonArray
        var data = JsonObject()
        orderDetails = jsonObj

        for (json in jsonArray)
        {
            data = parser.parse(json.toString()).asJsonObject
        }
        val foodTotal = data[key_food_total].asJsonObject
        restaurantDetails = data[key_restaurant_details].asJsonObject
        riderDispatchDetails =  try
        {
            data[key_rider_dispatch_details].asJsonObject
        } catch (jinx:Exception) {
            JsonObject()
        }

        Log.d("LOGS", "Data: " + data)
        Log.d("LOGS", "0restaurantDetails: " + restaurantDetails)
        Log.d("LOGS", "0riderDispatchDetails: " + riderDispatchDetails)

        val orderStatus = data[key_booked_status].asString
        currentOrderStatus = orderStatus
        val date = data[key_booked_date].asString

//        val serviceFeePercentage = "${getString(R.string.label_service_fee,foodTotal[key_vat].asString)}%"

        val orderNo = "$label_logs_order_no ${data[key_booked_id].asString}"
        val total = commaSeparated(foodTotal[key_total].asString.replace(",","")).asString()
        val statusDate = "${getOrderStatus(orderStatus).asString()} ${getLocalTimeStamp(date).asString()}"
        val riderNote = try {
            data[key_special_notes].asString
        } catch (jinx: Exception) {
            "Rider notes"
        }
        //Header
        edfood_log_details_textview_transaction_id.text = orderNo
        edfood_log_details_textview_amount.text = total
        edfood_log_details_textview_status_date.text = statusDate
        edfood_log_details_textview_payment_type.text = data[key_payment_type].asString
        //Address
        edfood_log_details_textview_merchant_address.text = restaurantDetails[key_restaurant_name].asString
        edfood_log_details_textview_delivery_address.text = data[key_booked_location].asString
        edfood_log_details_textview_rider_note.text = riderNote
        //Receipt
        edfood_log_details_textview_subtotal.text = commaSeparated(foodTotal[key_sub_total].asString.replace(",","")).asString()
        edfood_log_details_textview_container_fee.text = commaSeparated(foodTotal[key_container_fee].asString.replace(",","")).asString()
        edfood_log_details_textview_delivery_fee.text = commaSeparated(foodTotal[key_delivery_fee].asString.replace(",","")).asString()
        edfood_log_details_textview_vat_percentage.text = getString(R.string.label_vat,foodTotal[key_vat].asString)
        edfood_log_details_textview_vat.text = commaSeparated(foodTotal[key_vat_amount].asString.replace(",","")).asString()
        edfood_log_details_textview_service_fee.text = commaSeparated(foodTotal[key_service_fee].asString.replace(",","")).asString()
        edfood_log_details_textview_service_fee_percentage.text = getString(R.string.label_service_fee).replace(" %1\$s","")

        if(foodTotal[key_discount].asString.replace(",","").toDouble() == 0.00) {
            edfood_log_details_textview_voucher.text = commaSeparated(foodTotal[key_discount].asString.replace(",","")).asString()
        } else {
            edfood_log_details_textview_voucher.text = "-" + commaSeparated(foodTotal[key_discount].asString.replace(",","")).asString()
        }

        edfood_log_details_textview_total.text = commaSeparated(foodTotal[key_total].asString.replace(",","")).asString()

        arraylist.clear()
        val ordersArray = data[key_orders].asJsonArray
        for (order in ordersArray)
        {
            arraylist.add(order)
        }

        Timber.d("riderDispatchDetails: %s", riderDispatchDetails)
        if (riderDispatchDetails.toString().equals("{}"))
        {
            edfood_log_details_textview_no_rider.visibility = VISIBLE
            edfood_log_details_layout_rider_info_child.visibility = GONE
            edfood_log_details_textview_rider_info_child_title.visibility = GONE
        }
        else
        {
            edfood_log_details_textview_no_rider.visibility = GONE
            edfood_log_details_layout_rider_info_child.visibility = VISIBLE
            edfood_log_details_textview_rider_info_child_title.visibility = VISIBLE

            val riderInfo = riderDispatchDetails[key_rider_info].asJsonObject
            val name = "${riderInfo[key_first_name].asString} ${riderInfo[key_last_name].asString}"
            val orderId = "$label_logs_order_id $orderNo"
            edfood_log_details_textview_no_rider.visibility = GONE
            edfood_log_details_layout_rider_info_child.visibility = VISIBLE
            edfood_log_details_textview_rider_info_child_title.visibility = VISIBLE
            edfood_log_details_textview_plate_no.text = riderInfo[key_plate_no].asString
            edfood_log_details_textview_plate_rider_name.text = name
        }
        updateList()


    }

    private fun getRiderReview(
        customer_id: String,
        booked_order_id: String
    ) {
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCheckRiderReview,
            "customer_id:" + customer_id + "||" +
                    "booked_order_id:" + booked_order_id,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    Log.d("Logs", "Rating: Rider | " + response)

                    if (status == status_success) {
                        val data = jsonObj[key_data].asJsonObject
                        val rating = data[key_rating].asDouble
                        isRiderReviewed = true
                        riderRating = rating
                    } else {
                        val error_code = jsonObj[key_code].asString
                        if (!error_code.equals("ED89"))
                        {
                            showDialog_APIError("", "", true, error_code, "", "OK")
                        }
                        isRiderReviewed = false
                        riderRating = 0.00
                    }



                    if (isRiderReviewed)
                    {
                        edfood_log_details_button_rate_rider.visibility = GONE
                        edfood_log_details_ratingBar_rider.visibility = VISIBLE
                        edfood_log_details_ratingBar_rider.rating = riderRating.toFloat()
                    }
                    else
                    {
                        edfood_log_details_button_rate_rider.visibility = VISIBLE
                        edfood_log_details_ratingBar_rider.visibility = GONE
                    }



                    //hideProgress()
                    getMerchantReview(customer_id,booked_order_id)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            isRiderReviewed = false
            riderRating = 0.00

            //hideProgress()
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )

            getMerchantReview(customer_id,booked_order_id)

        }
    }

    private fun getMerchantReview(
        customer_id: String,
        booked_order_id: String
    ) {
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCheckMerchantReview,
            "customer_id:" + customer_id + "||" +
                    "booked_order_id:" + booked_order_id,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    Log.d("Logs", "Rating: Merchant | " + response)

                    if (status == status_success) {
                        val data = jsonObj[key_data].asJsonObject
                        val rating = data[key_rating].asDouble
                        isMerchantReviewed = true
                        merchantRating = rating
                    } else {
                        val error_code = jsonObj[key_code].asString
                        if (!error_code.equals("ED89"))
                        {
                            showDialog_APIError("", "", true, error_code, "", "OK")
                        }
                        isMerchantReviewed = false
                        merchantRating = 0.00
                    }



                    if (isMerchantReviewed)
                    {
                        edfood_log_details_button_rate_merchant.visibility = GONE
                        edfood_log_details_ratingBar_merchant.visibility = VISIBLE
                        edfood_log_details_ratingBar_merchant.rating = merchantRating.toFloat()
                    }
                    else
                    {
                        edfood_log_details_button_rate_merchant.visibility = VISIBLE
                        edfood_log_details_ratingBar_merchant.visibility = GONE
                    }



                    hideProgress()
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            isMerchantReviewed = false
            merchantRating = 0.00

            hideProgress()
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )
        }
    }

    private fun createReview(
        customer_id: String,
        booked_order_id: String,
        rider_id: String,
        restaurant_id: String,
        review_type: String, //RIDER or RESTAURANT
        rating: String,
        review: String
    ) {
        showProgress()
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCreateReview,
            "customer_id:" + customer_id + "||" +
                    "booked_order_id:" + booked_order_id + "||" +
                    "rider_id:" + rider_id + "||" +
                    "restaurant_id:" + restaurant_id + "||" +
                    "review_type:" + review_type + "||" +
                    "rating:" + rating + "||" +
                    "review:" + review,
            { response: String? ->
                launch(Dispatchers.Main) {
                    Log.d("", "RESPONSE: $response")
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {

                        var reviewedName = "";
                        if(rider_id == "" || rider_id == null) {
                            reviewedName = "Merchant"
                        } else {
                            reviewedName = "Rider"
                        }

                        showDialog_main(
                            "",
                            "tag_request_review_button_ok",
                            false,
                            "You rated your " + reviewedName,
                            "",
                            "",
                            "OK"
                        )
                    } else {
                        val error_code = jsonObj[key_code].asString
                        //showDialogError(request_error_dialog, error_code)
                        showDialog_APIError("", "", true, error_code, "", "OK")

                    }
                    hideProgress()
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            hideProgress()
            //showDialogError(request_error_dialog, failure!!.localizedMessage)
            showDialog_main(
                "",
                "",
                true,
                resources.getString(R.string.error_something_went_wrong),
                failure!!.localizedMessage,
                "",
                "OK"
            )

        }
    }

    private fun updateList()
    {
        this@EDFoodLogDetailsAcivity.runOnUiThread {
            val parser = JsonParser()
//            arraylist.sortWith(Comparator { jinks, jinx ->
//                parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
//            })
//            Timber.d("updateList %s", arraylist)
            adapter.notifyDataSetChanged()
        }
    }

    fun showRatingDialog(type:String)
    {
        var riderId = ""
        val resId:String?
        val displayName:String?


        Log.d("Logs" , "Rider: "  + riderDispatchDetails.toString())
        if (riderDispatchDetails.toString().equals("{}") || riderDispatchDetails.toString().equals("") || riderDispatchDetails.toString() == null)
        {

        } else {
            var rider_info = riderDispatchDetails["rider_info"].asJsonObject
            riderId = rider_info[key_id].asString
        }


        if (type.equals("RIDER"))
        {
            riderId = riderId
            resId = restaurantDetails[key_id].asString
            displayName = "Rider"
        }
        else
        {
            riderId = riderId
            resId = restaurantDetails[key_id].asString
            displayName = "Restaurant"
        }

        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_rating_review, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(dialogView)
        val  mAlertDialog = mBuilder.show()
//        mAlertDialog.setCancelable(false)
        dialogView.dialog_rating_review_textview_title.text = getString(R.string.label_title_rate_your,displayName)
        dialogView.dialog_rating_review_button_submit.setOnClickListener {
            mAlertDialog.dismiss()
            createReview(
                preference.getString(key_user_id),
                bookedId,
                riderId,
                resId.toString(),
                type,
                dialogView.dialog_rating_review_ratingbar.rating.toInt().toString(),
                dialogView.dialog_rating_review_edittext.text.toString()
            )
        }
    }



    private fun initToolBar() {
        edfood_log_details_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edfood_log_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edfood_log_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)

        when (requestTag) {
            "tag_request_review_button_ok" -> {
                getOrderDetails(bookedId)
            }
            else -> {
            }
        }

    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}