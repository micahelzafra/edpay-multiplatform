package com.ed2e.ed2eapp.view

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter
import com.ed2e.ed2eapp.adapter.ViewPagerAdapter
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayInputAmountActivity
import com.ed2e.ed2eapp.view.fragment.main.home.edpay.ScanMerchantFragment
import com.ed2e.ed2eapp.view.fragment.main.home.edpay.ViewMerchantFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edpay.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.util.*
import kotlin.coroutines.CoroutineContext


class EDPayActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var intentdisposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    private var soundPool: SoundPool? = null
    private var soundID = 0

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(ScanMerchantFragment(), getString(R.string.edpay_nav_scan))
        adapter.addFragment(ViewMerchantFragment(), getString(R.string.edpay_nav_view))

        edpay_view_pager.adapter = adapter
        edpay_tablayout_view.setupWithViewPager(edpay_view_pager)


        edpay_view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(edpay_tablayout_view))

        edpay_tablayout_view!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
//                edpay_view_pager!!.currentItem = tab.position
                Log.d("Logs", "Tab Position: " + tab.position)
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })


        // Iterate over all tabs and set the custom view
//        for (i in 0 until edpay_tablayout_view.getTabCount()) {
//            val tab: TabLayout.Tab = edpay_tablayout_view.getTabAt(i)!!
//            tab.setCustomView(edpay_tablayout_view.getTabView(i))
//        }

        initListener()
        initToolBar()
        initGUI()
    }

    private fun initGUI() {
        // Set the hardware buttons to control the music
        this.volumeControlStream = AudioManager.STREAM_MUSIC
        // Load the sound
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        soundPool!!.setOnLoadCompleteListener(object : SoundPool.OnLoadCompleteListener {
            override fun onLoadComplete(
                soundPool: SoundPool?, sampleId: Int,
                status: Int
            ) {

                //loaded = true
                //Timber.d("EDPayActivity %s %s %s")

                //isShaked = true
                //initCashBack(preference.getString(key_user_id),amount!!,paymentType(payment_type!!).asString())

//                Glide.with(this@EDPayShakeReward)
//                    .load(R.drawable.ic_gif_3)
//                    .into(edpay_pin_imageview_merchant_logo)
            }
        })
        soundID = soundPool!!.load(this, R.raw.qr_scanned, 1)


        edpay_view_pager
    }

    private fun initListener() {


        disposable = RxBus.listen(RxEvent.EventScanQR::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            scanQR(it.qrCode)
        }

        intentdisposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        when (tag) {
            tag_load_api_edpay_merchant -> {
                val fragment: ViewMerchantFragment = value as ViewMerchantFragment
                getMerchant(fragment,getCurrentLocationData(tag_current_location_country_code, this@EDPayActivity).toString())
            }
        }
    }

    private fun scanQR(barcode:String) {

        Log.d("Logs", "Scanned Data: " + barcode)
        playScanSound()
        //showProgress()

        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlOfflineMerchant,
                    "merchant_code:"+ barcode +"||"+
                    "country_code:" + getCurrentLocationData(tag_current_location_country_code,this@EDPayActivity).toString() ,
            success = {
                launch(Dispatchers.Main) {

                    Timber.d("scanQR success %s", it)

                    val parser = JsonParser()
                    val jsonObj: JsonObject = parser.parse(it).getAsJsonObject()
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        Timber.d("status %s", status)
//                        val data = jsonObj[key_data].asJsonObject



                        val data = jsonObj[key_data].asJsonArray
                        Log.d("Logs", "Scanned Data: " + data)

                        //val data2 = data[0].asJsonObject



                        var isValid = false

                        for (i in 0 until data.size()) {

                            val jsonObj = data[i].asJsonObject
                            val merchant_code = jsonObj["merchant_code"].asString

                            Log.d("Logs", "Looped Data: " + data[i].asJsonObject)

                            if (merchant_code.toString() == barcode) {
                                val intent = Intent(this@EDPayActivity, EDPayInputAmountActivity::class.java)
                                intent.putExtra(key_data, jsonObj.toString())
                                startActivity(intent)
                                finish()
                                isValid = true
                                break
                            } else {
                                isValid = false
                                continue
                            }
                        }


                        if(!isValid) {
                            showDialog_APIError("", "", true, "ED96", "", "OK")
                        }


//
                    } else {
                        val error_code = jsonObj[key_code].asString

                        showDialog_APIError("", "", true, error_code, "", "OK")

                        //showDialogError(request_error_dialog, error_code)
                    }
                }
            },
            failure = ::handleError
        )
    }

    private fun getMerchant(
        fragment:ViewMerchantFragment,
        country_code: String
    ) {
        fragment.textView_country.text = "Showing Merchants in " + getCurrentLocationData(tag_current_location_country_name,this@EDPayActivity)
        val params = "country_code:" + country_code
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlOfflineMerchant,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject
                val status = jsonObj[key_status].asString
                if (status == status_success) {


                    Log.d("Logs", "Data Merchant List: " + jsonObj)

                    val jsonArray = jsonObj[key_data].asJsonArray
                    val items = ArrayList<Any>()
                    for (data in jsonArray)
                    {
                        items.add(data)
                    }
                    fragment.displayList(items)
                } else {
                    val error_code = jsonObj[key_code].asString
                    showDialogError(request_error_dialog, error_code)
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }




    private fun playScanSound() {
        Timber.d("playScanSound")
        val audioManager =
            getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = audioManager
            .getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = audioManager
            .getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val volume = actualVolume / maxVolume


        soundPool!!.play(soundID, volume, volume, 1, 0, 1f)


//        if (loaded) {
//            soundPool!!.play(soundID, volume, volume, 1, 0, 1f)
//            //isShaked = true
//        }
    }


    private fun initToolBar() {
        edpay_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edpay_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edpay_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
    }


    private fun handleError(ex: Throwable?) {
        Timber.e(ex)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
        if (!intentdisposable.isDisposed) intentdisposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}