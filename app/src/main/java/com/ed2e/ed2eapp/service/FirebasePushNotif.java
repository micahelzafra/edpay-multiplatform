package com.ed2e.ed2eapp.service;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class FirebasePushNotif {

    public static final String FIREBASE_SERVER_KEY              = "AAAAdGlNK2g:APA91bFsHxsABmI7WffnwpqFzQ3QXkhZrd_DHs6zQ5gxUiCXJ7SvB3F9iK7Csh0XUQ_IU4Pyp2S9WHtsWloEQTdNxug1ulG7NWcCx32x32qsbemcH5HwUzOPHrG-fa6nOGoo7jG9VyxB";

    public static final String DISTRIBUTOR                      = "distributor";
    public static final String MERCHANT                         = "merchant";
    public static final String FIREBASE_KEY_TOKEN                = "token";
    public static final String FIREBASE_KEY_APP_VER              = "app_ver";
    public static final String FIREBASE_KEY_ID                   = "id";
    public static final String FIREBASE_KEY_PICTURE              = "picture";
    public static final String FIREBASE_KEY_URL                  = "url";
    public static final String FIREBASE_KEY_MESSAGE              = "message";
    public static final String FIREBASE_KEY_NOTIFTYPE            = "notif_type";
    public static final String FIREBASE_KEY_CUSTID               = "custid";
    public static final String FIREBASE_KEY_TITLE                = "title";
    public static final String FIREBASE_KEY_ICON                 = "icon";
    public static final String FIREBASE_KEY_TICKETING            = "ticketing";
    public static final String FIREBASE_KEY_CATEGORY             = "category";
    public static final String TAG = FirebasePushNotif.class.getSimpleName();

    public static void pushNotificationMerchant(String custId, ArrayList<String> merchantTokens, String message,String imagepath, String iconpath, String notifUrl, String notifType) {
        JSONObject jPayload = new JSONObject();
        JSONObject jData = new JSONObject();
        JSONObject jNotif = new JSONObject();
        try {

//            jNotif.put("body", message);
//            jNotif.put("title", "Payment Successful");
            jNotif.put(FIREBASE_KEY_CATEGORY, "edpointsNotifCategory");

            jData.put(FIREBASE_KEY_ID, MERCHANT);
            jData.put(FIREBASE_KEY_PICTURE, imagepath);
            jData.put(FIREBASE_KEY_URL, notifUrl);
            jData.put(FIREBASE_KEY_MESSAGE, message);
            jData.put(FIREBASE_KEY_NOTIFTYPE, notifType);
            jData.put(FIREBASE_KEY_CUSTID, custId);
            jData.put(FIREBASE_KEY_ICON, iconpath);

            JSONArray ja = new JSONArray();
            for (String token: merchantTokens) {
                Log.d(TAG,"merchantTokens:"+token);
                ja.put(token); //merchant token
            }
            jPayload.put("registration_ids", ja);
            jPayload.put("priority", "high");
            jPayload.put("data", jData);
            //for ios to accept the notifications
            jPayload.put("content_available", true);
            jPayload.put("notification", jNotif);

            /* This is static don't change if you don't want to die */
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key="+FIREBASE_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(() -> {
//                    mTextView.setText(resp);
                Log.d(TAG,"firebase resp:"+resp);
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void pushNotificationDistributor(String custId, ArrayList<String> distributor, String message,String imagepath, String iconpath, String notifUrl, String notifType) {
        JSONObject jPayload = new JSONObject();
        JSONObject jData = new JSONObject();
        JSONObject jNotif = new JSONObject();
        try {

//            jNotif.put("body", message);
//            jNotif.put("title", "Payment Successful");
            jNotif.put(FIREBASE_KEY_CATEGORY, "edpointsNotifCategory");

            jData.put(FIREBASE_KEY_ID, DISTRIBUTOR);
            jData.put(FIREBASE_KEY_PICTURE, imagepath);
            jData.put(FIREBASE_KEY_URL, notifUrl);
            jData.put(FIREBASE_KEY_MESSAGE, message);
            jData.put(FIREBASE_KEY_NOTIFTYPE, notifType);
            jData.put(FIREBASE_KEY_CUSTID, custId);
            jData.put(FIREBASE_KEY_ICON, iconpath);

            JSONArray ja = new JSONArray();
            for (String token: distributor) {
                Log.d(TAG,"merchantTokens:"+token);
                ja.put(token); //merchant token
            }
            jPayload.put("registration_ids", ja);
            jPayload.put("priority", "high");
            jPayload.put("data", jData);
            //for ios to accept the notification
            jPayload.put("content_available", true);
            jPayload.put("notification", jNotif);

            /* This is static don't change if you don't want to die */
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key="+FIREBASE_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(() -> {
//                    mTextView.setText(resp);
                Log.d(TAG,"firebase resp:"+resp);
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
