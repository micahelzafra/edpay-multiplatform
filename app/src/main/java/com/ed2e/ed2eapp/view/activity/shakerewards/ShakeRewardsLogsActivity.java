package com.ed2e.ed2eapp.view.activity.shakerewards;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.util.get30DaysBeforeDate;
import com.ed2e.ed2eapp.util.dateFormat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.adapter.AnnouncementListAdapter;
import com.ed2e.ed2eapp.adapter.RewardsLogAdapter;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.Cashback;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.mpin.CreateMPinActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.date_format_picker_display;
import static com.ed2e.ed2eapp.util.ConstantKt.date_format_server_api;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.tag_current_location_country_code;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;
import static com.ed2e.ed2eapp.util.ConstantKt.urlRewardsHistory;

public class ShakeRewardsLogsActivity extends BaseActivity {

    public static final String TAG = ShakeRewardsLogsActivity.class.getSimpleName();

    @BindView(R.id.activity_edpay_shake_reward_logs_button_date_from)
    Button button_date_from;
    @BindView(R.id.activity_edpay_shake_reward_logs_button_date_to)
    Button button_date_to;
    @BindView(R.id.activity_edpay_shake_reward_logs_textview_reward_amount)
    TextView textview_reward_amount;

    @BindView(R.id.activity_edpay_shake_reward_logs_recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.activity_edpay_shake_reward_logs_textview_message)
    TextView textview_message;



    @BindView(R.id.edpay_shake_reward_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edpay_shake_reward_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;



//    @BindView(R.id.otp_pinview)
//    PinView pinview;
//    @BindView(R.id.otp_textView_timer)
//    TextView textView_timer;
//    @BindView(R.id.otp_button_submit)
//    Button button_submit;

    private String mobileNumber = "";
    private String customerID = "";
    private String prevPage = "";

    private List<Cashback> modelList;
    static RecyclerView recyclerView;
    RewardsLogAdapter recyclerAdapter;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edpay_shake_reward_logs);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

//        Bundle extras = getIntent().getExtras();
//        if (extras == null) {
//            mobileNumber = "";
//            customerID = "";
//            prevPage = "";
//        } else {
//            mobileNumber = extras.getString(key_mobile_number);
//            customerID = extras.getString(key_cust_id);
//            prevPage = extras.getString(key_prev_page);
//        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        //-- initialize recycler view, layout, and adapter
        recyclerView = findViewById(R.id.activity_edpay_shake_reward_logs_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerAdapter = new RewardsLogAdapter(ShakeRewardsLogsActivity.this, modelList);
        recyclerView.setAdapter(recyclerAdapter);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(date_format_picker_display, Locale.US);
        String formattedDate = df.format(c);


        button_date_from.setText(new get30DaysBeforeDate().asString());
        button_date_to.setText(formattedDate);

        button_date_from.setOnClickListener(v -> {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dfFrom = new SimpleDateFormat(date_format_picker_display, Locale.US);

            Date date = new Date();
            try {
                date = dfFrom.parse(button_date_from.getText().toString().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal.setTime(date);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
                //showToast_short(button_date_from.getText().toString());
                initAPIGetRewardsHistory(
                        preference.getString(key_user_id),
                        "NG",
                        setDateToButton(cal, year, month, dayOfMonth, button_date_from),
                        button_date_to.getText().toString().trim());
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

            Date dateTo = new Date();
            try {
                dateTo = dfFrom.parse(button_date_to.getText().toString().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal.setTime(dateTo);

            datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
            datePickerDialog.show();

        });

        button_date_to.setOnClickListener(v -> {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dfFrom = new SimpleDateFormat(date_format_picker_display, Locale.US);

            Date date = new Date();
            try {
                date = dfFrom.parse(button_date_to.getText().toString().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal.setTime(date);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
                //showToast_short(button_date_to.getText().toString());
                initAPIGetRewardsHistory(
                        preference.getString(key_user_id),
                        "NG",
                        button_date_from.getText().toString().trim(),
                        setDateToButton(cal, year, month, dayOfMonth, button_date_to));
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

            Date dateFrom = new Date();
            try {
                dateFrom = dfFrom.parse(button_date_from.getText().toString().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal.setTime(dateFrom);

            datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
            datePickerDialog.show();

        });


        //initAPIGetRewardsHistory();

        initAPIGetRewardsHistory(
                preference.getString(key_user_id),
                getCurrentLocationData(tag_current_location_country_code, ShakeRewardsLogsActivity.this),
                button_date_from.getText().toString().trim(),
                button_date_to.getText().toString().trim());

        //updateButton(false);

        //mobileNumber = "";

//        textView_timer.setOnClickListener(v -> {
//            if(timerCount <= 0) {
//                timerCount = 60;
//                updateButton(false);
//            } else {
//                return;
//            }
//        });
//
//        button_submit.setOnClickListener(v -> {
//            if(pinview.getText().toString().trim().length() < 6) {
//                showDialog_main("", "", true,getResources().getString(R.string.dialog_missing_field_title), getResources().getString(R.string.dialog_missing_field_message), "", "OK");
//            } else {
//                initAPICheckOTP(mobileNumber, pinview.getText().toString().trim());
//            }
//
//            //showDialog_main("", "button_ok", true,"Registration Successful", "You may use your registered mobile number to login and enjoy EDPAY App features.", "", "OK");
//        });
//
//        if(mobileNumber.equalsIgnoreCase("") || mobileNumber == null) {
//            showDialog_APIError("", "tag_button_ok", true, "ED72", "", "OK");
//        } else {
//            initAPISendOTP(mobileNumber);
//            //startTimer();
//        }
    }

    private void initAPIGetRewardsHistory(String custID, String countryCode, String dateFrom, String dateTo) {
        showProgress();

        String formatted_date_from = new dateFormat(dateFrom, date_format_picker_display, date_format_server_api).asString();
        String formatted_date_to = new dateFormat(dateTo, date_format_picker_display, date_format_server_api).asString();

        String  params = "customer_id:" + custID + "||" +
                "country_code:" + countryCode + "||" +
                "date_from:" + formatted_date_from + "||" +
                "date_to:" + formatted_date_to;

        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlRewardsHistory,params,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | ");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    modelList = new ArrayList<>();
                    modelList.clear();

                    if (status.equals(status_success))
                    {
                        runOnUiThread(() -> {
                            textview_message.setVisibility(View.GONE);
                        });

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray cashback_data = obj.getJSONArray("data");

                            //showToast_short("SUCCESS pO: " + announcement_data.length());

                            runOnUiThread(() -> {
                                if (cashback_data.length() == 0) {
                                    textview_message.setVisibility(View.VISIBLE);
                                } else {
                                    textview_message.setVisibility(View.GONE);
                                }
                            });

                            double cashbackTotal = 0.00;

                            for(int i = 0; i < cashback_data.length(); i++)
                            {

                                JSONObject object = cashback_data.getJSONObject(i);

                                String transaction_id;
                                try {
                                    transaction_id = "" + (String) object.get("transaction_id");
                                } catch (Exception e) {
                                    transaction_id = "" + object.getInt("transaction_id");
                                }

                                String transaction_no = (String) object.get("transaction_no");

                                String payment_type;
                                try {
                                    payment_type = "" + (String) object.get("payment_type");
                                } catch (Exception e) {
                                    payment_type = "" + object.getInt("payment_type");
                                }

                                String created_date = (String) object.get("created_date");
                                String cashback = (String) object.get("cashback").toString();

                                if(cashback == null || cashback.isEmpty() || cashback.equalsIgnoreCase("") ||cashback.equalsIgnoreCase("null")) {

                                } else {
                                    try {
                                        JSONObject cashback_obj = new JSONObject(cashback);
                                        cashbackTotal = cashbackTotal + Double.parseDouble(cashback_obj.get("cashback").toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
//                                JSONObject cashback_obj = new JSONObject(cashback);
//                                cashbackTotal = cashbackTotal + Double.parseDouble(cashback_obj.get("cashback").toString());

                                //showToast_short("Cashback Details: " + cashback);

                                Cashback list = new Cashback(
                                        transaction_id,
                                        transaction_no,
                                        payment_type,
                                        created_date,
                                        cashback
                                );
                                modelList.add(list);
                            }

                            double finalCashbackTotal = cashbackTotal;

                            DecimalFormat formatter = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.US);/* new DecimalFormat("###,###.##");*/
                            formatter.applyPattern("###,##0.00");

                            runOnUiThread(() -> {
                                recyclerAdapter.setModelList(modelList);
                                textview_reward_amount.setText("" + formatter.format(finalCashbackTotal));
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "JSON (ERROR): " + e.getLocalizedMessage());
                            runOnUiThread(() -> {
                                textview_message.setVisibility(View.GONE);
                                textview_reward_amount.setText("0.00");
                            });
                            showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), e.getLocalizedMessage(), "","OK");
                        }

                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");

                        runOnUiThread(() -> {
                            if (error_code.equalsIgnoreCase("ED104")) {
                                textview_message.setVisibility(View.VISIBLE);
                            } else {
                                textview_message.setVisibility(View.GONE);
                            }

                            recyclerAdapter.setModelList(modelList);
                            textview_reward_amount.setText("0.00");
                        });

                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    runOnUiThread(() -> {
                        textview_message.setVisibility(View.GONE);
                        recyclerAdapter.setModelList(modelList);
                        textview_reward_amount.setText("0.00");
                    });
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getLocalizedMessage(), "","OK");
                    return null;
                });
    }

    private String setDateToButton(Calendar cal, int year, int monthOfYear, int dayOfMonth, Button button) {
        SimpleDateFormat sdf = new SimpleDateFormat(date_format_picker_display, Locale.US);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        button.setText(sdf.format(cal.getTime()));
        return button.getText().toString();
    }
//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }

    private void initGetCountryFlag() {
//        switch(getCurrentLocationData(tag_current_location_country_code)) {
//            case "PH":
//                Glide.with(MainActivity.this)
//                        .load(R.drawable.philippines)
//                        .apply(new RequestOptions().override(256, 256).centerCrop().placeholder(R.drawable.ic_flag_default))
//                        .into( MainScreen.HOME.fragment.fragment_home_imageview_flag);
//                break;
//            case "NG":
//                break;
//            case "MY":
//                break;
//            default:
//                break;
//        }
    }



    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "button_ok":
//                showToast_short("To Homescreen");
                intent = new Intent(getContext(), CreateMPinActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case "tag_register_success_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(ShakeRewardsLogsActivity.this, CreateMPinActivity.class);
                    intent.putExtra(key_cust_id, customerID);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(ShakeRewardsLogsActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    intent = new Intent(ShakeRewardsLogsActivity.this, Login1Activity.class);
                    intent.putExtra(key_mobile_number, mobileNumber.replaceAll("\\+", ""));
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }

                break;
            case "tag_otp_incomplete_button_ok":
                if(prevPage.equalsIgnoreCase("registration")) {
                    intent = new Intent(getContext(), LoginOrRegAcivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else  if(prevPage.equalsIgnoreCase("login")) {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    intent = new Intent(getContext(), Login1Activity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }

                break;


            default:
                break;
        }
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }


    @Override
    protected void onResume() {
        super.onResume();
        initAPIGetRewardsHistory(
                preference.getString(key_user_id),
                getCurrentLocationData(tag_current_location_country_code, ShakeRewardsLogsActivity.this),
                button_date_from.getText().toString().trim(),
                button_date_to.getText().toString().trim());
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initPreviousActivity() {
        finish();
    }


    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
