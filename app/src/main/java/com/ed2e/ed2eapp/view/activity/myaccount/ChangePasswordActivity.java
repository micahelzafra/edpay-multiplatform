package com.ed2e.ed2eapp.view.activity.myaccount;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlPINCreate;

public class ChangePasswordActivity extends BaseActivity {

    public static final String TAG = ChangePasswordActivity.class.getSimpleName();

    @BindView(R.id.change_password_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.change_password_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;


    @BindView(R.id.change_password_editText_current_password)
    EditText editText_current_password;
    @BindView(R.id.change_password_editText_new_password)
    EditText editText_new_password;
    @BindView(R.id.change_password_editText_retype_new_password)
    EditText editText_retype_new_password;

    @BindView(R.id.change_password_linearLayout_current_password_visibility)
    LinearLayout linearLayout_current_password_visibility;
    @BindView(R.id.change_password_linearLayout_new_password_visibility)
    LinearLayout linearLayout_new_password_visibility;
    @BindView(R.id.change_password_linearLayout_retype_retype_new_password_visibility)
    LinearLayout linearLayout_retype_retype_new_password_visibility;

    @BindView(R.id.change_password_textView_current_password_visibility)
    TextView textView_current_password_visibility;
    @BindView(R.id.change_password_textView_new_password_visibility)
    TextView textView_new_password_visibility;
    @BindView(R.id.change_password_textView_retype_new_password_visibility)
    TextView textView_retype_new_password_visibility;

    @BindView(R.id.change_password_imageView_req1)
    ImageView imageView_req1;
    @BindView(R.id.change_password_imageView_req2)
    ImageView imageView_req2;
    @BindView(R.id.change_password_imageView_req3)
    ImageView imageView_req3;

    @BindView(R.id.change_password_button_save)
    Button button_save;


    private String customerID = "";
    private Boolean isPasswordHidden1 = true;
    private Boolean isPasswordHidden2 = true;
    private Boolean isPasswordHidden3 = true;

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            customerID = "";
        } else {
            customerID = extras.getString(key_cust_id);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        linearLayout_current_password_visibility.setOnClickListener(v -> {
            if(isPasswordHidden1){
                isPasswordHidden1 = false;
                textView_current_password_visibility.setText("Hide");
                editText_current_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText_current_password.setSelection(editText_current_password.getText().length());
            } else {
                isPasswordHidden1 = true;
                textView_current_password_visibility.setText("Show");
                editText_current_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_current_password.setSelection(editText_current_password.getText().length());
            }

            setTextViewStyle();
        });

        linearLayout_new_password_visibility.setOnClickListener(v -> {
            if(isPasswordHidden2){
                isPasswordHidden2 = false;
                textView_new_password_visibility.setText("Hide");
                editText_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText_new_password.setSelection(editText_new_password.getText().length());
            } else {
                isPasswordHidden2 = true;
                textView_current_password_visibility.setText("Show");
                editText_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_new_password.setSelection(editText_new_password.getText().length());
            }

            setTextViewStyle();
        });

        linearLayout_retype_retype_new_password_visibility.setOnClickListener(v -> {
            if(isPasswordHidden3){
                isPasswordHidden3 = false;
                textView_retype_new_password_visibility.setText("Hide");
                editText_retype_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_NORMAL);
                editText_retype_new_password.setSelection(editText_retype_new_password.getText().length());
            } else {
                isPasswordHidden3 = true;
                textView_retype_new_password_visibility.setText("Show");
                editText_retype_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_retype_new_password.setSelection(editText_retype_new_password.getText().length());
            }

            setTextViewStyle();
        });




        editText_new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkPasswordRequirements(editText_new_password.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setTextViewStyle() {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Regular.ttf");
        editText_current_password.setTypeface(face);
        editText_current_password.setTextSize(18);
        editText_new_password.setTypeface(face);
        editText_new_password.setTextSize(18);
        editText_retype_new_password.setTypeface(face);
        editText_retype_new_password.setTextSize(18);
    }

    private void checkPasswordRequirements(String s) {
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN_LOWERCASE = "^[a-z]+$";
        final String PASSWORD_PATTERN_UPPERCASE = "^[A-Z]+$";
        final String PASSWORD_PATTERN_NUMBER = "^[0-9]+$";
        final String PASSWORD_PATTERN_SYMBOL = "^\\W+$";
        final String PASSWORD_PATTERN_UNDERSCORE = "_";

        char[] stringToCharArray = s.toCharArray();
        boolean hasValidLowerCase = false, hasValidUpperCase = false, hasValidNumber = false, hasValidSymbol = false, hasValidUnderscore = false;

        for (char output : stringToCharArray) {

            //lowercase checker
            pattern = Pattern.compile(PASSWORD_PATTERN_LOWERCASE);
            matcher = pattern.matcher(String.valueOf(output));

            if(matcher.matches()){
                hasValidLowerCase = true;
            }

            //uppercase checker
            pattern = Pattern.compile(PASSWORD_PATTERN_UPPERCASE);
            matcher = pattern.matcher(String.valueOf(output));

            if(matcher.matches()){
                hasValidUpperCase = true;
            }

            //number checker
            pattern = Pattern.compile(PASSWORD_PATTERN_NUMBER);
            matcher = pattern.matcher(String.valueOf(output));

            if(matcher.matches()){
                hasValidNumber = true;
            }

            //symbol checker
            pattern = Pattern.compile(PASSWORD_PATTERN_SYMBOL);
            matcher = pattern.matcher(String.valueOf(output));

            if(matcher.matches()){
                hasValidSymbol = true;
            }

            //underscore checker
            if(String.valueOf(output).equalsIgnoreCase(PASSWORD_PATTERN_UNDERSCORE)){
                hasValidUnderscore = true;
            }
        }

        if(!s.equalsIgnoreCase("")){
            imageView_req1.setImageResource(R.drawable.ic_circle_blue_check);
        } else {
            imageView_req1.setImageResource(R.drawable.ic_circle_grey);
        }

        if(s.length() >= 8 ){
            imageView_req2.setImageResource(R.drawable.ic_circle_blue_check);
        } else {
            imageView_req2.setImageResource(R.drawable.ic_circle_grey);
        }

        if(hasValidUpperCase){
            imageView_req3.setImageResource(R.drawable.ic_circle_blue_check);
        } else {
            imageView_req3.setImageResource(R.drawable.ic_circle_grey);
        }

//        if(hasValidLowerCase){
//            imageView_req6.setImageResource(R.drawable.ic_circle_blue_check);
//        } else {
//            imageView_req6.setImageResource(R.drawable.ic_circle_grey);
//        }
//
//        if(hasValidNumber){
//            imageView_req4.setImageResource(R.drawable.ic_circle_blue_check);
//        } else {
//            imageView_req4.setImageResource(R.drawable.ic_circle_grey);
//        }
//
//        if(hasValidSymbol||hasValidUnderscore){
//            imageView_req5.setImageResource(R.drawable.ic_circle_blue_check);
//        } else {
//            imageView_req5.setImageResource(R.drawable.ic_circle_grey);
//        }
    }

    private void initAPICreateMPin(String customer_id, String pin) {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlPINCreate,
                "customer_id:" + customer_id + "||" +
                        "pin:" + pin,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + customer_id + " | " + pin + " |");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        showDialog_main("", "tag_mpin_success_button_log_in", false, getResources().getString(R.string.dialog_mpin_success_title), getResources().getString(R.string.dialog_mpin_success_message), "", "LOG IN");
                    }
                    else
                    {
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }




//    @OnClick(R.id.login_or_reg_button_login) void submit() {
//
//        Intent intent=new Intent(RegistrationAcivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
//    }






    private void initPreviousActivity() {
        finish();
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "tag_mpin_success_button_log_in":
                intent = new Intent(getContext(), Login1Activity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
