//
//  ReportVC.swift
//  iosApp
//
//  Created by ED2E Technology iOS dev-zafyrajinx on 2/19/20.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class ReportVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()

    var segueData = Dictionary<String, AnyObject>()
    
    private var orderID = "";
    private var disputeType = "";
    private var reportedID = "";
    private var reportedName = "";
    private var reportedDesc = "";
    
    let downloader = ImageDownloader()
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tfReport: DesignableTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGUI()
    }

    
    func initGUI()
    {
        
    }

    @IBAction func didTapBackButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: false)
    }
    
    @IBAction func didTapSubmitButton(_ sender: DesignableButton) {
        if tfReport.text!.count <= 0
        {
            return
        }
        
    }
    
    private func initAPISubmitReport(
        customer_id: String,
        booked_order_id: String,
        dispute_type: String,
        rider_id: String,
        restaurant_id: String,
        dispute_title: String,
        dispute_description: String
    ){
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlDispute,
            params: "customer_id:\(customer_id)||booked_order_id:\(booked_order_id)||dispute_type:\(dispute_type)||rider_id:\(rider_id)||restaurant_id:\(restaurant_id)||dispute_title:\(dispute_title)||dispute_description:\(dispute_description)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    var actions = [UIAlertAction]()
                    let action = UIAlertAction(title: "Back", style: .default, handler: {
                        action in
                        self.dismiss(animated: false)
                    })
                    actions.append(action)
                    self.showDialog(title: "dialog_report_submitted_title".localized(), message: "dialog_report_submitted_message".localized(), actions: actions)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
}
