//
//  ConfirmMobilePINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/13/20.
//

import UIKit
import CBPinEntryView

class ConfirmNewMobilePINVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var confirmPinView: CBPinEntryView! {
    didSet {
                confirmPinView.delegate = self as? CBPinEntryViewDelegate
            }
    }
    
    @IBOutlet weak var btnSubmit: DesignableButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Reset MPIN successful", message: "Please remember your Mobile PIN Number and make sure not to share your PIN with anyone for your protection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Log in", style: .default, handler: {
            action in
            self.performSegue(withIdentifier: "segueLogin", sender: nil)
        }))
        
        
        self.present(alert, animated: true)
    }
    

}
