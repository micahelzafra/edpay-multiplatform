//
//  DesignableTextField.swift
//  EDPoints App
//
//  Created by ED2E Technology iOS on 19/12/2018.
//  Copyright © 2018 ED2E Technology. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    // Textfield border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    //Textfield border color
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var letfImage: UIImage? {
        
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var imageSize: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var underline: Bool = false {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var underlineColor: String = "D4AF37" {
        didSet {
            updateView()
        }
    }
    
    func updateView () {
        
        if let image = letfImage {
            leftViewMode = .always
            
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: imageSize, height: imageSize))
            imageView.image = image
            imageView.tintColor = tintColor
            imageView.contentMode = .scaleAspectFit
            
            var width = leftPadding + 20
            
            if (borderStyle == UITextField.BorderStyle.none) || (borderStyle == UITextField.BorderStyle.line) {
                width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: imageSize+20, height: imageSize ))
            view.addSubview(imageView)
            
            leftView = view
            
            
        } else {
            //image is nil
            leftViewMode = .always
            let view = UIView(frame: CGRect(x: leftPadding, y: 0, width: 10, height: 0 ))
            leftView = view
        }
        
        if let image = rightImage {
            rightViewMode = .always
            
            let imageView = UIImageView(frame: CGRect(x: rightPadding, y: 0, width: imageSize, height: imageSize))
            imageView.image = image
            imageView.tintColor = tintColor
            imageView.contentMode = .scaleAspectFit
            
            var width = rightPadding + 20
            
            if (borderStyle == UITextField.BorderStyle.none) || (borderStyle == UITextField.BorderStyle.line) {
                width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: imageSize+20, height: imageSize ))
            view.addSubview(imageView)
            
            rightView = view
            
            
        } else {
            //image is nil
            rightViewMode = .always
            let view = UIView(frame: CGRect(x: rightPadding, y: 0, width: 10, height: 0 ))
            rightView = view
        }
        
        if underline
        {
            let border = CALayer()
            let width = CGFloat(1.0)
            //border.borderColor = UIColor(named: self.underlineColor)!.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width*2, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
}

