package com.ed2e.ed2eapp.adapter

import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.model.MainScreen
import com.ed2e.ed2eapp.model.Profile
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.math.RoundingMode
import java.text.DecimalFormat


/**
 * Created by dev-michael-jinx on 11/01/2020.
 */


object ViewHolderFactory {

    fun create(view: View, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.child_profile -> ProfileViewHolder(view)
            R.layout.child_log_pay -> PayLogViewHolder(view)
            R.layout.child_log_food -> FoodLogViewHolder(view)
            R.layout.child_edpay_merchant -> EDPayMerchantViewHolder(view)
            R.layout.child_rewards_logs -> RewardsLogViewHolder(view)
            R.layout.child_edfood_merchant -> EDFoodMerchantViewHolder(view)
            R.layout.child_edfood_menu_item -> EDFoodMenuViewHolder(view)
            R.layout.child_edfood_menu_category -> EDFoodMenuCategoryViewHolder(view)
            R.layout.child_edfood_addon_category -> EDFoodAddonCategoryViewHolder(view)
            R.layout.child_edfood_addon_item -> EDFoodAddonItemViewHolder(view)
            R.layout.child_edfood_cart_item -> EDFoodCartItemViewHolder(view)
            R.layout.child_edfood_log_details_item -> EDFoodLogItemViewHolder(view)
            else -> {
                ProfileViewHolder(view)
            }
        }
    }

    class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Profile> {
        var textView: TextView
        var imageview: ImageView
        init {
            textView = itemView.findViewById(R.id.child_profile_textview_title)
            imageview = itemView.findViewById(R.id.child_profile_imageview_logo)
        }
        override fun bind(data: Profile) {
            textView.text = data.title
            data.image?.let {
                imageview.setImageResource(it)
            }
        }
    }

    class PayLogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_transaction_no: TextView
        var textView_date: TextView
        var textView_amount: TextView
        var buttonView: Button
        init {
            textView_name = itemView.findViewById(R.id.child_log_pay_textview_name)
            textView_transaction_no = itemView.findViewById(R.id.child_log_pay_textview_transaction_no)
            textView_date = itemView.findViewById(R.id.child_log_pay_textview_date)
            textView_amount = itemView.findViewById(R.id.child_log_pay_textview_amount)
            buttonView = itemView.findViewById(R.id.child_log_pay_button_view)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val transaction_no = jsonObj[key_transaction_no].asString
            val paid_amount = jsonObj[key_paid_amount].asString
            val date = jsonObj[key_created_date].asString
            val merchant_info =  jsonObj[key_offline_merchant].asJsonObject
            val name = merchant_info[key_trade_name].asString
            val transasction = "$label_transaction_number $transaction_no"

            textView_name.text = name
            textView_amount.text = paid_amount
            textView_date.text = getLocalTimeStamp(date).asString()
            textView_transaction_no.text = transasction
            buttonView.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_intent_edpay_history_detail, data)) }
        }
    }

    class FoodLogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_transaction_no: TextView
        var textView_date: TextView
        var textView_amount: TextView
        var button:Button
        init {
            textView_name = itemView.findViewById(R.id.child_log_food_textview_name)
            textView_transaction_no = itemView.findViewById(R.id.child_log_food_textview_transaction_no)
            textView_date = itemView.findViewById(R.id.child_log_food_textview_date)
            textView_amount = itemView.findViewById(R.id.child_log_food_textview_amount)
            button = itemView.findViewById(R.id.child_log_food_button_view)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val transaction_no = jsonObj[key_booked_id].asString
            val paid_amount = jsonObj[key_total].asString
            val date = jsonObj[key_booked_date].asString
            val booked_status = jsonObj[key_booked_status].asString
            val order_id = "$label_logs_order_id $transaction_no"
            val total_amount = "$label_logs_total_order_amount ${commaSeparated(paid_amount).asString()}"
            val displayStatus = "${getOrderStatus(booked_status).asString()} $label_logs_edfood"

            if (booked_status == "6")
            {
                textView_name.setTextColor(ContextCompat.getColor(itemView.context, R.color.greenColor))
            }
            else if (booked_status == "10")
            {
                textView_name.setTextColor(ContextCompat.getColor(itemView.context, R.color.redColor))
            }
            else
            {
                textView_name.setTextColor(ContextCompat.getColor(itemView.context, R.color.yellowColor))
            }

            textView_name.text = displayStatus
            textView_amount.text = total_amount
            textView_date.text = getLocalTimeStamp(date).asString()
            textView_transaction_no.text = order_id

            button.setOnClickListener {
                if (booked_status == "6"||booked_status == "10")
                {
                    RxBus.publish(RxEvent.EventMainActivity(tag_intent_order_history_detail, data))
                }
                else
                {
                    RxBus.publish(RxEvent.EventMainActivity(tag_intent_track_activity, data))
                }
            }
        }
    }

    class EDPayMerchantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_desc: TextView
        var textView_status: TextView
        var imageview: ImageView
        init {
            textView_name = itemView.findViewById(R.id.child_edpay_merchant_textview_name)
            textView_desc = itemView.findViewById(R.id.child_edpay_merchant_textview_description)
            textView_status = itemView.findViewById(R.id.child_edpay_merchant_textview_status)
            imageview = itemView.findViewById(R.id.child_edpay_merchant_imageview_logo)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject


//            val merchant_info =  jsonObj[key_data].asJsonObject
            val name = jsonObj[key_trade_name].asString
            val desc = jsonObj[key_product_services].asString
            val business_address = jsonObj[key_business_address].asString

            var shop_facade = ""
            if(jsonObj[key_shop_facade] == null || jsonObj[key_shop_facade].toString() == "" || jsonObj[key_shop_facade].toString() == "{}" || jsonObj[key_shop_facade].toString() == "null") {
                Glide.with(itemView).load(R.drawable.ic_default_merchant).into(imageview)
            } else {
                shop_facade = jsonObj[key_shop_facade].asString
                Glide.with(itemView).load(shop_facade).into(imageview)
            }


            textView_name.text = name
            textView_status.text = business_address
            textView_desc.text = desc

        }
    }

    class EDFoodMerchantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_desc: TextView
        var textView_status: TextView
        var textView_address: TextView
        var imageview: ImageView
        init {
            textView_name = itemView.findViewById(R.id.child_edfood_merchant_textview_name)
            textView_desc = itemView.findViewById(R.id.child_edfood_merchant_textview_description)
            textView_address = itemView.findViewById(R.id.child_edfood_merchant_textview_address)
            textView_status = itemView.findViewById(R.id.child_edfood_merchant_textview_status)
            imageview = itemView.findViewById(R.id.child_edfood_merchant_imageview_logo)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
//            val merchant_info =  jsonObj[key_data].asJsonObject
            val name = jsonObj[key_restaurant_name].asString

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.HALF_UP

            val desc = df.format(jsonObj[key_distance_from_customer].asString.toDouble()).toDouble()
            val business_address = jsonObj[key_address].asString
            val shop_facade = jsonObj[key_restaurant_photo].asString
            val operating_info = jsonObj[key_operating_info].asJsonArray

            var merchantStatus = label_close_today
            textView_status.setTextColor(ContextCompat.getColor(itemView.context, R.color.redColor))
            for (i in 0 until operating_info.size()) {
                if (operating_info[i].asJsonObject.toString().contains(getCurrentDay().asString()))
                {
                    merchantStatus = label_open_today
                    textView_status.setTextColor(ContextCompat.getColor(itemView.context, R.color.ed2e_green))
                }
            }

            textView_name.text = name
            textView_address.text = business_address
            textView_status.text = merchantStatus
            textView_desc.text = "$desc mi"
            Glide.with(itemView).load(shop_facade).into(imageview)
        }
    }

    class RewardsLogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_transaction: TextView
        var textView_date: TextView
        var textView_amount: TextView
        var imageview: ImageView
        var button: Button
        init {
            textView_name = itemView.findViewById(R.id.child_rewards_log_textview_name)
            textView_transaction = itemView.findViewById(R.id.child_rewards_log_textview_transaction)
            textView_date = itemView.findViewById(R.id.child_rewards_log_textview_date)
            textView_amount = itemView.findViewById(R.id.child_rewards_log_textview_amount)
            imageview = itemView.findViewById(R.id.child_rewards_log_imageview_logo)
            button = itemView.findViewById(R.id.child_rewards_log_button_shake)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val transaction_no = jsonObj[key_transaction_no].asString
            val date = jsonObj[key_created_date].asString

            var name = label_reward_edpay
            try {
                if (jsonObj[key_payment_type].asString.equals("1"))
                {
                    name = label_reward_edfood
                }
                else
                {
                    name = label_reward_edpay
                }
            }
            catch (e: Exception)
            {

            }



            try {
                val jsonItem = jsonObj.asJsonObject
                val country = jsonItem[key_country].asJsonObject
                val currency = country[key_currency_code].asString



                val cashback = jsonItem[key_cashback].asJsonObject
                Glide.with(itemView).load(R.drawable.ic_reward_redeem).into(imageview)

                if(cashback == null) {
                    button.visibility = VISIBLE
                    textView_amount.visibility = GONE
                    Glide.with(itemView).load(R.drawable.ic_reward_redeem).into(imageview)
                } else {
                    button.visibility = GONE
                    textView_amount.visibility = VISIBLE
                    Glide.with(itemView).load(R.drawable.ic_reward_redeemed).into(imageview)
                    val cashback_amount = cashback[key_cashback].asFloat
                    val displayed_amount = "$currency ${commaSeparated(cashback_amount.toString().replace(",","")).asString()}"
                    textView_amount.text = displayed_amount
                }






//                textView_amount.visibility = VISIBLE
//                button.visibility = GONE
//
//                Glide.with(itemView).load(R.drawable.ic_reward_redeemed).into(imageview)
            }
            catch (e : Exception)
            {
                //Glide.with(itemView).load(R.drawable.ic_reward_redeem).into(imageview)
            }
            textView_name.text = name
            textView_transaction.text = transaction_no
            textView_date.text = getLocalTimeStamp(date).asString()



            //button.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_intent_shake_activity, data)) }
//            Glide.with(itemView).load(shop_facade).into(imageview)
        }
    }

    class EDFoodMenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var textView_amount: TextView
        var imageview: ImageView
        var button: Button
        init {
            textView_name = itemView.findViewById(R.id.child_edfood_menu_item_textview_name)
            textView_amount = itemView.findViewById(R.id.child_edfood_menu_item_textview_amount)
            imageview = itemView.findViewById(R.id.child_edfood_menu_item_imageview_logo)
            button = itemView.findViewById(R.id.child_edfood_menu_item_button_add)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            var jsonObj = JsonObject()
            try {
                jsonObj = parser.parse(data.toString()).asJsonObject
            }
            catch (e:Exception)
            {
                jsonObj = parser.parse(parser.parse(data.toString()).asString).asJsonObject
            }
            val name = jsonObj[key_food_name].asString
            val amount = jsonObj[key_food_price].asString
            val shop_facade = jsonObj[key_food_photo].asString

            textView_name.text = name
            textView_amount.text = amount
            Glide.with(itemView)
                .load(shop_facade)
                .apply(
                    RequestOptions().override(
                        256,
                        256
                    ).centerCrop().placeholder(R.drawable.ic_default_food)
                )
                .into(
                    imageview
                )
            //Glide.with(itemView).load(shop_facade).into(imageview)
            button.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_intent_edfood_menu_details_activity, data)) }
        }
    }

    class EDFoodMenuCategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var recyclerView: RecyclerView
        init {
            textView_name = itemView.findViewById(R.id.child_edfood_menu_category_textview)
            recyclerView = itemView.findViewById(R.id.child_edfood_menu_category_recyclerview)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val name = jsonObj["category"].asString

            textView_name.text = name

            val arrayList = ArrayList<Any>()
            try
            {
                val jsonArray = parser.parse(jsonObj[key_data].toString()).asJsonArray
                for (i in 0 until jsonArray.size()) {
                    arrayList.add(jsonArray[i])
                }
            }
            catch (e :Exception)
            {
                Timber.w("Exception $e")
                val jsonArray = parser.parse(jsonObj[key_data].asString).asJsonObject
                arrayList.add(jsonArray)
            }

            val adapter = object : BaseRecyclerViewAdapter<Any>(arrayList, object :
                ItemClickListener<Any> {
                override fun onItemClick(v: View, item: Any, position: Int) {

                }
            }) {
                override fun getLayoutId(position: Int, obj: Any): Int {
                    return when(obj){
                        is String->R.layout.child_edfood_menu_category
                        else->R.layout.child_edfood_menu_item
                    }
                }

                override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                    return create(view,viewType)
                }
            }
            recyclerView.layoutManager= LinearLayoutManager(itemView.context)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter=adapter
        }
    }

    class EDFoodAddonCategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView_name: TextView
        var recyclerView: RecyclerView
        init {
            textView_name = itemView.findViewById(R.id.child_edfood_addon_category_textview)
            recyclerView = itemView.findViewById(R.id.child_edfood_addon_category_recyclerview)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val name = jsonObj[key_addon_category_name].asString
            val max = jsonObj[key_max].asString
            val min = jsonObj[key_min].asString
            val display = "$name (min $min max $max)"
            textView_name.text = display

            val arrayList = ArrayList<Any>()
            val jsonArray = jsonObj[key_addons].asJsonArray
            for (i in 0 until jsonArray.size()) {
                arrayList.add(jsonArray[i])
            }

            val adapter = object : BaseRecyclerViewAdapter<Any>(arrayList, object :
                ItemClickListener<Any> {
                override fun onItemClick(v: View, item: Any, position: Int) {

                }
            }) {
                override fun getLayoutId(position: Int, obj: Any): Int {
                    return R.layout.child_edfood_addon_item
                }

                override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                    return create(view,viewType)
                }
            }
            recyclerView.layoutManager= LinearLayoutManager(itemView.context)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter=adapter
        }
    }

    class EDFoodAddonItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textView: TextView
        var checkBox: CheckBox
        init {
            textView = itemView.findViewById(R.id.child_edfood_addon_item_textview_amount)
            checkBox = itemView.findViewById(R.id.child_edfood_addon_item_checkbox)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val name = jsonObj[key_addon_name].asString
            val amount = jsonObj[key_addon_price].asString
            textView.text = amount
            checkBox.text = name
            checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked)
                    RxBus.publish(RxEvent.EventMainActivity(tag_load_addon, data))
                else
                    RxBus.publish(RxEvent.EventMainActivity(tag_load_addon_remove, data))
            }
        }
    }

    class EDFoodCartItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textViewName: TextView
        var textViewPrice: TextView
        var textViewQuantity: TextView
        var textViewEdit: TextView
        var textViewRemove: TextView
        var buttonMinus: Button
        var buttonAdd: Button
        var imageView: ImageView
        var layout: LinearLayout
        init {
            textViewName = itemView.findViewById(R.id.child_edfood_cart_item_textview_name)
            textViewPrice = itemView.findViewById(R.id.child_edfood_cart_item_textview_price)
            textViewQuantity = itemView.findViewById(R.id.child_edfood_cart_item_textview_quantity)
            textViewEdit = itemView.findViewById(R.id.child_edfood_cart_item_button_edit)
            textViewRemove = itemView.findViewById(R.id.child_edfood_cart_item_button_remove)
            buttonMinus = itemView.findViewById(R.id.child_edfood_cart_item_button_minus)
            buttonAdd = itemView.findViewById(R.id.child_edfood_cart_item_button_add)
            imageView = itemView.findViewById(R.id.child_edfood_cart_item_imageview)
            layout = itemView.findViewById(R.id.child_edfood_cart_item_layout_addon)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val food_name = jsonObj[key_food_name].asString
            val food_photo = jsonObj[key_food_photo].asString
            val food_price = jsonObj[key_food_price].asString
            val food_quantity = jsonObj[key_food_quantity].asString
            val food_total_quantity_price = jsonObj[key_food_total_quantity_price].asString

            textViewName.text = food_name
            textViewPrice.text = commaSeparated(food_total_quantity_price).asString()
            textViewQuantity.text = food_quantity
            Glide.with(itemView)
                .load(food_photo)
                .apply(
                    RequestOptions().override(
                        256,
                        256
                    ).centerCrop().placeholder(R.drawable.ic_default_food)
                )
                .into(
                    imageView
                )
            //Glide.with(itemView).load(food_photo).into(imageView)

            for (addon in jsonObj[key_food_addons].asJsonArray)
            {
                val addonObject = addon.asJsonObject
                val food_addon = addonObject[key_food_addon].asString
                val textView = TextView(itemView.context)
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)
                val display = "w/ $food_addon"
                textView.text = display
                layout.addView(textView)
            }

            buttonAdd.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_load_quantity_increase, data)) }
            buttonMinus.setOnClickListener {
                if (food_quantity.toInt() >= 2)
                {
                    RxBus.publish(RxEvent.EventMainActivity(tag_load_quantity_decrease, data))
                }
            }
            textViewEdit.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_load_order_edit, data)) }
            textViewRemove.setOnClickListener { RxBus.publish(RxEvent.EventMainActivity(tag_load_order_remove, data)) }

        }
    }

    class EDFoodLogItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Any> {
        var textViewName: TextView
        var textViewPrice: TextView
        var textViewQuantity: TextView
        var layout: LinearLayout
        init {
            textViewName = itemView.findViewById(R.id.child_edfood_log_details_item_textview_name)
            textViewPrice = itemView.findViewById(R.id.child_edfood_log_details_item_textview_amount)
            textViewQuantity = itemView.findViewById(R.id.child_edfood_log_details_item_textview_quantity)
            layout = itemView.findViewById(R.id.child_edfood_log_details_item_layout_addon)
        }
        override fun bind(data: Any) {
            val parser = JsonParser()
            val jsonObj = parser.parse(data.toString()).asJsonObject
            val food_name = jsonObj[key_food_name].asString
            val food_price = jsonObj[key_order_price].asString
            val food_quantity = "x${jsonObj[key_food_quantity].asString}"

            textViewName.text = food_name
            textViewPrice.text = commaSeparated(food_price.replace(",","")).asString()
            textViewQuantity.text = food_quantity

            for (addon in jsonObj[key_addons].asJsonArray)
            {
                Timber.d("addon $addon")
                val addonObject = addon.asJsonObject
                val food_addon = addonObject[key_food_addon].asString
                val price = addonObject[key_price].asString
                val parent = LinearLayout(itemView.context)

                parent.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                parent.orientation = LinearLayout.HORIZONTAL

                val textView = TextView(itemView.context)
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)
                textView.setTextColor(ContextCompat.getColor(itemView.context, R.color.fontGreyColor))
                val display = "w/ $food_addon"
                textView.text = display
                val childParam1: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                childParam1.weight = 1f
                textView.layoutParams = childParam1

                val textViewPrice = TextView(itemView.context)
                textViewPrice.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)
                textViewPrice.setTextColor(ContextCompat.getColor(itemView.context, R.color.fontBlackColor))
                textViewPrice.text = commaSeparated(price.replace(",","")).asString()
                textViewPrice.gravity = Gravity.END
                val childParam2: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                childParam2.weight = 1f
                textViewPrice.layoutParams = childParam2

                parent.addView(textView)
                parent.addView(textViewPrice)
                layout.addView(parent)
            }

        }
    }

//    class BusViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BaseRecyclerViewAdapter.Binder<Bus> {
//
//        var textView: TextView
//        init {
//            textView = itemView.findViewById(R.id.busName)
//        }
//        override fun bind(bus: Bus) {
//            textView.setText(bus.name)
//        }
//    }
}