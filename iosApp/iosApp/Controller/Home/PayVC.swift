//
//  PayVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/21/20.
//

import UIKit
import SnapKit

class PayVC: UIViewController {
    @IBOutlet weak var segmentedMerchant: UISegmentedControl!
    @IBOutlet weak var containerMerchant: UIView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    
    let segmentindicator: UIView = {

        let v = UIView()

        v.translatesAutoresizingMaskIntoConstraints = false
        v.isOpaque = false
        v.contentMode = .scaleAspectFit
        v.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "four_pillars"))

        return v
    }()

    
    var scanMerchantSegmentView = ScanMerchantVC()
    var viewMerchantsSegmentView = ViewMerchantsVC()
    
   
    private lazy var scanMerchant: ScanMerchantVC = {
        let storyboard = UIStoryboard(name: "Pay", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "scanMerchantIdentifier") as! ScanMerchantVC
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var viewMerchant: ViewMerchantsVC = {
        let storyboard = UIStoryboard(name: "Pay", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "viewMerchantIdentifier") as! ViewMerchantsVC
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         updateView()
        
       
        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
            UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
            UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected )
        // End of customizing the segmented control
        
        self.view.addSubview(segmentindicator)
        
        setupLayout()
    }

    func setupLayout() {
        segmentindicator.snp.makeConstraints { (make) in
            
            make.top.equalTo(segmentedMerchant.snp.bottom).offset(-10)
            make.height.equalTo(10)
            make.width.equalTo(75 + segmentedMerchant.titleForSegment(at: 0)!.count * 8)
            make.centerX.equalTo(segmentedMerchant.snp.centerX).dividedBy(segmentedMerchant.numberOfSegments)
            
        }
        
    }
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func switchSegment(_ sender: Any) {
         updateView()
        
        let numberOfSegments = CGFloat(segmentedMerchant.numberOfSegments)
        let selectedIndex = CGFloat((sender as AnyObject).selectedSegmentIndex)
        segmentindicator.snp.remakeConstraints { (make) in
            
            make.top.equalTo(segmentedMerchant.snp.bottom).offset(-10)
            make.height.equalTo(10)
            make.width.equalTo(75 + segmentedMerchant.titleForSegment(at: 0)!.count * 8)
            make.centerX.equalTo(segmentedMerchant.snp.centerX).dividedBy(numberOfSegments / CGFloat(3.0 + CGFloat(selectedIndex-1.0)*2.0))
            
        }
        
//        UIView.animate(withDuration: 0.5, animations: {
//            self.view.layoutIfNeeded()
//            self.segmentindicator.transform = CGAffineTransform(scaleX: 1.4, y: 1)
//        }) { (finish) in
//            UIView.animate(withDuration: 0.4, animations: {
//                self.segmentindicator.transform = CGAffineTransform.identity
//            })
//        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        DispatchQueue.main.async(execute: {
            (sender as? UIMenuController)?.setMenuVisible(false, animated: false)
        })
        return false
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        containerMerchant.addSubview(viewController.view)
        viewController.view.frame = containerMerchant.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    private func updateView() {
        if segmentedMerchant.selectedSegmentIndex == 0 {
            remove(asChildViewController: viewMerchant)
            add(asChildViewController: scanMerchant)
        } else if segmentedMerchant.selectedSegmentIndex == 1 {
            remove(asChildViewController: scanMerchant)
            add(asChildViewController: viewMerchant)
        }
    }
}
