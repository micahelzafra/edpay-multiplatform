package com.ed2e.ed2eapp.view.activity.main.home.edpay

import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_edpay_input_amount.*
import kotlinx.android.synthetic.main.activity_edpay_log_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class EDPayLogDetailsActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edpay_log_details)

        job = Job()

        apiInterface = ApiInterface()
        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
    }

    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)

        val offlineMerchant = jsonObj[key_offline_merchant].asJsonObject
        val merchantName = offlineMerchant[key_trade_name].asString
        val amount = jsonObj[key_paid_amount].asString
        val country = jsonObj[key_country].asJsonObject
        val currency = country[key_currency_code].asString
        val displayAmount = "$currency ${commaSeparated(amount.replace(",","")).asString()}"
        val transactionNo = jsonObj[key_transaction_no].asString
        val date = jsonObj[key_created_date].asString

        val paymentMethod: String
        paymentMethod = try {
            country[key_payment_method].asString
        } catch (jinx: Exception) {
            "EDCredits"
        }

        edpay_log_details_textview_merchant_name.text = merchantName
        edpay_log_details_textview_amount.text = displayAmount
        edpay_log_details_textview_payment_method.text = paymentMethod
        edpay_log_details_textview_transaction_number.text = transactionNo
        edpay_log_details_textview_date.text = getLocalTimeStamp(date).asString()

    }




    private fun initToolBar() {
        edpay_log_details_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edpay_log_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edpay_log_details_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}