package com.ed2e.ed2eapp.adapter;

import com.ed2e.ed2eapp.util.getLocalTimeStamp;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.model.Cashback;
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsActivity;
import com.ed2e.ed2eapp.view.activity.announcements.AnnouncementsDetailsActivity;
import com.ed2e.ed2eapp.view.activity.login.Login1Activity;
import com.ed2e.ed2eapp.view.activity.main.home.edpay.EDPayShakeReward;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.ed2e.ed2eapp.util.ConstantKt.key_amount;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_payment_method;

public class RewardsLogAdapter extends RecyclerView.Adapter<RewardsLogAdapter.MyViewHolder> {

    Activity activity;
    List<Cashback> modelList;

    public RewardsLogAdapter(Activity activity, List<Cashback> modelList) {
        this.activity = activity;
        this.modelList = modelList;
    }

    public void setModelList(List<Cashback> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RewardsLogAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.child_rewards_logs, viewGroup, false);
        //view.setOnClickListener(new MainActivity.MyOnClickListener());
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RewardsLogAdapter.MyViewHolder myViewHolder, int position) {


        if(modelList.get(position).getPayment_type().equalsIgnoreCase("1")) {
            myViewHolder.textview_title.setText(activity.getResources().getString(R.string.label_reward_edfood));
        } else {
            myViewHolder.textview_title.setText(activity.getResources().getString(R.string.label_reward_edpay));
        }

        myViewHolder.textview_transaction_no.setText("" + modelList.get(position).getTransaction_no());
        myViewHolder.textView_date_time.setText(new getLocalTimeStamp(modelList.get(position).getCreated_date()).asString());

//        myViewHolder.textView_cashback_amount.setText(modelList.get(position).getCashback());
//        myViewHolder.textView_cashback_amount.setVisibility(View.VISIBLE);
//        myViewHolder.button_shake.setVisibility(View.GONE);

        if(modelList.get(position).getCashback() == null || modelList.get(position).getCashback().isEmpty() || modelList.get(position).getCashback().equalsIgnoreCase("") || modelList.get(position).getCashback().equalsIgnoreCase("null")) {
            Glide.with(activity)
                    .load(R.drawable.ic_reward_redeem)
                    .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_dummy))
                    .into(myViewHolder.imageView);

            myViewHolder.textView_cashback_amount.setVisibility(View.GONE);
            myViewHolder.button_shake.setVisibility(View.VISIBLE);
        } else {
            Glide.with(activity)
                    .load(R.drawable.ic_reward_redeemed)
                    .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_dummy))
                    .into(myViewHolder.imageView);

            //Log.d("", "cashback: " + modelList.get(position).getCashback());
                       String cashback_amount;
            try {
                JSONObject cashback_obj = new JSONObject(modelList.get(position).getCashback());
                //Log.d("", "cashback_obj: " + cashback_obj);
                cashback_amount = "" + (String) cashback_obj.get("cashback");
            } catch (JSONException e) {
                e.printStackTrace();
                cashback_amount = "(NaN)";
            }

            myViewHolder.textView_cashback_amount.setText(cashback_amount);
            myViewHolder.textView_cashback_amount.setVisibility(View.VISIBLE);
            myViewHolder.button_shake.setVisibility(View.GONE);


        }

//        String image_path, image;
//        int index = modelList.get(position).getImagesList().size() - 1;
//
//        try {
//            if(index <= 0) {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            } else {
//                image_path = modelList.get(position).getImagesList().get(index).getImage_path();
//                image = modelList.get(position).getImagesList().get(index).getImage();
//            }
//        } catch (Exception e) {
//            image_path = "";
//            image = "";
//        }
//
//
//        Glide.with(activity)
//                .load( modelList.get(position).getImage())
//                .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_ed2e_logo))
//                .into(myViewHolder.imageView);
//
//
//        String finalImage_path = image_path;
//        String finalImage = image;
//        myViewHolder.linearLayout_view_details.setOnClickListener(v -> {
//            //Toast.makeText(v.getContext(), /*"@" + position + " | " + */"View Details", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent (v.getContext(), AnnouncementDetailsActivity.class);
//            intent.putExtra(KEY_TITLE, modelList.get(position).getTitle());
//            intent.putExtra(KEY_EVENT_DATE, modelList.get(position).getEvent_date());
//            intent.putExtra(KEY_CREATED_DATE, formatDate_MMMMDDYYYY(modelList.get(position).getCreated_date()));
//            intent.putExtra(KEY_CONTENT, modelList.get(position).getContent());
//            intent.putExtra(KEY_IMAGE, "http://" + hostname + "/" + finalImage_path + "/" + finalImage);
//            activity.startActivity(intent);
//        });

//
        myViewHolder.button_shake.setOnClickListener(v -> {
            //Toast.makeText(v.getContext(), "@" + position + " | " + "Share", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent (v.getContext(), EDPayShakeReward.class);
            intent.putExtra(key_amount, modelList.get(position).getTransaction_no());
            intent.putExtra(key_payment_method, modelList.get(position).getPayment_type());
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

    }

    @Override
    public int getItemCount() {
        if(modelList != null){
            return modelList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textview_title, textview_transaction_no, textView_date_time, textView_cashback_amount;
        ImageView imageView;
        Button button_shake;

        public MyViewHolder(View itemView) {
            super(itemView);

            textview_title = itemView.findViewById(R.id.child_rewards_log_textview_name);
            textview_transaction_no = itemView.findViewById(R.id.child_rewards_log_textview_transaction);
            textView_date_time = itemView.findViewById(R.id.child_rewards_log_textview_date);
            textView_cashback_amount = itemView.findViewById(R.id.child_rewards_log_textview_amount);

            imageView = itemView.findViewById(R.id.child_rewards_log_imageview_logo);
            button_shake = itemView.findViewById(R.id.child_rewards_log_button_shake);
        }
    }
}
