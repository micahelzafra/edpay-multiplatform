//
//  PaymentMobilePINVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/23/20.
//

import UIKit
import CBPinEntryView
import SwiftyJSON
import app
import AlamofireImage

class PaymentMobilePINVC: BaseViewController {
    
    internal var apiInterface = ApiInterface()
    
    var segueData = Dictionary<String, AnyObject>()
    
    var merchantData = JSON()

    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var imageViewMerchant: RoundableImageView!
    @IBOutlet weak var lblMerchantName: UILabel!
    
    @IBOutlet weak var lblMerchantAmount: UILabel!
    @IBOutlet weak var pinVIew: CBPinEntryView!
    @IBOutlet weak var btnSubmit: DesignableButton!
    
    let downloader = ImageDownloader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGUI()
    }

    
    func initGUI()
    {
        merchantData = segueData[self.constant.key_data] as! JSON
        lblMerchantName.text = merchantData[self.constant.key_trade_name].string
                
        if let imageurl = merchantData[self.constant.key_shop_facade].string
        {
            let urlRequest = URLRequest(url: URL(string: imageurl)!)
            //let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
            downloader.download(urlRequest, filter: nil) { response in
                if let image = response.result.value {
                    print(image)
                    self.imageViewMerchant.image = image
                }
            }
        }
        
        lblMerchantAmount.text = "\(segueData[self.constant.key_currency_code] as! String) \(segueData[self.constant.key_amount] as! String)"
    }
    
    @IBAction func didTapButtonBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func didTapButtonSubmit(_ sender: Any) {
        if pinVIew.getPinAsString().count < 6
        {
            self.handleError(displaytitle: "dialog_missing_field_title".localized(), "dialog_missing_field_message".localized())
            return
        }
        
        self.initTransaction(
            customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))",
            merchant_code: merchantData[self.constant.key_merchant_code].string!,
            amount: "\(self.segueData[self.constant.key_amount] as! String)".replacingOccurrences(of: ",", with: ""),
            pin: self.pinVIew.getPinAsString()
        )
    }
    
    private func initTransaction(
        customer_id: String,
        merchant_code: String,
        amount: String,
        pin: String
    ){
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlQRTransaction,
            params: "customer_id:\(customer_id)||merchant_code:\(merchant_code)||amount:\(amount)||pin:\(pin)",
            success: { data in
                let jsonData = data.data(using: String.Encoding.utf8)!
                let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                let json: JSON = JSON(jsonDic)
                
                let status = json[self.constant.key_status].string
                if status == self.constant.status_success
                {
                    var segueData = Dictionary<String, AnyObject>()
                    segueData[self.constant.key_data] = json[self.constant.key_data] as AnyObject
                    self.performSegue(withIdentifier: "seguePaymentSuccess", sender: segueData)
                }
                else
                {
                    let error = json[self.constant.key_code].string
                    self.handleError(error)
                }
                return KotlinUnit()
        }, failure: {
            self.handleError($0?.message)
            return KotlinUnit()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "seguePaymentSuccess") {
            if segue.destination is EDPaySuccessVC
            {
                if let chidVC = segue.destination as? EDPaySuccessVC {
                    self.log("chidVC \(chidVC)")
                    chidVC.segueData = (sender as? [String:AnyObject])!
                }
            }
        }
    }
}
