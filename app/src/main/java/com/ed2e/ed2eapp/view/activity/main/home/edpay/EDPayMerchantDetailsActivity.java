package com.ed2e.ed2eapp.view.activity.main.home.edpay;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.model.Announcement;
import com.ed2e.ed2eapp.util.AppPreference;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_content;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_date;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_image;
import static com.ed2e.ed2eapp.util.ConstantKt.key_announcement_title;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_data;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_shop_facade;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;
import static com.ed2e.ed2eapp.util.ConstantKt.urlOfflineMerchantDetails;

public class EDPayMerchantDetailsActivity extends BaseActivity {

    public static final String TAG = EDPayMerchantDetailsActivity.class.getSimpleName();

    @BindView(R.id.edpay_merchant_details_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edpay_merchant_details_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.edpay_merchant_details_imageview_merchant_logo)
    ImageView imageview_merchant_logo;
    @BindView(R.id.edpay_merchant_details_textview_name)
    TextView textview_name;
    @BindView(R.id.edpay_merchant_details_textview_products)
    TextView textview_products;
    @BindView(R.id.edpay_merchant_details_textview_desc)
    TextView textview_desc;
    @BindView(R.id.edpay_merchant_details_textview_business_address)
    TextView textview_business_address;

    private String merchantID = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edpay_merchant_details);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initAPIGetMerchantDetails();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            merchantID = "";
        } else {
            merchantID = extras.getString(key_id);
        }

//        showToast_long(
//              "initData:\n" +
//                      "(" + merchantID + ")"
//        );
    }

    private void initGUI() {

//        textView_title.setText(annTitle);
//        textView_event_date.setText(annDate);
//        textView_content.setText(annContent);
//
//        Glide.with(EDPayMerchantDetailsActivity.this)
//                .load(annImage)
//                .apply(new RequestOptions().override(1280, 710).centerCrop().placeholder(R.drawable.ic_eddy_2))
//                .into(imageView_content);
        initAPIGetMerchantDetails();
    }

    private void initAPIGetMerchantDetails() {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlOfflineMerchantDetails,
                "offline_merchant_id:" + merchantID,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | ");
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();


                    if (status.equals(status_success))
                    {

                        JsonObject data = jsonObj.get(key_data).getAsJsonObject();

                        String trade_name = data.get("trade_name").getAsString();

                        String product_services = "";
                        try {
                            product_services = "" + data.get("product_services").getAsString();
                        } catch (Exception e) {
                            product_services = "";
                        }

                        String description = "";
                        try {
                            description = "" +  data.get("description").getAsString();
                        } catch (Exception e) {
                            description = "";
                        }

                        String business_address = data.get("business_address").getAsString();

                        String finalProduct_services = product_services;
                        String finalDescription = description;

                        runOnUiThread(() -> {

                            String shop_facade = "";

                            //showToast_short("show: " + data.get(key_shop_facade));

                            if(data.get(key_shop_facade) == null || data.get(key_shop_facade).toString().equalsIgnoreCase("") || data.get(key_shop_facade).toString().equalsIgnoreCase("{}") || data.get(key_shop_facade).toString().equalsIgnoreCase("null") ) {

                                //showToast_short("null");
                                Glide.with(EDPayMerchantDetailsActivity.this).load(R.drawable.ic_default_merchant).into(imageview_merchant_logo);
                            } else {
                                shop_facade = data.get(key_shop_facade).toString().replaceAll("\"", "");
                                Glide.with(EDPayMerchantDetailsActivity.this).load(shop_facade).into(imageview_merchant_logo);
                            }

                            textview_name.setText("" + trade_name);
                            textview_products.setText("" + finalProduct_services);
                            textview_desc.setText("" + finalDescription);
                            textview_business_address.setText(getResources().getString(R.string.label_address) + business_address);
                        });


                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }

    private void initPreviousActivity() {
//        Intent intent = new Intent(getContext(), AnnouncementsActivity.class);
//        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        
    }



//    @Override
//    protected void onDialogDismiss(String requestTag, String input_data) {
//        Intent intent;
//        switch (requestTag) {
//            case "tag_edpay_shake_reward_error":
//                intent = new Intent(EDPayShakeReward.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//
//
//            default:
//                break;
//        }
//    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
