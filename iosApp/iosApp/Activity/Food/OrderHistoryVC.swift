//
//  OrderHistoryVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 1/28/20.
//

import UIKit
import Cosmos
import SwiftyJSON
import app
import AlamofireImage

class OrderHistoryVC: BaseViewController , UITableViewDataSource, UITableViewDelegate {

    internal var apiInterface = ApiInterface()

    var segueData = Dictionary<String, AnyObject>()
    
    private var bookedId = ""
    private var bookeOrderID = ""
    private var orderDetails = JSON()
    private var restaurantDetails = JSON()
    private var riderDispatchDetails = JSON()
    
    private var isRiderReviewed = false
    private var riderRating = 0.0
    private var isMerchantReviewed = false
    private var merchantRating = 0.0
    
    private var currentOrderStatus = ""
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblEP: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lblEdpoints: UILabel!
   
    
    @IBOutlet weak var btnRateRider: DesignableButton!
    @IBOutlet weak var btnRateRestaurant: DesignableButton!
    @IBOutlet weak var lblNoInformationRider: UILabel!
    
    //delivered by
    @IBOutlet weak var imageViewRider: RoundableImageView!
    @IBOutlet weak var lblRiderName: UILabel!
    @IBOutlet weak var lblRiderPlateNumber: UILabel!
    @IBOutlet weak var vRiderRating: CosmosView!
    
    //ordered from
    @IBOutlet weak var lblOrderedFrom: UILabel!
    @IBOutlet weak var lblDeliveredAddress: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var vRating: CosmosView!
    
    //Subtotal
    @IBOutlet weak var lblTotalFoodPrice: UILabel!
    @IBOutlet weak var lblContainerFee: UILabel!
    @IBOutlet weak var lblDeliveryFee: UILabel!
    @IBOutlet weak var lblVATValue: UILabel!
    @IBOutlet weak var lblPromoCodeValue: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblVAT: UILabel!
    
    @IBOutlet weak var lblServiceFee: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnReportIssue: DesignableButton!
    
    @IBOutlet weak var viewRider: UIView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    var arraylist = [JSON]()
    //var isRiderReviewed = false
    //var isMerchantReviewed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.log("passed Data: \(segueData)")
        
        let jsonObj = segueData[self.constant.key_data] as! JSON
        self.log("passed data \(jsonObj)")
       
        self.bookedId = "\(jsonObj[self.constant.key_booked_id].int ?? -1)"
        self.getOrderDetails(booked_id: bookedId)
            }
    
    @IBAction func didTapButtonReportIssue(_ sender: Any) {
//        var segueData = Dictionary<String, AnyObject>()
//        segueData[self.constant.key_data] = data as AnyObject
        self.performSegue(withIdentifier: "segueReport", sender: self.orderDetails)
        
    }
    @IBAction func didTapButtonBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
        
    private func setData(jsonObj:JSON)
    {
        
        guard let jsonArray = jsonObj[self.constant.key_booked].array else { return  }
        var data = JSON()
        for json in jsonArray
        {
            data = json
        }
        let foodTotal = data[self.constant.key_food_total].self
        restaurantDetails = data[self.constant.key_restaurant_details].self
        
        if let json = data[self.constant.key_rider_dispatch_details].object as? Array<Any> {
            self.log("json \(json)")
            riderDispatchDetails = JSON()
        }
        else {
            riderDispatchDetails = data[self.constant.key_rider_dispatch_details].self
        }
        print("riderDispatchDetails \(riderDispatchDetails)")
        if !riderDispatchDetails.isEmpty
        {
            let riderInfo = riderDispatchDetails[self.constant.key_rider_info].self
            let name = "\(riderInfo[self.constant.key_first_name].string ?? "") \(riderInfo[self.constant.key_last_name].string ?? "")"
            self.lblRiderPlateNumber.text = riderInfo[self.constant.key_plate_no].string
            self.lblRiderName.text = name
//            @IBOutlet weak var imageViewRider: RoundableImageView!
//            @IBOutlet weak var vRiderRating: CosmosView!
        }
        
        var riderNote = ""
        if let json = data[self.constant.key_special_notes].object as? String {
            riderNote = json
        }
        
        let orderStatus = data[self.constant.key_booked_status].int
        let date = data[self.constant.key_booked_date].string
        
        let orderNo = "\(self.constant.label_logs_order_no) \(data[self.constant.key_booked_id].int ?? -1)"
        let total = commaSeparated(value: (foodTotal[self.constant.key_total].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        self.lblOrderNumber.text = orderNo
        self.lblEP.text = total
        self.lblStatus.text = "\(getOrderStatus(status: "\(orderStatus ?? -1)").asString())"
        self.lblDateAndTime.text = "\(getLocalTimeStamp(date: date!).asString())"
        self.lblEdpoints.text = data[self.constant.key_payment_type].string
        
        lblOrderedFrom.text = restaurantDetails[self.constant.key_restaurant_name].string
        lblDeliveredAddress.text = data[self.constant.key_booked_location].string
        lblNote.text = riderNote
        
        let voucher = commaSeparated(value: (foodTotal[self.constant.key_discount].string?.replacingOccurrences(of: ",", with: ""))!).asString()

        lblTotalFoodPrice.text = commaSeparated(value: (foodTotal[self.constant.key_sub_total].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        lblContainerFee.text = commaSeparated(value: (foodTotal[self.constant.key_container_fee].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        lblDeliveryFee.text = commaSeparated(value: (foodTotal[self.constant.key_delivery_fee].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        lblServiceFee.text = commaSeparated(value: (foodTotal[self.constant.key_service_fee].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        lblVAT.text = "Vat\(foodTotal[self.constant.key_vat].string ?? "")"
        lblVATValue.text = commaSeparated(value: (foodTotal[self.constant.key_vat_amount].string?.replacingOccurrences(of: ",", with: ""))!).asString()
        if voucher == "0.00"
        {
            lblPromoCodeValue.text = voucher
        }
        else
        {
            lblPromoCodeValue.text = "-\(voucher)"
        }
        lblTotal.text = total
        
        self.arraylist.removeAll()
        let ordersArray = data[self.constant.key_orders].array!
        for order in ordersArray
        {
            arraylist.append(order)
        }
        
        self.tableView.reloadData()


    }
        
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arraylist.count == 0
        {
            tableView.setEmptyView(title: "No data available", message: "Your data will be in here.")
        }
        else
        {
            tableView.restore()
        }
        return arraylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EDFoodLogTableCell", for: indexPath)
                        as! EDFoodLogTableCell

        let data = arraylist[indexPath.row]
        cell.lblFoodName.text = data[self.constant.key_food_name].string
        cell.lblPrice.text = data[self.constant.key_order_price].string
        cell.lblQuantity.text = "x\(data[self.constant.key_food_quantity].int ?? 0)"
        
        cell.layoutAddon.removeAllArrangedSubviews()
        for addon in data[self.constant.key_addons].array!
        {
            let addonObject = addon.self
            let food_addon = addonObject[self.constant.key_food_addon].string
            let display = "w/ \(food_addon ?? "")"
            let price = addonObject[self.constant.key_price].string
            //For side by side text dynamic
            let stackView = UIStackView()
            stackView.axis = .horizontal
            stackView.alignment = .fill // .Leading .FirstBaseline .Center .Trailing .LastBaseline
            stackView.distribution = .fillEqually // .FillEqually .FillProportionally .EqualSpacing .EqualCentering
            
            
            let lblAddonName = UILabel()
            lblAddonName.text = display
            lblAddonName.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            lblAddonName.translatesAutoresizingMaskIntoConstraints = false
            lblAddonName.font = lblAddonName.font.withSize(10)
                        
            let label = UILabel()
            label.text = commaSeparated(value: (price!.replacingOccurrences(of: ",", with: ""))).asString()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = label.font.withSize(10)
            label.textAlignment = .right
            
//            cell.layoutAddon.addArrangedSubview(lblAddonName)
            stackView.addArrangedSubview(lblAddonName)
            stackView.addArrangedSubview(label)
            cell.layoutAddon.addArrangedSubview(stackView)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var index = 125
        let data = arraylist[indexPath.row]
        let array = data[self.constant.key_addons].array!
        index = index + (array.count * 11)
        return CGFloat(index)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueReport") {

            if let navController = segue.destination as? UINavigationController {
                if let chidVC = navController.topViewController as? ReportSelectionVC {
                        self.log("chidVC \(chidVC)")
                        chidVC.segueData = (sender as? [String:AnyObject])!
                }
            }
        }
    }
    
    
    private func getOrderDetails(
      booked_id: String
    ) {
    showLoading()
      apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlOrderHistoryDetails,
          params: "booked_id:\(booked_id)",
          success: { data in
              let jsonData = data.data(using: String.Encoding.utf8)!
              let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
              let json: JSON = JSON(jsonDic)
              
              let status = json[self.constant.key_status].string
              if status == self.constant.status_success {
                self.setData(jsonObj: json)
                self.orderDetails = json
                self.bookeOrderID = booked_id
                let booked = json[self.constant.key_booked].array
                let booked_status = booked![0][self.constant.key_booked_status].int

                for riderDetails in booked! {
                    //let rider_info  = riderDetails["rider_dispatch_details"].self
                    
                    if let json = riderDetails[self.constant.key_rider_dispatch_details].object as? Array<Any> {
                        self.log("json \(json)")
                        self.riderDispatchDetails = JSON()
                    }
                    else {
                        self.riderDispatchDetails = riderDetails[self.constant.key_rider_dispatch_details].self
                    }
                    print("riderDispatchDetails \(self.riderDispatchDetails)")
                    if !self.riderDispatchDetails.isEmpty
                    {
                        //let riderInfo = self.riderDispatchDetails[self.constant.key_rider_info].self
                        self.viewRider.isHidden = false
                        self.lblNoInformationRider.isHidden = true
                    } else {
                        self.viewRider.isHidden = true
                         self.lblNoInformationRider.isHidden = false
                    }
                    
                    
                }
                
                if booked_status == 6 {
                     self.getRiderReview(customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))" , booked_order_id: self.bookeOrderID)

                } else {
                    
                    self.vRating.isHidden = true
                    self.btnRateRestaurant.isHidden = true
                    self.vRiderRating.isHidden = true
                    self.btnRateRider.isHidden = true
                    self.hideLoading()
                }
              }
              else
              {
                  let error = json[self.constant.key_code].string
                  self.handleError(error)
                    
              }

            
            return KotlinUnit()
      }, failure: {
        self.hideLoading()
        self.handleError($0?.message)
        return KotlinUnit()
      })
    }
    
    
    private func getRiderReview(
           customer_id: String,
           booked_order_id: String
           ) {
           showLoading()
           apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCheckRiderReview,
             params: "customer_id:\(customer_id) || booked_order_id:\(booked_order_id)",
             success: { data in
                 let jsonData = data.data(using: String.Encoding.utf8)!
                 let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
                 let json: JSON = JSON(jsonDic)
                 
                 let status = json[self.constant.key_status].string
                 
               if status == self.constant.status_success {
                   self.setData(jsonObj: json)
                   self.isRiderReviewed = true
                 }
                 else
                 {
                   let error = json[self.constant.key_code].string
                       self.handleError(error)
                   self.isRiderReviewed = false
                 }
               
               if self.isRiderReviewed {
                   self.btnRateRider.isHidden = true
                   self.vRiderRating.isHidden = false
                
                 //Rider Review
                let data = json[self.constant.key_data].self
                let riderReview = data[self.constant.key_rating].string
                self.vRiderRating.rating = (riderReview! as NSString).doubleValue
                 
               }
               else
               {
                   self.btnRateRider.isHidden = false
                   self.vRiderRating.isHidden = true
               }
               self.hideLoading()
               self.getMerchantReview(customer_id: "\(self.defaults.integer(forKey: self.constant.key_user_id))" , booked_order_id: self.bookeOrderID)
               
               return KotlinUnit()
         }, failure: {
           self.hideLoading()
           self.handleError($0?.message)
           return KotlinUnit()
         })
       }
    
   
    private func getMerchantReview(
        customer_id: String,
        booked_order_id: String
        ) {
        showLoading()
        apiInterface.parseAPI(access_code: self.defaults.string(forKey: self.constant.key_access_code)!, url: constant.urlCheckMerchantReview,
          params: "customer_id:\(customer_id)||booked_order_id:\(booked_order_id)",
          success: { data in
              let jsonData = data.data(using: String.Encoding.utf8)!
              let jsonDic = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
              let json: JSON = JSON(jsonDic)
              
              let status = json[self.constant.key_status].string
              if status == self.constant.status_success {
                  self.setData(jsonObj: json)
                  self.isMerchantReviewed = true
                }
                else
                {
                  let error = json[self.constant.key_code].string
                      self.handleError(error)
                  self.isMerchantReviewed = false
                }
              
              if self.isMerchantReviewed {
                  self.btnRateRestaurant.isHidden = true
                  self.vRating.isHidden = false
                
                //Rider Review
                let data = json[self.constant.key_data].self
                let merchantReview = data[self.constant.key_rating].string
                self.vRating.rating = (merchantReview! as NSString).doubleValue
              }
              else
              {
                  self.btnRateRestaurant.isHidden = false
                  self.vRating.isHidden = true
              }
              self.hideLoading()
            return KotlinUnit()
      }, failure: {
        self.hideLoading()
        self.handleError($0?.message)
        return KotlinUnit()
      })
    }
    
}
