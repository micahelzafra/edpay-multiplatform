package com.ed2e.ed2eapp.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.core.app.NotificationCompat;

import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.model.NotificationData;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.view.LoginOrRegAcivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import timber.log.Timber;

import static com.ed2e.ed2eapp.util.ConstantKt.key_booked_id;
import static com.ed2e.ed2eapp.util.ConstantKt.pref_jinxnotif_bookedid;

public class MyFirebaseMessagingService2 extends FirebaseMessagingService {

    AppPreference preference;

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        Timber.d("onNewToken: %s",s);
    }

    @Override
    public void onMessageSent(@NotNull String msgId) {
        Timber.d("onMessageSent: %s",msgId);
    }

    @Override
    public void onSendError(@NotNull String msgId, @NotNull Exception e) {
        Timber.e("onSendError: %s",msgId);
        Timber.e(e, "Exception");
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Timber.w("From: %s",remoteMessage.getFrom());
        preference = AppPreference.getInstance(this);

        JsonParser parser = new JsonParser();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            remoteMessage.getData();
            Timber.w("Message data payload: %s",remoteMessage.getData());

            if (remoteMessage.getData().get(key_booked_id) != null) {
                if (!preference.getString(pref_jinxnotif_bookedid, "").isEmpty()) {
                    JsonArray jsonArray = parser.parse(preference.getString(pref_jinxnotif_bookedid)).getAsJsonArray();
                    for (int j = 0; j < jsonArray.size(); j++) {
                        JsonObject jsonObject = jsonArray.get(j).getAsJsonObject();
                        String booked_id = jsonObject.get(key_booked_id).getAsString();
                        if (booked_id.equals(remoteMessage.getData().get(key_booked_id))) {
                            getImage(remoteMessage);
                        }
                    }
                }
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Timber.w("Message Notification Body: %s",remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(Bitmap bitmap)
    {

        NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
        style.bigPicture(bitmap);

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(getApplicationContext(), LoginOrRegAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,0);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            //Configure Notification Channel
            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(NotificationData.title)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentText(NotificationData.content)
                .setContentIntent(pendingIntent)
                .setStyle(style)
                .setLargeIcon(bitmap)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);


        if (notificationManager != null) {
            notificationManager.notify(1, notificationBuilder.build());
        }
    }

    private void getImage(final RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        NotificationData.title = data.get("title");
        NotificationData.content = data.get("body");
        NotificationData.booked_id = data.get("booked_id");
        //Create thread to fetch image from notification
        remoteMessage.getData();

        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(() -> {
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_ed2e_logo);
            sendNotification(icon);
//                    Glide.with(getApplicationContext()).load(NotificationData.imageUrl).into(target);
        }) ;
    }

}
