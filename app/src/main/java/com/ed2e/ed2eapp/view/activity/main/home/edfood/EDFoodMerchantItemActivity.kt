package com.ed2e.ed2eapp.view.activity.main.home.edfood

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ed2e.ed2eapp.ApiInterface
import com.ed2e.ed2eapp.R
import com.ed2e.ed2eapp.adapter.BaseRecyclerViewAdapter
import com.ed2e.ed2eapp.adapter.ViewHolderFactory
import com.ed2e.ed2eapp.base.BaseActivity
import com.ed2e.ed2eapp.util.*
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edfood_merchant_items.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import therajanmaurya.rxbus.kotlin.RxBus
import therajanmaurya.rxbus.kotlin.RxEvent
import timber.log.Timber
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class EDFoodMerchantItemActivity : BaseActivity(), CoroutineScope {

    private lateinit var job: Job
    private lateinit var disposable: Disposable
    private lateinit var apiInterface: ApiInterface
    private lateinit var preference: AppPreference
    lateinit var adapter: BaseRecyclerViewAdapter<Any>
    private var arraylist = ArrayList<Any>()
    private var categories = ArrayList<Any>()
    private var sortedArraylist = ArrayList<Any>()

    private var bookedId = ""
    private var restaurant_data = ""
    private var country_name :String? = ""
    private var booked_location :String? = ""
    private var restaurant_id = ""

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edfood_merchant_items)

        job = Job()

        apiInterface = ApiInterface()

        preference = AppPreference.getInstance(this)

        initToolBar()
        initGUI()
        initListener()
    }

    @SuppressLint("SetTextI18n")
    private fun initGUI() {
        val bundle: Bundle? = intent.extras
        val data = bundle!!.getString(key_data) // 1

        restaurant_data = data.toString()
        Log.d("HELP", "Data: " + data.toString())

        booked_location = bundle.getString(key_booked_location)
        val parser = JsonParser()
        val jsonObj: JsonObject = parser.parse(data).asJsonObject
        Timber.d("jsonObj: %s", jsonObj)
        Timber.d("booked_location: %s", booked_location)
        country_name = bundle.getString(key_country) // 1
        val restaurantPhoto = jsonObj[key_restaurant_photo].asString
        Glide.with(this@EDFoodMerchantItemActivity).load(restaurantPhoto).into(edfood_merchant_item_imageview_display)

        adapter = object : BaseRecyclerViewAdapter<Any>(sortedArraylist, object :
            ItemClickListener<Any> {
            override fun onItemClick(v: View, item: Any, position: Int) {
            }
        }) {
            override fun getLayoutId(position: Int, obj: Any): Int {
//                return when(obj){
//                    is String->R.layout.child_edfood_menu_category
//                    else->R.layout.child_edfood_menu_item
//                }
                return R.layout.child_edfood_menu_category
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view,viewType)
            }
        }
        edfood_merchant_item_recyclerview.layoutManager= LinearLayoutManager(context)
        edfood_merchant_item_recyclerview.setHasFixedSize(true)
        edfood_merchant_item_recyclerview.adapter=adapter
        edfood_merchant_item_recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItemCount: Int = (edfood_merchant_item_recyclerview.layoutManager as LinearLayoutManager).childCount
                val totalItemCount: Int = (edfood_merchant_item_recyclerview.layoutManager as LinearLayoutManager).itemCount
                Timber.d("totalItemCount $totalItemCount")
                val firstVisibleItemPosition: Int =  (edfood_merchant_item_recyclerview.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                val lastItem = firstVisibleItemPosition + visibleItemCount
                Timber.d("firstVisibleItemPosition $firstVisibleItemPosition")
                Timber.d("lastItem $lastItem")
                edfood_merchant_item_tablayout_view.setScrollPosition(firstVisibleItemPosition,0f,true)
            }
        })

        val name = jsonObj[key_restaurant_name].asString
        val distanceFromCustomer = jsonObj[key_distance_from_customer].asString
        val shortDescription = jsonObj[key_short_description].asString

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.HALF_UP

        val distanceFromCustomer_toDouble = df.format(distanceFromCustomer.toDouble()).toDouble()

        edfood_merchant_item_textview_name.text = name
        edfood_merchant_item_textview_distance.text = "$distanceFromCustomer_toDouble mi"
        edfood_merchant_item_textview_description.text = shortDescription

        val operatingInfo = jsonObj[key_operating_info].asJsonArray
        var closing = ""
        for (i in 0 until operatingInfo.size()) {
            if (operatingInfo[i].asJsonObject.toString().contains(getCurrentDay().asString()))
            {
                val operatingInfoObj = operatingInfo[i].asJsonObject
                closing = operatingInfoObj["closing"].asString
            }
        }

        // Get date from string
        val dateFormatter = SimpleDateFormat("HH:mm:ss")
        var dateClosing: Date? = null

        var finalClosing = ""

        try {
            dateClosing = dateFormatter.parse(closing)
            // Get time from date
            val timeFormatter = SimpleDateFormat("h:mm a")
            finalClosing = timeFormatter.format(dateClosing).toUpperCase()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        edfood_merchant_item_textview_closing_time.text = "Closing at $finalClosing"

        restaurant_id = jsonObj[key_restaurant_id].asString
        initGetOrderID(preference.getString(key_user_id),restaurant_id)
        initAPIGetRatingReviews(restaurant_id)

        edfood_merchant_item_layout_cart_items.setOnClickListener {
            val intent : Intent
            intent = Intent(context, EDFoodCartActivity::class.java)
            intent.putExtra(key_booked_location, booked_location)
//            intent.putExtra(key_data, value.toString())
            intent.putExtra(key_country, country_name)
            intent.putExtra(key_booked_id, bookedId)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        edfood_merchant_item_textview_more_info.setOnClickListener {
            val intent : Intent
            intent = Intent(context, EDFoodMoreInfoActivity::class.java)
            intent.putExtra(key_data, restaurant_data)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    private fun initListener() {
        disposable = RxBus.listen(RxEvent.EventMainActivity::class.java).subscribe {
            Timber.d("RxBus Listener %s", it)
            intentActivity(it.tag,it.value)
        }
    }

    private fun intentActivity(tag: Int, value: Any?) {
        when (tag) {
            tag_intent_edfood_menu_details_activity -> {
                val intent : Intent
                intent = Intent(context, EDFoodItemDetailsActivity::class.java)
                intent.putExtra(key_booked_location, booked_location)
                intent.putExtra(key_restaurant_id, restaurant_id)
                intent.putExtra(key_data, value.toString())
                intent.putExtra(key_booked_id, bookedId)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
            tag_intent_edfood_close_prev_act -> {
                finish()
            }
        }
    }


    private fun initTabs(data: ArrayList<Any>)
    {
        Timber.d("initTabs %s",data.size)
        this@EDFoodMerchantItemActivity.runOnUiThread {

            edfood_merchant_item_tablayout_view.removeAllTabs()
            Timber.d("initTabs() triggered" + data.size)
            for (title in data)
            {
                Timber.d("title: " + title)
                edfood_merchant_item_tablayout_view.addTab(edfood_merchant_item_tablayout_view.newTab().setText(title.toString()))
                //edfood_merchant_item_tablayout_view.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_tab_selector));

//                edfood_merchant_item_tablayout_view.addOnTabSelectedListener(new OnTabSelectedListener() {
//                        @Override
//                        public void onTabSelected(Tab tab) {
//                            switch(tab.getPosition()) {
//                                case 0:
//                                ....
//                            }
//                        }
            }

            edfood_merchant_item_tablayout_view.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    edfood_merchant_item_recyclerview.layoutManager?.scrollToPosition(tab.position)
                }
                override fun onTabUnselected(tab: TabLayout.Tab) {

                }
                override fun onTabReselected(tab: TabLayout.Tab) {

                }
            })

            val test: ViewGroup =
                edfood_merchant_item_tablayout_view.getChildAt(0) as ViewGroup //tabs is your Tablayout

            val tabLen: Int = test.getChildCount()

            for (i in 0 until tabLen) {
                val v: View = test.getChildAt(i)
                v.setPadding(1, 0, 1, 0)
            }
        }
    }

    private fun initAPIGetRatingReviews(
        restaurant_id: String
    ) {
        Timber.d("initAPIGetRatingReviews() triggered")
        val params = "review_type:" + "RESTAURANT" + "||" +
                "reviewee_id:" + restaurant_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlReviewListing,
            params,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        val dataObj =  jsonObj["data"].asJsonObject

                        var rating_average = "";
                        try {
                            rating_average = dataObj["average_rating"].asString
                        } catch (e: java.lang.Exception) {
                            rating_average = "" + dataObj["average_rating"].asDouble
                        }

                        var review_count = "";
                        try {
                            review_count = dataObj["review_count"].asString
                        } catch (e: java.lang.Exception) {
                            review_count = "" + dataObj["review_count"].asDouble
                        }

                        edfood_merchant_item_textview_rating.text = "$rating_average rating"
                        edfood_merchant_item_textview_review.text = "$review_count review(s)"

                    } else {
                        val errorCode = jsonObj[key_code].asString
                        showDialogError(request_error_dialog, errorCode)
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initMerchantMenu(
        restaurant_id: String
    ) {
        Timber.d("initMerchantMenu() triggered")
        val params = "restaurant_id:$restaurant_id"
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlRestaurantMenu,
            params,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    if (status == status_success) {
                        categories.clear()
                        arraylist.clear()
                        sortedArraylist.clear()
                        val jsonArray = jsonObj[key_data].asJsonArray//
                        val categoryList = ArrayList<Any>()
                        categoryList.clear()
                        for (i in 0 until jsonArray.size()) {
                            try {
                                val jsonItem = jsonArray[i].asJsonObject
                                val category = jsonItem[key_category].asString
                                categoryList.add(category)
                                arraylist.add(jsonItem)
                            }
                            catch (e : Exception)
                            {
                                Timber.e("cashback Exception $e ")
                            }
                        }

                        categories.addAll(categoryList.distinct())
                        initTabs(categories)
                        for (cat in categories)
                        {
//                        sortedArraylist.add(cat)
                            val rootObject= JSONObject()
                            val sortedjsonArray = JSONArray()
                            rootObject.put("category",cat)
                            for (menu in arraylist)
                            {
                                val menujsonObj = parser.parse(menu.toString()).asJsonObject
                                val category = menujsonObj[key_category].asString
                                if (category == cat)
                                {
                                    sortedjsonArray.put(menu.toString())
                                }

                            }
                            rootObject.put("data",sortedjsonArray)
                            sortedArraylist.add(rootObject)
                        }
                        updateList()

                        if(bookedId != "")
                        {
                            country_name?.let {
                                initGetCartOrderList(preference.getString(key_user_id),bookedId,
                                    it
                                )
                            }
                        }
                    } else {
                        val errorCode = jsonObj[key_code].asString
                        showDialogError(request_error_dialog, errorCode)
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initGetOrderID(
        customer_id: String,
        restaurant_id: String
    ) {

        Timber.d("initGetOrderID() triggered")
        val params =
            "customer_id:" + customer_id + "||" +
            "restaurant_id:" + restaurant_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCheckOrderID,
            params,
            { response: String? ->
                launch(Dispatchers.Main) {
                    val parser = JsonParser()
                    val jsonObj = parser.parse(response).asJsonObject
                    val status = jsonObj[key_status].asString
                    initMerchantMenu(restaurant_id)
                    if (status == status_success) {
                        edfood_merchant_item_layout_cart_items.visibility = VISIBLE
                        val data = jsonObj[key_data].asJsonObject
                        bookedId = data[key_booked_order_id].asString
                    }
                    else
                    {
                        edfood_merchant_item_layout_cart_items.visibility = GONE
                    }
                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun initGetCartOrderList(
        customer_id: String,
        booked_id: String,
        country_name: String
    ) {
        val params =
            "customer_id:" + customer_id + "||" +
                    "country_name:" + country_name + "||" +
                    "distance_km:1" + "||" +
                    "booked_id:" + booked_id
        Timber.d("params: %s", params)
        apiInterface.parseAPI(
            preference.getString(key_access_code),
            urlCartOrderList,
            params,
            { response: String? ->
                val parser = JsonParser()
                val jsonObj = parser.parse(response).asJsonObject


                var status = "";
                if(jsonObj[key_status] == null) {
                    status = ""
                } else {
                    status = jsonObj[key_status].asString
                }

                launch(Dispatchers.Main) {

                    if(status == status_fail) {

                    } else {
                        var original_sub_total = ""
                        if (jsonObj[key_original_sub_total].asString == null) {
                            original_sub_total = ""
                        } else {
                            original_sub_total = jsonObj[key_original_sub_total].asString
                        }

                        edfood_merchant_item_textview_cart_amount.text = original_sub_total

                        val jsonArray = jsonObj[key_order].asJsonArray
                        var quantities = 0
                        for (json in jsonArray)
                        {
                            val jsonItem = json.asJsonObject
                            val foodQuantity = jsonItem[key_food_quantity].asInt
                            quantities += foodQuantity
                        }
                        edfood_merchant_item_textview_cart_items.text = quantities.toString()
                    }

                }
            }
        ) { failure: Throwable? ->
            println("Throwable:$failure")
            showDialogError(request_error_dialog, failure!!.localizedMessage)
        }
    }

    private fun updateList()
    {
        this@EDFoodMerchantItemActivity.runOnUiThread {
//            val parser = JsonParser()
//            sortedArraylist.sortWith(Comparator { jinks, jinx ->
//                parser.parse(jinx.toString()).asJsonObject[key_created_date].asString.compareTo(parser.parse(jinks.toString()).asJsonObject[key_created_date].asString)
//            })
            adapter.notifyDataSetChanged()
        }
    }


    private fun initToolBar() {
        edfood_merchants_items_toolbar_LinearLayout_left.setOnTouchListener { v: View?, event: MotionEvent ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // PRESSED
                    edfood_merchants_items_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed)
                    return@setOnTouchListener true // if you want to handle the touch event
                }
                MotionEvent.ACTION_UP -> {
                    // RELEASED
                    edfood_merchants_items_toolbar_LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape)
                    initPreviousActivity()
                    return@setOnTouchListener true // if you want to handle the touch event
                }
            }
            false
        }
    }

    private fun initPreviousActivity() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onResume() {
        super.onResume()
        //initGUI()
        //initListener()
        initGetOrderID(preference.getString(key_user_id),restaurant_id)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        if (!disposable.isDisposed) disposable.dispose()
    }

    override fun onDialogDismiss(requestTag: String?, input_data: String?) {
        Timber.d("scanQR success %s", input_data)
    }

    override fun onUpdateLocation(latitude: String?, longitude: String?) {
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                initPreviousActivity()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}
