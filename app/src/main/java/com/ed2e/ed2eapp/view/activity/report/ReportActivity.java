package com.ed2e.ed2eapp.view.activity.report;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_dispute_type;
import static com.ed2e.ed2eapp.util.ConstantKt.key_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_mobile_number;
import static com.ed2e.ed2eapp.util.ConstantKt.key_order_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_reported_desc;
import static com.ed2e.ed2eapp.util.ConstantKt.key_reported_name;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlAnnouncementList;
import static com.ed2e.ed2eapp.util.ConstantKt.urlDispute;

public class ReportActivity extends BaseActivity {

    public static final String TAG = ReportActivity.class.getSimpleName();

    @BindView(R.id.edfood_report_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edfood_report_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.edfood_report_imageview_icon)
    ImageView imageview_icon;
    @BindView(R.id.edfood_report_textview_name)
    TextView textview_name;
    @BindView(R.id.edfood_report_textview_desc)
    TextView textview_desc;
    @BindView(R.id.edfood_report_editText_report)
    TextView editText_report;

    @BindView(R.id.edfood_report_button_report_issue)
    TextView button_report_issue;


    private String orderID = "";
    private String disputeType = "";
    private String reportedID = "";
    private String reportedName = "";
    private String reportedDesc = "";

    ApiInterface apiInterface;
    AppPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edfood_report);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initData();
        initToolBar();
        initGUI();
    }

    private void initData() {
        Log.d(TAG, "initData() triggered");

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            orderID = "";
            disputeType = "";
            reportedID = "";
            reportedName = "";
            reportedDesc = "";
        } else {
            orderID = extras.getString(key_order_id);
            disputeType = extras.getString(key_dispute_type);
            reportedID = extras.getString(key_id);
            reportedName = extras.getString(key_reported_name);
            reportedDesc = extras.getString(key_reported_desc);
        }
//
//        Log.d(TAG, "orderData: " + orderData);
//        Log.d(TAG, "riderData: " + riderData);
//        Log.d(TAG, "merchantData: " + merchantData);

//        showToast_long(
//              "initData:\n" +
//                      "(" + mobileNumber + ")"
//        );
    }

    private void initGUI() {

        if(disputeType.equalsIgnoreCase("RIDER")) {
            Glide.with(getContext())
                    .load(R.drawable.ic_profile_rider)
                    .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_user_default))
                    .into(imageview_icon);
        } else {
            Glide.with(getContext())
                    .load(R.drawable.ic_profile_merchant)
                    .apply(new RequestOptions().override(720, 720).centerCrop().placeholder(R.drawable.ic_user_default))
                    .into(imageview_icon);
        }

        textview_name.setText(reportedName);
        textview_desc.setText(reportedDesc);


        button_report_issue.setOnClickListener(v -> {
            //showDialog_main("", "tag_button_submit_report_back", false, getResources().getString(R.string.dialog_report_submitted_title), getResources().getString(R.string.dialog_report_submitted_message), "", "Back");


            if(disputeType.equalsIgnoreCase("RIDER")) {
                initAPISubmitReport(preference.getString(key_user_id), orderID, disputeType, reportedID, "Reporting Rider", editText_report.getText().toString().trim());
            } else {
                initAPISubmitReport(preference.getString(key_user_id), orderID, disputeType, reportedID, "Reporting Merchant", editText_report.getText().toString().trim());
            }
        });


//        try {
//            JSONObject obj = new JSONObject(response);
//            JSONArray announcement_data = obj.getJSONArray("announcement_records");
//
//            //showToast_short("SUCCESS pO: " + announcement_data.length());
//
//
//            for(int i = 0; i < announcement_data.length(); i++)
//            {
//
//                JSONObject object = announcement_data.getJSONObject(i);
//
//                String announcement_id;
//                try {
//                    announcement_id = "" + (String) object.get("id");
//                } catch (Exception e) {
//                    announcement_id = "" + object.getInt("id");
//                }
//
//                String title = (String) object.get("title");
//                String content = (String) object.get("content");
//                String event_date = (String) object.get("event_date");
//                String expiry_date = (String) object.get("expiry_date");
//                String image = (String) object.get("image");
//                String countries = "\"" + object.get("countries") + "\"";
//
//                String annc_status;
//                try {
//                    annc_status = "" + (String) object.get("annc_status");
//                } catch (Exception e) {
//                    annc_status = "" + object.getInt("annc_status");
//                }
//
//                Announcement list = new Announcement(
//                        announcement_id,
//                        title,
//                        content,
//                        event_date,
//                        expiry_date,
//                        image,
//                        countries,
//                        annc_status
//                );
//                modelList.add(list);
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        //initAPIGetAnnouncementsList();

//
//        button_submit.setOnClickListener(v -> {
//        });

    }



    private void initAPISubmitReport(String custID, String bookedID, String disputeType, String reportedID, String reportTitle,  String reportDesc) {
        showProgress();

        String params = "";

        if(disputeType.equalsIgnoreCase("RIDER")) {
            params = "customer_id:" + custID + "||" +
                    "booked_order_id:" + bookedID + "||" +
                    "dispute_type:"  + disputeType + "||" +
                    "rider_id:"  + reportedID + "||" +
                    "restaurant_id:"  + "0" + "||" +
                    "dispute_title:"  + reportTitle + "||" +
                    "dispute_description:" + reportDesc;
        } else {
            params = "customer_id:" + custID + "||" +
                    "booked_order_id:" + bookedID + "||" +
                    "dispute_type:"  + disputeType + "||" +
                    "rider_id:"  + "0" + "||" +
                    "restaurant_id:"  + reportedID + "||" +
                    "dispute_title:"  + reportTitle + "||" +
                    "dispute_description:" + reportDesc;
        }


        String finalParams = params;
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlDispute, finalParams,
                response -> {
                    hideProgress();
                    Log.d(TAG, "DATA: | " + finalParams);
                    Log.d(TAG, "RESPONSE: " + response);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();

                    if (status.equals(status_success))
                    {
                        showDialog_main("", "tag_button_submit_report_back", false, getResources().getString(R.string.dialog_report_submitted_title), getResources().getString(R.string.dialog_report_submitted_message), "", "Back");


//                        try {
//                            JSONObject obj = new JSONObject(response);
//                            JSONArray announcement_data = obj.getJSONArray("announcement_records");
//
//                            //showToast_short("SUCCESS pO: " + announcement_data.length());
//
//                            runOnUiThread(() -> {
//                                if (announcement_data.length() == 0) {
//                                    textview_message.setVisibility(View.VISIBLE);
//                                } else {
//                                    textview_message.setVisibility(View.GONE);
//                                }
//                            });
//
//                            for(int i = 0; i < announcement_data.length(); i++)
//                            {
//
//                                JSONObject object = announcement_data.getJSONObject(i);
//
//                                String announcement_id;
//                                try {
//                                    announcement_id = "" + (String) object.get("id");
//                                } catch (Exception e) {
//                                    announcement_id = "" + object.getInt("id");
//                                }
//
//                                String title = (String) object.get("title");
//                                String content = (String) object.get("content");
//                                String event_date = (String) object.get("event_date");
//                                String expiry_date = (String) object.get("expiry_date");
//                                String image = (String) object.get("image");
//                                String countries = "\"" + object.get("countries") + "\"";
//
//                                String annc_status;
//                                try {
//                                    annc_status = "" + (String) object.get("annc_status");
//                                } catch (Exception e) {
//                                    annc_status = "" + object.getInt("annc_status");
//                                }
//
//                                Announcement list = new Announcement(
//                                        announcement_id,
//                                        title,
//                                        content,
//                                        event_date,
//                                        expiry_date,
//                                        image,
//                                        countries,
//                                        annc_status
//                                );
//                                modelList.add(list);
//                            }
//
//                            runOnUiThread(() -> {
//                                recyclerAdapter.setModelList(modelList);
//                            });
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                    else
                    {

                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialog_APIError("", "", true, error_code, "", "OK");
                    }

                    return null;
                }
                , failure -> {
                    hideProgress();
                    Log.d(TAG, "RESPONSE (ERROR): " + failure);
                    showDialog_main("", "", true, getResources().getString(R.string.error_something_went_wrong), failure.getMessage(), "","OK");
                    return null;
                });
    }

    private void initPreviousActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
        switch (requestTag) {
            case "tag_button_submit_report_back":
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;


            default:
                break;
        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
