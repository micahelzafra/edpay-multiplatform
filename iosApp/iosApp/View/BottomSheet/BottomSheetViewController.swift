//
//  BottomSheetViewController.swift
//  BottomSheet
//
//  Created by Ahmed Elassuty on 7/1/16.
//  Copyright © 2016 Ahmed Elassuty. All rights reserved.
//

import UIKit
import SwiftyJSON
import app
import AlamofireImage

class BottomSheetViewController: BaseViewController, EventsDelegate {
    
    // holdView can be UIImageView instead
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var view2: UIView!
    //    @IBOutlet weak var holdView: UIView!
//    @IBOutlet weak var left: UIButton!
//    @IBOutlet weak var right: UIButton!
    @IBOutlet weak var heightViewDistance: NSLayoutConstraint!
    
    private var bookedId = ""
    private var restaurantDetails = JSON()
    private var riderDispatchDetails = JSON()
    var merchantData = JSON()
    
    let downloader = ImageDownloader()
    
    var fullView: CGFloat {
        if svRider.isHidden == true {
            return UIScreen.main.bounds.minY + (self.view2.frame.height - 315)
        } else {
            return UIScreen.main.bounds.minY + (self.view2.frame.height - 355)
        }
        
        
    }
    var partialView: CGFloat {
        return UIScreen.main.bounds.minY + ( self.view.frame.height - 50)
    }
    
    var screenHeight = UIScreen.main.bounds.height
    //Order Details
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblMerchantName: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblViewOrderDetails: UILabel!
    
    @IBOutlet weak var svRider: UIStackView!
    @IBOutlet weak var svNoInformation: UIStackView!
    @IBOutlet weak var heightConstraintsNoInformation: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintsRider: UIStackView!
    
    //Rider Details
    @IBOutlet weak var lblRiderName: UILabel!
    @IBOutlet weak var lblRiderPlateNo: UILabel!
    @IBOutlet weak var imageVIewRider: RoundableImageView!
    @IBOutlet weak var viewRider: UIView!
    @IBOutlet weak var lblNoRider: UILabel!
    @IBOutlet weak var ivStatus1: UIImageView!
    @IBOutlet weak var lblStatus1: UILabel!
    @IBOutlet weak var ivStatus2: UIImageView!
    @IBOutlet weak var lblStatus2: UILabel!
    @IBOutlet weak var ivStatus3: UIImageView!
    @IBOutlet weak var lblStatus3: UILabel!
    @IBOutlet weak var ivStatus4: UIImageView!
    @IBOutlet weak var lblStatus4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventDelegate = self
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        view.addGestureRecognizer(gesture)
        
//       //label actions
//        let tapViewOrderDetails = UITapGestureRecognizer(target: self, action: #selector(viewOrderDetails))
//        lblViewOrderDetails.isUserInteractionEnabled = true
//        lblViewOrderDetails.addGestureRecognizer(tapViewOrderDetails)
        //roundViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareBackgroundView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            
            self?.view.frame =  CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
        }) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @objc func viewOrderDetails(sender:UITapGestureRecognizer) {
//         let newStoryBoard : UIStoryboard = UIStoryboard(name: "Activity", bundle:nil)
//               let VC = newStoryBoard.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
//                   VC.modalPresentationStyle = .fullScreen
//                   self.present(VC, animated: true, completion: nil)
//
//    }
//
    func eventData(event: Int, data: AnyObject) {
        self.log("eventData \(event), data \(data)")
        let jsonObj = data as! JSON
        let jsonArray = jsonObj[self.constant.key_booked].array
        var data = JSON()
        for json in jsonArray!
        {
            data = json
        }
        let foodTotal = data[self.constant.key_food_total].self
        restaurantDetails = data[self.constant.key_restaurant_details].self
        self.log("foodTotal \(foodTotal)")

        let orderStatus = data[self.constant.key_booked_status].int
        let date = data[self.constant.key_booked_date].string
        
        let orderNo = "\(self.constant.label_logs_order_no) \(data[self.constant.key_booked_id].int ?? -1)"
        let total = commaSeparated(value: (foodTotal[self.constant.key_total].string?.replacingOccurrences(of: ",", with: ""))!).asString()
//        let statusDate = "\(getOrderStatus(status: "\(orderStatus)").asString()) \(getLocalTimeStamp(date: date!).asString())"
        
//        var riderNote = ""
//        if let note = data[self.constant.key_special_notes].object as? String {
//            self.log("json \(note)")
//            riderNote = note
//        }
        
        self.lblAmount.text = total
        self.lblMerchantName.text = restaurantDetails[self.constant.key_restaurant_name].string
        
        if let json = data[self.constant.key_rider_dispatch_details].object as? Array<Any> {
            self.log("json \(json)")
            riderDispatchDetails = JSON()
        }
        else {
            riderDispatchDetails = data[self.constant.key_rider_dispatch_details].self
        }
        print("riderDispatchDetails \(riderDispatchDetails)")
        if !riderDispatchDetails.isEmpty
        {
            self.svRider.isHidden = false
            self.svNoInformation.isHidden = true
            self.svNoInformation.heightAnchor.constraint(equalToConstant: 0).isActive = true
            
            let riderInfo = riderDispatchDetails[self.constant.key_rider_info].self
            let name = "\(riderInfo[self.constant.key_first_name].string ?? "") \(riderInfo[self.constant.key_last_name].string ?? "")"
            self.lblRiderPlateNo.text = riderInfo[self.constant.key_plate_no].string
            self.lblRiderName.text = name
            self.lblOrderID.text = orderNo
            
            let distanceTime = riderInfo["distance_time"].self
            let time = distanceTime["time"].string
            
            var hours = 0
            var minutes = 0
            
            let delimiter = ":"
            let timeSplitted = time!.components(separatedBy: delimiter)
            hours = Int(timeSplitted[0])!
            minutes = Int(timeSplitted[1])!

            if hours == 0 
            {
                self.lblDistance.text =
                    "Your order will arrive in \(minutes) minute(s)"
            } else {
                self.lblDistance.text =
                    "Your order will arrive in \(hours) hour(s) and \(minutes) minute(s)"
            }
        }
        else
        {
            self.svNoInformation.isHidden = false
            self.svRider.isHidden = true
            self.heightConstraintsRider.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if orderStatus == 3
        {
            self.ivStatus1.image = UIImage(named: "ordered_address")
            self.lblStatus1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.ivStatus2.image = UIImage(named: "ic_circle_grey")
            self.lblStatus2.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus3.image = UIImage(named: "ic_circle_grey")
            self.lblStatus3.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus4.image = UIImage(named: "ic_circle_grey")
            self.lblStatus4.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        else if orderStatus == 4
        {
            self.ivStatus1.image = UIImage(named: "ic_circle_grey")
            self.lblStatus1.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus2.image = UIImage(named: "ordered_address")
            self.lblStatus2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.ivStatus3.image = UIImage(named: "ic_circle_grey")
            self.lblStatus3.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus4.image = UIImage(named: "ic_circle_grey")
            self.lblStatus4.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        else if orderStatus == 5
        {
            self.ivStatus1.image = UIImage(named: "ic_circle_grey")
            self.lblStatus1.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus2.image = UIImage(named: "ic_circle_grey")
            self.lblStatus2.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus3.image = UIImage(named: "ordered_address")
            self.lblStatus3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.ivStatus4.image = UIImage(named: "ic_circle_grey")
            self.lblStatus4.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        else if orderStatus == 17
        {
            self.ivStatus1.image = UIImage(named: "ic_circle_grey")
            self.lblStatus1.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus2.image = UIImage(named: "ic_circle_grey")
            self.lblStatus2.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus3.image = UIImage(named: "ic_circle_grey")
            self.lblStatus3.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus4.image = UIImage(named: "ordered_address")
            self.lblStatus4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        else
        {
            self.ivStatus1.image = UIImage(named: "ic_circle_grey")
            self.lblStatus1.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus2.image = UIImage(named: "ic_circle_grey")
            self.lblStatus2.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus3.image = UIImage(named: "ic_circle_grey")
            self.lblStatus3.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.ivStatus4.image = UIImage(named: "ic_circle_grey")
            self.lblStatus4.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            if orderStatus == 6
            {
                
            }
        }
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {

        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        let y = self.view.frame.minY
        if ( y + translation.y >= fullView) && (y + translation.y <= partialView ) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }

        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )

            duration = duration > 1.3 ? 1 : duration

            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                }

                }, completion: nil)
        }
    }
    
//    func roundViews() {
//        view.layer.cornerRadius = 5
//        holdView.layer.cornerRadius = 3
//        left.layer.cornerRadius = 10
//        right.layer.cornerRadius = 10
//        left.layer.borderColor = #colorLiteral(red: 0.9621763825, green: 0.01609125175, blue: 0, alpha: 1)
//        left.layer.borderWidth = 1
//        view.clipsToBounds = true
//    }
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .dark)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        
        view.insertSubview(bluredView, at: 0)
    }

}
