//
//  EventsDelegate.swift
//  iosApp
//
//  Created by ED2E Technology iOS on 1/26/20.
//

import Foundation

protocol EventsDelegate {
    func eventData(event:Int, data:AnyObject)
}
