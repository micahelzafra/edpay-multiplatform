package com.ed2e.ed2eapp.view.activity.main.home.edfood;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ed2e.ed2eapp.ApiInterface;
import com.ed2e.ed2eapp.MainActivity;
import com.ed2e.ed2eapp.R;
import com.ed2e.ed2eapp.base.BaseActivity;
import com.ed2e.ed2eapp.util.AppPreference;
import com.ed2e.ed2eapp.util.ConstantKt;
import com.ed2e.ed2eapp.view.PinCreateAcivity;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import therajanmaurya.rxbus.kotlin.RxBus;
import therajanmaurya.rxbus.kotlin.RxEvent;
import timber.log.Timber;

import static com.ed2e.ed2eapp.util.AppPreference.getInstance;
import static com.ed2e.ed2eapp.util.CommonConstantKt.tag_intent_edfood_close_prev_act;
import static com.ed2e.ed2eapp.util.ConstantKt.button_no;
import static com.ed2e.ed2eapp.util.ConstantKt.button_yes;
import static com.ed2e.ed2eapp.util.ConstantKt.key_access_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_booked_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_code;
import static com.ed2e.ed2eapp.util.ConstantKt.key_country;
import static com.ed2e.ed2eapp.util.ConstantKt.key_cust_id;
import static com.ed2e.ed2eapp.util.ConstantKt.key_payment_method;
import static com.ed2e.ed2eapp.util.ConstantKt.key_special_notes;
import static com.ed2e.ed2eapp.util.ConstantKt.key_status;
import static com.ed2e.ed2eapp.util.ConstantKt.key_total;
import static com.ed2e.ed2eapp.util.ConstantKt.key_user_id;
import static com.ed2e.ed2eapp.util.ConstantKt.label_cod;
import static com.ed2e.ed2eapp.util.ConstantKt.label_credit_debit;
import static com.ed2e.ed2eapp.util.ConstantKt.label_dialog_msg_cod_confirm;
import static com.ed2e.ed2eapp.util.ConstantKt.label_dialog_msg_edcredit_confirm;
import static com.ed2e.ed2eapp.util.ConstantKt.label_edcredits;
import static com.ed2e.ed2eapp.util.ConstantKt.order_placed;
import static com.ed2e.ed2eapp.util.ConstantKt.pref_jinxnotif_bookedid;
import static com.ed2e.ed2eapp.util.ConstantKt.request_error_dialog;
import static com.ed2e.ed2eapp.util.ConstantKt.result_code_finish_act;
import static com.ed2e.ed2eapp.util.ConstantKt.status_success;
import static com.ed2e.ed2eapp.util.ConstantKt.urlPINCreate;
import static com.ed2e.ed2eapp.util.ConstantKt.urlUpdateOrderStatus;

public class EDFoodPaymentMethodActivity extends BaseActivity {

    public static final String TAG = EDFoodPaymentMethodActivity.class.getSimpleName();

    @BindView(R.id.edfood_payment_method_toolbar_LinearLayout_left)
    LinearLayout LinearLayout_left;
    @BindView(R.id.edfood_payment_method_toolbar_LinearLayout_left_container)
    LinearLayout LinearLayout_left_container;

    @BindView(R.id.edfood_payment_method_radioButton_creditCard)
    RadioButton radioButton_creditCard;
    @BindView(R.id.edfood_payment_method_radioButton_COD)
    RadioButton radioButton_COD;
    @BindView(R.id.edfood_payment_method_radioButton_edcredits)
    RadioButton radioButton_edcredits;

    @BindView(R.id.edfood_payment_method_button_place_order)
    Button button_place_order;

    ApiInterface apiInterface;
    AppPreference preference;

    private String bookedID;
    private String total;
    private String specialNotes;
    private String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edfood_payment_method);
        ButterKnife.bind(this);

        apiInterface = new ApiInterface();
        preference = getInstance(this);

        initToolBar();
        initData();
        initGUI();
    }

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            bookedID = "";
            total = "";
            specialNotes = "";
            country = "";
        } else {
            bookedID = extras.getString(key_booked_id);
            total = extras.getString(key_total);
            specialNotes = extras.getString(key_special_notes);
            country = extras.getString(key_country);
        }
    }

    private void initGUI() {


        button_place_order.setOnClickListener(v -> {

            if (radioButton_COD.isChecked())
            {
                AlertDialog alertDialog = new AlertDialog.Builder(EDFoodPaymentMethodActivity.this).create();
                alertDialog.setTitle(label_cod+" Payment");
                alertDialog.setMessage(label_dialog_msg_cod_confirm.replace("%s",total));
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, button_no,
                        (dialog, which) -> dialog.dismiss());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, button_yes,
                        (dialog, which) ->
                        {
                            dialog.dismiss();
                            this.initUpdateOrderStatus(bookedID,country,preference.getString(key_user_id), "1");
                        });
                alertDialog.show();
            }
            else
            {
                AlertDialog alertDialog = new AlertDialog.Builder(EDFoodPaymentMethodActivity.this).create();
                alertDialog.setTitle(label_edcredits+" Payment");
                alertDialog.setMessage(label_dialog_msg_edcredit_confirm.replace("%s",total));
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, button_no,
                        (dialog, which) -> dialog.dismiss());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, button_yes,
                        (dialog, which) ->
                        {
                            dialog.dismiss();
                            this.initUpdateOrderStatus(bookedID,country,preference.getString(key_user_id), "2");
                        });
                alertDialog.show();
            }
        });


    }

    private void initUpdateOrderStatus(String booked_id, String country_name, String approver_id, String payment_type)
    {
        showProgress();
        apiInterface.parseAPI(
                preference.getString(key_access_code),
                urlUpdateOrderStatus,
                        "booked_id:"+booked_id+"||" +
                                "country_name:"+country_name+"||" +
                                "booked_status:"+ order_placed+"||" +
                                "approver_id:"+approver_id+"||" +
                                "payment_type:"+payment_type,
                response -> {
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(response).getAsJsonObject();
                    String status = jsonObj.get(key_status).getAsString();
                    if (status.equals(status_success))
                    {
                        String payment_method = label_credit_debit;
                        if (payment_type.equals("1"))
                        {
                            payment_method = label_cod;
                        }
                        else if (payment_type.equals("2"))
                        {
                            payment_method = label_edcredits;
                        }

                        if (!preference.getString(pref_jinxnotif_bookedid, "").isEmpty())
                        {
                            JsonArray jsonArray = parser.parse(preference.getString(pref_jinxnotif_bookedid)).getAsJsonArray();
                            ArrayList<JsonObject> arrayList = new ArrayList<>();
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty(key_booked_id, booked_id);
                            arrayList.add(jsonObject);

                            for(int j=0; j < jsonArray.size(); j++){
                                jsonObject = jsonArray.get(j).getAsJsonObject();
                                arrayList.add(jsonObject);
                            }
                            preference.putString(pref_jinxnotif_bookedid,arrayList.toString());
                        }
                        else
                        {
                            ArrayList<JsonObject> arrayList = new ArrayList<>();
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty(key_booked_id, booked_id);
                            arrayList.add(jsonObject);
                            preference.putString(pref_jinxnotif_bookedid,arrayList.toString());
                        }

                        Intent intent=new Intent(EDFoodPaymentMethodActivity.this, EDFoodOrderSuccessActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(key_booked_id,booked_id);
                        intent.putExtra(key_total,total);
                        intent.putExtra(key_payment_method,payment_method);
                        intent.putExtra(key_special_notes,specialNotes);
                        startActivity(intent);
                        finish();
                        overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                    else
                    {
                        String error_code = jsonObj.get(key_code).getAsString();
                        showDialogError(request_error_dialog,error_code);
                    }
                    hideProgress();
                    return null;
                }
                , failure -> {
                    System.out.println("Throwable:"+ failure);
                    hideProgress();
                    showDialogError(request_error_dialog,failure.getLocalizedMessage());
                    return null;
                });
    }

    private void initPreviousActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }



    @Override
    protected void onDialogDismiss(String requestTag, String input_data) {
        Intent intent;
//        switch (requestTag) {
//            case "tag_mpin_success_button_log_in":
//                intent = new Intent(getContext(), Login1Activity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                break;
//            default:
//                break;
//        }
    }

    @Override
    protected void onUpdateLocation(String latitude, String longitude) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.i("onActivityResult %s",resultCode);
        if (resultCode == Activity.RESULT_OK) {
            Timber.i(data.getStringExtra("result"));
            finish();
        }
    }

    private void initToolBar() {
        LinearLayout_left.setOnTouchListener((v, event) -> {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape_pressed);
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    LinearLayout_left_container.setBackgroundResource(R.drawable.rounded_shape);
                    initPreviousActivity();
                    return true; // if you want to handle the touch event
            }
            return false;
        });
    }

//    overridePendingTransition (R.anim.slide_in_right, R.anim.slide_out_left)
//    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                initPreviousActivity();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
