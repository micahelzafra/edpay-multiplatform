//
//  AnnoucementDetailsVC.swift
//  iosApp
//
//  Created by Arnold Fanio on 2/8/20.
//

import UIKit

class AnnoucementDetailsVC: UIViewController {
    @IBOutlet weak var imageViewAnnouncementDetails: UIImageView!
    @IBOutlet weak var lblAnnouncementDetailsTitle: UILabel!
    
    @IBOutlet weak var lblAnnouncementDetailsEventDate: UILabel!
    @IBOutlet weak var lblAnnouncementDetailsContent: UILabel!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
